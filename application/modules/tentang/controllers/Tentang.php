<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Tentang extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->helper('tool');
	}


	public function index()
	{
		$data['listPeraturan'] = $this->listPeraturan();
		$this->template->load('template_admin', 'v_index', $data);
	}


	protected function listPeraturan()
	{
		return $this->db->get('master_peraturan')->result_array();
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */