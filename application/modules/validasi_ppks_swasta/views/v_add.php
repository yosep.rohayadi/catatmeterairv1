<div class="container-fluid">
    <br>
    <div class="breadcrumbs">
        <ul>
            <?php foreach ($breadcrumbs as $key => $value) { ?>
                <li>
                    <a href="<?php echo site_url($value['link']) ?>">
                        <?php echo $value['name']; ?></a>
                    <?php echo (count($breadcrumbs) - 1) == $key ? "" : "<i class='icon-angle-right'></i>"; ?>
                </li>
            <?php } ?>
        </ul>
        <div class="close-bread">
            <a href="#"><i class="icon-remove"></i></a>
        </div>
    </div>
</div>



<div class="row-fluid">
	
	<div class="span1">&nbsp;</div>

    <div class="span10">
        <?php
        if ($this->session->flashdata('message_gagal')) {
            echo '<hr><div class="alert alert-error"><button class="close" data-dismiss="alert" type="button">&times;</button>' . $this->session->flashdata('message_gagal') . '</div>';
        }
        if ($this->session->flashdata('message_sukses')) {
            echo '<hr><div class="alert alert-success"><button class="close" data-dismiss="alert" type="button">&times;</button>' . $this->session->flashdata('message_sukses') . '</div>';
        } ?>

        <div class="box box-bordered box-color">
            <div class="box-title">
                <h3><i class=" icon-plus-sign"></i><?php echo $judul_form . " " . $sub_judul_form; ?> </h3>
            </div>
            <div class="box-content nopadding">
                <!-- <form action="#" method="POST" class='form-horizontal form-validate' id="bb"> -->
                <?php echo form_open('validasi_ppks_swasta/tambih_robih', array('name' => 'bb', 'id' => 'bb', 'class' => 'form-horizontal form-bordered form-validate form-wysiwyg', 'enctype' => 'multipart/form-data')); ?>


                <input type="hidden" name="id" id="id" value="<?php echo isset($field->id) ? $field->id : ''; ?>">
                <input type="hidden" name="<?= $this->security->get_csrf_token_name(); ?>" value="<?= $this->security->get_csrf_hash(); ?>" style="display: none">

                <input type="hidden" value="<?php echo $this->input->ip_address(); ?>" name="ip_address">
				
				
				
				
					
					
					
					<div class="control-group no-border">
						<label class="control-label" for="textfield">Kab/Kota</label>

						<div class="controls">
							
							: <?php echo getKabkot($field->id_provinsi, $field->id_kabkot); ?>
						</div>
					</div>
					
					
					<div class="control-group no-border" id="div_kec">
						<label class="control-label" for="textfield">Kecamatan</label>

						<div class="controls">
							
							: <?php echo getKec($field->id_provinsi, $field->id_kabkot,$field->id_kecamatan); ?>
							
						</div>
					</div>
					
					
					
					<div class="control-group no-border" id="div_kel">
						<label class="control-label" for="textfield">Kelurahan</label>

						<div class="controls">
							
							: <?php echo getKel($field->id_provinsi, $field->id_kabkot,$field->id_kecamatan,$field->id_kelurahan); ?>
						</div>
					</div>
					
				
				
				<div class="control-group">
										<label class="control-label" for="textfield">Tgl.Register</label>                                        
                                        <div class="controls">
                                        : <?php echo isset($field->tgl_register) ? $field->tgl_register : $this->input->post("tgl_register"); ?>
										
										</div>
									</div>
				
				
				<div class="control-group">
                    <label for="textfield" class="control-label">Nama</label>
                    <div class="controls">
                        : <?php echo isset($field->nama) ? $field->nama : $this->input->post("nama"); ?>
                    </div>
                </div>
				
				
				<div class="control-group">
										<label class="control-label" for="textfield">NIK</label>                                        
                                        <div class="controls">
                                        : <?php echo isset($field->nik) ? $field->nik : $this->input->post("nik"); ?>
										</div>
									</div>
				
				
				
				<div class="control-group">
                    <label for="textfield" class="control-label">NO.KK </label>
                    <div class="controls">
					   : <?php echo isset($field->nokk) ? $field->nokk : $this->input->post("nokk"); ?>
                    </div>
                </div>
				
				
				<div class="control-group">
                    <label for="textfield" class="control-label">Tempat Lahir</label>
                    <div class="controls">
										   : <?php echo isset($field->tempat_lahir) ? $field->tempat_lahir : $this->input->post("tempat_lahir"); ?>

                    </div>
                </div>

                
				<div class="control-group">
										<label class="control-label" for="textfield">Tgl.Lahir</label>                                        
                                        <div class="controls">
                                        : <?php echo isset($field->tgl_lahir) ? $field->tgl_lahir : $this->input->post("tgl_lahir"); ?>
										
										</div>
									</div>
									
									
				<div class="control-group">
			
										<label class="control-label" for="textfield">Jenis Kelamin</label>                                        
                                        <div class="controls">
                                        : <?php echo isset($field->sex)?$field->sex:'';?>
										</div>
									</div>
									
									
									
				<div class="control-group">
                    <label for="textfield" class="control-label">Alamat</label>
                    <div class="controls">
                        : <?php echo isset($field->alamat) ? $field->alamat : $this->input->post("alamat"); ?>
                    </div>
                </div>
				
				
				
				
				
			
			
				
				
				<div class="control-group">
                    <label for="textfield" class="control-label">Kategori PPKS</label>
                    <div class="controls">
                        <?php $id_kategori = isset($field->id_kategori) ? $field->id_kategori : $this->input->post("id_kategori"); ?>

                        

							<select class="input-xxlarge" name="id_kategori" disabled>
							
							<option value="">-Pilih-</option>
							<?php foreach ($list_kategori as $row) { ?>
							<option value="<?php echo $row->id; ?>" <?php if ($id_kategori == $row->id) { echo "selected";} ?>><?php echo $row->nama; ?></option>
							<?php } ?>
							</select>
                            <hr>

                        



                        <span class="required-server"><?php echo form_error('uraian', '<p style="color:#F83A18">', '</p>'); ?></span>
                    </div>
                </div>
                
                
				
				
				<div class="control-group">
			<?php $status_keberadaan= isset($field->status_keberadaan)?$field->status_keberadaan:'';?>
										<label class="control-label" for="textfield">Keberadaan Saat Ini ?</label>                                        
                                        <div class="controls">
                                        <select name="status_keberadaan" disabled   onchange="doKeberadaan(this.value);">
										<option value="">-Pilih-</option>
                                           <option value="MASYARAKAT" <?php if ($status_keberadaan=="MASYARAKAT") { echo "selected";}?>>MASYARAKAT</option>
                                          <option value="LKS"  <?php if ($status_keberadaan=="LKS") { echo "selected";}?>>LKS</option>
                                        </select>
										</div>
									</div>
					
				
				
				
				
								
				
				
				


                

				<div id="v_lks">
				
				
					<div class="control-group">
											<label class="control-label" for="textfield">Nama LKS</label>                                        
											<div class="controls">
											<input disabled type="text" placeholder="" id="nama_lks" class="input-xxlarge" name="nama_lks" value="<?php echo isset($field->nama_lks) ? $field->nama_lks : $this->input->post("nama_lks"); ?>"  required>
											</div>
					</div>
				
				
				
				</div>
                                    
				
				
				<div id="v_masyarakat">
				
				
					<div class="control-group">
											<label class="control-label" for="textfield">Sudah Mendapatkan Bantuan ?</label>                                        
											<div class="controls">
											<?php $status_bantuan= isset($field->status_bantuan)?$field->status_bantuan:'';?> 
											<select name="status_bantuan" disabled   onchange="doBantuan(this.value);">
										<option value="">-Pilih-</option>
                                           <option value="YA" <?php if ($status_bantuan=="YA") { echo "selected";}?>>YA</option>
                                          <option value="TIDAK"  <?php if ($status_bantuan=="TIDAK") { echo "selected";}?>>TIDAK</option>
                                        </select>
											
											</div>
					</div>
					
					
			
				<div class="control-group" id="v_butuh_panti">
											<label class="control-label" for="textfield">Butuh Panti ?</label>                                        
											<div class="controls">
											<?php $status_panti= isset($field->status_panti)?$field->status_panti:'';?> 
											<select name="status_panti" disabled>
										<option value="">-Pilih-</option>
                                           <option value="YA" <?php if ($status_panti=="YA") { echo "selected";}?>>YA</option>
                                          <option value="TIDAK"  <?php if ($status_panti=="TIDAK") { echo "selected";}?>>TIDAK</option>
                                        </select>
											
											</div>
					</div>
				
				
				</div>
				
				
				
				
				
				<script type="text/javascript">
				
				
				
				
				   <?php if ($status_keberadaan=="MASYARAKAT") { ?>
				   
							$("#v_lks").hide();
							$("#v_masyarakat").show();
							 
							 <?php if ($status_bantuan=="YA") { ?>
								$("#v_butuh_panti").show();
							 <?php } else { ?>
								$("#v_butuh_panti").hide();
							 <?php } ?>
							
				   
				   
				   <?php } elseif ($status_keberadaan=="LKS") { ?>
				   
							$("#v_lks").show();
							$("#v_masyarakat").hide();
							$("#v_butuh_panti").hide();
				   
				   <?php } else { ?>
				   
				    $("#v_lks").hide();
					$("#v_masyarakat").hide();
				   
				   <?php } ?>
					
					
					function doKeberadaan(flag) {
						
						//alert(flag);
						if (flag=="MASYARAKAT") {
							
							$("#v_lks").hide();
							$("#v_masyarakat").show();
							$("#v_butuh_panti").hide();
							
						} else if (flag=="LKS") { 
						
							$("#v_lks").show();
							$("#v_masyarakat").hide();
							$("#v_butuh_panti").hide();
						}
						
					}
					
					
					
						function doBantuan(flag) {
						
						//alert(flag);
						if (flag=="YA") {
							
							$("#v_butuh_panti").show();
							
						} else if (flag=="TIDAK") { 
						
						    $("#v_butuh_panti").hide();

						}
						
					}
					
					
			    </script>
				
				
				
				
				

				
				

                <div class="control-group">
                    <label class="control-label" for="textfield">Photo Personal</label>
                    <div class="controls">
                        
                        

                        <?php if (isset($field->foto)) { ?>
                            <img src="<?php echo base_url(); ?>/<?php echo isset($field->foto) ? $field->foto : ''; ?>" >
                        <?php } ?>
                    </div>
                </div>
				
				
				
				<div class="control-group">
			<?php $approved= isset($field->approved)?$field->approved:'';?>
										<label class="control-label" for="textfield">Status Validasi</label>                                        
                                        <div class="controls">
                                        <select name="approved" required>
										<option value="">-Semua-</option>
                                           <option value="Approved" <?php if ($approved=="Approved") { echo "selected"; } ?>>Approved</option>
                                          <option value="Not Approved" <?php if ($approved=="Not Approved") { echo "selected"; } ?>>Not Approved</option> 
										  <option value="Belum Di-Validasi" <?php if ($approved=="Belum Di-Validasi") { echo "selected"; } ?>>Belum Di-Validasi</option>
                                        </select>
										</div>
									</div>


			<div class="control-group">
                    <label for="textfield" class="control-label">Catatan Validasi</label>
                    <div class="controls">
                        <input type="text"  id="catatan_validasi" class="input-xxlarge" name="catatan_validasi" value="<?php echo isset($field->catatan_validasi) ? $field->catatan_validasi : $this->input->post("catatan_validasi"); ?>"  required>
                        <span class="required-server"><?php echo form_error('catatan_validasi', '<p style="color:#F83A18">', '</p>'); ?></span>
                    </div>
                </div>
				





                <div class="form-actions">
                    <button id="btn_simpan" class="btn btn-blue" type="submit">Simpan</button>
                    <a class="btn btn-danger" href="<?php echo site_url(); ?>validasi_ppks_swasta">Kembali</a>
                </div>

                </form>
            </div>
        </div>
		
		
		<div class="span1">&nbsp;</div>
		
    </div>

