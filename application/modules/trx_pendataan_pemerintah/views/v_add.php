<div class="container-fluid">
    <br>
    <div class="breadcrumbs">
        <ul>
            <?php foreach ($breadcrumbs as $key => $value) { ?>
                <li>
                    <a href="<?php echo site_url($value['link']) ?>">
                        <?php echo $value['name']; ?></a>
                    <?php echo (count($breadcrumbs) - 1) == $key ? "" : "<i class='icon-angle-right'></i>"; ?>
                </li>
            <?php } ?>
        </ul>
        <div class="close-bread">
            <a href="#"><i class="icon-remove"></i></a>
        </div>
    </div>
</div>



<div class="row-fluid">
	
	<div class="span1">&nbsp;</div>

    <div class="span10">
        <?php
        if ($this->session->flashdata('message_gagal')) {
            echo '<hr><div class="alert alert-error"><button class="close" data-dismiss="alert" type="button">&times;</button>' . $this->session->flashdata('message_gagal') . '</div>';
        }
        if ($this->session->flashdata('message_sukses')) {
            echo '<hr><div class="alert alert-success"><button class="close" data-dismiss="alert" type="button">&times;</button>' . $this->session->flashdata('message_sukses') . '</div>';
        } ?>

        <div class="box box-bordered box-color">
            <div class="box-title">
                <h3><i class=" icon-plus-sign"></i><?php echo $judul_form . " " . $sub_judul_form; ?> </h3>
            </div>
            <div class="box-content nopadding">
                <!-- <form action="#" method="POST" class='form-horizontal form-validate' id="bb"> -->
                <?php echo form_open('trx_pendataan_pemerintah/tambih_robih', array('name' => 'bb', 'id' => 'bb', 'class' => 'form-horizontal form-bordered form-validate form-wysiwyg', 'enctype' => 'multipart/form-data')); ?>


                <input type="hidden" name="id" id="id" value="<?php echo isset($field->id) ? $field->id : ''; ?>">
                <input type="hidden" name="<?= $this->security->get_csrf_token_name(); ?>" value="<?= $this->security->get_csrf_hash(); ?>" style="display: none">

                <input type="hidden" value="<?php echo $this->input->ip_address(); ?>" name="ip_address">
				
				
				
				<?php 
				
				if ($this->session->userdata('sesi_bo_user_group')==1 or $this->session->userdata('sesi_bo_user_group')==2) {
					require_once(APPPATH . '/modules/filter_wilayah/views/v_filter_panti_gov.php');  
				} else {
					require_once(APPPATH . '/modules/filter_wilayah/views/v_filter_panti_text.php');  
				}
				
				?>
				
				<div class="control-group">
                    <label for="textfield" class="control-label">Kategori PPKS</label>
                    <div class="controls">
                        <?php $id_kategori = isset($field->id_kategori) ? $field->id_kategori : $this->input->post("id_kategori"); ?>

                        

							<select class="input-xxlarge" name="id_kategori" required>
							
							<option value="">-Pilih-</option>
							<?php foreach ($list_kategori as $row) { ?>
							<option value="<?php echo $row->id; ?>" <?php if ($id_kategori == $row->id) { echo "selected";} ?>><?php echo $row->nama; ?></option>
							<?php } ?>
							</select>
                            <hr>

                        



                        <span class="required-server"><?php echo form_error('uraian', '<p style="color:#F83A18">', '</p>'); ?></span>
                    </div>
                </div>
                
				
				<div class="control-group">
										<label class="control-label" for="textfield">Tgl.Masuk/Register</label>                                        
                                        <div class="controls">
                                        <input type="date" placeholder="" id="tgl_register" class="input-large" name="tgl_register" value="<?php echo isset($field->tgl_register) ? $field->tgl_register : $this->input->post("tgl_register"); ?>"  required>
										
										</div>
									</div>
				
				
				<div class="control-group">
                    <label for="textfield" class="control-label">Nama</label>
                    <div class="controls">
                        <input type="text"  id="nama" class="input-xxlarge" name="nama" value="<?php echo isset($field->nama) ? $field->nama : $this->input->post("nama"); ?>"  required>
                        <span class="required-server"><?php echo form_error('nama', '<p style="color:#F83A18">', '</p>'); ?></span>
                    </div>
                </div>
				
				
				<div class="control-group">
										<label class="control-label" for="textfield">NIK</label>                                        
                                        <div class="controls">
                                        <input type="number" placeholder="" id="nik" class="input-large" name="nik" value="<?php echo isset($field->nik) ? $field->nik : $this->input->post("nik"); ?>" maxlength="16" data-rule-minlength="16"  >
										</div>
									</div>
				
				
				
				<div class="control-group">
                    <label for="textfield" class="control-label">NO.KK </label>
                    <div class="controls">
					   <input type="number" placeholder="" id="nokk" class="input-xxlarge" name="nokk" value="<?php echo isset($field->nokk) ? $field->nokk : $this->input->post("nokk"); ?>"  ><span class="required-server"><?php echo form_error('nokk', '<p style="color:#F83A18">', '</p>'); ?></span>
                    </div>
                </div>
				
				
				<div class="control-group">
                    <label for="textfield" class="control-label">Tempat Lahir</label>
                    <div class="controls">
										   <input type="text" placeholder="" id="tempat_lahir" class="input-xxlarge" name="tempat_lahir" value="<?php echo isset($field->tempat_lahir) ? $field->tempat_lahir : $this->input->post("tempat_lahir"); ?>"  >

 <span class="required-server"><?php echo form_error('tempat_lahir', '<p style="color:#F83A18">', '</p>'); ?></span>
                    </div>
                </div>

                
				<div class="control-group">
										<label class="control-label" for="textfield">Tgl.Lahir</label>                                        
                                        <div class="controls">
                                        <input type="date" placeholder="" id="tgl_lahir" class="input-large" name="tgl_lahir" value="<?php echo isset($field->tgl_lahir) ? $field->tgl_lahir : $this->input->post("tgl_lahir"); ?>"  required>
										
										</div>
									</div>
									
									
				<div class="control-group">
			<?php $sex= isset($field->sex)?$field->sex:'';?>
										<label class="control-label" for="textfield">Jenis Kelamin</label>                                        
                                        <div class="controls">
                                        <select name="sex" required>
										<option value="">-Pilih-</option>
                                           <option value="LAKI-LAKI" <?php if ($sex=="LAKI-LAKI") { echo "selected";}?>>LAKI-LAKI</option>
                                          <option value="PEREMPUAN"  <?php if ($sex=="PEREMPUAN") { echo "selected";}?>>PEREMPUAN</option>
                                        </select>
										</div>
									</div>
									
									
									
				<div class="control-group">
                    <label for="textfield" class="control-label">Alamat</label>
                    <div class="controls">
                        <input type="text" placeholder="" id="alamat" class="input-xxlarge" name="alamat" value="<?php echo isset($field->alamat) ? $field->alamat : $this->input->post("alamat"); ?>"  >
                        <span class="required-server"><?php echo form_error('alamat', '<p style="color:#F83A18">', '</p>'); ?></span>
                    </div>
                </div>
				
				
				<?php $status_keberadaan= isset($field->status_keberadaan)?$field->status_keberadaan:'';?>
				
				<div class="control-group">
										<label class="control-label" for="textfield">Status</label>                                        
                                        <div class="controls">
                                        <select name="status_keberadaan" required   >
										<option value="">-Pilih-</option>
                                           <option value="SUDAH KELUAR" <?php if ($status_keberadaan=="SUDAH KELUAR") { echo "selected";}?>>SUDAH KELUAR</option>
                                          <option value="MASIH DIPANTI"  <?php if ($status_keberadaan=="MASIH DIPANTI") { echo "selected";}?>>MASIH DIPANTI</option>
                                        </select>
										</div>
									</div>
				
				
				
				<div class="control-group">
										<label class="control-label" for="textfield">Tgl.Keluar</label>                                        
                                        <div class="controls">
                                        <input type="date" placeholder="" id="tgl_out" class="input-large" name="tgl_out" value="<?php echo isset($field->tgl_out) ? $field->tgl_out : $this->input->post("tgl_out"); ?>"  >
										
										</div>
									</div>
			
			
				
				
				
                
				

                <div class="control-group">
                    <label class="control-label" for="textfield">Photo Personal</label>
                    <div class="controls">
                        <!-- <input type="hidden" name="nip" id="nip" class="input-large" value=""> -->
                        <input type="file" name="berkas" id="berkas" accept="image/*">
                        <span class="required-server">
                            <p>*<small>File tipe gambar</small>
							<br/>*<small>Ukuran maksimal 1 MB</small>
							</p>
                        </span>

                        <?php if (isset($field->foto)) { ?>
                            <img src="<?php echo base_url(); ?>/<?php echo isset($field->foto) ? $field->foto : ''; ?>" >
                        <?php } ?>
                    </div>
                </div>








                <div class="form-actions">
                    <button id="btn_simpan" class="btn btn-blue" type="submit">Simpan</button>
                    <a class="btn btn-danger" href="<?php echo site_url(); ?>trx_pendataan_pemerintah">Kembali</a>
                </div>

                </form>
            </div>
        </div>
		
		
		<div class="span1">&nbsp;</div>
		
    </div>

