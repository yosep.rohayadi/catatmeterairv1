<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;


class Rekap_pegawai extends MX_Controller
{
  //put your code here
  public function __construct()
  {
    parent::__construct();
    $this->load->helper('utility');
    // 
    $this->truly_madly_deeply();

    Modules::run('ref_role/permissions');

    $this->load->helper('login');
    $this->load->helper('tool');
  }

  public function truly_madly_deeply()
  {
    if (!$this->session->userdata('truly_madly_deeply')) {
      redirect('signon');
    }
  }


  public function index()
  {
    if (isset($_POST['id_prov'])) {
      $data2 = array('s_id_prov' => $_POST['id_prov']);
      $this->session->set_userdata($data2);
    }

    if (isset($_POST['id_kabkot'])) {
      $data3 = array('s_id_kabkot' => $_POST['id_kabkot']);
      $this->session->set_userdata($data3);
    }

    $sesi_bo_id_prov = $this->session->userdata('sesi_bo_id_prov');
    $sesi_bo_id_kabkot = $this->session->userdata('sesi_bo_id_kabkot');

    $s_prov = $this->session->userdata('s_id_prov');
    $s_kabkota = $this->session->userdata('s_id_kabkot');

    if (isset($s_prov)) {
      $id_prov = $s_prov;
    } else {
      $id_prov = $this->session->userdata('sesi_bo_id_prov');
    }

    if (isset($s_kabkota)) {
      $id_kabkot = $s_kabkota;
    } else {
      $id_kabkot = $this->session->userdata('sesi_bo_id_kabkot');
    }

    // status pegawai
    /*
			$qry = "SELECT 
					A.*,
					(
					SELECT COUNT(id) FROM ref_pegawai 
					WHERE 
					id_status_pegawai=A.id_status_pegawai and status_aktif='Y'";
					
					if ($this->session->userdata('s_id_prov')!="" or $sesi_bo_id_prov!="") {
			
						$qry .= " and 
						(id_prov='".$this->session->userdata('s_id_prov')."' or  id_prov='".$sesi_bo_id_prov."') ";
					
					}					
			
					if ($this->session->userdata('s_id_kabkot')!="" or $sesi_bo_id_kabkot!="") {
					
						$qry .= " and 
						(id_kabkot='".$this->session->userdata('s_id_kabkot')."' or id_kabkot='".$sesi_bo_id_kabkot."')
						";
					
					}				
					
					$qry.=" ) AS jml
					FROM
					ref_status_pegawai A";
		
			$data['ListData'] = $this->db->query($qry)->result_array();  
			*/


    // JUMLAH PNS SATPOLPP
    $qry = "SELECT COUNT(id) AS jml FROM ref_pegawai 
            WHERE id_status_pegawai=1 AND status_aktif='Y'
    ";

    if ($this->session->userdata('s_id_prov') != "" or $sesi_bo_id_prov != "") {

      $qry .= " AND (id_prov='" . $this->session->userdata('s_id_prov') . "' or  id_prov='" . $sesi_bo_id_prov . "') ";

      if ($this->session->userdata('s_id_kabkot') != "" or $sesi_bo_id_kabkot != "") {
        $qry .= " AND (id_kabkot='" . $this->session->userdata('s_id_kabkot') . "') ";
      } elseif ($this->session->userdata('s_id_kabkot') == "" or $sesi_bo_id_kabkot == "") {
        $qry .= " AND id_kabkot='' ";
      } else {
      }
    }

    // if ($this->session->userdata('s_id_prov') != "" or $sesi_bo_id_prov != "") {

    //   $qry .= " AND 
    // 				(id_prov='" . $this->session->userdata('s_id_prov') . "' or  id_prov='" . $sesi_bo_id_prov . "') ";
    // }

    // if ($this->session->userdata('s_id_kabkot') != "" or $sesi_bo_id_kabkot != "") {

    //   $qry .= " AND 
    // 				(id_kabkot='" . $this->session->userdata('s_id_kabkot') . "' or id_kabkot='" . $sesi_bo_id_kabkot . "')
    // 				";
    // }

    $data['jml_pns_satpol_pp'] = $this->db->query($qry)->row();


    // JUMLAH PNS - FUNGSIONAL JFT
    $qry = "SELECT COUNT(id) AS jml FROM ref_pegawai 
            WHERE id_status_pegawai=1 AND id_jenis_jabatan IN ('1') AND id_tingkat_jabatan>=1 AND status_aktif='Y'
    ";

    if ($this->session->userdata('s_id_prov') != "" or $sesi_bo_id_prov != "") {

      $qry .= " AND (id_prov='" . $this->session->userdata('s_id_prov') . "' or  id_prov='" . $sesi_bo_id_prov . "') ";

      if ($this->session->userdata('s_id_kabkot') != "" or $sesi_bo_id_kabkot != "") {
        $qry .= " AND (id_kabkot='" . $this->session->userdata('s_id_kabkot') . "') ";
      } elseif ($this->session->userdata('s_id_kabkot') == "" or $sesi_bo_id_kabkot == "") {
        $qry .= " AND id_kabkot='' ";
      } else {
      }
    }

    // if ($this->session->userdata('s_id_prov') != "" or $sesi_bo_id_prov != "") {

    //   $qry .= " and 
    // 				(id_prov='" . $this->session->userdata('s_id_prov') . "' or  id_prov='" . $sesi_bo_id_prov . "') ";
    // }

    // if ($this->session->userdata('s_id_kabkot') != "" or $sesi_bo_id_kabkot != "") {

    //   $qry .= " and 
    // 				(id_kabkot='" . $this->session->userdata('s_id_kabkot') . "' or id_kabkot='" . $sesi_bo_id_kabkot . "')
    // 				";
    // }

    $data['jml_fungsional'] = $this->db->query($qry)->row();


    // JUMLAH PNS - FUNGSIONAL JFU
    $qry = "SELECT COUNT(id) AS jml FROM ref_pegawai 
            WHERE id_status_pegawai=1 AND id_jenis_jabatan='2' AND status_aktif='Y'
    ";

    if ($this->session->userdata('s_id_prov') != "" or $sesi_bo_id_prov != "") {

      $qry .= " AND (id_prov='" . $this->session->userdata('s_id_prov') . "' or  id_prov='" . $sesi_bo_id_prov . "') ";

      if ($this->session->userdata('s_id_kabkot') != "" or $sesi_bo_id_kabkot != "") {
        $qry .= " AND (id_kabkot='" . $this->session->userdata('s_id_kabkot') . "') ";
      } elseif ($this->session->userdata('s_id_kabkot') == "" or $sesi_bo_id_kabkot == "") {
        $qry .= " AND id_kabkot='' ";
      } else {
      }
    }

    // if ($this->session->userdata('s_id_prov') != "" or $sesi_bo_id_prov != "") {

    //   $qry .= " and 
    // 				(id_prov='" . $this->session->userdata('s_id_prov') . "' or  id_prov='" . $sesi_bo_id_prov . "') ";
    // }

    // if ($this->session->userdata('s_id_kabkot') != "" or $sesi_bo_id_kabkot != "") {

    //   $qry .= " and 
    // 				(id_kabkot='" . $this->session->userdata('s_id_kabkot') . "' or id_kabkot='" . $sesi_bo_id_kabkot . "')
    // 				";
    // }

    $data['jml_jfu'] = $this->db->query($qry)->row();


    // JUMLAH PNS - STRUKTURAL
    $qry = "SELECT COUNT(id) AS jml FROM ref_pegawai 
            WHERE id_status_pegawai=1 AND id_jenis_jabatan='3' AND status_aktif='Y'
    ";

    if ($this->session->userdata('s_id_prov') != "" or $sesi_bo_id_prov != "") {

      $qry .= " AND (id_prov='" . $this->session->userdata('s_id_prov') . "' or  id_prov='" . $sesi_bo_id_prov . "') ";

      if ($this->session->userdata('s_id_kabkot') != "" or $sesi_bo_id_kabkot != "") {
        $qry .= " AND (id_kabkot='" . $this->session->userdata('s_id_kabkot') . "') ";
      } elseif ($this->session->userdata('s_id_kabkot') == "" or $sesi_bo_id_kabkot == "") {
        $qry .= " AND id_kabkot='' ";
      } else {
      }
    }

    // if ($this->session->userdata('s_id_prov') != "" or $sesi_bo_id_prov != "") {

    //   $qry .= " and 
    // 				(id_prov='" . $this->session->userdata('s_id_prov') . "' or  id_prov='" . $sesi_bo_id_prov . "') ";
    // }

    // if ($this->session->userdata('s_id_kabkot') != "" or $sesi_bo_id_kabkot != "") {

    //   $qry .= " and 
    // 				(id_kabkot='" . $this->session->userdata('s_id_kabkot') . "' or id_kabkot='" . $sesi_bo_id_kabkot . "')
    // 				";
    // }

    $data['jml_struktural'] = $this->db->query($qry)->row();


    // JUMLAH PNS DAMKAR
    $qry = "SELECT COUNT(id) AS jml FROM ref_pegawai 
            WHERE id_status_pegawai=4 AND status_aktif='Y'
    ";

    if ($this->session->userdata('s_id_prov') != "" or $sesi_bo_id_prov != "") {

      $qry .= " AND (id_prov='" . $this->session->userdata('s_id_prov') . "' or  id_prov='" . $sesi_bo_id_prov . "') ";

      if ($this->session->userdata('s_id_kabkot') != "" or $sesi_bo_id_kabkot != "") {
        $qry .= " AND (id_kabkot='" . $this->session->userdata('s_id_kabkot') . "') ";
      } elseif ($this->session->userdata('s_id_kabkot') == "" or $sesi_bo_id_kabkot == "") {
        $qry .= " AND id_kabkot='' ";
      } else {
      }
    }

    // if ($this->session->userdata('s_id_prov') != "" or $sesi_bo_id_prov != "") {

    //   $qry .= " and 
    // 				(id_prov='" . $this->session->userdata('s_id_prov') . "' or  id_prov='" . $sesi_bo_id_prov . "') ";
    // }

    // if ($this->session->userdata('s_id_kabkot') != "" or $sesi_bo_id_kabkot != "") {

    //   $qry .= " and 
    // 				(id_kabkot='" . $this->session->userdata('s_id_kabkot') . "' or id_kabkot='" . $sesi_bo_id_kabkot . "')
    // 				";
    // }

    $data['jml_pns_damkar'] = $this->db->query($qry)->row();


    // JUMLAH PNS SATPOLPP - PPNS
    $qry = "SELECT COUNT(id) AS jml FROM ref_pegawai 
            WHERE id_status_pegawai=1 and id_status_pegawai_ppns=1 and status_aktif='Y'
    ";

    if ($this->session->userdata('s_id_prov') != "" or $sesi_bo_id_prov != "") {

      $qry .= " AND (id_prov='" . $this->session->userdata('s_id_prov') . "' or  id_prov='" . $sesi_bo_id_prov . "') ";

      if ($this->session->userdata('s_id_kabkot') != "" or $sesi_bo_id_kabkot != "") {
        $qry .= " AND (id_kabkot='" . $this->session->userdata('s_id_kabkot') . "') ";
      } elseif ($this->session->userdata('s_id_kabkot') == "" or $sesi_bo_id_kabkot == "") {
        $qry .= " AND id_kabkot='' ";
      } else {
      }
    }

    // if ($this->session->userdata('s_id_prov') != "" or $sesi_bo_id_prov != "") {

    //   $qry .= " and 
    // 				(id_prov='" . $this->session->userdata('s_id_prov') . "' or  id_prov='" . $sesi_bo_id_prov . "') ";
    // }

    // if ($this->session->userdata('s_id_kabkot') != "" or $sesi_bo_id_kabkot != "") {

    //   $qry .= " and 
    // 				(id_kabkot='" . $this->session->userdata('s_id_kabkot') . "' or id_kabkot='" . $sesi_bo_id_kabkot . "')
    // 				";
    // }

    $data['jml_ppns_satpol_pp'] = $this->db->query($qry)->row();


    // JUMLAH PNS SATPOLPP - PPNS LAINNYA
    $qry = "SELECT COUNT(id) AS jml FROM ref_pegawai 
            WHERE id_status_pegawai=1 AND id_status_pegawai_ppns=2 and status_aktif='Y'
    ";

    if ($this->session->userdata('s_id_prov') != "" or $sesi_bo_id_prov != "") {

      $qry .= " AND (id_prov='" . $this->session->userdata('s_id_prov') . "' or  id_prov='" . $sesi_bo_id_prov . "') ";

      if ($this->session->userdata('s_id_kabkot') != "" or $sesi_bo_id_kabkot != "") {
        $qry .= " AND (id_kabkot='" . $this->session->userdata('s_id_kabkot') . "') ";
      } elseif ($this->session->userdata('s_id_kabkot') == "" or $sesi_bo_id_kabkot == "") {
        $qry .= " AND id_kabkot='' ";
      } else {
      }
    }

    // if ($this->session->userdata('s_id_prov') != "" or $sesi_bo_id_prov != "") {

    //   $qry .= " and 
    // 				(id_prov='" . $this->session->userdata('s_id_prov') . "' or  id_prov='" . $sesi_bo_id_prov . "') ";
    // }

    // if ($this->session->userdata('s_id_kabkot') != "" or $sesi_bo_id_kabkot != "") {

    //   $qry .= " and 
    // 				(id_kabkot='" . $this->session->userdata('s_id_kabkot') . "' or id_kabkot='" . $sesi_bo_id_kabkot . "')
    // 				";
    // }

    $data['jml_ppns_uk_lainnya'] = $this->db->query($qry)->row();


    // JUMLAH NON-PNS SATPOLPP
    $qry = "SELECT COUNT(id) AS jml FROM ref_pegawai 
            WHERE id_status_pegawai=5 AND status_aktif='Y'
    ";

    if ($this->session->userdata('s_id_prov') != "" or $sesi_bo_id_prov != "") {

      $qry .= " AND (id_prov='" . $this->session->userdata('s_id_prov') . "' or  id_prov='" . $sesi_bo_id_prov . "') ";

      if ($this->session->userdata('s_id_kabkot') != "" or $sesi_bo_id_kabkot != "") {
        $qry .= " AND (id_kabkot='" . $this->session->userdata('s_id_kabkot') . "') ";
      } elseif ($this->session->userdata('s_id_kabkot') == "" or $sesi_bo_id_kabkot == "") {
        $qry .= " AND id_kabkot='' ";
      } else {
      }
    }

    // if ($this->session->userdata('s_id_prov') != "" or $sesi_bo_id_prov != "") {

    //   $qry .= " and 
    // 				(id_prov='" . $this->session->userdata('s_id_prov') . "' or  id_prov='" . $sesi_bo_id_prov . "') ";
    // }

    // if ($this->session->userdata('s_id_kabkot') != "" or $sesi_bo_id_kabkot != "") {

    //   $qry .= " and 
    // 				(id_kabkot='" . $this->session->userdata('s_id_kabkot') . "' or id_kabkot='" . $sesi_bo_id_kabkot . "')
    // 				";
    // }

    $data['jml_non_pns_polpp'] = $this->db->query($qry)->row();


    // JUMLAH NON-PNS DAMKAR
    $qry = "SELECT COUNT(id) AS jml FROM ref_pegawai 
            WHERE id_status_pegawai=6 AND status_aktif='Y'
    ";

    if ($this->session->userdata('s_id_prov') != "" or $sesi_bo_id_prov != "") {

      $qry .= " AND (id_prov='" . $this->session->userdata('s_id_prov') . "' or  id_prov='" . $sesi_bo_id_prov . "') ";

      if ($this->session->userdata('s_id_kabkot') != "" or $sesi_bo_id_kabkot != "") {
        $qry .= " AND (id_kabkot='" . $this->session->userdata('s_id_kabkot') . "') ";
      } elseif ($this->session->userdata('s_id_kabkot') == "" or $sesi_bo_id_kabkot == "") {
        $qry .= " AND id_kabkot='' ";
      } else {
      }
    }

    // if ($this->session->userdata('s_id_prov') != "" or $sesi_bo_id_prov != "") {

    //   $qry .= " and 
    // 				(id_prov='" . $this->session->userdata('s_id_prov') . "' or  id_prov='" . $sesi_bo_id_prov . "') ";
    // }

    // if ($this->session->userdata('s_id_kabkot') != "" or $sesi_bo_id_kabkot != "") {

    //   $qry .= " and 
    // 				(id_kabkot='" . $this->session->userdata('s_id_kabkot') . "' or id_kabkot='" . $sesi_bo_id_kabkot . "')
    // 				";
    // }

    $data['jml_non_pns_damkar'] = $this->db->query($qry)->row();


    // JUMLAH NON-PNS - SATLINMAS
    $qry = "SELECT SUM(jumlah_laki+jumlah_wanita) AS jml FROM satlinmas WHERE 1=1 ";

    if ($this->session->userdata('s_id_prov') != "" or $sesi_bo_id_prov != "") {

      $qry .= " AND (id_prov='" . $this->session->userdata('s_id_prov') . "' or  id_prov='" . $sesi_bo_id_prov . "') ";

      if ($this->session->userdata('s_id_kabkot') != "" or $sesi_bo_id_kabkot != "") {
        $qry .= " AND (id_kabkot='" . $this->session->userdata('s_id_kabkot') . "') ";
      } elseif ($this->session->userdata('s_id_kabkot') == "" or $sesi_bo_id_kabkot == "") {
        $qry .= " AND id_kabkot='' ";
      } else {
      }
    }

    // if ($this->session->userdata('s_id_prov') != "" or $sesi_bo_id_prov != "") {

    //   $qry .= " and 
    // 				(id_prov='" . $this->session->userdata('s_id_prov') . "' or  id_prov='" . $sesi_bo_id_prov . "') ";
    // }

    // if ($this->session->userdata('s_id_kabkot') != "" or $sesi_bo_id_kabkot != "") {

    //   $qry .= " and 
    // 				(id_kabkot='" . $this->session->userdata('s_id_kabkot') . "' or id_kabkot='" . $sesi_bo_id_kabkot . "')
    // 				";
    // }

    $data['jml_satlinmas'] = $this->db->query($qry)->row();


    // pendidikan
    $qry = "SELECT 
				A.*,
				(
				SELECT COUNT(id) FROM ref_pegawai 
				WHERE 
        id_pendidikan=A.id  and status_aktif='Y'
    ";

    if ($this->session->userdata('s_id_prov') != "" or $sesi_bo_id_prov != "") {

      $qry .= " AND (id_prov='" . $this->session->userdata('s_id_prov') . "' or  id_prov='" . $sesi_bo_id_prov . "') ";

      if ($this->session->userdata('s_id_kabkot') != "" or $sesi_bo_id_kabkot != "") {
        $qry .= " AND (id_kabkot='" . $this->session->userdata('s_id_kabkot') . "') ";
      } elseif ($this->session->userdata('s_id_kabkot') == "" or $sesi_bo_id_kabkot == "") {
        $qry .= " AND id_kabkot='' ";
      } else {
      }
    }

    // if ($this->session->userdata('s_id_prov') != "" or $sesi_bo_id_prov != "") {

    //   $qry .= " and 
    // 				(id_prov='" . $this->session->userdata('s_id_prov') . "' or  id_prov='" . $sesi_bo_id_prov . "') ";
    // }

    // if ($this->session->userdata('s_id_kabkot') != "" or $sesi_bo_id_kabkot != "") {

    //   $qry .= " and 
    // 				(id_kabkot='" . $this->session->userdata('s_id_kabkot') . "' or id_kabkot='" . $sesi_bo_id_kabkot . "')
    // 				";
    // }

    $qry .= " ) AS jml
					FROM
					ref_pendidikan A";

    $data['ListData2'] = $this->db->query($qry)->result_array();


    // golongan
    $qry = "SELECT 
				A.*,
				(
				SELECT COUNT(id) FROM ref_pegawai 
				WHERE 
        id_golruang_grup=A.id  and status_aktif='Y'
    ";

    if ($this->session->userdata('s_id_prov') != "" or $sesi_bo_id_prov != "") {

      $qry .= " AND (id_prov='" . $this->session->userdata('s_id_prov') . "' or  id_prov='" . $sesi_bo_id_prov . "') ";

      if ($this->session->userdata('s_id_kabkot') != "" or $sesi_bo_id_kabkot != "") {
        $qry .= " AND (id_kabkot='" . $this->session->userdata('s_id_kabkot') . "') ";
      } elseif ($this->session->userdata('s_id_kabkot') == "" or $sesi_bo_id_kabkot == "") {
        $qry .= " AND id_kabkot='' ";
      } else {
      }
    }

    // if ($this->session->userdata('s_id_prov') != "" or $sesi_bo_id_prov != "") {

    //   $qry .= " and 
    // 				(id_prov='" . $this->session->userdata('s_id_prov') . "' or  id_prov='" . $sesi_bo_id_prov . "') ";
    // }

    // if ($this->session->userdata('s_id_kabkot') != "" or $sesi_bo_id_kabkot != "") {

    //   $qry .= " and 
    // 				(id_kabkot='" . $this->session->userdata('s_id_kabkot') . "' or id_kabkot='" . $sesi_bo_id_kabkot . "')
    // 				";
    // }

    $qry .= " ) AS jml
					FROM
					ref_golongan_grup A";

    $data['ListData3'] = $this->db->query($qry)->result_array();


    // diklat
    $qry = "SELECT 
				A.*,
				(
				SELECT COUNT(id) FROM ref_pegawai 
				WHERE 
        id_diklat=A.id_diklat  and status_aktif='Y'
    ";

    if ($this->session->userdata('s_id_prov') != "" or $sesi_bo_id_prov != "") {

      $qry .= " AND (id_prov='" . $this->session->userdata('s_id_prov') . "' or  id_prov='" . $sesi_bo_id_prov . "') ";

      if ($this->session->userdata('s_id_kabkot') != "" or $sesi_bo_id_kabkot != "") {
        $qry .= " AND (id_kabkot='" . $this->session->userdata('s_id_kabkot') . "') ";
      } elseif ($this->session->userdata('s_id_kabkot') == "" or $sesi_bo_id_kabkot == "") {
        $qry .= " AND id_kabkot='' ";
      } else {
      }
    }

    // if ($this->session->userdata('s_id_prov') != "" or $sesi_bo_id_prov != "") {

    //   $qry .= " and 
    // 				(id_prov='" . $this->session->userdata('s_id_prov') . "' or  id_prov='" . $sesi_bo_id_prov . "') ";
    // }

    // if ($this->session->userdata('s_id_kabkot') != "" or $sesi_bo_id_kabkot != "") {

    //   $qry .= " and 
    // 				(id_kabkot='" . $this->session->userdata('s_id_kabkot') . "' or id_kabkot='" . $sesi_bo_id_kabkot . "')
    // 				";
    // }

    $qry .= " ) AS jml
					FROM
					ref_diklat A";

    $data['ListData4'] = $this->db->query($qry)->result_array();


    $data['breadcrumbs'] = array(
      array(
        'link' => '',
        'name' => 'Laporan'
      ),
      array(
        'link' => 'rekap_pegawai',
        'name' => 'Rekapitulasi Data Pegawai'
      )
    );

    $data['sub_judul_form'] = "Laporan Rekapitulasi Data Pegawai ";

    $sql_prov = "select nama_provinsi from provinsi where (kode_provinsi='" . $this->session->userdata('s_id_prov') . "' or  kode_provinsi='" . $sesi_bo_id_prov . "') ";
    $query_prov = $this->db->query($sql_prov);
    $row_prov = $query_prov->row();
    $nama_provinsi = "";
    if (isset($row_prov)) {
      $nama_provinsi = $row_prov->nama_provinsi;
    }
    $data['nama_provinsi'] = $nama_provinsi;

    $sql_kabkot = "select nama from kabkota where (kode_kabkot='" . $this->session->userdata('s_id_kabkot') . "' or  kode_kabkot='" . $sesi_bo_id_kabkot . "') and (kode_provinsi='" . $this->session->userdata('s_id_prov') . "' or kode_provinsi='" . $sesi_bo_id_prov . "') ";
    $query_kabkot = $this->db->query($sql_kabkot);
    $row_kabkot = $query_kabkot->row();
    $nama_kabkot = "";
    if (isset($row_kabkot)) {
      $nama_kabkot = $row_kabkot->nama;
    }
    $data['nama_kabkot'] = $nama_kabkot;


    if ($this->uri->segment(3) == "cetak") {

      $data['provinsi'] = '';
      $data['kabupaten_kota'] = '';

      if (($id_prov != null  || $id_prov != '') && ($id_kabkot == null || $id_kabkot == '')) {
        $data['provinsi'] = $this->db->query("select * from provinsi where kode_provinsi='$id_prov'")->row()->nama_provinsi;
        $data['lembaga']  = $this->db->query("select * from kelembagaan where id_prov='$id_prov'")->row();
      }
      if (isset($id_prov) && isset($id_kabkot)) {
        $data['kabupaten_kota'] = $this->db->query("select * from kabkota where kode_provinsi='$id_prov' and kode_kabkot='$id_kabkot'")->row()->nama;
        $data['provinsi'] = $this->db->query("select * from provinsi where kode_provinsi='$id_prov'")->row()->nama_provinsi;
        $data['lembaga']  = $this->db->query("select * from kelembagaan where id_prov='$id_prov' and id_kabkot='$id_kabkot'")->row();
      }

      $html = $this->load->view('v_index_cetak', $data, true);

      $pdfFilePath = "laporan_rekap_pegawai-" . time() . ".pdf";

      $this->load->library('pdf');

      $this->pdf->pdf->AddPage(
        'P', // L - landscape, P - portrait
        '',
        '',
        '',
        '',
        20, // margin_left
        20, // margin right
        15, // margin top
        15, // margin bottom
        8, // margin header
        8 // margin footer
      );

      $this->pdf->pdf->WriteHTML($html);

      $this->pdf->pdf->Output($pdfFilePath, "I");
    } else {

      $this->template->load('template_admin', 'v_index', $data);
    }
  }


  public function clear_session()
  {
    $this->session->unset_userdata('s_id_prov');
    $this->session->unset_userdata('s_id_kabkot');
    redirect('rekap_pegawai');
  }

	public function export(){
		if (isset($_POST['id_prov'])) {
      $data2 = array('s_id_prov' => $_POST['id_prov']);
      $this->session->set_userdata($data2);
    }

    if (isset($_POST['id_kabkot'])) {
      $data3 = array('s_id_kabkot' => $_POST['id_kabkot']);
      $this->session->set_userdata($data3);
    }

    $sesi_bo_id_prov = $this->session->userdata('sesi_bo_id_prov');
    $sesi_bo_id_kabkot = $this->session->userdata('sesi_bo_id_kabkot');

    $s_prov = $this->session->userdata('s_id_prov');
    $s_kabkota = $this->session->userdata('s_id_kabkot');

    if (isset($s_prov)) {
      $id_prov = $s_prov;
    } else {
      $id_prov = $this->session->userdata('sesi_bo_id_prov');
    }

    if (isset($s_kabkota)) {
      $id_kabkot = $s_kabkota;
    } else {
      $id_kabkot = $this->session->userdata('sesi_bo_id_kabkot');
    }
		
		// JUMLAH PNS SATPOLPP
    $qry = "SELECT COUNT(id) AS jml FROM ref_pegawai 
            WHERE id_status_pegawai=1 AND status_aktif='Y'
    ";

    if ($this->session->userdata('s_id_prov') != "" or $sesi_bo_id_prov != "") {

      $qry .= " AND (id_prov='" . $this->session->userdata('s_id_prov') . "' or  id_prov='" . $sesi_bo_id_prov . "') ";

      if ($this->session->userdata('s_id_kabkot') != "" or $sesi_bo_id_kabkot != "") {
        $qry .= " AND (id_kabkot='" . $this->session->userdata('s_id_kabkot') . "') ";
      } elseif ($this->session->userdata('s_id_kabkot') == "" or $sesi_bo_id_kabkot == "") {
        $qry .= " AND id_kabkot='' ";
      } else {
      }
    }
		
		$data['jml_pns_satpol_pp'] = $this->db->query($qry)->row();
		
		
		// JUMLAH PNS - FUNGSIONAL JFT
    $qry = "SELECT COUNT(id) AS jml FROM ref_pegawai 
            WHERE id_status_pegawai=1 AND id_jenis_jabatan IN ('1') AND id_tingkat_jabatan>=1 AND status_aktif='Y'
    ";

    if ($this->session->userdata('s_id_prov') != "" or $sesi_bo_id_prov != "") {

      $qry .= " AND (id_prov='" . $this->session->userdata('s_id_prov') . "' or  id_prov='" . $sesi_bo_id_prov . "') ";

      if ($this->session->userdata('s_id_kabkot') != "" or $sesi_bo_id_kabkot != "") {
        $qry .= " AND (id_kabkot='" . $this->session->userdata('s_id_kabkot') . "') ";
      } elseif ($this->session->userdata('s_id_kabkot') == "" or $sesi_bo_id_kabkot == "") {
        $qry .= " AND id_kabkot='' ";
      } else {
      }
    }
		
    $data['jml_fungsional'] = $this->db->query($qry)->row();
		
		
		// JUMLAH PNS - FUNGSIONAL JFU
    $qry = "SELECT COUNT(id) AS jml FROM ref_pegawai 
            WHERE id_status_pegawai=1 AND id_jenis_jabatan='2' AND status_aktif='Y'
    ";

    if ($this->session->userdata('s_id_prov') != "" or $sesi_bo_id_prov != "") {

      $qry .= " AND (id_prov='" . $this->session->userdata('s_id_prov') . "' or  id_prov='" . $sesi_bo_id_prov . "') ";

      if ($this->session->userdata('s_id_kabkot') != "" or $sesi_bo_id_kabkot != "") {
        $qry .= " AND (id_kabkot='" . $this->session->userdata('s_id_kabkot') . "') ";
      } elseif ($this->session->userdata('s_id_kabkot') == "" or $sesi_bo_id_kabkot == "") {
        $qry .= " AND id_kabkot='' ";
      } else {
      }
    }

    $data['jml_jfu'] = $this->db->query($qry)->row();


    // JUMLAH PNS - STRUKTURAL
    $qry = "SELECT COUNT(id) AS jml FROM ref_pegawai 
            WHERE id_status_pegawai=1 AND id_jenis_jabatan='3' AND status_aktif='Y'
    ";

    if ($this->session->userdata('s_id_prov') != "" or $sesi_bo_id_prov != "") {

      $qry .= " AND (id_prov='" . $this->session->userdata('s_id_prov') . "' or  id_prov='" . $sesi_bo_id_prov . "') ";

      if ($this->session->userdata('s_id_kabkot') != "" or $sesi_bo_id_kabkot != "") {
        $qry .= " AND (id_kabkot='" . $this->session->userdata('s_id_kabkot') . "') ";
      } elseif ($this->session->userdata('s_id_kabkot') == "" or $sesi_bo_id_kabkot == "") {
        $qry .= " AND id_kabkot='' ";
      } else {
      }
    }
		
    $data['jml_struktural'] = $this->db->query($qry)->row();


    // JUMLAH PNS DAMKAR
    $qry = "SELECT COUNT(id) AS jml FROM ref_pegawai 
            WHERE id_status_pegawai=4 AND status_aktif='Y'
    ";

    if ($this->session->userdata('s_id_prov') != "" or $sesi_bo_id_prov != "") {

      $qry .= " AND (id_prov='" . $this->session->userdata('s_id_prov') . "' or  id_prov='" . $sesi_bo_id_prov . "') ";

      if ($this->session->userdata('s_id_kabkot') != "" or $sesi_bo_id_kabkot != "") {
        $qry .= " AND (id_kabkot='" . $this->session->userdata('s_id_kabkot') . "') ";
      } elseif ($this->session->userdata('s_id_kabkot') == "" or $sesi_bo_id_kabkot == "") {
        $qry .= " AND id_kabkot='' ";
      } else {
      }
    }

    $data['jml_pns_damkar'] = $this->db->query($qry)->row();


    // JUMLAH PNS SATPOLPP - PPNS
    $qry = "SELECT COUNT(id) AS jml FROM ref_pegawai 
            WHERE id_status_pegawai=1 and id_status_pegawai_ppns=1 and status_aktif='Y'
    ";

    if ($this->session->userdata('s_id_prov') != "" or $sesi_bo_id_prov != "") {

      $qry .= " AND (id_prov='" . $this->session->userdata('s_id_prov') . "' or  id_prov='" . $sesi_bo_id_prov . "') ";

      if ($this->session->userdata('s_id_kabkot') != "" or $sesi_bo_id_kabkot != "") {
        $qry .= " AND (id_kabkot='" . $this->session->userdata('s_id_kabkot') . "') ";
      } elseif ($this->session->userdata('s_id_kabkot') == "" or $sesi_bo_id_kabkot == "") {
        $qry .= " AND id_kabkot='' ";
      } else {
      }
    }

    $data['jml_ppns_satpol_pp'] = $this->db->query($qry)->row();


    // JUMLAH PNS SATPOLPP - PPNS LAINNYA
    $qry = "SELECT COUNT(id) AS jml FROM ref_pegawai 
            WHERE id_status_pegawai=1 AND id_status_pegawai_ppns=2 and status_aktif='Y'
    ";

    if ($this->session->userdata('s_id_prov') != "" or $sesi_bo_id_prov != "") {

      $qry .= " AND (id_prov='" . $this->session->userdata('s_id_prov') . "' or  id_prov='" . $sesi_bo_id_prov . "') ";

      if ($this->session->userdata('s_id_kabkot') != "" or $sesi_bo_id_kabkot != "") {
        $qry .= " AND (id_kabkot='" . $this->session->userdata('s_id_kabkot') . "') ";
      } elseif ($this->session->userdata('s_id_kabkot') == "" or $sesi_bo_id_kabkot == "") {
        $qry .= " AND id_kabkot='' ";
      } else {
      }
    }

    $data['jml_ppns_uk_lainnya'] = $this->db->query($qry)->row();


    // JUMLAH NON-PNS SATPOLPP
    $qry = "SELECT COUNT(id) AS jml FROM ref_pegawai 
            WHERE id_status_pegawai=5 AND status_aktif='Y'
    ";

    if ($this->session->userdata('s_id_prov') != "" or $sesi_bo_id_prov != "") {

      $qry .= " AND (id_prov='" . $this->session->userdata('s_id_prov') . "' or  id_prov='" . $sesi_bo_id_prov . "') ";

      if ($this->session->userdata('s_id_kabkot') != "" or $sesi_bo_id_kabkot != "") {
        $qry .= " AND (id_kabkot='" . $this->session->userdata('s_id_kabkot') . "') ";
      } elseif ($this->session->userdata('s_id_kabkot') == "" or $sesi_bo_id_kabkot == "") {
        $qry .= " AND id_kabkot='' ";
      } else {
      }
    }

    $data['jml_non_pns_polpp'] = $this->db->query($qry)->row();


    // JUMLAH NON-PNS DAMKAR
    $qry = "SELECT COUNT(id) AS jml FROM ref_pegawai 
            WHERE id_status_pegawai=6 AND status_aktif='Y'
    ";

    if ($this->session->userdata('s_id_prov') != "" or $sesi_bo_id_prov != "") {

      $qry .= " AND (id_prov='" . $this->session->userdata('s_id_prov') . "' or  id_prov='" . $sesi_bo_id_prov . "') ";

      if ($this->session->userdata('s_id_kabkot') != "" or $sesi_bo_id_kabkot != "") {
        $qry .= " AND (id_kabkot='" . $this->session->userdata('s_id_kabkot') . "') ";
      } elseif ($this->session->userdata('s_id_kabkot') == "" or $sesi_bo_id_kabkot == "") {
        $qry .= " AND id_kabkot='' ";
      } else {
      }
    }

    $data['jml_non_pns_damkar'] = $this->db->query($qry)->row();


    // JUMLAH NON-PNS - SATLINMAS
    $qry = "SELECT SUM(jumlah_laki+jumlah_wanita) AS jml FROM satlinmas WHERE 1=1 ";

    if ($this->session->userdata('s_id_prov') != "" or $sesi_bo_id_prov != "") {

      $qry .= " AND (id_prov='" . $this->session->userdata('s_id_prov') . "' or  id_prov='" . $sesi_bo_id_prov . "') ";

      if ($this->session->userdata('s_id_kabkot') != "" or $sesi_bo_id_kabkot != "") {
        $qry .= " AND (id_kabkot='" . $this->session->userdata('s_id_kabkot') . "') ";
      } elseif ($this->session->userdata('s_id_kabkot') == "" or $sesi_bo_id_kabkot == "") {
        $qry .= " AND id_kabkot='' ";
      } else {
      }
    }

    $data['jml_satlinmas'] = $this->db->query($qry)->row();


    // pendidikan
    $qry = "SELECT 
				A.*,
				(
				SELECT COUNT(id) FROM ref_pegawai 
				WHERE 
        id_pendidikan=A.id  and status_aktif='Y'
    ";

    if ($this->session->userdata('s_id_prov') != "" or $sesi_bo_id_prov != "") {

      $qry .= " AND (id_prov='" . $this->session->userdata('s_id_prov') . "' or  id_prov='" . $sesi_bo_id_prov . "') ";

      if ($this->session->userdata('s_id_kabkot') != "" or $sesi_bo_id_kabkot != "") {
        $qry .= " AND (id_kabkot='" . $this->session->userdata('s_id_kabkot') . "') ";
      } elseif ($this->session->userdata('s_id_kabkot') == "" or $sesi_bo_id_kabkot == "") {
        $qry .= " AND id_kabkot='' ";
      } else {
      }
    }

    $qry .= " ) AS jml
					FROM
					ref_pendidikan A";

    $data['ListData2'] = $this->db->query($qry)->result_array();


    // golongan
    $qry = "SELECT 
				A.*,
				(
				SELECT COUNT(id) FROM ref_pegawai 
				WHERE 
        id_golruang_grup=A.id  and status_aktif='Y'
    ";

    if ($this->session->userdata('s_id_prov') != "" or $sesi_bo_id_prov != "") {

      $qry .= " AND (id_prov='" . $this->session->userdata('s_id_prov') . "' or  id_prov='" . $sesi_bo_id_prov . "') ";

      if ($this->session->userdata('s_id_kabkot') != "" or $sesi_bo_id_kabkot != "") {
        $qry .= " AND (id_kabkot='" . $this->session->userdata('s_id_kabkot') . "') ";
      } elseif ($this->session->userdata('s_id_kabkot') == "" or $sesi_bo_id_kabkot == "") {
        $qry .= " AND id_kabkot='' ";
      } else {
      }
    }

    $qry .= " ) AS jml
					FROM
					ref_golongan_grup A";

    $data['ListData3'] = $this->db->query($qry)->result_array();


    // diklat
    $qry = "SELECT 
				A.*,
				(
				SELECT COUNT(id) FROM ref_pegawai 
				WHERE 
        id_diklat=A.id_diklat  and status_aktif='Y'
    ";

    if ($this->session->userdata('s_id_prov') != "" or $sesi_bo_id_prov != "") {

      $qry .= " AND (id_prov='" . $this->session->userdata('s_id_prov') . "' or  id_prov='" . $sesi_bo_id_prov . "') ";

      if ($this->session->userdata('s_id_kabkot') != "" or $sesi_bo_id_kabkot != "") {
        $qry .= " AND (id_kabkot='" . $this->session->userdata('s_id_kabkot') . "') ";
      } elseif ($this->session->userdata('s_id_kabkot') == "" or $sesi_bo_id_kabkot == "") {
        $qry .= " AND id_kabkot='' ";
      } else {
      }
    }

    $qry .= " ) AS jml
					FROM
					ref_diklat A";

    $data['ListData4'] = $this->db->query($qry)->result_array();

		
		
		$sql_prov = "select nama_provinsi from provinsi where (kode_provinsi='" . $this->session->userdata('s_id_prov') . "' or  kode_provinsi='" . $sesi_bo_id_prov . "') ";
    $query_prov = $this->db->query($sql_prov);
    $row_prov = $query_prov->row();
    $nama_provinsi = "";
    if (isset($row_prov)) {
      $nama_provinsi = $row_prov->nama_provinsi;
    }
    $data['nama_provinsi'] = $nama_provinsi;

    $sql_kabkot = "select nama from kabkota where (kode_kabkot='" . $this->session->userdata('s_id_kabkot') . "' or  kode_kabkot='" . $sesi_bo_id_kabkot . "') and (kode_provinsi='" . $this->session->userdata('s_id_prov') . "' or kode_provinsi='" . $sesi_bo_id_prov . "') ";
    $query_kabkot = $this->db->query($sql_kabkot);
    $row_kabkot = $query_kabkot->row();
    $nama_kabkot = "";
    if (isset($row_kabkot)) {
      $nama_kabkot = $row_kabkot->nama;
    }
    $data['nama_kabkot'] = $nama_kabkot;
		
		$data['provinsi'] = '';
		$data['kabupaten_kota'] = '';

		if (($id_prov != null  || $id_prov != '') && ($id_kabkot == null || $id_kabkot == '')) {
			$data['provinsi'] = $this->db->query("select * from provinsi where kode_provinsi='$id_prov'")->row()->nama_provinsi;
			$data['lembaga']  = $this->db->query("select * from kelembagaan where id_prov='$id_prov'")->row();
		}
		if (($id_prov != null  || $id_prov != '') && ($id_kabkot != null || $id_kabkot != '')) {
			$data['kabupaten_kota'] = $this->db->query("select * from kabkota where kode_provinsi='$id_prov' and kode_kabkot='$id_kabkot'")->row()->nama;
			$data['provinsi'] = $this->db->query("select * from provinsi where kode_provinsi='$id_prov'")->row()->nama_provinsi;
			$data['lembaga']  = $this->db->query("select * from kelembagaan where id_prov='$id_prov' and id_kabkot='$id_kabkot'")->row();
		}
		
		
		// PRINT EXCEL
		// header 
		$reader = IOFactory::createReader('Xls');
		$spreadsheet = $reader->load(FCPATH.'/uploads/template/rekap_pegawai.xlt');
		
		// print_r($data);
		
		$location = strtoupper($data['provinsi']) . ' ' . strtoupper($data['kabupaten_kota']);
		
		$jml_non_pns_polpp = $data['jml_non_pns_polpp']->jml;
		$jml_non_pns_damkar = $data['jml_non_pns_damkar']->jml;
		$jml_satlinmas = $data['jml_satlinmas']->jml;
		$total_npns = $jml_non_pns_polpp + $jml_non_pns_damkar + $jml_satlinmas;
		
		
		$spreadsheet->getActiveSheet()->setCellValue('A2', $location);
		
		// Jumlah PNS Satuan Polisi Pamong Praja
		$spreadsheet->getActiveSheet()->setCellValue('C4', ': '.$data['jml_pns_satpol_pp']->jml.' Orang');
		$spreadsheet->getActiveSheet()->setCellValue('C5', ': '.$data['jml_fungsional']->jml.' Orang');
		$spreadsheet->getActiveSheet()->setCellValue('C6', ': '.$data['jml_jfu']->jml.' Orang');
		$spreadsheet->getActiveSheet()->setCellValue('C7', ': '.$data['jml_struktural']->jml.' Orang');
		$spreadsheet->getActiveSheet()->setCellValue('C8', ': '.$data['jml_ppns_satpol_pp']->jml.' Orang');
		$spreadsheet->getActiveSheet()->setCellValue('C9', ': '.$data['jml_ppns_uk_lainnya']->jml.' Orang');
		$spreadsheet->getActiveSheet()->setCellValue('C10', ': '.$data['jml_pns_damkar']->jml.' Orang');
		
		// Jumlah Pegawai Pol PP, Linmas dan Damkar Non PNS
		$spreadsheet->getActiveSheet()->setCellValue('C12', ': '.$total_npns.' Orang');
		$spreadsheet->getActiveSheet()->setCellValue('C13', ': '.$jml_non_pns_polpp.' Orang');
		$spreadsheet->getActiveSheet()->setCellValue('C14', ': '.$jml_non_pns_damkar.' Orang');
		$spreadsheet->getActiveSheet()->setCellValue('C15', ': '.$jml_satlinmas.' Orang');
		
		// Pendidikan
		$baseRow = 20;
		$row = 0;
		$total = 0;
		foreach($data['ListData2'] as $k => $v){
			$row = $baseRow+$k;
			$spreadsheet->getActiveSheet()->insertNewRowBefore($row, 1);
			
			$spreadsheet->getActiveSheet()->setCellValue('B' . $row, $v['nama'])
				->setCellValue('C' . $row, $v['jml']);
			
			$total = $total + $v['jml'];
		}
		
		$row = $row+1;
		$spreadsheet->getActiveSheet()->setCellValue('B' . $row, 'Jumlah Keseluruhan');
		$spreadsheet->getActiveSheet()->setCellValue('C' . $row, $total);
		
		// Golongan
		$baseRow = $row+4;
		$row = 0;
		$total = 0;
		foreach($data['ListData3'] as $k => $v){
			$row = $baseRow+$k;
			$spreadsheet->getActiveSheet()->insertNewRowBefore($row, 1);
			
			$spreadsheet->getActiveSheet()->setCellValue('B' . $row, $v['nama'])
				->setCellValue('C' . $row, $v['jml']);
			
			$total = $total + $v['jml'];
		}
		
		$row = $row+1;
		$spreadsheet->getActiveSheet()->setCellValue('B' . $row, 'Jumlah Keseluruhan');
		$spreadsheet->getActiveSheet()->setCellValue('C' . $row, $total);
		
		// Diklat
		$baseRow = $row+4;
		$row = 0;
		$total = 0;
		foreach($data['ListData4'] as $k => $v){
			$row = $baseRow+$k;
			$spreadsheet->getActiveSheet()->insertNewRowBefore($row, 1);
			
			$spreadsheet->getActiveSheet()->setCellValue('B' . $row, $v['nama'])
				->setCellValue('C' . $row, $v['jml']);
			
			$total = $total + $v['jml'];
		}
		
		$row = $row+1;
		$spreadsheet->getActiveSheet()->setCellValue('B' . $row, 'Jumlah Keseluruhan');
		$spreadsheet->getActiveSheet()->setCellValue('C' . $row, $total);
		// print_r($data);exit;
		
		if(isset($data['lembaga'])){
			// ttd
			$row = $row+6;
			$spreadsheet->getActiveSheet()->setCellValue('C' . $row, $data['lembaga']->nama_kepala);
			$row++;
			$spreadsheet->getActiveSheet()->setCellValue('C' . $row, $data['lembaga']->nip_kepala);
		}
		
		// print_r();exit;
		
		
		// print_r($data['ListData2']);exit;
		// $row = 0;
		// $no = 0;
		// $baseRow = 10;
		// $location = '';
		// $ka_nama = '';
		// $ka_nip = '';
		
		$writer = new Xlsx($spreadsheet);
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename="rekap_pegawai_'.$data['provinsi'].'_'.$data['kabupaten_kota'].'.xlsx"');
		$writer->save("php://output");
		
	}


}
