<!DOCTYPE html>
<html>

<head>
  <title>Laporan Rekapitulasi Kepegawaian</title>

  <style type="text/css">
    body {
      font-family: Arial, Helvetica, sans-serif;
      font-size: 10px;
      color: #000;
    }

    table tr th {
      font-size: 10px;
      padding: 5px;
    }

    table tr td {
      font-size: 10px;
      padding: 5px;
    }

    .table tr td {
      vertical-align: top;
    }
  </style>
</head>

<body>

  <h2>
    LAPORAN REKAPITULASI KEPEGAWAIAN<br>
    <!-- SATUAN POLISI PAMONG PRAJA<br> -->
    <?php echo strtoupper($nama_provinsi); ?> <?php echo strtoupper($nama_kabkot); ?>
  </h2>

  <hr>

  <table width="100%" class="table table-hover">
    <tr>
      <td colspan="2">I. Jumlah PNS Satuan Polisi Pamong Praja</td>
      <td width="21%">: <?php echo isset($jml_pns_satpol_pp->jml) ? $jml_pns_satpol_pp->jml : 0; ?> Orang</td>
    </tr>
    <tr>
      <td width="25%">&nbsp;</td>
      <td width="25%">- Fungsional (JFT) Pol PP</td>
      <td>: <?php echo isset($jml_fungsional->jml) ? $jml_fungsional->jml : 0; ?> Orang</td>
    </tr>
    <tr>
      <td width="25%">&nbsp;</td>
      <td width="25%">- JFU Pol PP</td>
      <td>: <?php echo isset($jml_jfu->jml) ? $jml_jfu->jml : 0; ?> Orang</td>
    </tr>
    <tr>
      <td width="34%">&nbsp;</td>
      <td width="45%">- Struktural Pol PP</td>
      <td>: <?php echo isset($jml_struktural->jml) ? $jml_struktural->jml : 0; ?> Orang</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>A. PPNS Satuan Polisi Pamong Praja</td>
      <td>: <?php echo isset($jml_ppns_satpol_pp->jml) ? $jml_ppns_satpol_pp->jml : 0; ?> Orang</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>B. PPNS Unit Kerja Lainnya</td>
      <td>: <?php echo isset($jml_ppns_uk_lainnya->jml) ? $jml_ppns_uk_lainnya->jml : 0; ?> Orang</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>C. Anggota Damkar</td>
      <td>: <?php echo isset($jml_pns_damkar->jml) ? $jml_pns_damkar->jml : 0; ?> Orang</td>
    </tr>
    <tr>
      <td colspan="2">II. Jumlah Pegawai Pol PP, Linmas dan Damkar Non PNS</td>
      <td>: <?php
            $np1 = isset($jml_non_pns_polpp->jml) ? $jml_non_pns_polpp->jml : 0;
            $np2 = isset($jml_non_pns_damkar->jml) ? $jml_non_pns_damkar->jml : 0;
            $np3 = isset($jml_satlinmas->jml) ? $jml_satlinmas->jml : 0;
            echo $np1 + $np2 + $np3;
            ?> Orang</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>- Anggota Pol PP Non PNS</td>
      <td>: <?php echo isset($jml_non_pns_polpp->jml) ? $jml_non_pns_polpp->jml : 0; ?> Orang</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>- Anggota Damkar Non PNS</td>
      <td>: <?php echo isset($jml_non_pns_damkar->jml) ? $jml_non_pns_damkar->jml : 0; ?> Orang</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>- Anggota Linmas di Kelurahan/Desa</td>
      <td>: <?php echo isset($jml_satlinmas->jml) ? $jml_satlinmas->jml : 0; ?> Orang</td>
    </tr>
  </table>


  <h3> III. Rincian Pegawai Satuan Polisi Pamong Praja</h3>
  <h3> a) Tingkat Pendidikan</h3>
  <table width="100%" border="1" cellpadding="0" cellspacing="0" widtd="100%">
    <tr>
      <td width="80%">PENDIDIKAN</td>
      <td width="20%">JUMLAH</td>
    </tr>
    <?php $gt_pendidikan = 0;
    if (count($ListData2) > 0) {
      foreach ($ListData2 as $row) {
        $gt_pendidikan += $row['jml'];
    ?>
        <tr style="font-size:11.5px">
          <td><?php echo $row['nama']; ?> </td>
          <td>
            <?php echo $row['jml']; ?>&nbsp;</td>
        </tr>
    <?php
      }
    } else {
      echo "Data Tidak Tersedia";
    }
    ?><tr style="font-size:11.5px">
      <td>JUMLAH KESELURUHAN</td>
      <td><?php echo $gt_pendidikan; ?></td>
    </tr>
  </table>

  <h3> b) Kepangkatan/Golongan</h3>
  <table width="100%" border="1" cellpadding="0" cellspacing="0" widtd="100%">
    <tr>
      <td width="80%">GOLONGAN</td>
      <td width="20%">JUMLAH</td>
    </tr>
    <?php $gt_gol = 0;
    if (count($ListData3) > 0) {
      foreach ($ListData3 as $row) {
        $gt_gol += $row['jml'];
    ?>
        <tr style="font-size:11.5px">
          <td><?php echo $row['nama']; ?> </td>
          <td>
            <?php echo $row['jml']; ?>&nbsp;</td>
        </tr>
    <?php
      }
    } else {
      echo "Data Tidak Tersedia";
    }
    ?><tr style="font-size:11.5px">
      <td>JUMLAH KESELURUHAN</td>
      <td><?php echo $gt_gol; ?></td>
    </tr>
  </table>

  <h3> c) Jenis Kediklatan</h3>
  <table width="100%" border="1" cellpadding="0" cellspacing="0" widtd="100%">
    <tr>
      <td width="80%">DIKLAT</td>
      <td width="20%">JUMLAH</td>
    </tr>
    <?php $gt_diklat = 0;
    if (count($ListData4) > 0) {
      foreach ($ListData4 as $row) {
        $gt_diklat += $row['jml'];
    ?>
        <tr style="font-size:11.5px">
          <td><?php echo $row['nama']; ?> </td>
          <td>
            <?php echo $row['jml']; ?>&nbsp;</td>
        </tr>
    <?php
      }
    } else {
      echo "Data Tidak Tersedia";
    }
    ?><tr style="font-size:11.5px">
      <td>JUMLAH KESELURUHAN</td>
      <td><?php echo $gt_diklat; ?></td>
    </tr>
  </table>

  <br><br>

  <div class="signature" style="margin-left: 450px">
    KEPALA SATPOL PP PROV/KAB/KOTA
    <br>
    <br>
    <br>
    <br>
    <br>
    <u>
      <?php echo (isset($lembaga->nama_kepala) ? '<strong>' . $lembaga->nama_kepala . '</strong>' :
        ' &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      '); ?>
    </u><br>
    NIP. <?php echo (isset($lembaga->nip_kepala) ? $lembaga->nip_kepala : ' '); ?>
  </div>

</body>

</html>