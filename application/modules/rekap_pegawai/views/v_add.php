<div class="container-fluid">
    <br>
    <div class="breadcrumbs">
        <ul>
            <?php foreach ($breadcrumbs as $key => $value) { ?>
                <li>
                    <a href=<?php echo site_url($value[ 'link'])?> >
				<?php echo $value['name']; ?></a>
                    <?php echo (count($breadcrumbs)-1)==$key?"":"<i class='icon-angle-right'></i>"; ?>
                </li>
                <?php } ?>
        </ul>
        <div class="close-bread">
            <a href="#"><i class="icon-remove"></i></a>
        </div>
    </div>
</div>



<div class="row-fluid">
    <div class="span8 offset2">
    	  <?php 
				if ($this->session->flashdata('message_gagal')) {
					echo '<hr><div class="alert alert-error"><button class="close" data-dismiss="alert" type="button">&times;</button>'.$this->session->flashdata('message_gagal').'</div>';
				}					
				if ($this->session->flashdata('message_sukses')) {
					echo '<hr><div class="alert alert-success"><button class="close" data-dismiss="alert" type="button">&times;</button>'.$this->session->flashdata('message_sukses').'</div>';
				}?>

        <div class="box box-bordered box-color">
            <div class="box-title">
                <h3><i class=" icon-user"></i><?php echo $judul_form." ".$sub_judul_form;?> </h3>
            </div>
            <div class="box-content nopadding">
                <!-- <form action="#" method="POST" class='form-horizontal form-validate' id="bb"> -->
                <?php echo form_open('ref_pegawai/tambih_robih',array('name'=>'bb', 'id'=>'bb','class'=>'form-horizontal form-bordered form-validate form-wysiwyg','enctype'=>'multipart/form-data'));?>

                  
                        <input type="hidden" name="id_pegawai" id="id_pegawai" value="<?php echo isset($field->id)?$field->id:'';?>">
                        <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" style="display: none">

                        <input type="hidden" value="<?php echo $this->input->ip_address(); ?>" name="ip_address">

						<div class="control-group">
													<label class="control-label" for="textfield">Photo</label>
														<div class="controls">
															

															<?php if(isset($field->id)){ ?>
																<img src="<?php echo base_url(); ?>/<?php echo isset($field->foto)?$field->foto: '';?>" width="200">
															<?php } ?>
														</div>
													</div>
						
						
                        <div class="control-group">
                            <label for="textfield" class="control-label">NIP/NO.ANGGOTA</label>
                            <div class="controls">
                               <?php echo isset($field->nip)?$field->nip: $this->input->post("nip");?>
                         
                            </div>
                        </div>
                       

                        <div class="control-group">
                            <label for="textfield" class="control-label">Nama Lengkap</label>
                            <div class="controls">
                                <?php echo isset($field->nama_lengkap)?$field->nama_lengkap:'';?>
                            </div>
                        </div>


												<div class="control-group">
													<label for="textfield" class="control-label">Tempat Lahir</label>
													<div class="controls">
													
														<?php echo isset($field->tempat_lahir)?$field->tempat_lahir:'';?>
													</div>
												</div>

												<div class="control-group">
														<label for="textfield" class="control-label">Tanggal Lahir</label>
														<div class="controls">
														<?php echo isset($field->tgl_lahir)?$field->tgl_lahir:'';?>
															
														</div>
													</div>


												<div class="control-group">
													<label for="textfield" class="control-label">Jenis Kelamin</label>
													<div class="controls">
													<?php echo isset($field->jenis_kelamin)?$field->jenis_kelamin:'';?>
													
														
													</div>
												</div>


												<div class="control-group">
													<label for="textfield" class="control-label">Alamat</label>
													<div class="controls">
													<?php echo isset($field->alamat)?$field->alamat:'';?>
														
													</div>
												</div>


												<div class="control-group">
													<label for="textfield" class="control-label">Email</label>
													<div class="controls">
													<?php echo isset($field->email)?$field->email:'';?>
														
													</div>
												</div>
												<div class="control-group">
													<label for="textfield" class="control-label">No Hand Phone</label>
													<div class="controls">
														
														<?php echo isset($field->hp)?$field->hp:'';?>
														
													</div>
												</div>


												<div class="control-group">
													<label for="textfield" class="control-label">Status Pegawai</label>
													<div class="controls">
														<?php echo isset($field->status_peg)?$field->status_peg:'';?>
													</div>
												</div>

												<div class="control-group">
													<label for="textfield" class="control-label">Golongan</label>
													<div class="controls">
														<?php echo isset($field->kode_golongan)?$field->kode_golongan:'';?>&nbsp;
														<?php echo isset($field->nama_gol)?$field->nama_gol:'';?>
													</div>
												</div>	

												<div class="control-group">
													<label for="textfield" class="control-label">Pendidikan</label>
													<div class="controls">
														<?php echo isset($field->nama_pendidikan)?$field->nama_pendidikan:'';?>
													</div>
												</div>	

												<div class="control-group">
													<label for="textfield" class="control-label">Diklat</label>
													<div class="controls">
														<?php echo isset($field->nama_diklat)?$field->nama_diklat:'';?>
													</div>
												</div>

												<div class="control-group">
													<label for="textfield" class="control-label">Jabatan</label>
													<div class="controls">
													<?php echo isset($field->jabatan)?$field->jabatan:'';?>
														
													</div>
												</div>

												<div class="control-group">
													<label for="textfield" class="control-label">Unit Kerja</label>
													<div class="controls">
														<?php echo isset($field->unitkerja)?$field->unitkerja:'';?>
													</div>
												</div>



													<div class="control-group">
														<label for="textfield" class="control-label">Status Aktif</label>
														<div class="controls">
															<?php echo isset($field->status_aktif)?$field->status_aktif:'';?>
														</div>
													</div>

													




                       

                      

                                <div class="form-actions">
                                   
                                    <a class="btn btn-danger" href="<?php echo site_url();?>lap_rincian_pegawai/">Kembali</a>
                                </div>

                                </form>
            </div>
        </div>
    </div>
</div>
