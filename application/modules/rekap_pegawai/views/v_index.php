<div class="container-fluid">
  <br>
  <div class="breadcrumbs">
    <ul>
      <?php foreach ($breadcrumbs as $key => $value) { ?>
        <li>
          <a href=<?php echo site_url($value['link']) ?>>
            <?php echo $value['name']; ?></a>
          <?php echo (count($breadcrumbs) - 1) == $key ? "" : "<i class='icon-angle-right'></i>"; ?>
        </li>
      <?php } ?>
    </ul>
    <div class="close-bread">
      <a href="#"><i class="icon-remove"></i></a>
    </div>
  </div>
</div>

<div class="row-fluid">
  <div class="span12">
    <div class="box">
      <div class="box-title">
        <h3>
          <i class="icon-reorder"></i>
          <?php echo $sub_judul_form; ?>
        </h3>
      </div>
      <div class="box-content">
        <?php
        if ($this->session->flashdata('message_gagal')) {
          echo '<div class="alert alert-error"><button class="close" data-dismiss="alert" type="button">&times;</button>' . $this->session->flashdata('message_gagal') . '</div>';
        }
        if ($this->session->flashdata('message_sukses')) {
          echo '<hr><div class="alert alert-success"><button class="close" data-dismiss="alert" type="button">&times;</button>' . $this->session->flashdata('message_sukses') . '</div>';
        } ?>
        <form action="<?php echo site_url('rekap_pegawai/index'); ?>" method="post" name="form1" class="form-horizontal form-bordered">
          <input type="hidden" name="<?= $this->security->get_csrf_token_name(); ?>" value="<?= $this->security->get_csrf_hash(); ?>" style="display: none">

          <?php require_once(APPPATH . '/modules/form/views/v_form.php'); ?>

          <div class="control-group">
            <label class="control-label" for="textfield">&nbsp;</label>
            <div class="controls">
              <input type="submit" name="submit" value="Cari" class="btn btn-blue">
              <a class="btn btn-red" href="<?php echo site_url(); ?>rekap_pegawai/clear_session"><i class="icon-trash"></i> Hapus Pencarian</a>
			  <!--
              <a target="_blank" class="btn btn-green" href="<?php echo site_url("rekap_pegawai/index/cetak "); ?>"><i class="icon-print"></i> Cetak</a>
			  -->
	<div class="btn-group">
      <a class="btn dropdown-toggle btn-success" data-toggle="dropdown" href="#">
				<i class="icon-print"></i> 
        Cetak Laporan
        <span class="caret"></span>
      </a>
      <ul class="dropdown-menu">
        <li><a href="<?php echo site_url("rekap_pegawai/index/print"); ?>">PDF</a></li>
        <li><a href="<?php echo site_url("rekap_pegawai/export"); ?>">Excel</a></li>
      </ul>
    </div>
            </div>
          </div>

          <br>

          <p>
            <b>CATATAN :</b>
          </p>
          <ul>
            <li>Data yang ditampilkan dibawah ini adalah data pegawai yang <i>Status Aktif</i> saja.</li>
            <li>Jika ada ketidaksesuaian jumlah data, mohon dapat cek & update data pegawai masing-masing kembali di Menu <u>Manajemen Data</u> -> <u>Kepegawaian</u>.</li>
            <li>Jumlah PNS Fungsional (JFT) Pol PP = Pegawai dengan Jenis Jabatan JFT dan Tingkat Jabatan <i>*wajib diisi</i> (Ahli / Terampil).</li>
            <li>Jumlah PNS Struktural Pol PP = Pegawai dengan Jenis Jabatan Struktural dan Tingkat Jabatan <i>*wajib diisi</i> (Eselon II / III / IV).</li>
            <li>Untuk mempermudah pencarian data pegawai Status <i>(Aktif/Tidak Aktif)</i>, Jabatan <i>(Fungsional/Struktural)</i> yang belum sesuai dapat melakukan pencarian detail di Menu <u>Laporan</u> -> <u>Laporan Kepegawaian</u> -> <u>Rincian PNS Satpol PP</u>.</li>
          </ul>

          <br>

          <div class="table-responsive">
            <table width="100%" class="table table-hover">
              <tr>
                <td colspan="2">I. Jumlah PNS Satuan Polisi Pamong Praja</td>
                <td width="48%">: <?php echo isset($jml_pns_satpol_pp->jml) ? $jml_pns_satpol_pp->jml : 0; ?> Orang</td>
              </tr>
              <tr>
                <td width="25%">&nbsp;</td>
                <td width="25%">- Fungsional (JFT) Pol PP</td>
                <td>: <?php echo isset($jml_fungsional->jml) ? $jml_fungsional->jml : 0; ?> Orang</td>
              </tr>
              <tr>
                <td width="25%">&nbsp;</td>
                <td width="25%">- JFU Pol PP</td>
                <td>: <?php echo isset($jml_jfu->jml) ? $jml_jfu->jml : 0; ?> Orang</td>
              </tr>
              <tr>
                <td width="25%">&nbsp;</td>
                <td width="25%">- Struktural Pol PP</td>
                <td>: <?php echo isset($jml_struktural->jml) ? $jml_struktural->jml : 0; ?> Orang</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>A. PPNS Satuan Polisi Pamong Praja</td>
                <td>: <?php echo isset($jml_ppns_satpol_pp->jml) ? $jml_ppns_satpol_pp->jml : 0; ?> Orang</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>B. PPNS Unit Kerja Lainnya</td>
                <td>: <?php echo isset($jml_ppns_uk_lainnya->jml) ? $jml_ppns_uk_lainnya->jml : 0; ?> Orang</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>C. Anggota Damkar</td>
                <td>: <?php echo isset($jml_pns_damkar->jml) ? $jml_pns_damkar->jml : 0; ?> Orang</td>
              </tr>
              <tr>
                <td colspan="2">II. Jumlah Pegawai Pol PP, Linmas dan Damkar Non PNS</td>
                <td>: <?php
                      $np1 = isset($jml_non_pns_polpp->jml) ? $jml_non_pns_polpp->jml : 0;
                      $np2 = isset($jml_non_pns_damkar->jml) ? $jml_non_pns_damkar->jml : 0;
                      $np3 = isset($jml_satlinmas->jml) ? $jml_satlinmas->jml : 0;
                      echo $np1 + $np2 + $np3;
                      ?> Orang</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>- Anggota Pol PP Non PNS</td>
                <td>: <?php echo isset($jml_non_pns_polpp->jml) ? $jml_non_pns_polpp->jml : 0; ?> Orang</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>- Anggota Damkar Non PNS</td>
                <td>: <?php echo isset($jml_non_pns_damkar->jml) ? $jml_non_pns_damkar->jml : 0; ?> Orang</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>- Anggota Linmas di Kelurahan/Desa</td>
                <td>: <?php echo isset($jml_satlinmas->jml) ? $jml_satlinmas->jml : 0; ?> Orang</td>
              </tr>
            </table>
            <table width="100%" class="table table-hover">
              <thead>
                <tr>
                  <th>III. Rincian Pegawai Satuan Polisi Pamong Praja</th>
                  <th>&nbsp;</th>
                </tr>
                <tr>
                  <th colspan="2">a) Tingkat Pendidikan</th>
                </tr>
                <tr>
                  <th>PENDIDIKAN</th>
                  <th>JUMLAH</th>
                </tr>
              </thead>
              <tbody>
                <?php $gt_pendidikan = 0;
                if (count($ListData2) > 0) {
                  foreach ($ListData2 as $row) {
                    $gt_pendidikan += $row['jml'];
                ?>
                    <tr style="font-size:11.5px">
                      <td><?php echo $row['nama']; ?> </td>
                      <td>
                        <?php echo $row['jml']; ?>&nbsp;</td>
                    </tr>
                <?php
                  }
                } else {
                  echo "Data Tidak Tersedia";
                }
                ?><tr style="font-size:11.5px">
                  <td>JUMLAH KESELURUHAN</td>
                  <td><?php echo $gt_pendidikan; ?></td>
                </tr>
              </tbody>
            </table>

            <table width="100%" class="table table-hover">
              <thead>
                <tr>
                  <th colspan="2">b) Kepangkatan/Golongan</th>
                </tr>
                <tr>
                  <th>GOLONGAN</th>
                  <th>JUMLAH</th>
                </tr>
              </thead>
              <tbody>
                <?php $gt_gol = 0;
                if (count($ListData3) > 0) {
                  foreach ($ListData3 as $row) {
                    $gt_gol += $row['jml'];
                ?>
                    <tr style="font-size:11.5px">
                      <td><?php echo $row['nama']; ?> </td>
                      <td>
                        <?php echo $row['jml']; ?>&nbsp;</td>
                    </tr>
                <?php
                  }
                } else {
                  echo "Data Tidak Tersedia";
                }
                ?><tr style="font-size:11.5px">
                  <td>JUMLAH KESELURUHAN</td>
                  <td><?php echo $gt_gol; ?></td>
                </tr>
              </tbody>
            </table>


            <table width="100%" class="table table-hover">
              <thead>
                <tr>
                  <th colspan="2">c) Jenis Kediklatan</th>
                </tr>
                <tr>
                  <th>DIKLAT</th>
                  <th>JUMLAH</th>
                </tr>
              </thead>
              <tbody>
                <?php $gt_diklat = 0;
                if (count($ListData4) > 0) {
                  foreach ($ListData4 as $row) {
                    $gt_diklat += $row['jml'];
                ?>
                    <tr style="font-size:11.5px">
                      <td><?php echo $row['nama']; ?> </td>
                      <td>
                        <?php echo $row['jml']; ?>&nbsp;</td>
                    </tr>
                <?php
                  }
                } else {
                  echo "Data Tidak Tersedia";
                }
                ?><tr style="font-size:11.5px">
                  <td>JUMLAH KESELURUHAN</td>
                  <td><?php echo $gt_diklat; ?></td>
                </tr>
              </tbody>
            </table>

          </div>

        </form>

      </div>
    </div>
  </div>
</div>