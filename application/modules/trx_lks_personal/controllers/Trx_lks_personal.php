<?php if (!defined('BASEPATH')) exit('No direct script access allowed');



class Trx_lks_personal extends MX_Controller
{
	//put your code here
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('utility');
		// 
		$this->truly_madly_deeply();

		Modules::run('ref_role/permissions');
		// $this->load->model('users_model');

		$this->load->helper('login');
		$this->load->helper('tool');
	}

	public function truly_madly_deeply()
	{

		if (!$this->session->userdata('truly_madly_deeply')) {
			redirect('signon');
		}
	}



	public function index($offset = 0)

	{
		
	



				if (isset($_POST['cari_personal'])) {
					$data1 = array('s_cari_personal' => $_POST['cari_personal']);
					$this->session->set_userdata($data1);
				}

				$sesi_bo_id_lks = $this->session->userdata('sesi_bo_id_lks');
				

				// echo $id_prov;

				$per_page = 10;
				$qry = "
						SELECT 
a.*,
b.nama AS kategori
FROM 
trx_lks_personal a,
ref_kategori b
WHERE
a.id_kategori=b.id
					";

				if ($sesi_bo_id_lks != null) {
					$qry .= " and a.id_lks='$sesi_bo_id_lks' ";
				}

				

				if ($this->session->userdata('s_cari_personal') != "") {
					$qry .= "  and (a.nama like '%" . $this->db->escape_like_str($this->session->userdata('s_cari_personal')) . "%'
							OR a.alamat like '%" . $this->db->escape_like_str($this->session->userdata('s_cari_personal')) . "%'
						 )  ";
				} elseif ($this->session->userdata('s_cari_personal') == "") {
					$this->session->unset_userdata('s_cari_personal');
				}


				$qry .= " ORDER BY id asc";
				//echo $qry;


				$offset = ($this->uri->segment(3) != '' ? $this->uri->segment(3) : 0);
				$config['total_rows'] = $this->db->query($qry)->num_rows();
				$config['per_page'] = $per_page;
				$config['full_tag_open'] = '<div class="table-pagination">';
				$config['full_tag_close'] = '</div>';
				$config['cur_tag_open'] = '<a href="#" class="active"><b>';
				$config['cur_tag_close'] = '</b></a>';
				$config['first_link'] = 'First';
				$config['last_link'] = 'Last';
				$config['next_link'] = 'Next';
				$config['prev_link'] = 'Previous';
				$config['last_tag_open'] = "<span>";
				$config['first_tag_close'] = "</span>";
				$config['uri_segment'] = 3;
				$config['base_url'] = base_url() . '/trx_lks_personal/index';
				$config['suffix'] = '?' . http_build_query($_GET, '', "&");
				$this->pagination->initialize($config);
				$data['paginglinks'] = $this->pagination->create_links();
				$data['per_page'] = $this->uri->segment(3);
				$data['offset'] = $offset;
				if ($data['paginglinks'] != '') {
					$data['pagermessage'] = 'Showing ' . ((($this->pagination->cur_page - 1) * $this->pagination->per_page) + 1) . ' to ' . ($this->pagination->cur_page * $this->pagination->per_page) . ' of ' . $this->db->query($qry)->num_rows();
				}
				$qry .= " limit {$per_page} offset {$offset} ";
				$data['ListData'] = $this->db->query($qry)->result_array();
				$data['role'] = get_role($this->session->userdata('sesi_bo_user_group'));



				$data['breadcrumbs'] = array(
					array(
						'link' => '',
						'name' => 'LKS'
					),
					array(
						'link' => 'trx_lks_personal',
						'name' => 'PENDATAAN - PERSONAL BY NAME BY ADDRESS '
					)
				);



				$data['sub_judul_form'] = "LKS - PENDATAAN PERSONAL BY NAME BY ADDRESS ";
				$this->template->load('template_admin', 'v_index', $data);
		
		
	}

	public function clear_session()
	{



		$this->session->unset_userdata('s_cari_personal');
		redirect('trx_lks_personal');
	}

	public function hupus()
	{
		get_role($this->session->userdata('sesi_bo_user_group'), 'delete');
		// $id=$this->uri->segment(3);

		$id = deCrypt($this->uri->segment(3));
		try {

			$this->db->where('id', $id);
			$this->db->delete('trx_lks_personal');
			redirect('trx_lks_personal');
		} catch (Exception $err) {
			log_message("error", $err->getMessage());
			return show_error($err->getMessage());
		}
	}

	


	
	
	
	
		public function tambih()
	{
		get_role($this->session->userdata('sesi_bo_user_group'), 'insert');
		$data['breadcrumbs'] = array(
			array(
				'link' => 'trx_lks_personal/tambih/#',
				'name' => 'LKS'
			),
			array(
				'link' => 'trx_lks_personal',
				'name' => 'LKS - PERSONAL BY NAME BY ADDRESS '
			),
			array(
				'link' => 'trx_lks_personal/tambih/#',
				'name' => 'Tambah Data'
			)
		);

		$data['judul_form'] = "Tambah Data LKS - PERSONAL BY NAME BY ADDRESS ";
		$data['sub_judul_form'] = "";
		
		$sql_kat = "select * from ref_kategori ";
		$query_kat = $this->db->query($sql_kat);
		$data["list_kategori"] = $query_kat->result();


		$this->template->load('template_admin', 'v_add', $data);
	}

	public function robih($idx)
	{
		$id = deCrypt($idx);

		$data['field'] = $this->db->query("select * from trx_lks_personal where id='$id'")->row();
		
		
		$data['breadcrumbs'] = array(
			array(
				'link' => 'trx_lks_personal/robih/#',
				'name' => 'LKS'
			),
			array(
				'link' => 'trx_lks_personal',
				'name' => 'LKS - PERSONAL BY NAME BY ADDRESS '
			),
			array(
				'link' => 'trx_lks_personal/robih/#',
				'name' => 'Ubah Data'
			)
		);

		$data['judul_form'] = "";
		$data['sub_judul_form'] = "Ubah Data LKS - PERSONAL BY NAME BY ADDRESS ";
		
		$sql_kat = "select * from ref_kategori ";
		$query_kat = $this->db->query($sql_kat);
		$data["list_kategori"] = $query_kat->result();

		$this->template->load('template_admin', 'v_add', $data);
	}

	public function tambih_robih()
	{


		try {
			
			$id  					= $this->input->post('id');
			$data['nama']	= strtoupper($this->input->post('nama'));			
			$data['alamat']	= strtoupper($this->input->post('alamat'));
			$data['sex']		= strtoupper($this->input->post('sex'));
			$data['nik'] = strtoupper($this->input->post('nik'));
			$data['tempat_lahir'] = strtoupper($this->input->post('tempat_lahir'));
			$data['nokk'] 			= strtoupper($this->input->post('nokk'));
			$data['tgl_lahir'] 			= strtoupper($this->input->post('tgl_lahir'));
			$data['tinggal_dimana'] 			= strtoupper($this->input->post('tinggal_dimana'));
			$data['tgl_pendataan'] 			= strtoupper($this->input->post('tgl_pendataan'));
			$data['status_aktif'] 			= strtoupper($this->input->post('status_aktif'));
		$data['id_kategori'] 	=	$this->input->post('id_kategori');
			$data['id_lks'] 			= $this->session->userdata('sesi_bo_id_lks');

			$dir = './uploads/trx_lks_personal/';
			if (!file_exists(FCPATH . $dir)) {
				mkdir(FCPATH . $dir, 0777, true);
			}

			if (isset($_FILES['berkas_peta']) and is_uploaded_file($_FILES['berkas_peta']['tmp_name'])) {
			  $folder = "uploads/trx_lks_personal/";
			  $upload_image = $_FILES['berkas_peta']['name'];
			  // tentukan ukuran width yang diharapkan
			  $width_size = 800;

			  // tentukan di mana image akan ditempatkan setelah diupload
			  $filesave = $folder . $upload_image;
			  move_uploaded_file($_FILES['berkas_peta']['tmp_name'], $filesave);

			  // menentukan nama image setelah dibuat
			  $resize_image = $folder . "resize_map_" . uniqid(rand()) . ".jpg";

			  // mendapatkan ukuran width dan height dari image
			  list($width, $height) = getimagesize($filesave);

			  // mendapatkan nilai pembagi supaya ukuran skala image yang dihasilkan sesuai dengan aslinya
			  $k = $width / $width_size;

			  // menentukan width yang baru
			  $newwidth = $width / $k;

			  // menentukan height yang baru
			  $newheight = $height / $k;

			  // fungsi untuk membuat image yang baru
			  $thumb = imagecreatetruecolor($newwidth, $newheight);
			  $source = imagecreatefromjpeg($filesave);

			  // men-resize image yang baru
			  imagecopyresized($thumb, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);

			  // menyimpan image yang baru
			  imagejpeg($thumb, $resize_image);

			  imagedestroy($thumb);
			  imagedestroy($source);

			  unlink($filesave);

			  $data['foto_peta'] = $resize_image;
			}
			
			
			
			if (isset($_FILES['berkas']) and is_uploaded_file($_FILES['berkas']['tmp_name'])) {
			  $folder = "uploads/trx_lks_personal/";
			  $upload_image = $_FILES['berkas']['name'];
			  // tentukan ukuran width yang diharapkan
			  $width_size = 800;

			  // tentukan di mana image akan ditempatkan setelah diupload
			  $filesave = $folder . $upload_image;
			  move_uploaded_file($_FILES['berkas']['tmp_name'], $filesave);

			  // menentukan nama image setelah dibuat
			  $resize_image = $folder . "resize_location_" . uniqid(rand()) . ".jpg";

			  // mendapatkan ukuran width dan height dari image
			  list($width, $height) = getimagesize($filesave);

			  // mendapatkan nilai pembagi supaya ukuran skala image yang dihasilkan sesuai dengan aslinya
			  $k = $width / $width_size;

			  // menentukan width yang baru
			  $newwidth = $width / $k;

			  // menentukan height yang baru
			  $newheight = $height / $k;

			  // fungsi untuk membuat image yang baru
			  $thumb = imagecreatetruecolor($newwidth, $newheight);
			  $source = imagecreatefromjpeg($filesave);

			  // men-resize image yang baru
			  imagecopyresized($thumb, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);

			  // menyimpan image yang baru
			  imagejpeg($thumb, $resize_image);

			  imagedestroy($thumb);
			  imagedestroy($source);

			  unlink($filesave);

			  $data['foto_lokasi'] = $resize_image;
			}



		


			if ($id == '' || $id == null) {
				get_role($this->session->userdata('sesi_bo_user_group'),'insert');	
				$data['created_by'] = $this->session->userdata('sesi_bo_nama_lengkap');
				$data['created_date'] = date('Y-m-d H:i:s');
				$xss_data = $this->security->xss_clean($data);
				$this->db->insert('trx_lks_personal', $xss_data);
				
				$this->session->set_flashdata('message_sukses', 'Penambahan Data Berhasil Disimpan');

				
			} else {
				
				get_role($this->session->userdata('sesi_bo_user_group'),'update');	
				
				$data['update_by'] = $this->session->userdata('sesi_bo_nama_lengkap');
				$data['updated_date'] = date('Y-m-d H:i:s');
				$xss_data = $this->security->xss_clean($data);

				$this->db->where('id', $id);
				$this->db->update('trx_lks_personal', $xss_data);
				
				$this->session->set_flashdata('message_sukses', 'Perubahan Data Berhasil Disimpan');

			}


			redirect('trx_lks_personal');

		} catch (Exception $err) {
			log_message("error", $err->getMessage());
			return show_error($err->getMessage());
		}
	}
	
	
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
