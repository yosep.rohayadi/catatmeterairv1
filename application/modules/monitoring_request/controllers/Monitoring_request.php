<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Dompdf\Dompdf;

class Monitoring_request extends MX_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('utility');
		$this->truly_madly_deeply();

		Modules::run('ref_role/permissions');
		$this->load->helper('login');
		$this->load->helper('tool');
		$this->load->model('Main_model');
		$this->load->helper('download');
	}


	public function truly_madly_deeply()
	{
		if (!$this->session->userdata('truly_madly_deeply')) {
			redirect('signon');
		}
	}


	public function index()
	{
		$id_provinsi = $this->session->userdata("id_provinsi");
		if ($this->input->post("id_provinsi") != null && $this->input->post("submit") != null) {
			$id_provinsi 	= $this->input->post("id_provinsi");
			$this->session->set_userdata("id_provinsi", $id_provinsi);
		} else if ($this->input->post("id_provinsi") == null && $this->input->post("submit") != null) {
			$id_provinsi = null;
			$this->session->unset_userdata("id_provinsi");
		}

		$id_kabkot = $this->session->userdata("id_kabkot");
		if ($this->input->post("id_kabkot") != null && $this->input->post("submit") != null) {
			$id_kabkot		= $this->input->post("id_kabkot");
			$this->session->set_userdata("id_kabkot", $id_kabkot);
		} else if ($this->input->post("id_kabkot") == null && $this->input->post("submit") != null) {
			$id_kabkot = null;
			$this->session->unset_userdata("id_kabkot");
		}

		$id_kecamatan = $this->session->userdata("id_kecamatan");
		if ($this->input->post("id_kecamatan") != null && $this->input->post("submit") != null) {
			$id_kecamatan	= $this->input->post("id_kecamatan");
			$this->session->set_userdata("id_kecamatan", $id_kecamatan);
		} else if ($this->input->post("id_kecamatan") == null && $this->input->post("submit") != null) {
			$id_kecamatan = null;
			$this->session->unset_userdata("id_kecamatan");
		}

		$data['role'] 			= get_role($this->session->userdata('sesi_bo_user_group'));
		$data['breadcrumbs'] 	= array(
			array(
				'link' => '#',
				'name' => 'Laporan'
			),
			array(
				'link' => 'monitoring_request',
				'name' => 'Monitoring Request Data Login'
			)
		);

		$data['id_provinsi']	= $id_provinsi;
		$data['id_kabkot']		= $id_kabkot;
		$data['id_kecamatan']	= $id_kecamatan;
		$data['title'] 			= 'Monitoring Request Data Login';
		$this->template->load('template_admin', 'v_index', $data);
	}


	public function load_list_data()
	{
		$request = $_GET;
		$start 	 = $request['start'];
		$length  = $request['length'];

		// UNTUK PENCARIAN
		$data_cari = $request['search']['value'];
		$cari_spesifik 	= $this->db->escape($data_cari);
		$cari_like 		= $this->db->escape('%' . $data_cari . '%');
		$this->session->set_userdata('cari_data', $data_cari);

		// UNTUK FILTER
		$id_provinsi	= $this->session->userdata("id_provinsi");
		$id_kabkot		= $this->session->userdata("id_kabkot");
		$id_kecamatan	= $this->session->userdata("id_kecamatan");

		$qury = "SELECT 
					A.*,
					B.nama AS PROVINSI,
					C.nama AS KABKOT
				FROM trx_request_data A 
					LEFT JOIN ref_wil_provinsi B ON A.id_prov = B.id_provinsi
					LEFT JOIN ref_wil_kabkot C ON A.id_prov = C.id_provinsi AND A.id_kabkot = C.id_kabkot
				WHERE 1=1 ";

		if ($id_provinsi != null) {
			$id_provinsi = $this->db->escape($id_provinsi);
			$qury .= " AND A.id_prov = $id_provinsi ";
		}

		if ($id_kabkot != null) {
			$id_kabkot = $this->db->escape($id_kabkot);
			$qury .= "AND  A.id_kabkot = $id_kabkot ";
		}

		if ($data_cari != null) {
			$qury .= " AND ( A.nama_lengkap LIKE $cari_like 
							OR A.email LIKE $cari_like 
							OR A.hp LIKE $cari_like 
							OR B.nama LIKE $cari_like 
							OR C.nama LIKE $cari_like )";
		}


		// TOTAL ROW
		$total_data = $this->db->query($qury)->num_rows();

		// ORDERY BY colum table ('nol' berarti column table yang paling kiri)
		$orderby = $request['order'][0]['dir'];
		if ($request['order'][0]['column'] == 0) {
			$qury .= " ORDER BY A.id_prov ASC";
		}

		// 	LIMIT
		$qury .= " LIMIT $start, $length ";


		$result = $this->db->query($qury)->result_array();
		$arr 	= [];
		foreach ($result as $key => $value) {
			$arr = array_merge($arr, [
				[
					$start + ($key + 1),
					$value['nama_lengkap'] ?? '-',
					$value['email'] ?? '-',
					$value['KABKOT'] ?? '-',
					$value['PROVINSI'] ?? '-',
					$value['id_user_group'] == 10 ? 'AKUN PROVINSI' : 'AKUN KABKOT',
					$value['excell_daerah'] != null ? "<a target='_blank' href='" . base_url('monitoring_request/download_data_daerah/' . enCrypt($value['excell_daerah'])) . "' > Download Data Daerah </a>" : '-',
					'<center>' . (($value['status_kirim_email'] == 'Y') ? 'TELAH DI PROSES' : 'SIAP DI REQUEST ULANG') . '</center>',
					'<a href="' . base_url('monitoring_request/reset_request/' . $value['id']) . '" onclick="return confirm(`Anda yakin?`)" >RESET REQUEST</a>'
				]
			]);
		}

		$data = [
			'draw' 				=> $request['draw'],
			"recordsTotal"		=> $total_data,
			"recordsFiltered"	=> $total_data,
			"data"				=> $arr,
		];
		echo json_encode($data);
	}


	public function reset_filter()
	{
		$this->session->unset_userdata(['id_provinsi', 'id_kabkot', 'id_kecamatan', 'periode']);
		return redirect(base_url('monitoring_request'));
	}


	public function reset_request($id)
	{
		$this->db->where('id', $id);
		$this->db->set('status_kirim_email', 'N');
		$this->db->update('trx_request_data');

		return redirect(base_url('monitoring_request'));
	}


	public function download_data_daerah($url)
	{
		return force_download(deCrypt($url), NULL);
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
