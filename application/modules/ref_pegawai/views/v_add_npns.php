<div class="container-fluid">
  <br>
  <div class="breadcrumbs">
    <ul>
      <?php foreach ($breadcrumbs as $key => $value) { ?>
        <li>
          <a href=<?php echo site_url($value['link']) ?>>
            <?php echo $value['name']; ?></a>
          <?php echo (count($breadcrumbs) - 1) == $key ? "" : "<i class='icon-angle-right'></i>"; ?>
        </li>
      <?php } ?>
    </ul>
    <div class="close-bread">
      <a href="#"><i class="icon-remove"></i></a>
    </div>
  </div>
</div>

<div class="row-fluid">
  <div class="span8 offset2">
    <?php
    if ($this->session->flashdata('message_gagal')) {
      echo '<hr><div class="alert alert-error"><button class="close" data-dismiss="alert" type="button">&times;</button>' . $this->session->flashdata('message_gagal') . '</div>';
    }
    if ($this->session->flashdata('message_sukses')) {
      echo '<hr><div class="alert alert-success"><button class="close" data-dismiss="alert" type="button">&times;</button>' . $this->session->flashdata('message_sukses') . '</div>';
    } ?>

    <div class="box box-bordered box-color">
      <div class="box-title">
        <h3><i class=" icon-plus-sign"></i><?php echo $judul_form . " " . $sub_judul_form; ?> </h3>
      </div>

      <div class="box-content nopadding">
        <!-- <form action="#" method="POST" class='form-horizontal form-validate' id="bb"> -->
        <?php echo form_open('ref_pegawai/tambih_robih_non', array('name' => 'bb', 'id' => 'bb', 'class' => 'form-horizontal form-bordered form-validate form-wysiwyg', 'enctype' => 'multipart/form-data')); ?>

        <input type="hidden" name="<?= $this->security->get_csrf_token_name(); ?>" value="<?= $this->security->get_csrf_hash(); ?>" >

        <input type="hidden" value="<?php echo $this->input->ip_address(); ?>" name="ip_address">

        <input type="hidden" name="id_pegawai" id="id_pegawai" value="<?php echo isset($field->id) ? $field->id : ''; ?>">

        <?php
        $sesi_bo_id_prov = $this->session->userdata('sesi_bo_id_prov');
        $sesi_bo_id_kabkot = $this->session->userdata('sesi_bo_id_kabkot');
        if ($sesi_bo_id_prov == "" && $sesi_bo_id_kabkot == "") {
          require_once(APPPATH . '/modules/form/views/v_form_global.php');
        } else {
          require_once(APPPATH . '/modules/form/views/v_form_text.php');
        }
        ?>

		
		<input type="hidden" name="status_pegawai" value="2">

        <div class="control-group">
          <label for="textfield" class="control-label">Nama Lengkap</label>
          <div class="controls">
            <input type="text" name="nama_pegawai" id="nama_pegawai" class="input-xxlarge" data-rule-required="true" value="<?php echo isset($field->nama_lengkap) ? $field->nama_lengkap : ''; ?>">
          </div>
        </div>

        <div class="control-group">
          <label for="textfield" class="control-label">Tempat Lahir</label>
          <div class="controls">
            <input type="text" data-rule-required="true" data-rule-email="false" placeholder="" id="tempat_lahir" class="input-xlarge" name="tempat_lahir" value="<?php echo isset($field->tempat_lahir) ? $field->tempat_lahir : $this->input->post("tempat_lahir"); ?>">
            <span class="required-server"><?php echo form_error('tempat_lahir', '<p style="color:#F83A18">', '</p>'); ?></span>
          </div>
        </div>

        <div class="control-group">
          <label for="textfield" class="control-label">Tanggal Lahir</label>
          <div class="controls">
            <input type="text" data-rule-required="true" placeholder="" name="tgl_lahir" id="tgl_lahir" class="input-xlarge datepick" data-date-format="dd-mm-yyyy" value="<?php echo isset($field->tgl_lahir) ? $field->tgl_lahir : $this->input->post("tgl_lahir"); ?>">
            <?php if (!empty($tgl_lahir)) {
              echo $tgl_lahir;
            } ?>
            <span class="required-server"><?php echo form_error('tgl_lahir', '<p style="color:#F83A18">', '</p>'); ?></span>
          </div>
        </div>

        <div class="control-group">
          <label for="textfield" class="control-label">Jenis Kelamin</label>
          <div class="controls">
            <select name="jenis_kelamin" id="jenis_kelamin" data-rule-required="true" class="input-xlarge">
              <option value="">-- Pilih Jenis Kelamin --</option>
              <option value="L" <?php if (isset($field->jenis_kelamin) && $field->jenis_kelamin == 'L') {
                                  echo 'selected';
                                } ?>>Laki-laki</option>
              <option value="P" <?php if (isset($field->jenis_kelamin) && $field->jenis_kelamin == 'P') {
                                  echo 'selected';
                                } ?>>Perempuan</option>
            </select>
            <?php if (!empty($jenis_kelamin)) {
              echo $jenis_kelamin;
            } ?>
            <span class="required-server"><?php echo form_error('jenis_kelamin', '<p style="color:#F83A18">', '</p>'); ?></span>
          </div>
        </div>

        <div class="control-group">
          <label for="textfield" class="control-label">Alamat</label>
          <div class="controls">
            <textarea name="alamat" id="alamat" class="input-xxlarge"><?php echo isset($field->alamat) ? $field->alamat : $this->input->post("alamat"); ?></textarea>
            <span class="required-server"><?php echo form_error('alamat', '<p style="color:#F83A18">', '</p>'); ?></span>
          </div>
        </div>

        <div class="control-group">
          <label for="textfield" class="control-label">Email</label>
          <div class="controls">
            <input type="text" data-rule-required="true" data-rule-email="false" placeholder="" id="email" class="input-xlarge" name="email" value="<?php echo isset($field->email) ? $field->email : $this->input->post("email"); ?>">
            <?php if (!empty($email)) {
              echo $email;
            } ?>
            <span class="required-server"><?php echo form_error('email', '<p style="color:#F83A18">', '</p>'); ?></span>
          </div>
        </div>

        <div class="control-group">
          <label for="textfield" class="control-label">No. Handphone</label>
          <div class="controls">
            <input type="text" data-rule-required="" placeholder="" id="hp" class="input-xlarge" name="hp" value="<?php echo isset($field->hp) ? $field->hp : $this->input->post("hp"); ?>">
            <?php if (!empty($hp)) {
              echo $hp;
            } ?>
            <span class="required-server"><?php echo form_error('hp', '<p style="color:#F83A18">', '</p>'); ?></span>
            <span class="required-server">
              <p>* <small>Harus Angka ( Contoh : 08123456782)</small> </p>
            </span>
          </div>
        </div>

     


<div class="control-group">
          
          <label for="textfield" class="control-label"> Jabatan <?php// echo " = " . $id_tktjab; ?></label>
          <div class="controls">
            <div id="div_tingkat_jabatan" >
              <input type="text" name="jabatan" id="jabatan" class="input-xxlarge" data-rule-required="true" value="<?php echo isset($field->jabatan) ? $field->jabatan : ''; ?>">
          
              <span class="required-server"><?php echo form_error('jabatan', '<p style="color:#F83A18">', '</p>'); ?></span>
            </div>
          </div>
        </div>

        

        

        <div class="control-group" id="div_no_sk" >
          <label for="textfield" class="control-label">No. SK</label>
          <div class="controls">
            <input type="text" data-rule-required="true" placeholder="" id="nosk" class="input-xxlarge" name="nosk" value="<?php echo isset($field->nosk) ? $field->nosk : $this->input->post("nosk"); ?>">
            <span class="required-server"><?php echo form_error('nosk', '<p style="color:#F83A18">', '</p>'); ?></span>
          </div>
        </div>

        
      

        

       

        <div class="control-group">
          <label for="textfield" class="control-label">Status Aktif</label>
          <div class="controls">
            <select name="status_aktif" id="status_aktif" data-rule-required="true" class="input-xlarge">
              <option value="">-- Pilih Status --</option>
              <option value="Y" <?php if (isset($field->status_aktif) && $field->status_aktif == 'Y') {
                                  echo 'selected';
                                } ?>>Aktif</option>
              <option value="N" <?php if (isset($field->status_aktif) && $field->status_aktif == 'N') {
                                  echo 'selected';
                                } ?>>Tidak Aktif</option>
            </select>
            <?php if (!empty($status_aktif)) {
              echo $status_aktif;
            } ?>
            <span class="required-server"><?php echo form_error('status_aktif', '<p style="color:#F83A18">', '</p>'); ?></span>
          </div>
        </div>

        <div class="control-group">
          <label class="control-label" for="textfield">Photo</label>
          <div class="controls">
            <input type="file" name="userfile" id="userfile"> File harus bertipe JPG,JPEG
            <?php if (isset($field->foto)) { ?>
              <img src="<?php echo base_url(); ?>/<?php echo isset($field->foto) ? $field->foto : ''; ?>" width="200">
            <?php } ?>
          </div>
        </div>

        <div class="form-actions">
          <button class="btn btn-primary" type="submit">Simpan</button>
          <a class="btn btn-danger" href="<?php echo site_url(); ?>ref_pegawai/">Kembali</a>
        </div>

        </form>
      </div>
    </div>
  </div>
</div>

