<div class="container-fluid">
  <br>
  <div class="breadcrumbs">
    <ul>
      <?php foreach ($breadcrumbs as $key => $value) { ?>
        <li>
          <a href="<?php echo site_url($value['link']) ?>">
            <?php echo $value['name']; ?>
          </a>
          <?php echo (count($breadcrumbs) - 1) == $key ? "" : "<i class='icon-angle-right'></i>"; ?>
        </li>
      <?php } ?>
    </ul>
    <div class="close-bread">
      <a href="#"><i class="icon-remove"></i></a>
    </div>
  </div>
</div>

<div class="row-fluid">
  <div class="span12">
    <div class="box">
      <div class="box-title">
        <h3>
          <i class="icon-reorder"></i>
          <?php echo $sub_judul_form; ?>
        </h3>
      </div>
      <div class="box-content">

        <?php
        if ($this->session->flashdata('message_gagal')) {
          echo '<div class="alert alert-error"><button class="close" data-dismiss="alert" type="button">&times;</button>' . $this->session->flashdata('message_gagal') . '</div>';
        }
        if ($this->session->flashdata('message_sukses')) {
          echo '<hr><div class="alert alert-success"><button class="close" data-dismiss="alert" type="button">&times;</button>' . $this->session->flashdata('message_sukses') . '</div>';
        } ?>

        <form action="<?php echo site_url('ref_pegawai/index'); ?>" method="post" name="form1" class="form-horizontal form-bordered">

          <input type="hidden" name="<?= $this->security->get_csrf_token_name(); ?>" value="<?= $this->security->get_csrf_hash(); ?>" style="display: none">

          <?php if ($role['insert'] == "TRUE") { ?>
            <div align="right">
              <div class="btn-group" align="left">
                <a class="btn dropdown-toggle btn-success" data-toggle="dropdown" href="#">
                  <i class="icon-plus-sign"></i>
                  Tambah Data
                  <span class="caret"></span>
                </a>
                <ul class="dropdown-menu">
                  <li><a href="<?php echo site_url("ref_pegawai/tambih"); ?>"> PNS</a></li>
                  <li><a href="<?php echo site_url("ref_pegawai/tambih_npns"); ?>"> Non PNS</a></li>
                </ul>
              </div>
              <!-- <a class="btn btn-green" href="<?php echo site_url("ref_pegawai/tambih "); ?>"><i class="icon-plus-sign"></i> Tambah Data</a> -->
            </div>

          <?php } ?>

          <div class="control-group">
            <label class="control-label" for="textfield">Pencarian Nama/NIP</label>
            <div class="controls">
              <input type="text" value="<?php echo $this->session->userdata('s_cari_global_pegawai'); ?>" class="form-control" name="cari_global_pegawai" placeholder="Masukan kata kunci... (Nama)">
            </div>
          </div>

          <div class="control-group">
            <label class="control-label" for="textfield">Periode Input</label>
            <div class="controls">
              <input type="text" data-rule-required="true" placeholder="Dari Tanggal" name="tgl_dari" id="tgl_dari" class="input-large datepick" data-date-format="dd-mm-yyyy" value="<?php echo $this->session->userdata('s_tgl_dari_bencana'); ?>">
              s/d
              <input type="text" data-rule-required="true" placeholder="Sampai Tanggal" name="sampai_tgl" id="sampai_tgl" class="input-large datepick" data-date-format="dd-mm-yyyy" value="<?php echo $this->session->userdata('s_tgl_sampai_bencana'); ?>">
            </div>
          </div>

          <div class="control-group">
            <label class="control-label" for="textfield">Urutan</label>
            <div class="controls">
              <select name="urutan" id="urutan" class="input-large">
                <option value="">-- Pilih --</option>
                <?php foreach ($data_urutan as $row) : ?>
                  <option value="<?php echo $row['id']; ?>" <?php if ($this->session->userdata('s_cari_urutan') == $row['id']) {
                                                              echo 'selected';
                                                            } ?>><?php echo $row['nama']; ?></option>

                <?php endforeach ?>
              </select>
            </div>
          </div>

          <div class="control-group">
            <div class="controls">
              <input type="submit" name="submit" value="Cari" class="btn btn-blue">
              <a class="btn btn-red" href="<?php echo site_url(); ?>ref_pegawai/clear_session"><i class="icon-trash"></i> Hapus Pencarian</a>
            </div>
          </div>

          <div class="table-responsive">
            <table width="100%" class="table table-hover">
              <thead>
                <tr>
                  <th>FOTO</th>
                  <th>JABATAN</th>
                  <th>NIP</th>
                  <th>NAMA LENGKAP</th>
                  <th>TEMPAT/TGL LAHIR</th>
                  <th>STATUS PEGAWAI</th>
                  <th>GOLONGAN</th>
                  <th>PENDIDIKAN</th>
                  <th style="text-align: center">Aksi</th>
                </tr>
              </thead>
              <tbody>
                <?php $no = $this->uri->segment('3') + 1;
                if (count($ListData) > 0) {
                  foreach ($ListData as $row) {    ?>
                    <tr>
                      <td><img src="<?php echo base_url(); ?><?php echo $row['foto'] ? $row['foto'] : 'assets_users/img/img_placeholder.png'; ?>" width="80" height="80" /> </td>
                      <td><?php echo $row['jabatan']; ?></td>
                      <td><?php echo $row['nip']; ?></td>
                      <td><?php echo $row['nama_lengkap']; ?></td>
                      <td><?php echo $row['tempat_lahir']; ?><br>
                        <?php echo $row['tgl_lahir']; ?>                      </td>
                      <td><?php echo $row['status_pegawai']; ?> </td>
                      <td><?php echo $row['golruang']; ?> <?php echo $row['golruang_nama']; ?></td>
                      <td><?php echo $row['pendidikan']; ?></td>
                      <td style="text-align: center">
                        <?php if ($role['update'] == "TRUE") { ?>
                          <?php if ($row["id_status_pegawai"] != 1 ) { ?>
                            <a class="btn btn-mini btn-warning " href="<?php echo site_url(); ?>ref_pegawai/robih_npns/<?php echo enCrypt($row['id']); ?>"><i class="icon-pencil"></i>Ubah</a>
                          <?php } else { ?>
                            <a class="btn btn-mini btn-warning " href="<?php echo site_url(); ?>ref_pegawai/robih/<?php echo enCrypt($row['id']); ?>"><i class="icon-pencil"></i>Ubah</a>
                          <?php } ?>
                          <!-- robih_npns -->
                        <?php } ?>
                        <?php if ($role['delete'] == "TRUE") { ?>
                          <a class="btn btn-mini btn-danger" href="<?php echo site_url('ref_pegawai/hupus/' . enCrypt($row['id'])); ?>" onclick="return confirm('Anda Yakin ingin Menghapus?'); "><i class="icon-trash"></i> Hapus</a>
                        <?php } ?>                      </td>
                    </tr>
                <?php
                    $paging = (!empty($pagermessage) ? $pagermessage : '');
                  }
                  echo "<tr><td colspan='13'><div style='background:000;'>$paging &nbsp;" . $this->pagination->create_links() . "</div></td></tr>";
                } else {
                  echo "<tbody><tr><td colspan='13' style='padding:10px; background:#F00; border:none; color:#FFF;'>Data Tidak Tersedia</td></tr></tbody>";
                }
                ?>
              </tbody>
            </table>
          </div>

        </form>

      </div>
    </div>
  </div>
</div>