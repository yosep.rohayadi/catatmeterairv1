<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Ref_pegawai extends MX_Controller
{
  //put your code here
  public function __construct()
  {
    parent::__construct();
    $this->load->helper('utility');
    //
    $this->truly_madly_deeply();

    Modules::run('ref_role/permissions');
    // $this->load->model('users_model');

    $this->load->helper('login');
    $this->load->helper('tool');
  }

  public function truly_madly_deeply()
  {
    if (!$this->session->userdata('truly_madly_deeply')) {
      redirect('signon');
    }
  }


  public function index($offset = 0)
  {
    if (isset($_POST['cari_global_pegawai'])) {
      $data1 = array('s_cari_global_pegawai' => $_POST['cari_global_pegawai']);
      $this->session->set_userdata($data1);
    }
		
    if (isset($_POST['tgl_dari'])) {
      $data1 = array('s_tgl_dari_bencana' => $_POST['tgl_dari']);
      $this->session->set_userdata($data1);
    }

    if (isset($_POST['sampai_tgl'])) {
      $data1 = array('s_tgl_sampai_bencana' => $_POST['sampai_tgl']);
      $this->session->set_userdata($data1);
    }
		
    if (isset($_POST['urutan'])) {
      $data1 = array('s_cari_urutan' => $_POST['urutan']);
      $this->session->set_userdata($data1);
    }

    $id_prov = $this->session->userdata('sesi_bo_id_prov');
    $id_kabkot = $this->session->userdata('sesi_bo_id_kabkot');

    // echo $id_prov;

    $per_page = 20;
    $qry = "SELECT a.*
            FROM
            ref_pegawai a
            
            WHERE a.status_aktif IN ('Y', 'N')
    ";

    if (($id_prov != null  || $id_prov != '') && ($id_kabkot == null || $id_kabkot == '')) {
      $qry .= " and a.id_prov='$id_prov' and (id_kabkot IS NULL OR id_kabkot='')";
    }

    if (($id_kabkot != null  || $id_kabkot != '')) {
      $qry .= " and a.id_prov='$id_prov' and id_kabkot='$id_kabkot' ";
    }

    if ($this->session->userdata('s_cari_global_pegawai') != "") {
      $qry .= "  AND (a.nama_lengkap like '%" . $this->db->escape_like_str($this->session->userdata('s_cari_global_pegawai')) . "%' or
			a.nip = '" . $this->db->escape_like_str($this->session->userdata('s_cari_global_pegawai')) . "'

			)  ";
    } elseif ($this->session->userdata('s_cari_global_pegawai') == "") {
      $this->session->unset_userdata('s_cari_global_pegawai');
    }

    if ($this->session->userdata('s_tgl_dari_bencana') != "" && $this->session->userdata('s_tgl_sampai_bencana')) {
      $tgl_dari = $this->db->escape_like_str($this->session->userdata('s_tgl_dari_bencana'));
      $tgl_sampai = $this->db->escape_like_str($this->session->userdata('s_tgl_sampai_bencana'));
      // $qry .= "  AND a.tgl between '$tgl_dari' and '$tgl_sampai' ";
      $qry .= " AND DATE_FORMAT(a.input_tgl, '%Y-%m-%d') between '".$tgl_dari."' AND '".$tgl_sampai."' ";
    } elseif ($this->session->userdata('s_tgl_dari_bencana') == "") {
      $this->session->unset_userdata('s_tgl_dari_bencanaf');
      $this->session->unset_userdata('s_tgl_sampai_bencana');
    }
		
		if ($this->session->userdata('s_cari_urutan') == 1) {
			$qry .= " ORDER BY a.input_tgl ASC ";
		} else if($this->session->userdata('s_cari_urutan') == 2) {
			$qry .= " ORDER BY a.input_tgl DESC ";
		} else {
			$qry .= " ORDER BY a.id_prov ASC, a.id_kabkot ASC, a.id_golruang DESC ";
		}

		// print_r(array($tgl_dari,$tgl_sampai));exit;
    //echo $qry;

    $offset = ($this->uri->segment(3) != '' ? $this->uri->segment(3) : 0);
    $config['total_rows'] = $this->db->query($qry)->num_rows();
    $config['per_page'] = $per_page;
    $config['full_tag_open'] = '<div class="table-pagination">';
    $config['full_tag_close'] = '</div>';
    $config['cur_tag_open'] = '<a href="#" class="active"><b>';
    $config['cur_tag_close'] = '</b></a>';
    $config['first_link'] = 'First';
    $config['last_link'] = 'Last';
    $config['next_link'] = 'Next';
    $config['prev_link'] = 'Previous';
    $config['last_tag_open'] = "<span>";
    $config['first_tag_close'] = "</span>";
    $config['uri_segment'] = 3;
    $config['base_url'] = base_url() . '/ref_pegawai/index';
    $config['suffix'] = '?' . http_build_query($_GET, '', "&");
    $this->pagination->initialize($config);
    $data['paginglinks'] = $this->pagination->create_links();
    $data['per_page'] = $this->uri->segment(3);
    $data['offset'] = $offset;
    if ($data['paginglinks'] != '') {
      $data['pagermessage'] = 'Showing ' . ((($this->pagination->cur_page - 1) * $this->pagination->per_page) + 1) . ' to ' . ($this->pagination->cur_page * $this->pagination->per_page) . ' of ' . $this->db->query($qry)->num_rows();
    }
    $qry .= " limit {$per_page} offset {$offset} ";
    $data['ListData'] = $this->db->query($qry)->result_array();
    $data['role'] = get_role($this->session->userdata('sesi_bo_user_group'));
		$data['data_urutan'] = array(
				array(
					'id' => 1
					,'nama' => 'Terbaru'
				)
				,array(
					'id' => 2
					,'nama' => 'Terlama'
				)
			);
			
    $data['breadcrumbs'] = array(
      array(
        'link' => '',
        'name' => 'Kepegawaian'
      ),
      array(
        'link' => 'ref_pegawai',
        'name' => 'Data pegawai'
      )
    );

    $data['sub_judul_form'] = "Data Pegawai ";
    $this->template->load('template_admin', 'v_index', $data);
  }


  public function clear_session()
  {
    $this->session->unset_userdata('s_cari_global_pegawai');
    redirect('ref_pegawai');
  }


  public function tambih()
  {
    get_role($this->session->userdata('sesi_bo_user_group'), 'insert');
    $data['breadcrumbs'] = array(
      array(
        'link' => '#',
        'name' => 'Kepegawaian'
      ),
      array(
        'link' => 'ref_pegawai',
        'name' => 'Data Pegawai'
      ),
      array(
        'link' => '#',
        'name' => 'Tambah Data'
      )
    );

    $data['judul_form'] = "Tambah Data";
    $data['sub_judul_form'] = "Pegawai ";
    $data['status_peg'] = $this->db->query('select * from ref_status_pegawai order by id_status_pegawai asc');
    $data['golongan'] = $this->db->query('select * from ref_golongan order by id asc');
    $data['pendidikan'] = $this->db->query('select * from ref_pendidikan order by id asc');
    $data['diklat'] = $this->db->query('select * from ref_diklat order by id_diklat asc');
    $data['jenis_jabatan'] = $this->db->query('select * from ref_jenis_jabatan order by id_jenis_jabatan asc');
    $data['tingkat_jabatan'] = $this->db->query('select * from ref_tingkat_jabatan order by id_tingkat_jabatan asc');
    $data['status_peg_ppns'] = $this->db->query('select * from ref_status_pegawai_ppns order by id_status_pegawai_ppns asc');
    // $data['user_group']=$this->users_model->get_group();
    // $data['dinas']=Modules::run("master/get_dinas");
    $this->template->load('template_admin', 'v_add', $data);
  }


  public function robih()
  {
    $id = deCrypt($this->uri->segment(3));

    // $data['user_group']=$this->users_model->get_group();
    $data['field'] = $this->db->query("select * from ref_pegawai where id='$id'")->row();

    // print_r($data['field']);

    $data['breadcrumbs'] = array(
      array(
        'link' => '#',
        'name' => 'Kepegawaian'
      ),
      array(
        'link' => 'ref_pegawai',
        'name' => 'Data Pegawai'
      ),
      array(
        'link' => '#',
        'name' => 'Ubah Data'
      )
    );

    $data['judul_form'] = "Ubah Data";
    $data['sub_judul_form'] = "Data Pegawai ";
    $data['status_peg'] = $this->db->query('select * from ref_status_pegawai order by id_status_pegawai asc');
    $data['golongan'] = $this->db->query('select * from ref_golongan order by id asc');
    $data['pendidikan'] = $this->db->query('select * from ref_pendidikan order by id asc');
    $data['diklat'] = $this->db->query('select * from ref_diklat order by id_diklat asc');
    $data['jenis_jabatan'] = $this->db->query('select * from ref_jenis_jabatan order by id_jenis_jabatan asc');
    $data['tingkat_jabatan'] = $this->db->query('select * from ref_tingkat_jabatan order by id_tingkat_jabatan asc');
    $data['status_peg_ppns'] = $this->db->query('select * from ref_status_pegawai_ppns order by id_status_pegawai_ppns asc');
   
    // $data['dinas']=Modules::run("master/get_dinas");
    $this->template->load('template_admin', 'v_add', $data);
  }


  public function tambih_robih()
  {
    // echo "<pre>";
    // print_r($_POST);
    // echo "</pre>";

    // die;

    try {
      $id = $this->input->post('id_pegawai');
      $ip = $this->input->post('ip_address');
      $nip = $this->input->post('nip');
      $nama = $this->input->post('nama_pegawai');
      $tempat_lahir = $this->input->post('tempat_lahir');
      $tgl_lahir = $this->input->post('tgl_lahir');
      $jenis_kelamin = $this->input->post('jenis_kelamin');
      $alamat = $this->input->post('alamat');

      $email = $this->input->post('email');
      $hp = $this->input->post('hp');
      $status_pegawai = $this->input->post('status_pegawai');
      $golongan = $this->input->post('golongan');
      $pendidikan = $this->input->post('pendidikan');
      $diklat = $this->input->post('diklat');

      $id_jenis_jabatan = $this->input->post('id_jenis_jabatan');
      $nosk = $this->input->post('nosk');


      $status_aktif = $this->input->post('status_aktif');
	  
	  $id_jabatan = $this->input->post('id_jabatan');


      if ($id == '' || $id == null) {

        //cek username ganda
        get_role($this->session->userdata('sesi_bo_user_group'), 'insert');
        $sql     = "select count(nip) as ceknip from ref_pegawai where nip = '" . $nip . "'";
        $row     = $this->db->query($sql)->row_array();

        if ($row['ceknip'] == 0) {

       
          $data['id_prov'] = $this->input->post('id_provinsi');
		  $data['nama_prov'] = getValue("nama", "ref_wil_provinsi", "id_provinsi='" . $id_provinsi . "'");
          $data['id_kabkot'] = $this->input->post('id_kabkot');
		  $data['id_kec'] = $this->input->post('id_kec');
		  $data['id_kel'] = $this->input->post('id_kel');

          $data['ip'] = $ip;
          $data['nip'] = $nip;
          $data['nama_lengkap'] = $nama;
          $data['tempat_lahir'] = $tempat_lahir;
          $data['tgl_lahir'] = $tgl_lahir;
          $data['jenis_kelamin'] = $jenis_kelamin;
          $data['alamat'] = $alamat;
          $data['email'] = $email;
          $data['hp'] = $hp;

          $data['id_status_pegawai'] = $status_pegawai;
		  $data['status_pegawai'] = getValue("nama", "ref_status_pegawai", "id_status_pegawai='" . $status_pegawai . "'");
          $data['id_golruang'] = $golongan;
		  $data['golruang'] = getValue("id", "ref_golongan", "id='" . $golongan . "'");
		   $data['golruang_nama'] = getValue("nama", "ref_golongan", "id='" . $golongan . "'");
          $data['id_golruang_grup'] = getValue("grup", "ref_golongan", "id='" . $golongan . "'");
          $data['id_pendidikan'] = $pendidikan;
		  $data['pendidikan'] = getValue("nama", "ref_pendidikan", "id='" . $pendidikan . "'");
          $data['id_diklat'] = $diklat;
		   $data['diklat'] = getValue("nama", "ref_diklat", "id_diklat='" . $diklat . "'");

          $data['id_jenis_jabatan'] = $id_jenis_jabatan;
		  $data['jenis_jabatan'] = getValue("nama", "ref_jenis_jabatan", "id_jenis_jabatan='" . $id_jenis_jabatan . "'");
          $data['nosk'] = $nosk;

          
		 $data['id_jabatan'] = $id_jabatan;
		 $data['jabatan'] = getValue("nama", "ref_jabatan_sotk", "id='" . $id_jabatan . "'");
          

          $data['status_aktif'] = $status_aktif;

          $data['input_tgl'] = date('Y-m-d H:i:s');
          $data['input_oleh'] = $this->session->userdata('sesi_bo_nama_lengkap');


          if (isset($_FILES['userfile']) and is_uploaded_file($_FILES['userfile']['tmp_name'])) {

            // HAPUS FOTO & THUMBNAIL APABILA DI DATABSE MEMILIKI DATA FOTO, MAKA HAPUS DI DIREKTORI NYA(unlink)

            if (!empty($FILE_FOTO)) {
              if (file_exists($FILE_FOTO)) unlink($FILE_FOTO);
              if (file_exists($FILE_FOTO . ".thumb.jpg")) unlink($FILE_FOTO . ".thumb.jpg");
            }

            $dir = './uploads/pns/' . $nip . '/';

            if (!file_exists(FCPATH . $dir)) {
              mkdir(FCPATH . $dir, 0777, true);
            }

            // MELAKUKAN DUPLIKASI IMAGE & MENGAMBIL NAMA IMAGE UNTUK KEPENTINGAN DATABASE (FILE_FOTO)
            // $nip = $this->session->userdata("sesi_nip");
            $this->load->library('image_processing');
            $data['foto'] = $this->image_processing->process($dir, 'potrait', '100', '100');
          }
          $xss_data = $this->security->xss_clean($data);
          $this->db->insert('ref_pegawai', $xss_data);
          $this->session->set_flashdata('message_sukses', 'Data Berhasil Disimpan');
          $this->tambih();
        } else {

          $this->session->set_flashdata('message_sukses', 'NIP Yang Anda Masukan Sudah Terdaftar !!!');
          $this->tambih();
        }
      } else {

        $data['id_prov'] = $this->input->post('id_provinsi');
		  $data['nama_prov'] = getValue("nama", "ref_wil_provinsi", "id_provinsi='" . $id_provinsi . "'");
          $data['id_kabkot'] = $this->input->post('id_kabkot');
		  $data['id_kec'] = $this->input->post('id_kec');
		  $data['id_kel'] = $this->input->post('id_kel');

          $data['ip'] = $ip;
          $data['nip'] = $nip;
          $data['nama_lengkap'] = $nama;
          $data['tempat_lahir'] = $tempat_lahir;
          $data['tgl_lahir'] = $tgl_lahir;
          $data['jenis_kelamin'] = $jenis_kelamin;
          $data['alamat'] = $alamat;
          $data['email'] = $email;
          $data['hp'] = $hp;

          $data['id_status_pegawai'] = $status_pegawai;
		  $data['status_pegawai'] = getValue("nama", "ref_status_pegawai", "id_status_pegawai='" . $status_pegawai . "'");
          $data['id_golruang'] = $golongan;
		  $data['golruang'] = getValue("id", "ref_golongan", "id='" . $golongan . "'");
		  $data['golruang_nama'] = getValue("nama", "ref_golongan", "id='" . $golongan . "'");
		  
          $data['id_golruang_grup'] = getValue("grup", "ref_golongan", "id='" . $golongan . "'");
          $data['id_pendidikan'] = $pendidikan;
		  $data['pendidikan'] = getValue("nama", "ref_pendidikan", "id='" . $pendidikan . "'");
          $data['id_diklat'] = $diklat;
		   $data['diklat'] = getValue("nama", "ref_diklat", "id_diklat='" . $diklat . "'");

          $data['id_jenis_jabatan'] = $id_jenis_jabatan;
		  $data['jenis_jabatan'] = getValue("nama", "ref_jenis_jabatan", "id_jenis_jabatan='" . $id_jenis_jabatan . "'");
          $data['nosk'] = $nosk;

          
		 $data['id_jabatan'] = $id_jabatan;
		 $data['jabatan'] = getValue("nama", "ref_jabatan_sotk", "id='" . $id_jabatan . "'");
          

       

        $data['status_aktif'] = $status_aktif;

        $data['update_tgl'] = date('Y-m-d H:i:s');
        $data['update_oleh'] = $this->session->userdata('sesi_bo_nama_lengkap');


        if (isset($_FILES['userfile']) and is_uploaded_file($_FILES['userfile']['tmp_name'])) {

          // HAPUS FOTO & THUMBNAIL APABILA DI DATABSE MEMILIKI DATA FOTO, MAKA HAPUS DI DIREKTORI NYA(unlink)

          if (!empty($FILE_FOTO)) {
            if (file_exists($FILE_FOTO)) unlink($FILE_FOTO);
            if (file_exists($FILE_FOTO . ".thumb.jpg")) unlink($FILE_FOTO . ".thumb.jpg");
          }

          $dir = './uploads/pns/' . $nip . '/';

          if (!file_exists(FCPATH . $dir)) {
            mkdir(FCPATH . $dir, 0777, true);
          }

          // MELAKUKAN DUPLIKASI IMAGE & MENGAMBIL NAMA IMAGE UNTUK KEPENTINGAN DATABASE (FILE_FOTO)
          // $nip = $this->session->userdata("sesi_nip");
          $this->load->library('image_processing');
          $data['foto'] = $this->image_processing->process($dir, 'potrait', '100', '100');
        }
        $xss_data = $this->security->xss_clean($data);
        $this->db->where('id', $id);
        $this->db->update('ref_pegawai', $xss_data);

        $this->session->set_flashdata('message_sukses', 'Perubahan Data Berhasil Disimpan');
        redirect('ref_pegawai');

        $data['judul_form'] = "Ubah Data";
        $data['sub_judul_form'] = "Data Pegawai ";
        $data['field'] = $this->users_model->get_all(array('id' => $id));
        $data['breadcrumbs'] = array(
          array(
            'link' => '',
            'name' => 'Settings'
          ),
          array(
            'link' => 'ref_pegawai',
            'name' => 'Data Pegawai'
          ),
          array(
            'link' => '#',
            'name' => 'Ubah Data'
          )
        );

        $this->template->load('template_admin', 'v_add', $data);
      }
    } catch (Exception $err) {
      log_message("error", $err->getMessage());
      return show_error($err->getMessage());
    }
  }


  public function hupus()
  {
    // $id=$this->uri->segment(3);
    get_role($this->session->userdata('sesi_bo_user_group'), 'delete');

    $id = deCrypt($this->uri->segment(3));
    try {

      $this->db->where('id', $id);
      $this->db->delete('ref_pegawai');
      redirect('ref_pegawai');
    } catch (Exception $err) {
      log_message("error", $err->getMessage());
      return show_error($err->getMessage());
    }
  }

  
  public function tambih_npns()
  {
    get_role($this->session->userdata('sesi_bo_user_group'), 'insert');
    $data['breadcrumbs'] = array(
      array(
        'link' => '#',
        'name' => 'Kepegawaian'
      ),
      array(
        'link' => 'ref_pegawai',
        'name' => 'Data Pegawai'
      ),
      array(
        'link' => '#',
        'name' => 'Tambah Data Pegawai NON PNS'
      )
    );

    $data['judul_form'] = "Tambah Data";
    $data['sub_judul_form'] = "Pegawai NON PNS";
    $data['status_peg'] = $this->db->query('select * from ref_status_pegawai WHERE id_status_pegawai IN (5,6) order by id_status_pegawai ASC');
    $data['golongan'] = $this->db->query('select * from ref_golongan order by id asc');
    $data['pendidikan'] = $this->db->query('select * from ref_pendidikan order by id asc');
    $data['diklat'] = $this->db->query('select * from ref_diklat order by id_diklat asc');
    $data['jenis_jabatan'] = $this->db->query('select * from ref_jenis_jabatan order by id_jenis_jabatan asc');
    $data['tingkat_jabatan'] = $this->db->query('select * from ref_tingkat_jabatan order by id_tingkat_jabatan asc');
    $data['status_peg_ppns'] = $this->db->query('select * from ref_status_pegawai_ppns order by id_status_pegawai_ppns asc');
    // $data['user_group']=$this->users_model->get_group();
    // $data['dinas']=Modules::run("master/get_dinas");
    $this->template->load('template_admin', 'v_add_npns', $data);
  }

  
  public function robih_npns()
  {
    $id = deCrypt($this->uri->segment(3));

    // $data['user_group']=$this->users_model->get_group();
    $data['field'] = $this->db->query("select * from ref_pegawai where id='$id'")->row();

    // print_r($data['field']);

    $data['breadcrumbs'] = array(
      array(
        'link' => '#',
        'name' => 'Kepegawaian'
      ),
      array(
        'link' => 'ref_pegawai',
        'name' => 'Data Pegawai'
      ),
      array(
        'link' => '#',
        'name' => 'Ubah Data Pegawai NON PNS'
      )
    );

    $data['judul_form'] = "Ubah Data";
    $data['sub_judul_form'] = "Pegawai NON PNS";
    $data['status_peg'] = $this->db->query('select * from ref_status_pegawai WHERE id_status_pegawai IN (5,6) order by id_status_pegawai ASC');
    $data['golongan'] = $this->db->query('select * from ref_golongan order by id asc');
    $data['pendidikan'] = $this->db->query('select * from ref_pendidikan order by id asc');
    $data['diklat'] = $this->db->query('select * from ref_diklat order by id_diklat asc');
    $data['jenis_jabatan'] = $this->db->query('select * from ref_jenis_jabatan order by id_jenis_jabatan asc');
    $data['tingkat_jabatan'] = $this->db->query('select * from ref_tingkat_jabatan order by id_tingkat_jabatan asc');
    $data['status_peg_ppns'] = $this->db->query('select * from ref_status_pegawai_ppns order by id_status_pegawai_ppns asc');
   

    // $data['dinas']=Modules::run("master/get_dinas");
    $this->template->load('template_admin', 'v_add_npns', $data);
  }


  
  
  public function tambih_robih_non()
  {
    // echo "<pre>";
    // print_r($_POST);
    // echo "</pre>";

    // die;

    try {
      $id = $this->input->post('id_pegawai');
      $ip = $this->input->post('ip_address');
      $nama = $this->input->post('nama_pegawai');
      $tempat_lahir = $this->input->post('tempat_lahir');
      $tgl_lahir = $this->input->post('tgl_lahir');
      $jenis_kelamin = $this->input->post('jenis_kelamin');
      $alamat = $this->input->post('alamat');

      $email = $this->input->post('email');
      $hp = $this->input->post('hp');
      $status_pegawai = $this->input->post('status_pegawai');
      
      $nosk = $this->input->post('nosk');
	  
	  $jabatan = $this->input->post('jabatan');



      $status_aktif = $this->input->post('status_aktif');
	  
	  
	  $data['id_prov'] = $this->input->post('id_provinsi');
		  $data['nama_prov'] = getValue("nama", "ref_wil_provinsi", "id_provinsi='" . $id_provinsi . "'");
          $data['id_kabkot'] = $this->input->post('id_kabkot');
		  $data['id_kec'] = $this->input->post('id_kec');
		  $data['id_kel'] = $this->input->post('id_kel');

          $data['ip'] = $ip;
          $data['nama_lengkap'] = $nama;
          $data['tempat_lahir'] = $tempat_lahir;
          $data['tgl_lahir'] = $tgl_lahir;
          $data['jenis_kelamin'] = $jenis_kelamin;
          $data['alamat'] = $alamat;
          $data['email'] = $email;
          $data['hp'] = $hp;

          $data['id_status_pegawai'] = $status_pegawai;
		  $data['status_pegawai'] = getValue("nama", "ref_status_pegawai", "id_status_pegawai='" . $status_pegawai . "'");
         
          $data['nosk'] = $nosk;

          
		 $data['jabatan'] = $jabatan;
          

          $data['status_aktif'] = $status_aktif;
	  


      if ($id == '' || $id == null) {

        get_role($this->session->userdata('sesi_bo_user_group'), 'insert');
      
       
          

          $data['input_tgl'] = date('Y-m-d H:i:s');
          $data['input_oleh'] = $this->session->userdata('sesi_bo_nama_lengkap');


          if (isset($_FILES['userfile']) and is_uploaded_file($_FILES['userfile']['tmp_name'])) {

            // HAPUS FOTO & THUMBNAIL APABILA DI DATABSE MEMILIKI DATA FOTO, MAKA HAPUS DI DIREKTORI NYA(unlink)

            if (!empty($FILE_FOTO)) {
              if (file_exists($FILE_FOTO)) unlink($FILE_FOTO);
              if (file_exists($FILE_FOTO . ".thumb.jpg")) unlink($FILE_FOTO . ".thumb.jpg");
            }

            $dir = './uploads/nonpns/';

            if (!file_exists(FCPATH . $dir)) {
              mkdir(FCPATH . $dir, 0777, true);
            }

            // MELAKUKAN DUPLIKASI IMAGE & MENGAMBIL NAMA IMAGE UNTUK KEPENTINGAN DATABASE (FILE_FOTO)
            // $nip = $this->session->userdata("sesi_nip");
            $this->load->library('image_processing');
            $data['foto'] = $this->image_processing->process($dir, 'potrait', '100', '100');
          }
          $xss_data = $this->security->xss_clean($data);
          $this->db->insert('ref_pegawai', $xss_data);
          $this->session->set_flashdata('message_sukses', 'Data Berhasil Disimpan');
          $this->tambih();

      } else {

        
        $data['update_tgl'] = date('Y-m-d H:i:s');
        $data['update_oleh'] = $this->session->userdata('sesi_bo_nama_lengkap');


        if (isset($_FILES['userfile']) and is_uploaded_file($_FILES['userfile']['tmp_name'])) {

          // HAPUS FOTO & THUMBNAIL APABILA DI DATABSE MEMILIKI DATA FOTO, MAKA HAPUS DI DIREKTORI NYA(unlink)

          if (!empty($FILE_FOTO)) {
            if (file_exists($FILE_FOTO)) unlink($FILE_FOTO);
            if (file_exists($FILE_FOTO . ".thumb.jpg")) unlink($FILE_FOTO . ".thumb.jpg");
          }

          $dir = './uploads/nonpns';

          if (!file_exists(FCPATH . $dir)) {
            mkdir(FCPATH . $dir, 0777, true);
          }

          // MELAKUKAN DUPLIKASI IMAGE & MENGAMBIL NAMA IMAGE UNTUK KEPENTINGAN DATABASE (FILE_FOTO)
          // $nip = $this->session->userdata("sesi_nip");
          $this->load->library('image_processing');
          $data['foto'] = $this->image_processing->process($dir, 'potrait', '100', '100');
        }
        $xss_data = $this->security->xss_clean($data);
        $this->db->where('id', $id);
        $this->db->update('ref_pegawai', $xss_data);

        $this->session->set_flashdata('message_sukses', 'Perubahan Data Berhasil Disimpan');
        redirect('ref_pegawai');

        $data['judul_form'] = "Ubah Data";
        $data['sub_judul_form'] = "Data Pegawai ";
        $data['field'] = $this->users_model->get_all(array('id' => $id));
        $data['breadcrumbs'] = array(
          array(
            'link' => '',
            'name' => 'Settings'
          ),
          array(
            'link' => 'ref_pegawai',
            'name' => 'Data Pegawai'
          ),
          array(
            'link' => '#',
            'name' => 'Ubah Data'
          )
        );

        $this->template->load('template_admin', 'v_add', $data);
      }
    } catch (Exception $err) {
      log_message("error", $err->getMessage());
      return show_error($err->getMessage());
    }
  }
  
  

}
