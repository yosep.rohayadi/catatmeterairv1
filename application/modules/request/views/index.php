<div class="container-fluid">
	<div class="row-fluid">
		<div class="span12" style="margin-top:180px;">
			<form novalidate="novalidate" class="form-horizontal form-validate" id="bb" name="bb" accept-charset="utf-8" enctype="multipart/form-data" method="post" action="<?php echo base_url("request/index") ?>">
				<?php if ($pesan != '') { ?>
					<div class="alert alert-danger"><?php echo $pesan; ?></div>
				<?php } ?>

				<?php if (($pesan_success ?? '') != '') { ?>
					<div class="alert alert-success"><?php echo $pesan_success; ?></div>
				<?php } ?>

				<h4>Form Request Data Akun Daerah</h4>
				<div style="color: red; padding-bottom: 20px">
					Pastikan data yang anda masukan sesuai. Karena request data hanya bisa dilakukan <b>sekali saja</b> tiap akun daerah.
				</div>

				<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">

				<div class="control-group">
					<label for="textfield" class="control-label">NIP <span style="color: red">*</span></label>
					<div class="controls">
						<input type="text" data-rule-required="true" <?php echo $nip ?? '' ?> placeholder="" id="nip" class="input-xxlarge" name="nip" value="<?php echo $nip ?? '' ?>">
						<span class="required-server"><?php echo form_error('nip', '<p style="color:#F83A18">', '</p>'); ?></span>
					</div>
				</div>

				<div class="control-group">
					<label for="textfield" class="control-label">Nama Lengkap <span style="color: red">*</span></label>
					<div class="controls">
						<input type="text" data-rule-required="true" value="<?php echo $nama_lengkap ?? '' ?>" placeholder="" id="nama_lengkap" class="input-xxlarge" name="nama_lengkap">
						<span class="required-server"><?php echo form_error('nama_lengkap', '<p style="color:#F83A18">', '</p>'); ?></span>
					</div>
				</div>

				<div class="control-group">
					<label for="textfield" class="control-label">Jabatan <span style="color: red">*</span></label>
					<div class="controls">
						<input type="text" data-rule-required="true" value="<?php echo $jabatan ?? '' ?>" placeholder="" id="jabatan" class="input-xxlarge" name="jabatan">
						<span class="required-server"><?php echo form_error('jabatan', '<p style="color:#F83A18">', '</p>'); ?></span>
					</div>
				</div>

				<div class="control-group">
					<label for="textfield" class="control-label">Pangkat <span style="color: red">*</span></label>
					<div class="controls">
						<input type="text" data-rule-required="true" value="<?php echo $pangkat ?? '' ?>" placeholder="" id="pangkat" class="input-xxlarge" name="pangkat">
						<span class="required-server"><?php echo form_error('pangkat', '<p style="color:#F83A18">', '</p>'); ?></span>
					</div>
				</div>

				<div class="control-group">
					<label for="textfield" class="control-label">Email <span style="color: red">*</span></label>
					<div class="controls">
						<input type="email" data-rule-required="true" value="<?php echo $email ?? '' ?>" placeholder="" id="email" class="input-xxlarge" name="email">
						<span class="required-server"><?php echo form_error('email', '<p style="color:#F83A18">', '</p>'); ?></span>
					</div>
				</div>

				<div class="control-group">
					<label for="textfield" class="control-label">Nomor HP <span style="color: red">*</span></label>
					<div class="controls">
						<input type="text" data-rule-required="true" value="<?php echo $hp ?? '' ?>" placeholder="" id="hp" class="input-xxlarge" name="hp">
						<span class="required-server"><?php echo form_error('hp', '<p style="color:#F83A18">', '</p>'); ?></span>
					</div>
				</div>

				<div class="control-group">
					<label for="textfield" class="control-label">Instansi <span style="color: red">*</span></label>
					<div class="controls">
						<select class="input-xxlarge select2" name="id_user_group" onchange="instansi(this.value)" required>
							<?php $iug = ($id_user_group ?? ''); ?>
							<option value="">PILIH</option>
							<option value="10" <?php echo $iug == 10 ? 'selected' : '' ?>>Provinsi</option>
							<option value="5" <?php echo $iug == 5 ? 'selected' : '' ?>>Kapubaten / Kota</option>
						</select>
						<span class="required-server"><?php echo form_error('id_user_group', '<p style="color:#F83A18">', '</p>'); ?></span>
					</div>
				</div>

				<div class="control-group">
					<label for="textfield" class="control-label">Provinsi <span style="color: red">*</span></label>
					<div class="controls">
						<select class="input-xxlarge select2" onchange="load_kabkot(this.value, '')" name="id_prov" required>
							<option value="">PILIH</option>
							<?php foreach ($listProvinsi as $key => $value) { ?>
								<?php $s = ($id_prov ?? '') == $value['id_provinsi'] ? 'selected' : '' ?>
								<option value="<?php echo $value['id_provinsi'] ?>" <?php echo $s ?>>
									<?php echo $value['nama'] ?>
								</option>
							<?php } ?>
						</select>
						<span class="required-server"><?php echo form_error('id_prov', '<p style="color:#F83A18">', '</p>'); ?></span>
					</div>
				</div>

				<div class="control-group" id="box_kabkot" style="display: <?php echo ($id_kabkot ?? '') == '' ? 'none' : 'block' ?>">
					<label for="textfield" class="control-label">Kabupaten / Kota <span style="color: red">*</span></label>
					<div class="controls">
						<select class="input-xxlarge select2" id="id_kabkot" name="id_kabkot">
							<option value="">PILIH</option>
						</select>
						<span class="required-server"><?php echo form_error('id_kabkot', '<p style="color:#F83A18">', '</p>'); ?></span>
					</div>
				</div>

				<div class="control-group">
					<label for="textfield" class="control-label">
						Upload File Data Daerah <span style="color: red">*</span>
					</label>
					<div class="controls">
						<div style="margin-bottom: 10px">
							Upload data daerah menggunakan excell, untuk format excellnya gunakan excell berikut.
							<br>
							<a target="_blank" href="<?php echo base_url("request/download_template"); ?>">Download Format Excell Pengisian Data Daerah.</a>
						</div>
						<input type="file" class="input-xlarge" name="excell_daerah" required>
						<span class="required-server"><?php echo form_error('excell_daerah', '<p style="color:#F83A18">', '</p>'); ?></span>
						<div style="color: red;">
							<small>Hanya upload file excell (.xlsx) maksimal ukuran 5MB.</small>
						</div>
					</div>
				</div>

				<div class="control-group">
					<label for="password" class="control-label">Masukan Kode Keamanan <span style="color: red">*</span></label>
					<div class="controls">
						<input type="text" data-rule-maxlength="6" data-rule-required="true" data-rule-number="true" autocomplete="off" name="userCaptcha" class="input-small" placeholder="" />
						<?php echo $captcha['image']; ?>
						<span class="required-server"><?php echo form_error('userCaptcha', '<p style="color:#F83A18">', '</p>'); ?></span>
					</div>
				</div>
				<button onclick="return confirm('Anda yakin data yang anda masukan telah sesuai?')" name="submitlogin" type="submit" class="btn btn-primary"><i class="icon-check"></i>&nbsp;KIRIM</button>
			</form>
		</div>
	</div>
</div>


<script>
	if ('<?php echo $id_kabkot ?? '' ?>' != '') {
		load_kabkot('<?php echo $id_prov ?? '' ?>', '<?php echo $id_kabkot ?? '' ?>');
	}

	function instansi(id_user_group) {
		if (id_user_group == 10) {
			$('#box_kabkot').css('display', 'none');
			//$('#id_kabkot').attr('required', false);
			$('#id_kabkot').val('');
		} else {
			$('#box_kabkot').css('display', 'block');
			//$('#id_kabkot').attr('required', true);
		}
	}

	function load_kabkot(id_provinsi, id_kabkot) {
		$.ajax({
			url: '<?php echo base_url('form/list_kabkot') ?>',
			type: 'post',
			data: {
				'id_provinsi': id_provinsi,
				'id_kabkot': id_kabkot,
			},
			success: function(response) {
				$('#id_kabkot').html(response);
			},
			error: function(response) {
				alert(response.error);
			}
		});
	}
</script>