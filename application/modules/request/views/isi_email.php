<html>

<head>
</head>

<body>
    <h4>Gunakan data berikut untuk login ke simpel.kemendagri.go.id</h4>
    <table>
        <tr>
            <td>Nama Lengkap</td>
            <td>:</td>
            <td><b><?php echo $user['nama_lengkap'] ?? '' ?></b></td>
        </tr>
        <tr>
            <td>username</td>
            <td>:</td>
            <td><b><?php echo $user['username'] ?? '' ?></b></td>
        </tr>
        <tr>
            <td>password</td>
            <td>:</td>
            <td><b>123456</b></td>
        </tr>
    </table>
    <br>
    <div style="color: red">
        Setelah login, harap segera ganti password anda di menu <b>Edit Password</b> yang telah disediakan di halaman depan.
    </div>
    <?php if ($user['id_user_group'] == 5) { ?>
        <br>
        <div>
            Untuk mengunduh data login akun kelurahan se kabupaten / kota wilayah <?php echo  $nama_kabkot ?? '' ?>.
            <b><a href="<?php echo base_url('request/kelurahan/' . enCrypt($user['id'])) ?>">Unduh datanya disini</a></b>.
        </div>
    <?php } ?>

    <br>
    <br>
    <div>
        Harap jangan hapus email ini. Simpan data diatas untuk login ke simpel.kemendagri.go.id . Email ini
        <b>hanya sekali kirim</b>. Anda tidak bisa melakukan request data login untuk yang kedua kalinya.
    </div>
</body>

</html>