<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_user extends CI_Model
{

	public function submit_request($data)
	{
		$last_id = null;
		if ($data['id_user_group'] == 10) {
			$exist = $this->isExistProvinsi(10, $data['id_prov'], 'N');
			if ($exist != null) {
				$this->db->where('id', $exist['id']);
				$this->db->set($this->security->xss_clean($data));
				$this->db->set('updated_at', date('Y-m-d H:i:s'));
				$this->db->update('trx_request_data');
				$last_id = $exist['id'];
			} else {
				$this->db->set($this->security->xss_clean($data));
				$this->db->set('created_at', date('Y-m-d H:i:s'));
				$this->db->insert('trx_request_data');
				$last_id = $this->db->insert_id();
			}
		} else if ($data['id_user_group'] == 5) {
			$exist = $this->isExistKabkot(5, $data['id_prov'], $data['id_kabkot'], 'N');
			if ($exist != null) {
				$this->db->where('id', $exist['id']);
				$this->db->set($this->security->xss_clean($data));
				$this->db->set('updated_at', date('Y-m-d H:i:s'));
				$this->db->update('trx_request_data');
				$last_id = $exist['id'];
			} else {
				$this->db->set($this->security->xss_clean($data));
				$this->db->set('created_at', date('Y-m-d H:i:s'));
				$this->db->insert('trx_request_data');
				$last_id = $this->db->insert_id();
			}
		}
		return $last_id;
	}


	public function isExistProvinsi($id_user_group, $id_prov, $status = 'Y')
	{
		$this->db->select(['id']);
		$this->db->where('id_user_group', $id_user_group);
		$this->db->where('id_prov', $id_prov);
		$this->db->where('status_kirim_email', $status);
		return $this->db->get("trx_request_data")->row_array();
	}


	public function isExistKabkot($id_user_group, $id_prov, $id_kabkot, $status = 'Y')
	{
		$this->db->select(['id']);
		$this->db->where('id_user_group', $id_user_group);
		$this->db->where('id_prov', $id_prov);
		$this->db->where('id_kabkot', $id_kabkot);
		$this->db->where('status_kirim_email', $status);
		return $this->db->get("trx_request_data")->row_array();
	}


	public function userProvinsi($id_prov)
	{
		$this->db->where('id_user_group', 10);
		$this->db->where('id_prov', $id_prov);
		return $this->db->get("ref_users")->row_array();
	}


	public function userKabkot($id_prov, $id_kabkot)
	{
		$this->db->where('id_user_group', 5);
		$this->db->where('id_prov', $id_prov);
		$this->db->where('id_kabkot', $id_kabkot);
		return $this->db->get("ref_users")->row_array();
	}


	public function listUserKelurahan($id_prov, $id_kabkot)
	{
		$this->db->where('id_user_group', 4);
		$this->db->where('id_prov', $id_prov);
		$this->db->where('id_kabkot', $id_kabkot);
		return $this->db->get("ref_users")->result_array();
	}


	public function listProvinsi()
	{
		return $this->db->get("ref_wil_provinsi")->result_array();
	}


	public function listKabkot($id_provinsi)
	{
		$this->db->where('id_provinsi', $id_provinsi);
		return $this->db->get("ref_wil_kabkot")->result_array();
	}


	public function user($id)
	{
		$this->db->where('id', $id);
		return $this->db->get("ref_users")->row_array();
	}


	public function nama_kabkot($id_prov, $id_kabkot)
	{
		$this->db->select(['nama']);
		$this->db->where('id_provinsi', $id_prov);
		$this->db->where('id_kabkot', $id_kabkot);
		$result = $this->db->get("ref_wil_kabkot")->row_array();
		if ($result != null) {
			return $result['nama'];
		}
		return '';
	}
}

/* End of file M_user.php */
/* Location: ./application/models/M_user.php */
