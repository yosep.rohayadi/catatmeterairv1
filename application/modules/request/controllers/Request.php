<?php

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Request extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('captcha');
		$this->load->model('M_user');
		$this->load->helper('login');
		$this->load->helper('log');
		$this->load->helper('tool');
		$this->load->helper('download');
	}


	public function index()
	{
		$this->form_validation->set_rules('nip', 'NIP', 'required');
		$this->form_validation->set_rules('nama_lengkap', 'Nama Lengkap', 'required');
		$this->form_validation->set_rules('jabatan', 'Jabatan', 'required');
		$this->form_validation->set_rules('pangkat', 'Pangkat', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required');
		$this->form_validation->set_rules('hp', 'HP', 'required');
		$this->form_validation->set_rules('id_user_group', 'Instansi', 'required');
		$this->form_validation->set_rules('id_prov', 'Provinsi', 'required');
		if ($this->input->post('id_user_group') == 5) {
			$this->form_validation->set_rules('id_kabkot', 'Kabupaten / Kota', 'required');
		}

		$this->form_validation->set_rules('userCaptcha', 'Captcha', 'required|callback_check_captcha');

		$dataPost['nip']			= $this->input->post('nip');
		$dataPost['nama_lengkap']	= $this->input->post('nama_lengkap');
		$dataPost['jabatan']		= $this->input->post('jabatan');
		$dataPost['pangkat']		= $this->input->post('pangkat');
		$dataPost['email']			= $this->input->post('email');
		$dataPost['hp']				= $this->input->post('hp');
		$dataPost['id_user_group']	= $this->input->post('id_user_group');
		$dataPost['id_prov']		= $this->input->post('id_prov');
		$dataPost['id_kabkot']		= $this->input->post('id_kabkot');
		$data['listProvinsi'] 		= $this->M_user->listProvinsi();
		$data['pesan'] 				= '';

		if ($this->form_validation->run() == false) {
			$data['captcha'] = $this->buat_captcha();
			$this->session->set_userdata('captchaWordAdmin', $data['captcha']['word']);
			$this->template->load('template_login', 'index', array_merge($data, $dataPost));
		} else {

			$id_user_group 	= $dataPost['id_user_group'];
			$id_prov 		= $dataPost['id_prov'];
			$id_kabkot 		= $dataPost['id_kabkot'];

			// Upload
			$dir = './uploads/data_daerah/';
			if (!file_exists(FCPATH . $dir)) {
				mkdir(FCPATH . $dir, 0777, true);
			}

			if (isset($_FILES['excell_daerah'])) {
				if ($_FILES['excell_daerah']['name'] != '') {
					$config['upload_path']          = $dir;
					$config['allowed_types']        = 'xlsx';
					$config['overwrite']            = true;
					$config['max_size']             = 5024; // 5MB
					$config['encrypt_name']         = TRUE;
					$this->load->library('upload', $config);

					// jika upload success
					if ($this->upload->do_upload('excell_daerah')) {

						$full_path = $dir . "" . $this->upload->data("file_name");
						$dataPost['excell_daerah'] = substr($full_path, 2, strlen($full_path));

						// Jika yang request provinsi
						if ($dataPost['id_user_group'] == 10) {
							if ($this->M_user->isExistProvinsi($id_user_group, $id_prov) == null) {

								// USER
								$user = $this->M_user->userProvinsi($id_prov);

								if ($user != null) {

									$last_id = $this->M_user->submit_request($this->security->xss_clean($dataPost));

									$this->update_ref_users([
										'id_user_group'			=> $id_user_group,
										'id_prov'				=> $id_prov,
										'nip'					=> $dataPost['nip'],
										'nama_lengkap'			=> $dataPost['nama_lengkap'],
										'jabatan'				=> $dataPost['jabatan'],
										'pangkat'				=> $dataPost['pangkat'],
										'email'					=> $dataPost['email'],
										'hp'					=> $dataPost['hp'],
									]);


									// KIRIM EMAIL
									$user = $this->M_user->user($user['id']);
									$status = $this->kirim_email($dataPost['email'], $user);
									if ($status) {
										$data['pesan_success'] = "Permohonan berhasil diproses.";
										$this->db->where('id', $last_id);
										$this->db->set('status_kirim_email', 'Y');
										$this->db->update('trx_request_data');
									} else {
										$data['pesan'] = "Sistem tidak dapat memproses email, silahkan coba lagi.";
									}
								} else {
									$data['pesan'] = "User tidak ditemukan, silahkan cobalagi.";
								}
							} else {
								$data['pesan'] = "Permohonan tidak dapat diproses. Data dengan pemohon ini telah dikirim sebelumnya.";
							}

							// Jika yang request kabkot
						} else if ($dataPost['id_user_group'] == 5) {
							if ($this->M_user->isExistKabkot($id_user_group, $id_prov, $id_kabkot) == null) {

								// DATA KAKBOT
								$user = $this->M_user->userKabkot($id_prov, $id_kabkot);

								if ($user != null) {

									// SIMPAN PEMOHON
									$last_id = $this->M_user->submit_request($this->security->xss_clean($dataPost));

									$this->update_ref_users([
										'id_user_group'			=> $id_user_group,
										'id_prov'				=> $user['id_prov'],
										'id_kabkot'				=> $user['id_kabkot'],
										'nip'					=> $dataPost['nip'],
										'nama_lengkap'			=> $dataPost['nama_lengkap'],
										'jabatan'				=> $dataPost['jabatan'],
										'pangkat'				=> $dataPost['pangkat'],
										'email'					=> $dataPost['email'],
										'hp'					=> $dataPost['hp'],
									]);

									// KIRIM EMAIL
									$user = $this->M_user->user($user['id']);
									$status = $this->kirim_email($dataPost['email'], $user);
									if ($status) {
										$data['pesan_success'] = "Permohonan berhasil di proses.";
										$this->db->where('id', $last_id);
										$this->db->set('status_kirim_email', 'Y');
										$this->db->update('trx_request_data');
									} else {
										$data['pesan'] = "Sistem tidak dapat memproses email, silahkan coba lagi.";
									}
								} else {
									$data['pesan'] = "Akun tidak ditemukan, silahkan coba lagi.";
								}
							} else {
								$data['pesan'] = "Permohonan tidak dapat diproses. Data dengan pemohon ini telah dikirim sebelumnya.";
							}
						}
					} else {
						$data['pesan'] = $this->upload->display_errors();
					}
				}
			}

			$data['captcha'] = $this->buat_captcha();
			$this->session->set_userdata('captchaWordAdmin', $data['captcha']['word']);

			$this->template->load('template_login', 'index', array_merge($data, $dataPost));
		}
	}


	protected function buat_captcha()
	{
		$random_number = substr(number_format(time() * rand(), 0, '', ''), 0, 4);
		$vals = array(
			'word' => $random_number,
			'img_path' => './captcha/',
			'img_url' => base_url() . 'captcha/',
			'img_width' => 140,
			'img_height' => 32,
			'expiration' => 7200
		);
		return create_captcha($vals);
	}


	protected function kirim_email($email_to, $user)
	{
		$this->load->library('email');
		$this->email->from('akun.simpel@kemendagri.go.id', 'simpel.kemendagri.go.id');
		$this->email->to($email_to);

		//$this->email->attach('https://images.pexels.com/photos/169573/pexels-photo-169573.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940');
		$this->email->subject('Data Login Anda ke simpel.kemendagri.go.id');

		// Isi email
		$data = [
			'user'			=> $user,
			'nama_kabkot'	=> $this->M_user->nama_kabkot($user['id_prov'], $user['id_kabkot']),
		];
		$isi_email = $this->load->view('isi_email', $data, true);
		$this->email->message($isi_email);

		if ($this->email->send()) {
			return true;
		}
		// echo "<pre>";
		// print_r($this->email);
		return false;
	}


	protected function update_ref_users($data)
	{
		$this->db->where('id_user_group', $data['id_user_group']);
		$this->db->where('id_prov', $data['id_prov']);
		if (isset($data['id_kabkot'])) {
			$this->db->where('id_kabkot', $data['id_kabkot']);
		}
		$this->db->set($data);
		$this->db->set('password', password_hash("123456", PASSWORD_DEFAULT, ['cost' => 10]));
		$this->db->update('ref_users');
	}


	public function check_captcha($str)
	{
		$word = $this->session->userdata('captchaWordAdmin');
		if (strcmp(strtoupper($str), strtoupper($word)) == 0) {
			return true;
		} else {
			$this->form_validation->set_message('check_captcha', 'KODE CAPTCHA SALAH !');
			return false;
		}
	}


	public function kelurahan($_id_kabkot)
	{
		$user = $this->M_user->user(deCrypt($_id_kabkot));

		$spreadsheet 	= new Spreadsheet();
		$sheet 			= $spreadsheet->getActiveSheet();
		$nama_excell 	= "Data Login Akun Seluruh Kelurahan di " . $this->M_user->nama_kabkot($user['id_prov'], $user['id_kabkot']);


		$sheet->setCellValue('A1', 'NAMA KELURAHAN');
		$sheet->setCellValue('B1', 'USERNAME');
		$sheet->setCellValue('C1', 'PASSWORD');

		$data = $this->M_user->listUserKelurahan($user['id_prov'], $user['id_kabkot']);
		if (count($data) > 0) {
			foreach ($data as $key => $value) {

				$password = substr(number_format(time() * rand(), 0, '', ''), 0, 6);

				$sheet->setCellValue('A' . ($key + 2), $value['nama_lengkap']);
				$sheet->setCellValue('B' . ($key + 2), $value['username']);
				$sheet->setCellValue('C' . ($key + 2), $password);

				// UPDATE PASSWORD REF_USERS
				$this->db->where('id', $value['id']);
				$this->db->set('password', password_hash($password, PASSWORD_DEFAULT, ['cost' => 10]));
				$this->db->update('ref_users');
			}
		}

		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="' . $nama_excell . ' .xlsx"');
		header('Cache-Control: max-age=0');

		$writer = new Xlsx($spreadsheet);
		$writer->save('php://output');
	}


	public function download_template()
	{
		return force_download("./uploads/data_daerah/format_data_camat_dan_lurah.xlsx", NULL);
	}
}
