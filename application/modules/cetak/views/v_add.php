



<div class="row-fluid">
	
	<div class="span1">&nbsp;</div>

    <div class="span10">
        <?php
        if ($this->session->flashdata('message_gagal')) {
            echo '<hr><div class="alert alert-error"><button class="close" data-dismiss="alert" type="button">&times;</button>' . $this->session->flashdata('message_gagal') . '</div>';
        }
        if ($this->session->flashdata('message_sukses')) {
            echo '<hr><div class="alert alert-success"><button class="close" data-dismiss="alert" type="button">&times;</button>' . $this->session->flashdata('message_sukses') . '</div>';
        } ?>

        <div class="box box-bordered box-color">
            <div class="box-title">
                <h3><i class=" icon-plus-sign"></i><?php echo $judul_form . " " . $sub_judul_form; ?> </h3>
            </div>
            <div class="box-content nopadding">
                <!-- <form action="#" method="POST" class='form-horizontal form-validate' id="bb"> -->
                <?php echo form_open('trx_pendataan/tambih_robih', array('name' => 'bb', 'id' => 'bb', 'class' => 'form-horizontal form-bordered form-validate form-wysiwyg', 'enctype' => 'multipart/form-data')); ?>


                <input type="hidden" name="id" id="id" value="<?php echo isset($field->id) ? $field->id : ''; ?>">
                <input type="hidden" name="<?= $this->security->get_csrf_token_name(); ?>" value="<?= $this->security->get_csrf_hash(); ?>" style="display: none">

                <input type="hidden" value="<?php echo $this->input->ip_address(); ?>" name="ip_address">
				
				<div class="control-group">
                    <label for="textfield" class="control-label">Hari ini</label>
                    <div class="controls">
                        <?php echo date("d-M-Y"); ?>
                    </div>
                </div>
				
				<div class="control-group">
                    <label for="textfield" class="control-label">Pilih Pelanggan</label>
                    <div class="controls">
                        <?php $id_pelanggan = isset($field->id_pelanggan) ? $field->id_pelanggan : $this->input->post("id_pelanggan"); ?>

                        

							<select class="input-xxlarge" name="id_pelanggan" required onchange="doView(this.value)">
							
							<option value="">-Pilih-</option>
							<?php foreach ($list_kategori as $row) { ?>
							<option value="<?php echo $row->id; ?>" <?php if ($id_pelanggan == $row->id) { echo "selected";} ?>><?php echo $row->nama; ?></option>
							<?php } ?>
							</select>
                       

                        



                        <span class="required-server"><?php echo form_error('uraian', '<p style="color:#F83A18">', '</p>'); ?></span>
                    </div>
                </div>
				
				
				<div id="txtHint">
				
				
						
				
				
				</div>

                </form>
            </div>
        </div>
		
		
		<div class="span1">&nbsp;</div>
		
    </div>

<script language="javascript">
				
				function doHitung(m_after) {
					
					
					
					
					var m_before = document.getElementById("m_before").value;
					var harga = document.getElementById("harga").value;
					var abudemen = document.getElementById("abudemen").value;					
					var jml_pakai= m_after-m_before;
					
					if ( m_after < m_before) {
						alert("Jumlah Meter Akhir Yang Anda Masukan Kurang !");
						return false;
					}
					
					var hasil=jml_pakai*harga+10000;
					
					
					document.getElementById("jml_pakai").value=jml_pakai;
					document.getElementById("subtot_pakai").value=hasil;
				}
				
				
				
				function doView(str) {
					
					//alert(id_pelanggan);
					
					if (str.length == 0) {
						document.getElementById("txtHint").innerHTML = "";
						return;
					  } else {
						var xmlhttp = new XMLHttpRequest();
						xmlhttp.onreadystatechange = function() {
						  if (this.readyState == 4 && this.status == 200) {
							document.getElementById("txtHint").innerHTML = this.responseText;
						  } else {
							  document.getElementById("txtHint").innerHTML = "Mohon tunggu .....";
						  }
						};
						xmlhttp.open("GET", "<?php echo site_url();?>trx_pendataan/detail/"+str, true);
						xmlhttp.send();
					  }
					
					
				}
				
				
				</script>