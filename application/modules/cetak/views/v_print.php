
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td colspan="2"><H2>AKUR WATER MANAGEMENT SYSTEM</H2></td>
  </tr>
  <tr>
    <td colspan="2"><H3>BUKTI TRANSAKSI</H3></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>Kode Transaksi</td>
    <td>:<span class="controls"><?php echo isset($field_bio->kode_transaksi) ? $field_bio->kode_transaksi : ''; ?></span></td>
  </tr>
  <tr>
    <td width="13%">Tgl.</td>
    <td width="87%">: <span class="controls"><?php echo date("d-m-Y"); ?></span></td>
  </tr>
  <tr>
    <td>Nama</td>
    <td>: <span class="controls"><?php echo isset($field_bio->nama) ? $field_bio->nama : ''; ?></span></td>
  </tr>
  <tr>
    <td>Alamat</td>
    <td>: <span class="controls"><?php echo isset($field_bio->alamat) ? $field_bio->alamat : ''; ?></span></td>
  </tr>
  <tr>
    <td>Meter Awal</td>
    <td>: <span class="controls"><?php echo isset($field_bio->m_after) ? $field_bio->m_after : ''; ?></span></td>
  </tr>
  <tr>
    <td>Meter Akhir</td>
    <td>: <span class="controls"><?php echo isset($field_bio->m_before) ? $field_bio->m_before : ''; ?></span></td>
  </tr>
  <tr>
    <td>Harga per-m3</td>
    <td>: <span class="controls"><?php echo isset($field_bio->harga) ? $field_bio->harga : ''; ?></span></td>
  </tr>
  <tr>
    <td>Jumlah Pakai</td>
    <td>: <span class="controls"><?php echo isset($field_bio->jml_pakai) ? $field_bio->jml_pakai : ''; ?></span></td>
  </tr>
  <tr>
    <td>Abudemen</td>
    <td>: <span class="controls"><?php echo isset($field_bio->abudemen) ? $field_bio->abudemen : ''; ?></span></td>
  </tr>
  <tr>
    <td>Total Bayar</td>
    <td>: <span class="controls"><?php echo isset($field_bio->subtot_pakai) ? $field_bio->subtot_pakai : ''; ?></span></td>
  </tr>
</table>
<script>
		
		window.print();
		
		</script>