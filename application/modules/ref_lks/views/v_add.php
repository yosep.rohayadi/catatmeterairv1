<div class="container-fluid">
    <br>
    <div class="breadcrumbs">
        <ul>
            <?php foreach ($breadcrumbs as $key => $value) { ?>
                <li>
                    <a href="<?php echo site_url($value['link']) ?>">
                        <?php echo $value['name']; ?></a>
                    <?php echo (count($breadcrumbs) - 1) == $key ? "" : "<i class='icon-angle-right'></i>"; ?>
                </li>
            <?php } ?>
        </ul>
        <div class="close-bread">
            <a href="#"><i class="icon-remove"></i></a>
        </div>
    </div>
</div>



<div class="row-fluid">
	
	<div class="span1">&nbsp;</div>

    <div class="span10">
        <?php
        if ($this->session->flashdata('message_gagal')) {
            echo '<hr><div class="alert alert-error"><button class="close" data-dismiss="alert" type="button">&times;</button>' . $this->session->flashdata('message_gagal') . '</div>';
        }
        if ($this->session->flashdata('message_sukses')) {
            echo '<hr><div class="alert alert-success"><button class="close" data-dismiss="alert" type="button">&times;</button>' . $this->session->flashdata('message_sukses') . '</div>';
        } ?>

        <div class="box box-bordered box-color">
            <div class="box-title">
                <h3><i class=" icon-plus-sign"></i><?php echo $judul_form . " " . $sub_judul_form; ?> </h3>
            </div>
            <div class="box-content nopadding">
                <!-- <form action="#" method="POST" class='form-horizontal form-validate' id="bb"> -->
                <?php echo form_open('ref_lks/tambih_robih', array('name' => 'bb', 'id' => 'bb', 'class' => 'form-horizontal form-bordered form-validate form-wysiwyg', 'enctype' => 'multipart/form-data')); ?>


                <input type="hidden" name="id" id="id" value="<?php echo isset($field->id) ? $field->id : ''; ?>">
                <input type="hidden" name="<?= $this->security->get_csrf_token_name(); ?>" value="<?= $this->security->get_csrf_hash(); ?>" style="display: none">

                <input type="hidden" value="<?php echo $this->input->ip_address(); ?>" name="ip_address">

                
                <div class="control-group">
                    <label for="textfield" class="control-label">Nama</label>
                    <div class="controls">
                        <input type="text"  id="nama" class="input-xxlarge" name="nama" value="<?php echo isset($field->nama) ? $field->nama : $this->input->post("nama"); ?>" style="text-transform:uppercase" required>
                        <span class="required-server"><?php echo form_error('nama', '<p style="color:#F83A18">', '</p>'); ?></span>
                    </div>
                </div>
				
				
				<div class="control-group">
                    <label for="textfield" class="control-label">Alamat</label>
                    <div class="controls">
                        <input type="text" placeholder="" id="alamat" class="input-xxlarge" name="alamat" value="<?php echo isset($field->alamat) ? $field->alamat : $this->input->post("alamat"); ?>" style="text-transform:uppercase" >
                        <span class="required-server"><?php echo form_error('alamat', '<p style="color:#F83A18">', '</p>'); ?></span>
                    </div>
                </div>
				
				<?php 
				if ($this->session->userdata('sesi_bo_user_group')==1 or $this->session->userdata('sesi_bo_user_group')==2) {
					require_once(APPPATH . '/modules/filter_wilayah/views/v_filter_wilayah.php');  
				} else {
					require_once(APPPATH . '/modules/filter_wilayah/views/v_filter_wilayah_text.php');  
				}
				?>
				
				
				<div class="control-group">
										<label class="control-label" for="textfield">No.Telp</label>                                        
                                        <div class="controls">
                                        <input type="number" placeholder="" id="no_telp" class="input-large" name="no_telp" value="<?php echo isset($field->no_telp) ? $field->no_telp : $this->input->post("no_telp"); ?>" style="text-transform:uppercase" required>
										</div>
									</div>
				
				
				
				<div class="control-group">
                    <label for="textfield" class="control-label">Akreditasi </label>
                    <div class="controls">
                       <!-- <textarea class="input-xxlarge ckeditor" id="ckeditor" name="akreditasi"><?php echo isset($field->akreditasi)?$field->akreditasi: $this->input->post("akreditasi");?></textarea> -->
					   
					   <input type="text" placeholder="" id="akreditasi" class="input-xxlarge" name="akreditasi" value="<?php echo isset($field->akreditasi) ? $field->akreditasi : $this->input->post("akreditasi"); ?>" style="text-transform:uppercase" >
                      

                        <span class="required-server"><?php echo form_error('akreditasi', '<p style="color:#F83A18">', '</p>'); ?></span>
                    </div>
                </div>
				
				
				
				<div class="control-group">
                    <label for="textfield" class="control-label">Ketua Yayasan</label>
                    <div class="controls">
					   <input type="text" placeholder="" id="ketua_yayasan" class="input-xxlarge" name="ketua_yayasan" value="<?php echo isset($field->ketua_yayasan) ? $field->ketua_yayasan : $this->input->post("ketua_yayasan"); ?>" style="text-transform:uppercase" >
                        <span class="required-server"><?php echo form_error('ketua_yayasan', '<p style="color:#F83A18">', '</p>'); ?></span>
						
                    </div>
                </div>



                
<div class="control-group">
                    <label for="textfield" class="control-label">No. Surat Terdaftar</label>
                    <div class="controls">
										   <input type="text" placeholder="" id="no_surat_terdaftar" class="input-xxlarge" name="no_surat_terdaftar" value="<?php echo isset($field->no_surat_terdaftar) ? $field->no_surat_terdaftar : $this->input->post("no_surat_terdaftar"); ?>" style="text-transform:uppercase" >

 <span class="required-server"><?php echo form_error('no_surat_terdaftar', '<p style="color:#F83A18">', '</p>'); ?></span>
                    </div>
                </div>


                
                

                
                
				<div class="control-group">
										<label class="control-label" for="textfield">Notaris Pencatat Pendirian</label>                                        
                                        <div class="controls">
                                        <input type="text" placeholder="" id="notaris" class="input-xxlarge" name="notaris" value="<?php echo isset($field->notaris) ? $field->notaris : $this->input->post("notaris"); ?>"  required>
										
										</div>
									</div>
				
                                    
				<div class="control-group">
										<label class="control-label" for="textfield">No.Akta Pendirian</label>                                        
                                        <div class="controls">
                                        <input type="text" placeholder="" id="no_akta_pendirian" class="input-xxlarge" name="no_akta_pendirian" value="<?php echo isset($field->no_akta_pendirian) ? $field->no_akta_pendirian : $this->input->post("no_akta_pendirian"); ?>"  required>
										</div>
				</div>
				
				
				<div class="control-group">
										<label class="control-label" for="textfield">No.Ijin Kemenhumham</label>                                        
                                        <div class="controls">
                                        <input type="text" placeholder="" id="no_ijin_kemenkumham" class="input-xxlarge" name="no_ijin_kemenkumham" value="<?php echo isset($field->no_ijin_kemenkumham) ? $field->no_ijin_kemenkumham : $this->input->post("no_ijin_kemenkumham"); ?>"  required>
										</div>
				</div>
				
				
					
				
				
				
				

                <div class="control-group">
                    <label class="control-label" for="textfield">Photo Lokasi</label>
                    <div class="controls">
                        <!-- <input type="hidden" name="nip" id="nip" class="input-large" value=""> -->
                        <input type="file" name="berkas" id="berkas" accept="image/*">
                        <span class="required-server">
                            <p>*<small>File tipe gambar</small>
							<br/>*<small>Ukuran maksimal 1 MB</small>
							</p>
                        </span>

                        <?php if (isset($field->id)) { ?>
                            <img src="<?php echo base_url(); ?>/<?php echo isset($field->foto_lokasi) ? $field->foto_lokasi : ''; ?>" width="400">
                        <?php } ?>
                    </div>
                </div>








                <div class="form-actions">
                    <button id="btn_simpan" class="btn btn-blue" type="submit">Simpan</button>
                    <a class="btn btn-danger" href="<?php echo site_url(); ?>ref_lks">Kembali</a>
                </div>

                </form>
            </div>
        </div>
		
		
		<div class="span1">&nbsp;</div>
		
    </div>


