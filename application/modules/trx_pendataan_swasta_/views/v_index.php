<div class="container-fluid">
    <br>
    <div class="breadcrumbs">
        <ul>
            <?php foreach ($breadcrumbs as $key => $value) { ?>
                <li>
                    <a href=<?php echo site_url($value['link']) ?>>
                        <?php echo $value['name']; ?></a>
                    <?php echo (count($breadcrumbs) - 1) == $key ? "" : "<i class='icon-angle-right'></i>"; ?>
                </li>
            <?php } ?>
        </ul>
        <div class="close-bread">
            <a href="#"><i class="icon-remove"></i></a>
        </div>
    </div>
</div>

<div class="row-fluid">
    <div class="span12">
        <div class="box">
            <div class="box-title">
                <h3>
                    <i class="icon-reorder"></i>
                    <?php echo $sub_judul_form; ?>
                </h3>
            </div>
            <div class="box-content">
                <?php
                if ($this->session->flashdata('message_gagal')) {
                    echo '<div class="alert alert-error"><button class="close" data-dismiss="alert" type="button">&times;</button>' . $this->session->flashdata('message_gagal') . '</div>';
                }
                if ($this->session->flashdata('message_sukses')) {
                    echo '<hr><div class="alert alert-success"><button class="close" data-dismiss="alert" type="button">&times;</button>' . $this->session->flashdata('message_sukses') . '</div>';
                } ?>
                <form action="<?php echo site_url('trx_pendataan_swasta/index'); ?>" method="post" name="form1" class="form-horizontal form-bordered">
                    <input type="hidden" name="<?= $this->security->get_csrf_token_name(); ?>" value="<?= $this->security->get_csrf_hash(); ?>" style="display: none">

                    <?php if ($role['insert'] == "TRUE") { ?>
                        <div align="right">
                            <a class="btn btn-blue" href="<?php echo site_url("trx_pendataan_swasta/tambih "); ?>"><i class="icon-plus-sign"></i> Tambah Data</a>
                        </div>
                    <?php } ?>
						
						
						<?php 
				/*if ($this->session->userdata('sesi_bo_user_group')==1 or $this->session->userdata('sesi_bo_user_group')==2) {
					require_once(APPPATH . '/modules/filter_wilayah/views/v_filter_wilayah.php');  
				} else {
					require_once(APPPATH . '/modules/filter_wilayah/views/v_filter_wilayah_text.php');  
				}*/
				?>
						
						
						<div class="control-group">
										<label class="control-label" for="textfield">Periode Tgl.Register</label>                                        
                                        <div class="controls">
                                        <input type="date" placeholder="" id="tgl_register" class="input-large" name="tgl_register1" value="<?php echo $this->session->userdata('s_tgl_register1'); ?>"  >
										s/d
										<input type="date" placeholder="" id="tgl_register" class="input-large" name="tgl_register2" value="<?php echo $this->session->userdata('s_tgl_register2'); ?>"  >
										</div>
									</div>
						
						<div class="control-group">
                            <label class="control-label" for="textfield">Kategori PPKS</label>
                            <div class="controls">
                                <?php $id_kategori = $this->session->userdata('s_id_kategori'); ?>
							   <select class="input-xxlarge" name="id_kategori" >
							
							<option value="">-Semua-</option>
							<?php foreach ($list_kategori as $row) { ?>
							<option value="<?php echo $row->id; ?>" <?php if ($id_kategori == $row->id) { echo "selected";} ?>><?php echo $row->nama; ?></option>
							<?php } ?>
							</select>
							   
                            </div>
                        </div>
						
						<div class="control-group">
                            <label class="control-label" for="textfield">Keberadaan saat ini?</label>
                            <div class="controls">
							<?php $status_keberadaan= $this->session->userdata('s_status_keberadaan');?>
                                <select name="status_keberadaan"    onchange="doKeberadaan(this.value);">
										<option value="">-Semua-</option>
                                           <option value="MASYARAKAT" <?php if ($status_keberadaan=="MASYARAKAT") { echo "selected";}?>>MASYARAKAT</option>
                                          <option value="LKS"  <?php if ($status_keberadaan=="LKS") { echo "selected";}?>>LKS</option>
                                        </select>
                            </div>
                        </div>
						
						<div class="control-group">
                            <label class="control-label" for="textfield">Sudah Mendapatkan Bantuan?</label>
                            <div class="controls">
                                <?php $status_bantuan= $this->session->userdata('s_status_bantuan');?> 
								<select name="status_bantuan"    onchange="doBantuan(this.value);">
										<option value="">-Semua-</option>
                                           <option value="YA" <?php if ($status_bantuan=="YA") { echo "selected";}?>>YA</option>
                                          <option value="TIDAK"  <?php if ($status_bantuan=="TIDAK") { echo "selected";}?>>TIDAK</option>
                                        </select>
								
                            </div>
                        </div>
						
						
						
						<div class="control-group" id="v_butuh_panti">
											<label class="control-label" for="textfield">Butuh Panti ?</label>                                        
											<div class="controls">
											<?php $status_panti= $this->session->userdata('s_status_panti');?> 
											<select name="status_panti" >
										<option value="">-Semua-</option>
                                           <option value="YA" <?php if ($status_panti=="YA") { echo "selected";}?>>YA</option>
                                          <option value="TIDAK"  <?php if ($status_panti=="TIDAK") { echo "selected";}?>>TIDAK</option>
                                        </select>
											
											</div>
					</div>
						
						
						<div class="control-group">
                            <label class="control-label" for="textfield">Status Validasi</label>
                            <div class="controls">
                                <?php $approved= $this->session->userdata('s_approved');?> 
								<select name="approved" >
										<option value="">-Semua-</option>
                                           <option value="Approved" <?php if ($approved=="Approved") { echo "selected";}?>>Approved</option>
                                          <option value="Not Approved"  <?php if ($approved=="Not Approved") { echo "selected";}?>>Not Approved</option> <option value="Belum Di-Validasi" <?php if ($approved=="Belum Di-Validasi") { echo "selected";}?>>Belum Di-Validasi</option>
                                        </select>
								
                            </div>
                        </div>
						
						
              
                        <div class="control-group">
                            <label class="control-label" for="textfield">Pencarian kata kunci</label>
                            <div class="controls">
                                <input type="text" value="<?php echo $this->session->userdata('s_cari_personal'); ?>" class="input-xlarge" name="cari_personal" placeholder="Kode Register/NIK/KK/Nama/Alamat/Jenis Kelamin">
                                <input type="submit" name="submit" value="Cari" class="btn btn-blue">
								 <a target="_blank" class="btn btn-green" href="<?php echo site_url(); ?>trx_pendataan_swasta/export"><i class="icon-print"></i> Export Excel</a>
                                <a class="btn btn-red" href="<?php echo site_url(); ?>trx_pendataan_swasta/clear_session"><i class="icon-trash"></i> Hapus Pencarian</a>
                            </div>
                        </div>
                    


                    <div class="table-responsive">
                        <table width="100%" class="table table-hover table-striped table-bordered">
<thead>
                                
                                <tr>
                                  <th width="10%">Kategori PPKS</th>
                                  <th width="10%">Kode/Tgl.Register</th>
                                  <th width="10%">NIK/KK</th>
                                  <th width="10%"><center>
                                    Nama
                                  </center></th>
                                  <th width="10%">Jenis Kelamin</th>
                                  <th width="11%">TTL</th>
                                  <th width="11%">Alamat</th>
                                  <th width="11%">Keberadaan Saat Ini ?</th>
                                  <th width="11%">Sudah Mendapatkan Bantuan ?</th>
                                  <th width="11%">Butuh Panti ?</th>
                                  <th width="9%">Created By</th>
                              <th width="9%">Updated By</th>
                              <th width="9%">Validasi Kab/Kot By</th>
                              <th width="9%">Catatan Validasi</th>
                              <th colspan="2">&nbsp;</th>
                              </tr>
                            </thead>
                            <tbody>

                                <?php if (count($ListData) > 0) {
                                    foreach ($ListData as $key => $value) {
                                ?>

                                        <tr style="font-size:10px;">
                                          <td><?php echo $value['kategori'] ?></td>
                                            <td><?php echo $value['kode_register'] ?>/<?php echo $value['tgl_register'] ?></td>
                                            <td><?php echo $value['nik'] ?> / <?php echo $value['nokk'] ?></td>
                                            <td><?php echo $value['nama'] ?></td>
                                            <td><?php echo $value['sex'] ?></td>
                                            <td><?php echo $value['tempat_lahir'] ?><br />
                                            <?php echo $value['tgl_lahir'] ?>
                                            <td><?php echo $value['alamat'] ?>                                            
                                            <td><?php echo $value['status_keberadaan'] ?>                                            
                                              <br />
                                              <?php echo $value['nama_lks'] ?>
                                            <td><?php echo $value['status_bantuan'] ?>                                            
                                            <td><?php echo $value['status_panti'] ?>                                            
                                          <td><?php echo $value['created_by'] ?><br />
                                        <?php echo $value['created_date'] ?></td>
                                      <td><?php echo $value['update_by'] ?><br />
                                        <?php echo $value['updated_date'] ?></td>
                                        
                                        <?php 
										if ($value['approved']=="Approved") { $bgcolor="#00FF00"; $cat="Approved"; } 
										elseif ($value['approved']=="Not Approved") {  $bgcolor="#FF0000"; $cat="Not Approved"; } 
										elseif ($value['approved']=="Belum Di-Validasi") {  $bgcolor="#FFFF00"; $cat="Belum Di-Validasi"; } 
										?>
                                        
                                      <td style="background-color:<?php echo $bgcolor; ?>"><?php echo $cat; ?><br />
                                        <?php echo $value['approve_date'] ?><br />
                                        <?php echo $value['approve_by'] ?></td>
                                      <td><?php echo $value['catatan_validasi'] ?></td>
                                      <!--
											-->
                                          <td width="13%">
                          <center>
                                                    <?php if ($role['update'] == "TRUE") { ?>
                                                        <a class="btn btn-green " href="<?php echo site_url(); ?>trx_pendataan_swasta/robih/<?php echo enCrypt($value['id']); ?>"><i class="icon-pencil"></i> Ubah</a>

                                                    <?php } ?>
                                                </center>                                            </td>
                                      <td width="13%">
                      <center>
                                                <?php if ($role['delete'] == "TRUE") { ?>
                                                    <a class="btn btn-red" href="<?php echo site_url('trx_pendataan_swasta/hupus/' . enCrypt($value['id'])); ?>" onclick="return confirm('Anda Yakin ingin Menghapus?'); "><i class="icon-trash"></i> Hapus</a>
                                                <?php } ?>
                                                </center>                                            </td>
                              </tr>

                                <?php
                                    }
                                    $paging = (!empty($pagermessage) ? $pagermessage : '');


                                    echo "<tr><td colspan='13'><div style='background:000;'>$paging &nbsp;" . $this->pagination->create_links() . "</div></td></tr>";
                                } else {
                                    echo "<tbody><tr><td colspan='13' style='padding:10px; background:#F00; border:none; color:#FFF;'>Data Tidak Tersedia</td></tr></tbody>";
                                }

                                ?>
                            </tbody>
                        </table>
                  </div>

                </form>

            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    $('#add-peserta').click(function() {
        window.open('<?php echo site_url('/penerimaan_berkas/list_peserta/' . $kode_usulan) ?>', '1576730953022', 'width=1000,height=650,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=0,top=0');
    })
</script>
