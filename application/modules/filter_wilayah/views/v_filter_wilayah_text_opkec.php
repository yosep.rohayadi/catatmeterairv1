				<div class="control-group no-border">
						<label class="control-label" for="textfield">Provinsi</label>

						<div class="controls">
							
							<input type="hidden" name="id_provinsi" id="id_provinsi" value="<?php echo $this->session->userdata('sesi_bo_id_prov'); ?>">
							<?php echo $this->session->userdata('sesi_bo_nama_prov'); ?>
						</div>
					</div>
					
					
					
					<div class="control-group no-border">
						<label class="control-label" for="textfield">Kab/Kota</label>

						<div class="controls">
							
							<input type="hidden" name="id_kabkot" id="id_kabkot" value="<?php echo $this->session->userdata('sesi_bo_id_kabkot'); ?>">
							<?php echo $this->session->userdata('sesi_bo_nama_kabkot'); ?>
						</div>
					</div>
					
					
					<div class="control-group no-border" id="div_kec">
						<label class="control-label" for="textfield">Kecamatan</label>

						<div class="controls">
							
							<input type="hidden" name="id_kecamatan" id="id_kecamatan" value="<?php echo $this->session->userdata('sesi_bo_id_kec'); ?>">
							<?php echo $this->session->userdata('sesi_bo_nama_kec'); ?>
						</div>
					</div>
					
					
					
					<div class="control-group no-border" id="div_kel">
						<label class="control-label" for="textfield">Kelurahan</label>

						<div class="controls">
							
							<select name="id_kelurahan" id="id_kelurahan"  class="form-control" >
							<option value="">-Pilih-</option>
								<?php 
									$id_kelurahan = isset($field->id_kelurahan) ? $field->id_kelurahan : $this->input->post("id_kelurahan"); 
									$id_kecamatanx=$this->session->userdata('sesi_bo_id_kec');
									  $sql3="select * from ref_wil_kel where id_kecamatan='$id_kecamatanx'   order by nama asc  ";
									  $q3=$this->db->query($sql3);
									  foreach($q3->result() as $rq3){
								  ?>
								
								<option value="<?php echo $rq3->id_kelurahan; ?>" <?php if ($id_kelurahan==$rq3->id_kelurahan) { echo "selected"; } ?>><?php echo $rq3->nama; ?></option>
								<?php } ?>
							</select>
							
						</div>
					</div>
					
				