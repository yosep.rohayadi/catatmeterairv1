<div class="control-group no-border">
						<label class="control-label" for="textfield">Pemda</label>

						<div class="controls">
							<?php $jenis = isset($_POST["jenis"])?$_POST["jenis"]:'';  ?>
							<select name="jenis" id="jenis"  class="form-control" data-rule-required="true" onchange="doView(this.value);" >
								<option value="">-Pilih-</option>
								<option value="1" <?php if ($jenis=="1") { echo "selected"; }?>>Provinsi</option>
								<option value="2"  <?php if ($jenis=="2") { echo "selected"; } ?>>Kabupaten/Kota</option>
								
							</select>
							
						</div>
					</div>

	
				
		<div id="MyProvinsi">
					
					<div class="control-group no-border">
						<label class="control-label" for="textfield">Provinsi</label>

						<div class="controls">
							
							<select name="id_prov" id="id_prov"  class="form-control" data-rule-required="true"  >
								<option value="">-Pilih-</option>
								<?php 


									$id_prov = isset($_POST["id_prov"])?$_POST["id_prov"]:'';
									
									  
									  $sql="select * from provinsi where kode_provinsi <> 0  ";
									  
									  
									  
									  $sql.= "order by nama_provinsi asc";
									  
									  $q2=$this->db->query($sql);
									  foreach($q2->result() as $rq2){
								  ?>
								
								<option value="<?php echo $rq2->kode_provinsi; ?>" <?php if ($id_prov==$rq2->kode_provinsi) { echo "selected"; } ?>><?php echo $rq2->nama_provinsi; ?></option>
								<?php } ?>
							</select>
							
						</div>
					</div>
					
					
					<div class="control-group no-border">
						<label class="control-label" for="textfield">&nbsp;</label>

						<div class="controls">
							
							<input type="submit" value="View" name="submit">
							
						</div>
					</div>
					
					
					

					

				</div>

				

				<div id="Mykabkot">
					<div class="control-group no-border">
						<label class="control-label" for="textfield">Provinsi</label>

						<div class="controls">
							
							<select name="id_prov2" id="id_prov2"  class="form-control" data-rule-required="true" onchange="showUser(this.value);" >
								<option value="">-Pilih-</option>
								<?php 


									$id_prov2 = isset($_POST["id_prov2"])?$_POST["id_prov2"]:'';
									
									  
									  $sql="select * from provinsi where kode_provinsi <> 0";
									  
									  
									  
									  $sql.= " order by nama_provinsi asc";
									  
									  $q2=$this->db->query($sql);
									  foreach($q2->result() as $rq2){
								  ?>
								
								<option value="<?php echo $rq2->kode_provinsi; ?>" <?php if ($id_prov2==$rq2->kode_provinsi) { echo "selected"; } ?>><?php echo $rq2->nama_provinsi; ?></option>
								<?php } ?>
							</select>
							
						</div>
					</div>

					<div class="control-group  no-border" >
						<label class="control-label" for="textfield">Kab/Kota</label>

						<div class="controls">

							<div id ="txtHint" >
							<select name="id_kabkot" id="id_kabkot"  class="form-control" data-rule-required="true" >
								<option value="">-Pilih-</option>			
								
								<?php 
								
								   
									$id_kabkot = isset($_POST["id_kabkot"])?$_POST["id_kabkot"]:'';


									$query1 = $this->db->query("select * from kabkota where kode_provinsi='$id_prov' order by nama asc");
									foreach ($query1->result_array() as $row)
									{
										$id=$row['kode_kabkot'];
										$nama=$row['nama'];
									?>
										<option value=<?php echo $id; ?>  <?php if ($id_kabkot==$id) { echo "selected"; } ?>><?php echo $nama; ?></option>
								<?php } ?>
								
							</select>
							</div>
						</div>
					</div>
					
					
					<div class="control-group no-border">
						<label class="control-label" for="textfield">&nbsp;</label>

						<div class="controls">
							
							<input type="submit" value="View" name="submit">
							
						</div>
					</div>

				</div>
			 
				 

<script>

$(document).ready(function(){
					
	
	
	<?php if (isset($_POST["submit"])) { ?>
	
	<?php if ($_POST["jenis"]==1) {  ?>
	
		$("#MyProvinsi").show();
		$("#Mykabkot").hide();
	
	<?php } elseif ($_POST["jenis"]==2) { ?>
		
		$("#MyProvinsi").hide();
		$("#Mykabkot").show();
	
	<?php } else {  ?>
	
	$("#MyProvinsi").hide();
	$("#Mykabkot").hide();
	
	<?php } ?>
	
	<?php } else { ?>
	
	$("#MyProvinsi").hide();
	$("#Mykabkot").hide();
	
	<?php } ?>
	
	
});

function doView(id_instansi){

	if (id_instansi==1) {
		
		$("#MyProvinsi").show();
		$("#Mykabkot").hide();
		
	} else if (id_instansi==2) {
		
		$("#MyProvinsi").hide();
		$("#Mykabkot").show();
		
	} 
}

function showUser(str) {
    if (str == "") {
        document.getElementById("txtHint").innerHTML = "";
        return;
    } else { 
        if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else {
            // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("txtHint").innerHTML = this.responseText;
             
			} else {
                document.getElementById("txtHint").innerHTML = "Mohon Tunggu....";
            }
        };
        xmlhttp.open("GET","<?php echo site_url(); ?>filter/load_kabkota_tugas/"+str,true);
        xmlhttp.send();
    }
}

</script>