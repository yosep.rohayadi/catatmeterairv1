				<div class="control-group no-border">
						<label class="control-label" for="textfield">Provinsi</label>

						<div class="controls">
							
							<select name="id_provinsi" id="id_provinsi"  class="form-control" >
								<?php 
									$id_provinsi = isset($field->id_provinsi) ? $field->id_provinsi : $this->input->post("id_provinsi"); 
									
									  $sql="select * from ref_wil_provinsi  order by nama asc ";
									  $q2=$this->db->query($sql);
									  foreach($q2->result() as $rq2){
								  ?>
								
								<option value="<?php echo $rq2->id_provinsi; ?>" <?php if ($id_provinsi==$rq2->id_provinsi) { echo "selected"; } ?>><?php echo $rq2->nama; ?></option>
								<?php } ?>
							</select>
							
						</div>
					</div>
					
					
					
					<div class="control-group no-border">
						<label class="control-label" for="textfield">Kab/Kota</label>

						<div class="controls">
							
							<select name="id_kabkot" id="id_kabkot"  class="form-control" onchange="doKec(this.value);">
							<option value="">-Pilih-</option>
								<?php 
									$id_kabkot = isset($field->id_kabkot) ? $field->id_kabkot : $this->input->post("id_kabkot"); 
									
									  $sql1="select * from ref_wil_kabkot   order by nama asc  ";
									  $q1=$this->db->query($sql1);
									  foreach($q1->result() as $rq1){
								  ?>
								
								<option value="<?php echo $rq1->id_kabkot; ?>" <?php if ($id_kabkot==$rq1->id_kabkot) { echo "selected"; } ?>><?php echo $rq1->nama; ?></option>
								<?php } ?>
							</select>
							
						</div>
					</div>
					
					
					<div class="control-group no-border" id="div_kec">
						<label class="control-label" for="textfield">Kecamatan</label>

						<div class="controls">
							
							<select name="id_kecamatan" id="id_kecamatan"  class="form-control"   onchange="doKel(this.value);">
							<option value="">-Pilih-</option>
								<?php 
									$id_kecamatan = isset($field->id_kecamatan) ? $field->id_kecamatan : $this->input->post("id_kecamatan"); 
									
									  $sql2="select * from ref_wil_kec where id_kabkot='$id_kabkot'   order by nama asc ";
									  $q2=$this->db->query($sql2);
									  foreach($q2->result() as $rq2){
								  ?>
								
								<option value="<?php echo $rq2->id_kecamatan; ?>" <?php if ($id_kecamatan==$rq2->id_kecamatan) { echo "selected"; } ?>><?php echo $rq2->nama; ?></option>
								<?php } ?>
							</select>
							
						</div>
					</div>
					
					
					
					<div class="control-group no-border" id="div_kel">
						<label class="control-label" for="textfield">Kelurahan</label>

						<div class="controls">
							
							<select name="id_kelurahan" id="id_kelurahan"  class="form-control" >
							<option value="">-Pilih-</option>
								<?php 
									$id_kelurahan = isset($field->id_kelurahan) ? $field->id_kelurahan : $this->input->post("id_kelurahan"); 
									
									  $sql3="select * from ref_wil_kel where id_kecamatan='$id_kecamatan'   order by nama asc  ";
									  $q3=$this->db->query($sql3);
									  foreach($q3->result() as $rq3){
								  ?>
								
								<option value="<?php echo $rq3->id_kelurahan; ?>" <?php if ($id_kelurahan==$rq3->id_kelurahan) { echo "selected"; } ?>><?php echo $rq3->nama; ?></option>
								<?php } ?>
							</select>
							
						</div>
					</div>
					
					
					
<script>


					
	
function doKec(str) {
    if (str == "") {
        document.getElementById("div_kec").innerHTML = "";
        return;
    } else { 
        if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else {
            // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("div_kec").innerHTML = this.responseText;
             
			} else {
                document.getElementById("div_kec").innerHTML = "Mohon Tunggu....";
            }
        };
        xmlhttp.open("GET","<?php echo site_url(); ?>filter_wilayah/load_kec/"+str,true);
        xmlhttp.send();
    }
}




function doKel(str) {
    if (str == "") {
        document.getElementById("div_kel").innerHTML = "";
        return;
    } else { 
        if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else {
            // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("div_kel").innerHTML = this.responseText;
             
			} else {
                document.getElementById("div_kel").innerHTML = "Mohon Tunggu....";
            }
        };
        xmlhttp.open("GET","<?php echo site_url(); ?>filter_wilayah/load_kel/"+str,true);
        xmlhttp.send();
    }
}

</script>