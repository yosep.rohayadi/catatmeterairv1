<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Filter_wilayah extends CI_Controller {


	
	function __construct(){
		parent::__construct();
		$this->load->helper('tool');
	}


	public function index() {
		$data["test"]="";
		$this->template->load('template_admin','v_form',$data);
   }
   
   
    public function load_kec($id_kabkot) {
		$data["id_kabkot"]=$id_kabkot;
		$this->load->view('v_kec',$data);
	}
	
	public function load_kel($id_kecamatan) {
		$data["id_kecamatan"]=$id_kecamatan;
		$this->load->view('v_kel',$data);
	}
	

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
