<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Ref_users_kabkot extends MX_Controller
{


	//put your code here
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('utility');
		// 
		$this->truly_madly_deeply();

		Modules::run('ref_role/permissions');
		$this->load->model('users_model');

		$this->load->helper('login');
		$this->load->helper('tool');
	}




	


	public function truly_madly_deeply()
	{
		if (!$this->session->userdata('truly_madly_deeply')) {
			redirect('signon');
		}
	}




	public function index($offset = 0)
	{
		if (isset($_POST['cari_global'])) {
			$data1 = array('s_cari_global' => $_POST['cari_global']);
			$this->session->set_userdata($data1);
		}

		$per_page = 20;
		$qry = "SELECT u.*,ug.nama_user_group
				FROM 	ref_users u
						left join ref_user_group ug on  u.id_user_group=ug.id_user_group 
						left join ref_wil_kabkot xx on  u.id_kabkot=xx.id_kabkot 
						
				WHERE u.id_user_group=6 ";


		if ($this->session->userdata('s_cari_global') != "") {
			$qry .= "  AND 
			
			(u.nama_lengkap like '%" . $this->db->escape_like_str($this->session->userdata('s_cari_global')) . "%'  or 
			 u.username like '%" . $this->db->escape_like_str($this->session->userdata('s_cari_global')) . "%' or 
			 u.email like '%" . $this->db->escape_like_str($this->session->userdata('s_cari_global')) . "%' 
			 )
			
			";
		} elseif ($this->session->userdata('s_cari_global') == "") {
			$this->session->unset_userdata('s_cari_global');
		}


		$qry .= " ORDER BY u.username asc";

		$offset = ($this->uri->segment(3) != '' ? $this->uri->segment(3) : 0);
		$config['total_rows'] = $this->db->query($qry)->num_rows();
		$config['per_page'] = $per_page;
		$config['full_tag_open'] = '<div class="table-pagination">';
		$config['full_tag_close'] = '</div>';
		$config['cur_tag_open'] = '<a href="#" class="active"><b>';
		$config['cur_tag_close'] = '</b></a>';
		$config['first_link'] = 'First';
		$config['last_link'] = 'Last';
		$config['next_link'] = 'Next';
		$config['prev_link'] = 'Previous';
		$config['last_tag_open'] = "<span>";
		$config['first_tag_close'] = "</span>";
		$config['uri_segment'] = 3;
		$config['base_url'] = base_url() . '/ref_users/index';
		$config['suffix'] = '?' . http_build_query($_GET, '', "&");
		$this->pagination->initialize($config);
		$data['paginglinks'] = $this->pagination->create_links();
		$data['per_page'] = $this->uri->segment(3);
		$data['offset'] = $offset;
		if ($data['paginglinks'] != '') {
			$data['pagermessage'] = 'Showing ' . ((($this->pagination->cur_page - 1) * $this->pagination->per_page) + 1) . ' to ' . ($this->pagination->cur_page * $this->pagination->per_page) . ' of ' . $this->db->query($qry)->num_rows();
		}
		$qry .= " limit {$per_page} offset {$offset} ";
		$data['ListData'] = $this->db->query($qry)->result_array();

		$data['breadcrumbs'] = array(
			array(
				'link' => '#',
				'name' => 'Setting Users'
			),
			array(
				'link' => '#',
				'name' => 'Operator Dinsos Kab/Kota'
			)
		);
		$data['role'] = get_role($this->session->userdata('sesi_bo_user_group'));
		$data['sub_judul_form'] = "Data Operator Dinsos Kab/Kota ";
		$this->template->load('template_admin', 'v_index', $data);
	}



	public function tambih()
	{

		$data['breadcrumbs'] = array(
			array(
				'link' => '#',
				'name' => 'Settings'
			),
			array(
				'link' => 'ref_users',
				'name' => 'Operator Dinsos Kab/Kota'
			),
			array(
				'link' => '#',
				'name' => 'Tambah Data'
			)
		);

		$data['judul_form'] 	= "Tambah Data";
		$data['sub_judul_form'] = "Operator Dinsos Kab/Kota ";
		$data['id_provinsi']	= null;
		$data['id_kabkot']		= null;
		$data['id_kecamatan']	= null;
		$data['id_kelurahan']	= null;
		$data['user_group'] 	= $this->users_model->get_group();

		$this->template->load('template_admin', 'v_add', $data);
	}




	public function tambih_robih()
	{
		try {
			$id = $this->input->post('id_users');
			$ip = $this->input->post('ip_address');
			$username = $this->input->post('username_users');
			$nama = $this->input->post('nama_users');
			$email = $this->input->post('email_users');
			$hp = $this->input->post('hp_users');
			$id_kabkot = $this->input->post('id_kabkot');
			$status_aktif = $this->input->post('status_aktif');

		

			//$pwd = sha1(md5($this->input->post('password_users')));

			$pwd = get_hash($this->input->post('password_users'));

			


			$password_users = $this->input->post('password_users');
			$password_users_text = $this->input->post('password_users_text');

			//echo $password_users."<br>".$password_users_text; exit;




			if ($id == '' || $id == null) {

				//cek username ganda			

				$sql     = "select count(username) as ceknik from ref_users where username = '" . $username . "'";
				$row     = $this->db->query($sql)->row_array();

				if ($row['ceknik'] == 0) {

					$data['ip'] = $ip;
					$data['username'] = $username;
					$data['ip'] = $ip;
					$data['password'] = $pwd;
					//strlen($password)>0?$data['password']=$password:'';
					$data['nama_lengkap'] = $nama;
					$data['email'] = $email;
					$data['status_aktif'] = $status_aktif;
					$data['hp'] = $hp;
					$data['id_user_group']	= 6;
					$data['id_kabkot'] 		= $id_kabkot;

					// ALAMAT DAERAH
					$data['id_prov']	= 32;
					$data['id_kabkot']	= $this->input->post('id_kabkot');
					$data['id_kec']		= $this->input->post('id_kecamatan');
					$data['id_kel']		= $this->input->post('id_kelurahan');

					$data['update_oleh']   = $this->session->userdata('sesi_bo_nama_lengkap');
					$data['update_tgl']		= date('Y-m-d H:i:s');
					// $data['tim_penilai_ke']=$tim_penilai;


					
					$data['tgl_daftar'] = date('Y-m-d H:i:s');

					$xss_data = $this->security->xss_clean($data);
					$this->db->insert('ref_users', $xss_data);
					$this->session->set_flashdata('message_sukses', 'Data Berhasil Disimpan');
					$this->tambih();
				} else {

					$this->session->set_flashdata('message_sukses', 'NIP Yang Anda Masukan Sudah Ada !!!');
					$this->tambih();
				}
			} else {

				if ($password_users != $password_users_text) {

					$data['password'] = $pwd;
				} else {

					$data['password'] = $password_users_text;
				}

				$data['ip'] = $ip;

				$data['update_oleh']   = $this->session->userdata('sesi_bo_nama_lengkap');
				$data['update_tgl']		= date('Y-m-d H:i:s');

				$data['username'] = $username;
				$data['status_aktif'] = $status_aktif;
				$data['nama_lengkap'] = $nama;
				$data['email'] = $email;
				$data['hp'] = $hp;
			
				$data['id_user_group'] = 6;

				// ALAMAT DAERAH
				$data['id_prov']	=32;
				$data['id_kabkot']	= $this->input->post('id_kabkot');
				$data['id_kec']		= $this->input->post('id_kecamatan');
				$data['id_kel']		= $this->input->post('id_kelurahan');


				$xss_data = $this->security->xss_clean($data);
				$this->db->where('id', $id);
				$this->db->update('ref_users', $xss_data);

				$this->session->set_flashdata('message_sukses', 'Perubahan Data Berhasil Disimpan');
				redirect('ref_users_kabkot');

				$data['judul_form'] = "Ubah Data";
				$data['sub_judul_form'] = "Operator Dinsos Kab/Kota ";
				$data['field'] = $this->users_model->get_all(array('id' => $id));
				$data['breadcrumbs'] = array(
					array(
						'link' => '#',
						'name' => 'Settings'
					),
					array(
						'link' => 'ref_users',
						'name' => 'Operator Dinsos Kab/Kota'
					),
					array(
						'link' => '#',
						'name' => 'Ubah Data'
					)
				);

				$this->template->load('template_admin', 'v_add', $data);
			}
		} catch (Exception $err) {
			log_message("error", $err->getMessage());
			return show_error($err->getMessage());
		}
	}




	


	public function robih()
	{
		$id = $this->uri->segment(3);
		$data['user_group'] 	= $this->users_model->get_group();
		$data['field'] 			= $this->users_model->get_all(array('id' => $id));
		$data['wilayah_ref'] 	= $this->db->query("SELECT * from ref_wilayah where id > 0")->result_array();

		// print_r($data['wilayah_ref']);exit;

		$data['breadcrumbs'] = array(
			array(
				'link' => '#',
				'name' => 'Settings'
			),
			array(
				'link' => 'ref_users',
				'name' => 'Operator Dinsos Kab/Kota'
			),
			array(
				'link' => '#',
				'name' => 'Ubah Data'
			)
		);

		$data['judul_form'] = "Ubah Data";
		$data['sub_judul_form'] = "Operator Dinsos Kab/Kota ";

		$sql = $this->db->query("SELECT * FROM ref_wil_provinsi");
		$data['prov'] 			= $sql;
		$data['id_provinsi']	= $data['field']->id_prov;
		$data['id_kabkot']		= $data['field']->id_kabkot;
		$data['id_kecamatan']	= $data['field']->id_kec;
		$data['id_kelurahan']	= $data['field']->id_kel;
		$this->template->load('template_admin', 'v_add', $data);
	}


	


	public function hupus()
	{

		$id = $this->uri->segment(3);
		try {

			$this->db->where('id', $id);
			$this->db->delete('ref_users');
			redirect('ref_users_kabkot');
		} catch (Exception $err) {
			log_message("error", $err->getMessage());
			return show_error($err->getMessage());
		}
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
