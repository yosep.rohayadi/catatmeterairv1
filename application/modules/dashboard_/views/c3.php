<canvas id="myChart3" style="width:100%;max-width:100%"></canvas>

<script>
var xValues = [<?php foreach ($list_ppks as $row) { ?>"<?php echo $row->nama; ?>",<?php } ?>];
var yValues = [<?php foreach ($list_ppks as $row) { ?><?php echo $row->ppks; ?>,<?php } ?>];
var barColors = [<?php foreach ($list_ppks as $row) { ?>"#09aede",<?php } ?>];

new Chart("myChart3", {
  type: "bar",
  data: {
    labels: xValues,
    datasets: [{
      backgroundColor: barColors,
      data: yValues
    }]
  },
  options: {
    legend: {display: false},
    title: {
      display: true,
      text: ""
    }
  }
});
</script>

<!--
 <table width="100%" border="1" class="">
                                <tr style="font-weight:bold; background-color:#CCCCCC">
                                  <td>Kategori</td>
                                  <td>Jumlah</td>
                                </tr>
                                <?php foreach ($list_ppks as $row) { ?>
                                <tr>
                                  <td><?php echo $row->nama; ?></td>
                                  <td><?php echo $row->ppks; ?></td>
                                </tr>
                                <?php } ?>
                              </table>  -->