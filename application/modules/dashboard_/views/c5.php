<canvas id="myChart5" style="width:100%;max-width:100%"></canvas>

<script>
var xValues = [<?php foreach ($list_wilayah as $row) { ?>"<?php echo $row->nama; ?>",<?php } ?>];
var yValues = [<?php foreach ($list_wilayah as $row) { ?><?php echo $row->ppks; ?>,<?php } ?>];
var barColors = [<?php foreach ($list_wilayah as $row) { ?>"#0aba60",<?php } ?>];

new Chart("myChart5", {
  type: "bar",
  data: {
    labels: xValues,
    datasets: [{
      backgroundColor: barColors,
      data: yValues
    }]
  },
  options: {
    legend: {display: false},
    title: {
      display: true,
      text: ""
    }
  }
});
</script>

<!--
 <table width="100%" border="1" class="">
                                <tr style="font-weight:bold; background-color:#CCCCCC">
                                  <td>Kab/Kota</td>
                                  <td>PPKS</td>
                                  <td>PSKS</td>
                                </tr>
                                <?php foreach ($list_wilayah as $row) { ?>
                                <tr>
                                  <td><?php echo $row->nama; ?></td>
                                  <td><?php echo $row->ppks; ?></td>
                                  <td><?php echo $row->psks; ?></td>
                                </tr>
                                <?php } ?>
                              </table> -->