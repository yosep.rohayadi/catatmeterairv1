<canvas id="myChart1" style="width:100%;max-width:600px"></canvas>

<script>
var xValues = ["PPKS", "PSKS Individu"];
var yValues = [<?php echo isset($field->jml_ppks) ? $field->jml_ppks : 0; ?>, <?php echo isset($field->jml_psks) ? $field->jml_psks : 0; ?>];
var barColors = [
  "#15a8dc",
  "#ffb91b"
];

new Chart("myChart1", {
  type: "pie",
  data: {
    labels: xValues,
    datasets: [{
      backgroundColor: barColors,
      data: yValues
    }]
  },
  options: {
    title: {
      display: true,
      text: ""
    }
  }
});
</script>