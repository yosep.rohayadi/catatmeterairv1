<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.js"></script>


<div class="container-fluid">
				<div class="page-header">
					<div class="pull-left">
						<h1>Dashboard</h1>
					</div>
					<div class="pull-right">
						
						<ul class="stats">
							
							<li class="lightred">
								<i class="icon-calendar"></i>
								<div class="details">
									<span class="big">November 16, 2021</span>
									<span>Wednesday, 13:56</span>
								</div>
							</li>
						</ul>
					</div>
				</div>
				<div class="breadcrumbs">
					<ul>
						<li>
							<a href="#">Beranda</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="#">Dashboard</a>
							<i class="icon-angle-right"></i>
						</li>
						
					</ul>
					<div class="close-bread">
						<a href="#"><i class="icon-remove"></i></a>
					</div>
				</div>
				
				
				
				<div class="row-fluid">
					
					<div class="span6">
						<div class="box">
							<div class="box-title">
								<h3>
									<i class="icon-th-large"></i>
									Jumlah PPKS & PSKS Individu Yang Sudah DiValidasi
								</h3>
							</div>
							<div class="box-content">
							<?php include("c1.php");?>
							</div>
						</div>
					</div>
					
					
					<div class="span6">
						<div class="box">
							<div class="box-title">
								<h3>
									<i class="icon-th-large"></i>
									Jumlah PPKS Berdasarkan Jenis Kelamin
								</h3>
							</div>
							<div class="box-content">
							<?php include ("c2.php"); ?>
							</div>
						</div>
					</div>
					
					
					
				</div>
				
				
				<div class="row-fluid">
					<div class="span12">
						<div class="box">
							<div class="box-title">
								<h3>
									<i class="icon-th-large"></i>
									Jumlah PPKS & PSKS Yang Sudah DiValidasi
								</h3>
							</div>
							<div class="box-content">
								
								<ul class="stats">
									
									<!--<li class="lime">
										<i class="icon-user"></i>
										<div class="details">
											<span class="big"><?php echo isset($field->jml_ppks) ? $field->jml_ppks : 0; ?></span>
											<span>PPKS</span>
										</div>
									</li>-->
									
									<li class="lime">
										<i class="icon-user"></i>
										<div class="details">
											<span class="big"><?php echo isset($field->jml_ppks_masyarakat) ? $field->jml_ppks_masyarakat : 0; ?></span>
											<span>PPKS di Masyarakat</span>
										</div>
									</li>
									
									<li class="lime">
										<i class="icon-user"></i>
										<div class="details">
											<span class="big"><?php echo isset($field->jml_ppks_lks) ? $field->jml_ppks_lks : 0; ?></span>
											<span>PPKS di Panti</span>
										</div>
									</li>
									
									
									<li class="lime">
										<i class="icon-user"></i>
										<div class="details">
											<span class="big"><?php echo isset($field->jml_ppks_bantuan) ? $field->jml_ppks_bantuan : 0; ?></span>
											<span>PPKS Sudah Mendapat Bantuan</span>
										</div>
									</li>
									<!--
									<li class="lime">
										<i class="icon-user"></i>
										<div class="details">
											<span class="big"><?php echo isset($field->jml_psks) ? $field->jml_psks : 0; ?></span>
											<span>PSKS Individu</span>
										</div>
									</li>
									-->
									
									
								</ul>
							</div>
						</div>
					</div>
				</div>
				
				
				
				<div class="row-fluid">
					<div class="span12">
						<div class="box">
							<div class="box-title">
								<h3>
									<i class="icon-th-large"></i>
									Jumlah PPKS  Yang Sudah DiValidasi Per-Kategori
								</h3>
							</div>
							<div class="box-content">
							
							<?php include ("c3.php"); ?>
                           
                          </div>
						</div>
					</div>
				</div>
				
				
				
				
				<div class="row-fluid">
					<div class="span12">
						<div class="box">
							<div class="box-title">
								<h3>
									<i class="icon-th-large"></i>
									Jumlah PSKS Individu  Yang Sudah DiValidasi Per-Kategori
								</h3>
							</div>
							<div class="box-content">
							<?php include ("c4.php"); ?>
							  
							</div>
						</div>
					</div>
				</div>
				
				
				
				
				
				
				
				<div class="row-fluid">
					<div class="span12">
						<div class="box">
							<div class="box-title">
								<h3>
									<i class="icon-th-large"></i>
									Jumlah PPKS  Yang Sudah DiValidasi Per-Wilayah Kab/Kota
								</h3>
							</div>
							<div class="box-content">
							<?php include ("c5.php"); ?>
							  
							</div>
						</div>
					</div>
				</div>
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
			</div>