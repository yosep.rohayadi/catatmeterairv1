<canvas id="myChart2" style="width:100%;max-width:600px"></canvas>

<script>
var xValues = ["Pria", "Wanita"];
var yValues = [<?php echo isset($field_jk->CWO) ? $field_jk->CWO : 0; ?>, <?php echo isset($field_jk->CW) ? $field_jk->CW : 0; ?>];
var barColors = [
  
  "#13a75b",
  "#ffb91b"
];

new Chart("myChart2", {
  type: "doughnut",
  data: {
    labels: xValues,
    datasets: [{
      backgroundColor: barColors,
      data: yValues
    }]
  },
  options: {
    title: {
      display: true,
      text: ""
    }
  }
});
</script>