<canvas id="myChart4" style="width:100%;max-width:100%"></canvas>

<script>
var xValues = [<?php foreach ($list_psks as $row) { ?>"<?php echo $row->nama; ?>",<?php } ?>];
var yValues = [<?php foreach ($list_psks as $row) { ?><?php echo $row->psks; ?>,<?php } ?>];
var barColors = [
 "red", "green","blue","orange","brown","cyan"
];

new Chart("myChart4", {
  type: "pie",
  data: {
    labels: xValues,
    datasets: [{
      backgroundColor: barColors,
      data: yValues
    }]
  },
  options: {
    title: {
      display: true,
      text: ""
    }
  }
});
</script>
