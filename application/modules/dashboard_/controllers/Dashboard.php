<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends MX_Controller {


	
	function __construct(){
		parent::__construct();
		$this->load->helper('tool');
		$this->truly_madly_deeply();
	}
	
	
	public function truly_madly_deeply()
	{

		if (!$this->session->userdata('truly_madly_deeply')) {
			redirect('signon');
		}
	}
	
	
	public function index() {
		
		$sql="
		SELECT 

(SELECT COUNT(id) FROM trx_pendataan WHERE jenis_pendataan IN ('PPKS KELURAHAN','PPKS PANTI SWASTA','PPKS PANTI PEMERINTAH') AND approved='Approved') AS jml_ppks,
(SELECT COUNT(id) FROM trx_pendataan WHERE jenis_pendataan IN ('PPKS KELURAHAN','PPKS PANTI SWASTA','PPKS PANTI PEMERINTAH') AND approved='Approved' AND status_keberadaan='MASYARAKAT')  AS jml_ppks_masyarakat,
(SELECT COUNT(id) FROM trx_pendataan WHERE jenis_pendataan IN ('PPKS KELURAHAN','PPKS PANTI SWASTA','PPKS PANTI PEMERINTAH') AND approved='Approved' AND status_keberadaan='LKS')  AS jml_ppks_lks,
(SELECT COUNT(id) FROM trx_pendataan WHERE jenis_pendataan IN ('PPKS KELURAHAN','PPKS PANTI SWASTA','PPKS PANTI PEMERINTAH') AND approved='Approved' AND status_bantuan='YA')  AS jml_ppks_bantuan,

(SELECT COUNT(id) FROM trx_pendataan WHERE jenis_pendataan IN ('PSKS KELURAHAN') AND approved='Approved') AS jml_psks,
(SELECT COUNT(id) FROM trx_pendataan WHERE jenis_pendataan IN ('PSKS KELURAHAN') AND approved='Approved' AND status_keberadaan='MASYARAKAT')  AS jml_psks_masyarakat,
(SELECT COUNT(id) FROM trx_pendataan WHERE jenis_pendataan IN ('PSKS KELURAHAN') AND approved='Approved' AND status_keberadaan='LKS')  AS jml_psks_lks,
(SELECT COUNT(id) FROM trx_pendataan WHERE jenis_pendataan IN ('PSKS KELURAHAN') AND approved='Approved' AND status_bantuan='YA')  AS jml_psks_bantuan


		";
		$data['field'] = $this->db->query($sql)->row();
		
		
		$sql1="SELECT 
		a.nama,
		(SELECT COUNT(id) FROM trx_pendataan WHERE jenis_pendataan IN ('PPKS KELURAHAN','PPKS PANTI SWASTA','PPKS PANTI PEMERINTAH') AND approved='Approved' AND id_kabkot=a.id_kabkot) AS ppks,
		(SELECT COUNT(id) FROM trx_pendataan WHERE jenis_pendataan IN ('PSKS KELURAHAN') AND approved='Approved' AND id_kabkot=a.id_kabkot) AS psks
		FROM
		ref_wil_kabkot a ORDER BY ppks DESC,psks DESC ";
		$query1 = $this->db->query($sql1);
			$data['list_wilayah']=$query1->result();
			
			
			
		$sql2="SELECT 
a.nama,
(SELECT COUNT(id) FROM trx_pendataan WHERE jenis_pendataan IN ('PPKS KELURAHAN','PPKS PANTI SWASTA','PPKS PANTI PEMERINTAH') AND approved='Approved' AND id_kategori=a.id) AS ppks
FROM
ref_kategori a WHERE a.flag='PPKS' ORDER BY ppks DESC ";
		$query2 = $this->db->query($sql2);
			$data['list_ppks']=$query2->result();
			
			$sql3="SELECT 
a.nama,
(SELECT COUNT(id) FROM trx_pendataan WHERE jenis_pendataan IN ('PSKS KELURAHAN') AND approved='Approved' AND id_kategori=a.id) AS psks
FROM
ref_kategori a WHERE a.flag='PSKS' ORDER BY psks DESC  ";
		$query3 = $this->db->query($sql3);
			$data['list_psks']=$query3->result();
			
			
			$sql4="SELECT 
(SELECT COUNT(id) FROM trx_pendataan WHERE sex='PEREMPUAN' AND approved='Approved') AS CW,
(SELECT COUNT(id) FROM trx_pendataan WHERE sex='LAKI-LAKI' AND approved='Approved') AS CWO
  ";
			$data['field_jk'] = $this->db->query($sql4)->row();
	

	 $this->template->load('template_admin','v_index',$data);	

	
   }
   
   
	
	
	
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */