<div class="box box-bordered box-color">
            <div class="box-title">
                <h3><i class=" icon-plus-sign"></i>Transaksi Berhasil </h3>
            </div>
            <div class="box-content nopadding">
                <!-- <form action="#" method="POST" class='form-horizontal form-validate' id="bb"> -->
                <?php echo form_open('trx_pendataan/tambih_robih', array('name' => 'bb', 'id' => 'bb', 'class' => 'form-horizontal form-bordered form-validate form-wysiwyg', 'enctype' => 'multipart/form-data')); ?>


<div class="control-group">
							<label for="textfield" class="control-label">Tgl</label>
							<div class="controls">
								<?php echo date("d-m-Y"); ?>
							</div>
						</div>
<div class="control-group">
							<label for="textfield" class="control-label">Nama</label>
							<div class="controls">
								<?php echo isset($field_bio->nama) ? $field_bio->nama : ''; ?>
							</div>
						</div>

<div class="control-group">
							<label for="textfield" class="control-label">Alamat</label>
							<div class="controls">
								<?php echo isset($field_bio->alamat) ? $field_bio->alamat : ''; ?>
							</div>
						</div>

<div class="control-group">
							<label for="textfield" class="control-label">Meter Awal</label>
							<div class="controls">
								<?php echo isset($field_bio->m_before) ? $field_bio->m_before : ''; ?>
							</div>
						</div>
						
						
						<div class="control-group">
												<label class="control-label" for="textfield">Meter Akhir</label>                                        
												<div class="controls">
<?php echo isset($field_bio->m_after) ? $field_bio->m_after : ''; ?>												</div>
											</div>
						
						
						<div class="control-group">
							<label for="textfield" class="control-label">Harga per-m3 </label>
							<div class="controls">
<?php echo isset($field_bio->harga) ? $field_bio->harga : ''; ?>							</div>
						</div>
						
						
						<div class="control-group">
							<label for="textfield" class="control-label">Jumlah Pakai </label>
							<div class="controls">
<?php echo isset($field_bio->jml_pakai) ? $field_bio->jml_pakai : ''; ?>							</div>
						</div>
						
						
						<div class="control-group">
							<label for="textfield" class="control-label">Abudemen </label>
							<div class="controls">
<?php echo isset($field_bio->abudemen) ? $field_bio->abudemen : ''; ?>							</div>
						</div>
						
						
						<div class="control-group">
							<label for="textfield" class="control-label">Subtotal </label>
							<div class="controls">
<?php echo isset($field_bio->subtot_pakai) ? $field_bio->subtot_pakai : ''; ?>							</div>
						</div>
						

						<div class="control-group">
							<label class="control-label" for="textfield">Photo Bukti Pencatatan</label>
							<div class="controls">
								

								<?php if (isset($field_bio->foto)) { ?>
									<img src="<?php echo base_url(); ?>/<?php echo isset($field_bio->foto) ? $field_bio->foto : ''; ?>" >
								<?php } ?>
							</div>
						</div>

						
						<div class="control-group">
							<label for="textfield" class="control-label">Pembayaran </label>
							<div class="controls">
							  <?php echo isset($field_bio->jenis_pembayaran) ? $field_bio->jenis_pembayaran : ''; ?>	
							</div>
						</div>
				





                <div class="form-actions">
				 <button class="btn btn-info" onclick="javascript:void window.open('<?php echo site_url();?>cetak/struk/<?php echo isset($field_bio->id) ? $field_bio->id : ''; ?>','1431534138220','width=1280,height=600,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=0,top=0');return false;" >
								<i class="icon-map-marker"> CETAK</i>
							</button>	
                                      
                </div>
				
				
				</form>
            </div>
        </div>