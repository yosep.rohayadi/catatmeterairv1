<div class="control-group">
							<label for="textfield" class="control-label">Alamat</label>
							<div class="controls">
								<?php echo isset($field_bio->alamat) ? $field_bio->alamat : ''; ?>
							</div>
						</div>

<div class="control-group">
							<label for="textfield" class="control-label">Meter Awal</label>
							<div class="controls">
								<input readonly type="text"  id="m_before" class="input-small" name="m_before" value="<?php echo $m_awal; ?>"  required>
								<span class="required-server"><?php echo form_error('m_before', '<p style="color:#F83A18">', '</p>'); ?></span>
							</div>
						</div>
						
						
						<div class="control-group">
												<label class="control-label" for="textfield">Meter Akhir</label>                                        
												<div class="controls">
												<input onchange="doHitung(this.value)"  type="number" placeholder="" id="m_after" class="input-small" name="m_after" value="<?php echo isset($field->m_after) ? $field->m_after : $this->input->post("m_after"); ?>"  >
												</div>
											</div>
						
						
						<div class="control-group">
							<label for="textfield" class="control-label">Harga per-m3 </label>
							<div class="controls">
							   <input readonly type="number" placeholder="" id="harga" class="input-small" name="harga" value="2000"  ><span class="required-server"><?php echo form_error('nokk', '<p style="color:#F83A18">', '</p>'); ?></span>
							</div>
						</div>
						
						
						<div class="control-group">
							<label for="textfield" class="control-label">Jumlah Pakai </label>
							<div class="controls">
							   <input  readonly type="number" placeholder="" id="jml_pakai" class="input-small" name="jml_pakai" value=""  ><span class="required-server"><?php echo form_error('nokk', '<p style="color:#F83A18">', '</p>'); ?></span> m3
							</div>
						</div>
						
						
						<div class="control-group">
							<label for="textfield" class="control-label">Abudemen </label>
							<div class="controls">
							   <input readonly type="number" placeholder="" id="abudemen" class="input-small" name="abudemen" value="10000"  ><span class="required-server"><?php echo form_error('nokk', '<p style="color:#F83A18">', '</p>'); ?></span>
							</div>
						</div>
						
						
						<div class="control-group">
							<label for="textfield" class="control-label">Subtotal </label>
							<div class="controls">
							   <input readonly  type="number" placeholder="" id="subtot_pakai" class="input-small" name="subtot_pakai" value="<?php echo isset($field->nokk) ? $field->nokk : $this->input->post("nokk"); ?>"  ><span class="required-server"><?php echo form_error('nokk', '<p style="color:#F83A18">', '</p>'); ?></span> 
							</div>
						</div>
						

						<div class="control-group">
							<label class="control-label" for="textfield">Photo Bukti Pencatatan</label>
							<div class="controls">
								<!-- <input type="hidden" name="nip" id="nip" class="input-large" value=""> -->
								<input type="file" name="berkas" id="berkas" accept="image/*">
								<span class="required-server">
									<p>*<small>File tipe gambar</small>
									<br/>*<small>Ukuran maksimal 1 MB</small>
									</p>
								</span>

								<?php if (isset($field->foto)) { ?>
									<img src="<?php echo base_url(); ?>/<?php echo isset($field->foto) ? $field->foto : ''; ?>" >
								<?php } ?>
							</div>
						</div>

						
						<div class="control-group">
							<label for="textfield" class="control-label">Pembayaran </label>
							<div class="controls">
							   <input type="radio" name="jenis_pembayaran"  value="sekarang"> Sekarang
							   <input type="radio" name="jenis_pembayaran"  value="nanti"> Nanti Saja
							</div>
						</div>
				





                <div class="form-actions">
                    <button id="btn_simpan" class="btn btn-blue" type="submit">Simpan</button>
                    <a class="btn btn-danger" href="<?php echo site_url(); ?>trx_pendataan">Kembali</a>
                </div>