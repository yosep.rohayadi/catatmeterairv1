<div class="container-fluid">
    <br>
    <div class="breadcrumbs">
        <ul>
            <?php foreach ($breadcrumbs as $key => $value) { ?>
                <li>
                    <a href="<?php echo site_url($value['link']) ?>">
                        <?php echo $value['name']; ?></a>
                    <?php echo (count($breadcrumbs) - 1) == $key ? "" : "<i class='icon-angle-right'></i>"; ?>
                </li>
            <?php } ?>
        </ul>
        <div class="close-bread">
            <a href="#"><i class="icon-remove"></i></a>
        </div>
    </div>
</div>



<div class="row-fluid">
	
	<div class="span1">&nbsp;</div>

    <div class="span10">
        <?php
        if ($this->session->flashdata('message_gagal')) {
            echo '<hr><div class="alert alert-error"><button class="close" data-dismiss="alert" type="button">&times;</button>' . $this->session->flashdata('message_gagal') . '</div>';
        }
        if ($this->session->flashdata('message_sukses')) {
            echo '<hr><div class="alert alert-success"><button class="close" data-dismiss="alert" type="button">&times;</button>' . $this->session->flashdata('message_sukses') . '</div>';
        } ?>

        <div class="box box-bordered box-color">
            <div class="box-title">
                <h3><i class=" icon-plus-sign"></i><?php echo $judul_form . " " . $sub_judul_form; ?> </h3>
            </div>
            <div class="box-content nopadding">
                <!-- <form action="#" method="POST" class='form-horizontal form-validate' id="bb"> -->
                <?php echo form_open('trx_pendataan/tambih_robih', array('name' => 'bb', 'id' => 'bb', 'class' => 'form-horizontal form-bordered form-validate form-wysiwyg', 'enctype' => 'multipart/form-data')); ?>


                <input type="hidden" name="id" id="id" value="<?php echo isset($field->id) ? $field->id : ''; ?>">
                <input type="hidden" name="<?= $this->security->get_csrf_token_name(); ?>" value="<?= $this->security->get_csrf_hash(); ?>" style="display: none">

                <input type="hidden" value="<?php echo $this->input->ip_address(); ?>" name="ip_address">
				
				<div class="control-group">
                    <label for="textfield" class="control-label">Hari ini</label>
                    <div class="controls">
                        <?php echo date("d-M-Y"); ?>
                    </div>
                </div>
				
				<div class="control-group">
                    <label for="textfield" class="control-label">Pilih Pelanggan</label>
                    <div class="controls">
                        <?php $id_pelanggan = isset($field->id_pelanggan) ? $field->id_pelanggan : $this->input->post("id_pelanggan"); ?>

                        

							<select class="input-xxlarge" name="id_pelanggan" required onchange="doView(this.value)">
							
							<option value="">-Pilih-</option>
							<?php foreach ($list_kategori as $row) { ?>
							<option value="<?php echo $row->id; ?>" <?php if ($id_pelanggan == $row->id) { echo "selected";} ?>><?php echo $row->nama; ?></option>
							<?php } ?>
							</select>
                       

                        



                        <span class="required-server"><?php echo form_error('uraian', '<p style="color:#F83A18">', '</p>'); ?></span>
                    </div>
                </div>
				
				
				
				
				
				<div class="control-group">
                    <label for="textfield" class="control-label">Meter Awal</label>
                    <div class="controls">
                        <input type="text"  id="m_before" class="input-small" name="m_before" value="50"  required>
                        <span class="required-server"><?php echo form_error('m_before', '<p style="color:#F83A18">', '</p>'); ?></span>
                    </div>
                </div>
				
				
				<div class="control-group">
										<label class="control-label" for="textfield">Meter Akhir</label>                                        
                                        <div class="controls">
                                        <input onchange="doHitung(this.value)"  type="number" placeholder="" id="m_after" class="input-small" name="m_after" value="<?php echo isset($field->m_after) ? $field->m_after : $this->input->post("m_after"); ?>"  >
										</div>
									</div>
				
				
				<div class="control-group">
                    <label for="textfield" class="control-label">Harga per-m3 </label>
                    <div class="controls">
					   <input readonly type="number" placeholder="" id="harga" class="input-small" name="harga" value="2000"  ><span class="required-server"><?php echo form_error('nokk', '<p style="color:#F83A18">', '</p>'); ?></span>
                    </div>
                </div>
				
				
				<div class="control-group">
                    <label for="textfield" class="control-label">Jumlah Pakai </label>
                    <div class="controls">
					   <input  readonly type="number" placeholder="" id="jml_pakai" class="input-small" name="jml_pakai" value="<?php echo isset($field->nokk) ? $field->nokk : $this->input->post("nokk"); ?>"  ><span class="required-server"><?php echo form_error('nokk', '<p style="color:#F83A18">', '</p>'); ?></span> m3
                    </div>
                </div>
				
				
				<div class="control-group">
                    <label for="textfield" class="control-label">Abudemen </label>
                    <div class="controls">
					   <input readonly type="number" placeholder="" id="abudemen" class="input-small" name="abudemen" value="10000"  ><span class="required-server"><?php echo form_error('nokk', '<p style="color:#F83A18">', '</p>'); ?></span>
                    </div>
                </div>
				
				
				<div class="control-group">
                    <label for="textfield" class="control-label">Subtotal </label>
                    <div class="controls">
					   <input readonly  type="number" placeholder="" id="subtot_pakai" class="input-small" name="subtot_pakai" value="<?php echo isset($field->nokk) ? $field->nokk : $this->input->post("nokk"); ?>"  ><span class="required-server"><?php echo form_error('nokk', '<p style="color:#F83A18">', '</p>'); ?></span> 
                    </div>
                </div>
				

                <div class="control-group">
                    <label class="control-label" for="textfield">Photo Bukti Pencatatan</label>
                    <div class="controls">
                        <!-- <input type="hidden" name="nip" id="nip" class="input-large" value=""> -->
                        <input type="file" name="berkas" id="berkas" accept="image/*">
                        <span class="required-server">
                            <p>*<small>File tipe gambar</small>
							<br/>*<small>Ukuran maksimal 1 MB</small>
							</p>
                        </span>

                        <?php if (isset($field->foto)) { ?>
                            <img src="<?php echo base_url(); ?>/<?php echo isset($field->foto) ? $field->foto : ''; ?>" >
                        <?php } ?>
                    </div>
                </div>


				<script language="javascript">
				
				function doHitung(m_after) {
					
					
					
					
					var m_before = document.getElementById("m_before").value;
					var harga = document.getElementById("harga").value;
					var abudemen = document.getElementById("abudemen").value;					
					var jml_pakai= m_after-m_before;
					
					if ( m_after < m_before) {
						alert("Jumlah Meter Akhir Yang Anda Masukan Kurang !");
						return false;
					}
					
					var hasil=jml_pakai*harga+10000;
					
					
					document.getElementById("jml_pakai").value=jml_pakai;
					document.getElementById("subtot_pakai").value=hasil;
				}
				
				
				function doView(str) {
					
					//alert(id_pelanggan);
					
					if (str.length == 0) {
						document.getElementById("txtHint").innerHTML = "";
						return;
					  } else {
						var xmlhttp = new XMLHttpRequest();
						xmlhttp.onreadystatechange = function() {
						  if (this.readyState == 4 && this.status == 200) {
							document.getElementById("txtHint").innerHTML = this.responseText;
						  }
						};
						xmlhttp.open("GET", "gethint.php?q=" + str, true);
						xmlhttp.send();
					  }
					
					
				}
				
				
				</script>





                <div class="form-actions">
                    <button id="btn_simpan" class="btn btn-blue" type="submit">Simpan</button>
                    <a class="btn btn-danger" href="<?php echo site_url(); ?>trx_pendataan">Kembali</a>
                </div>

                </form>
            </div>
        </div>
		
		
		<div class="span1">&nbsp;</div>
		
    </div>

