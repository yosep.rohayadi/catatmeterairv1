<?php if (!defined('BASEPATH')) exit('No direct script access allowed');



class Trx_pendataan extends MX_Controller
{
	//put your code here
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('utility');
		// 
		$this->truly_madly_deeply();

		Modules::run('ref_role/permissions');
		// $this->load->model('users_model');

		$this->load->helper('login');
		$this->load->helper('tool');
		$this->load->model('Site', 'site');
	}

	public function truly_madly_deeply()
	{

		if (!$this->session->userdata('truly_madly_deeply')) {
			redirect('signon');
		}
	}



	public function index()

	{
		
		
		get_role($this->session->userdata('sesi_bo_user_group'), 'insert');
		

		$data['judul_form'] = "Transaksi Catat Meter Air ";
		$data['sub_judul_form'] = "";
	
		$sql_kat = "select * from ref_pelanggan order by nama asc ";
		$query_kat = $this->db->query($sql_kat);
		$data["list_kategori"] = $query_kat->result();	
		
		$this->template->load('template_admin', 'v_add', $data);
	}

	

	public function detail($id_pelanggan)
	{
		
		$sql="select * from ref_pelanggan where id='$id_pelanggan'";
		$data['field_bio'] = $this->db->query($sql)->row();
		
		$sql2="SELECT * FROM trx_pendataan WHERE  id_pelanggan='$id_pelanggan'";
		$query2 = $this->db->query($sql2);
		$ceking=$query2->num_rows();
		
		if ($ceking==0) {
			
			$row = $this->db->query($sql)->row();
			$data["m_awal"]=$row->inisiasi_m_awal;
			//echo $m_awal;
		
		} else {
			
			$data["m_awal"]=0;
			
		}
		

		
		
		$this->load->view('v_trans', $data);
	}

	public function tambih_robih()
	{


		try {
			
			$id  					= $this->input->post('id');
		
			$data['id_pelanggan'] 			= $this->input->post('id_pelanggan');
			$data['m_before'] 			= $this->input->post('m_before');
			$data['m_after'] 			= $this->input->post('m_after');
			$data['m_after'] 			= $this->input->post('m_after');
			$data['harga'] 			= $this->input->post('harga');
			$data['jml_pakai'] 			= $this->input->post('jml_pakai');
			$data['subtot_pakai'] 			= $this->input->post('subtot_pakai');
			$data['jenis_pembayaran'] 			= $this->input->post('jenis_pembayaran');
			$data['abudemen'] 			= $this->input->post('abudemen');
			
			$data['tgl_transaksi'] = date('Y-m-d');

			$dir = './uploads/trx_pendataan/';
			if (!file_exists(FCPATH . $dir)) {
				mkdir(FCPATH . $dir, 0777, true);
			}

		
			
			
			if (isset($_FILES['berkas']) and is_uploaded_file($_FILES['berkas']['tmp_name'])) {
			  $folder = "uploads/trx_pendataan/";
			  $upload_image = $_FILES['berkas']['name'];
			  // tentukan ukuran width yang diharapkan
			  $width_size = 500;

			  // tentukan di mana image akan ditempatkan setelah diupload
			  $filesave = $folder . $upload_image;
			  move_uploaded_file($_FILES['berkas']['tmp_name'], $filesave);

			  // menentukan nama image setelah dibuat
			  $resize_image = $folder . "resize_location_" . uniqid(rand()) . ".jpg";

			  // mendapatkan ukuran width dan height dari image
			  list($width, $height) = getimagesize($filesave);

			  // mendapatkan nilai pembagi supaya ukuran skala image yang dihasilkan sesuai dengan aslinya
			  $k = $width / $width_size;

			  // menentukan width yang baru
			  $newwidth = $width / $k;

			  // menentukan height yang baru
			  $newheight = $height / $k;

			  // fungsi untuk membuat image yang baru
			  $thumb = imagecreatetruecolor($newwidth, $newheight);
			  $source = imagecreatefromjpeg($filesave);

			  // men-resize image yang baru
			  imagecopyresized($thumb, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);

			  // menyimpan image yang baru
			  imagejpeg($thumb, $resize_image);

			  imagedestroy($thumb);
			  imagedestroy($source);

			  unlink($filesave);

			  $data['foto'] = $resize_image;
			}



		


			if ($id == '' || $id == null) {
				get_role($this->session->userdata('sesi_bo_user_group'),'insert');	
				$data['kode_transaksi'] 			= time();
				$data['created_by'] = $this->session->userdata('sesi_bo_nama_lengkap');
				$data['created_date'] = date('Y-m-d H:i:s');
				$xss_data = $this->security->xss_clean($data);
				$this->db->insert('trx_pendataan', $xss_data);
				
				$this->session->set_flashdata('message_sukses', 'Transaksi Berhasil');
				
				 $insert_id = $this->db->insert_id();
				
				$sql="SELECT
B.nama,
B.alamat,
A.*
FROM
trx_pendataan A,
ref_pelanggan B
WHERE
A.id_pelanggan=B.id AND A.id='$insert_id'";
				$data['field_bio'] = $this->db->query($sql)->row();
				
				$this->template->load('template_admin', 'v_print', $data);
				

				
			} else {
				
				get_role($this->session->userdata('sesi_bo_user_group'),'update');	
				
				$data['update_by'] = $this->session->userdata('sesi_bo_nama_lengkap');
				$data['updated_date'] = date('Y-m-d H:i:s');
				$xss_data = $this->security->xss_clean($data);

				$this->db->where('id', $id);
				$this->db->update('trx_pendataan', $xss_data);
				
				$this->session->set_flashdata('message_sukses', 'Perubahan Data Berhasil Disimpan');
				redirect('trx_pendataan');

			}


			

		} catch (Exception $err) {
			log_message("error", $err->getMessage());
			return show_error($err->getMessage());
		}
	}
	
	
	
	
	
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
