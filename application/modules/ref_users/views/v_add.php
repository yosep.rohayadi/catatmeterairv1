<div class="container-fluid">
	<br>
	<div class="breadcrumbs">
		<ul>
			<?php foreach ($breadcrumbs as $key => $value) { ?>
				<li>
					<a href=<?php echo site_url($value['link']) ?>>
						<?php echo $value['name']; ?></a>
					<?php echo (count($breadcrumbs) - 1) == $key ? "" : "<i class='icon-angle-right'></i>"; ?>
				</li>
			<?php } ?>
		</ul>
		<div class="close-bread">
			<a href="#"><i class="icon-remove"></i></a>
		</div>
	</div>
</div>



<div class="row-fluid">
	<div class="span12">
		<div class="box">
			<div class="box-title">
				<h3><i class=" icon-plus-sign"></i><?php echo $judul_form . " " . $sub_judul_form; ?> </h3>
			</div>
			<div class="box-content">
				<!-- <form action="#" method="POST" class='form-horizontal form-validate' id="bb"> -->
				<?php echo form_open('ref_users/tambih_robih', array('name' => 'bb', 'id' => 'bb', 'class' => 'form-horizontal form-validate form-wysiwyg', 'enctype' => 'multipart/form-data')); ?>

				<?php
				if ($this->session->flashdata('message_gagal')) {
					echo '<hr><div class="alert alert-error"><button class="close" data-dismiss="alert" type="button">&times;</button>' . $this->session->flashdata('message_gagal') . '</div>';
				}
				if ($this->session->flashdata('message_sukses')) {
					echo '<hr><div class="alert alert-success"><button class="close" data-dismiss="alert" type="button">&times;</button>' . $this->session->flashdata('message_sukses') . '</div>';
				} ?>

				<input type="hidden" name="id_users" id="id_users" value="<?php echo isset($field->id) ? $field->id : ''; ?>">
				<input type="hidden" name="<?= $this->security->get_csrf_token_name(); ?>" value="<?= $this->security->get_csrf_hash(); ?>" style="display: none">

				<input type="hidden" value="<?php echo $this->input->ip_address(); ?>" name="ip_address">


				<div class="control-group">
					<label for="textfield" class="control-label">Username</label>
					<div class="controls">
						<?php $cusername = isset($field->id) ? $field->id : '';
						if ($cusername != "") {
							$ro = "readonly";
						} else {
							$ro = "";
						} ?>
						<input type="text" name="username_users" id="username_users" class="input-xxlarge" data-rule-required="true" value="<?php echo isset($field->username) ? $field->username : ''; ?>" <?php echo $ro; ?>>
					</div>
				</div>

				<div class="control-group">
					<label for="textfield" class="control-label">Nama</label>
					<div class="controls">
						<input type="text" name="nama_users" id="nama_users" class="input-xxlarge" data-rule-required="true" value="<?php echo isset($field->nama_lengkap) ? $field->nama_lengkap : ''; ?>">
					</div>
				</div>

				

				<div class="control-group">
					<label for="textfield" class="control-label">No. Handphone</label>
					<div class="controls">
						<div class="input-prepend">
							<span class="add-on"><i>+62</i></span>
							<input type="number" name="hp_users" id="hp_users" class="input-xlarge" maxlength="12" value="<?php echo isset($field->hp) ? $field->hp : ''; ?>">
						</div>
					</div>
				</div>

				<div class="control-group">
					<label for="textfield" class="control-label">Email</label>
					<div class="controls">
						<input type="email" name="email_users" id="email_users" class="input-xxlarge" value="<?php echo isset($field->email) ? $field->email : ''; ?>">
					</div>
				</div>

				<div class="control-group password">
					<label for="textfield" class="control-label">Password</label>
					<div class="controls">
						<input type="password" name="password_users" id="password_users" class="input-xxlarge" data-rule-required="true" value="<?php echo isset($field->password) ? $field->password : ''; ?>">
						<input type="hidden" name="password_users_text" id="password_users_text" class="input-xxlarge" data-rule-required="false" value="<?php echo isset($field->password) ? $field->password : ''; ?>">
					</div>
				</div>

				<div class="control-group">
					<label for="textfield" class="control-label">User Group</label>
					<div class="controls">
						<select name="group_users" class="input-xxlarge" data-rule-required="true" onchange="pilihGroup(this.value)">
							<option value="">-Pilih-</option>
							<?php $group_users = isset($field->id_user_group) ? $field->id_user_group : '';
							$query = $this->db->query("SELECT * from ref_user_group where id_user_group IN (4,9) order  by nama_user_group asc"); ?>
							<?php foreach ($query->result() as $row) { ?>
								<option value="<?php echo $row->id_user_group; ?>" <?php echo ($group_users == $row->id_user_group) ? 'selected' : "" ?>>
									<?php echo $row->nama_user_group; ?>
								</option>
							<?php } ?>
						</select>
					</div>
				</div>

				<?php $id_group = ($field->id_user_group ?? '')  ?>
				<?php $sts = ($id_group == 4 or $id_group == 9) ? true : false; ?>
				<div id="box_daerah" style="display: <?php echo $sts ? 'block' : 'none'  ?>;">
					<?php require_once(APPPATH . '/modules/form/views/v_form_filter_daerah_sampai_kelurahan.php'); ?>
				</div>


				


				


				<div class="control-group">
					<label for="textfield" class="control-label">Status Aktif</label>
					<div class="controls">
						<select name="status_aktif" required>
							<option value="">PILIH STATUS</option>
							<option value="Y" <?php if (isset($field->status_aktif)) {
													if ($field->status_aktif == 'Y') {
														echo "selected";
													}
												} ?>>Aktif</option>
							<option value="N" <?php if (isset($field->status_aktif)) {
													if ($field->status_aktif == 'N') {
														echo "selected";
													}
												} ?>>Tidak Aktif</option>
						</select>
					</div>
				</div>



				<div class="form-actions">
					<button class="btn btn-primary" type="submit">Simpan</button>
					<a class="btn btn-danger" href="<?php echo site_url(); ?>ref_users/">Kembali</a>
				</div>

				</form>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		$('.select2').select2();

		if ($("#provinsi").val() == '') {
			$('#kokab').attr("disabled", true);
		} else {
			getKabProv($("#provinsi").val());
		}

		if ('<?php echo ($field->id_user_group ?? '') ?>' != '') {
			pilihGroup(<?php echo ($field->id_user_group ?? '') ?>);
		}
	});

	function getKabProv(idprov) {

		$('#kokab').empty();
		$.ajax({
			url: '<?php echo site_url('master/get_kokab_prov'); ?>/' + idprov,
			dataType: 'json',
			success: function(data) {
				$('#kokab').attr("disabled", false);
				$('#kokab').append(data);
				if ($('#id_kabkot_db').val() != '') {
					$('#kokab').val($('#id_kabkot_db').val());
				}
			}
		});
	}

	function pilihGroup(idGroup) {
		
		if (idGroup == 10) {
			// ADMIN PROVINSI
			$('#box_daerah').show();
			$('#id_provinsi').prop('disabled', false);
			$('#id_kabkot').prop('disabled', true);
			$('#id_kecamatan').prop('disabled', true);
			$('#id_kelurahan').prop('disabled', true);

			$('#id_provinsi').prop('required', true);
			$('#id_kabkot').prop('required', false);
			$('#id_kecamatan').prop('required', false);
			$('#id_kelurahan').prop('required', false);

		} else if (idGroup == 5) {
			// ADMIN KAB/KOT
			$('#box_daerah').show();
			$('#id_provinsi').prop('disabled', false);
			$('#id_kabkot').prop('disabled', false);
			$('#id_kecamatan').prop('disabled', true);
			$('#id_kelurahan').prop('disabled', true);

			$('#id_provinsi').prop('required', true);
			$('#id_kabkot').prop('required', true);
			$('#id_kecamatan').prop('required', false);
			$('#id_kelurahan').prop('required', false);

		} else if (idGroup == 9) {
			// ADMIN KECAMATAN
			$('#box_daerah').show();
			$('#id_provinsi').prop('disabled', false);
			$('#id_kabkot').prop('disabled', false);
			$('#id_kecamatan').prop('disabled', false);
			$('#id_kelurahan').prop('disabled', false);

			$('#id_provinsi').prop('required', true);
			$('#id_kabkot').prop('required', true);
			$('#id_kecamatan').prop('required', true);
			//$('#id_kelurahan').prop('disabled', true);

		} else if (idGroup == 4) {
			// ADMIN DAERAH
			$('#box_daerah').show();
			$('#id_provinsi').prop('disabled', false);
			$('#id_kabkot').prop('disabled', false);
			$('#id_kecamatan').prop('disabled', false);
			$('#id_kelurahan').prop('disabled', false);

			$('#id_provinsi').prop('required', true);
			$('#id_kabkot').prop('required', true);
			$('#id_kecamatan').prop('required', true);
			$('#id_kelurahan').prop('required', true);
			
		} else {
			$('#box_daerah').hide();
			$('#id_provinsi').prop('required', false);
			$('#id_kabkot').prop('required', false);
			$('#id_kecamatan').prop('required', false);
			$('#id_kelurahan').prop('required', false);
		}
	}

	function pilihKabKot(idKabKota) {
		$('#kabkot').show();
		var url = '<?php echo site_url(); ?>master/get_kab_kot/' + idKabKota;

		$.ajax({
			url: url,
			method: 'GET',
			success: function(response) {
				$('#kabkot').html(response);
			}
		});

	}
</script>