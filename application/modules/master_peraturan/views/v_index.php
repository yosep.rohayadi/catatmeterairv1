<div class="container-fluid">
	<br>
	<div class="breadcrumbs">
		<ul>
			<?php foreach ($breadcrumbs as $key => $value) { ?>
				<li>
					<a href=<?php echo site_url($value['link']) ?>> <?php echo $value['name']; ?></a>
					<?php echo (count($breadcrumbs) - 1) == $key ? "" : "<i class='icon-angle-right'></i>"; ?>
				</li>
			<?php } ?>
		</ul>

		<div class="close-bread">
			<a href="#"><i class="icon-remove"></i></a>
		</div>
	</div>
</div>

<div class="row-fluid">
	<div class="span12">
		<div class="box">
			<div class="box-title">
				<h3>
					<i class="icon-reorder"></i>
					<?php echo $sub_judul_form; ?>
				</h3>
			</div>

			<div class="box-content">
				<div align="right" style="margin-bottom: 20px;">
					<a href="<?php echo base_url("master_peraturan/add") ?>" class="btn btn-sm btn-primary">
						Tambah File Peraturan
					</a>
				</div>
				<div class="table-responsive">
					<table width="100%" class="table table-hover table-bordered">
						<thead>
							<tr>
								<th>
									<center>#</center>
								</th>
								<th>
									<center>Judul</center>
								</th>
								<th>
									<center>Files</center>
								</th>
								<th style="width: 150px;">
									<center>Aksi</center>
								</th>
							</tr>
						</thead>
						<tbody>
							<?php if (count($listData) > 0) { ?>
								<?php foreach ($listData as $key => $value) { ?>
									<tr>
										<td>
											<center><?php echo ($key + 1) ?></center>
										</td>
										<td>
											<?php echo $value['nama'] ?>
										</td>
										<td>
											<center>
												<a target="_blank" href="<?php echo base_url('uploads/files/' . $value['file']) ?>">Lihat File</a>
											</center>
										</td>
										<td>
											<center>
												<a href="<?php echo base_url("master_peraturan/edit/" . $value['id']) ?>" class="btn btn-warning">Edit</a>
												<a onclick="return confirm('Yakin hapus?')" href="<?php echo base_url("master_peraturan/delete/" . enCrypt($value['id'])) ?>" class="btn btn-danger">Hapus</a>
											</center>
										</td>
									</tr>
								<?php } ?>
							<?php } else { ?>
								<tr>
									<td colspan="10">Belum ada data.</td>
								</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>