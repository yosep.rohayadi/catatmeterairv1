<div class="container-fluid">
	<br>

	<div class="breadcrumbs">
		<ul>
			<?php foreach ($breadcrumbs as $key => $value) { ?>
				<li>
					<a href=<?php echo site_url($value['link']) ?>>
						<?php echo $value['name']; ?></a>
					<?php echo (count($breadcrumbs) - 1) == $key ? "" : "<i class='icon-angle-right'></i>"; ?>
				</li>
			<?php } ?>
		</ul>
		<div class="close-bread">
			<a href="#"><i class="icon-remove"></i></a>
		</div>
	</div>
</div>


<div class="row-fluid">
	<div class="span12">
		<div class="box">
			<div class="box-title">
				<h3><i class=" icon-plus-sign"></i><?php echo $judul_form . " " . $sub_judul_form; ?> </h3>
			</div>
			<div class="box-content">

				<?php echo form_open('master_peraturan/submit', array('name' => 'bb', 'id' => 'bb', 'class' => 'form-horizontal form-validate form-wysiwyg', 'enctype' => 'multipart/form-data')); ?>

				<?php
				if ($this->session->flashdata('message_gagal')) {
					echo '<hr><div class="alert alert-error"><button class="close" data-dismiss="alert" type="button">&times;</button>' . $this->session->flashdata('message_gagal') . '</div>';
				}
				if ($this->session->flashdata('message_sukses')) {
					echo '<hr><div class="alert alert-success"><button class="close" data-dismiss="alert" type="button">&times;</button>' . $this->session->flashdata('message_sukses') . '</div>';
				} ?>

				<input type="hidden" name="id" id="id" value="<?php echo $data['id'] ?? null ?>">

				<div class="control-group">
					<label for="textfield" class="control-label">Nama Peraturan *</label>
					<div class="controls">
						<textarea class="input-xxlarge" name="nama" required><?php echo $data['nama'] ?? '' ?></textarea>
					</div>
				</div>

				<div class="control-group">
					<label for="textfield" class="control-label">File *</label>
					<div class="controls">
						<input type="file" name="file" id="file" class="input-xxlarge">
						<div>
							<small>Hanya pdf maksimal 5MB</small>
						</div>
						<?php if (($data['file'] ?? '') != '') { ?>
							<div style="margin-top: 10px;">
								<a href="<?php echo base_url("uploads/files/" . $data['file']); ?>" target="_blank">
									Lihat File Lama
								</a>
							</div>
						<?php  } ?>
					</div>
				</div>

				<div class="form-actions">
					<button class="btn btn-primary" type="submit">Simpan</button>
					<a class="btn btn-danger" href="<?php echo site_url(); ?>master_peraturan/">Kembali</a>
				</div>

				</form>
			</div>
		</div>
	</div>
</div>