<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Master_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}


	public function listData()
	{
		return $this->db->get("master_peraturan")->result_array();
	}


	public function getData($id)
	{
		$this->db->where('id', $id);
		return $this->db->get("master_peraturan")->row_array();
	}
}
