<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


class Master_peraturan extends MX_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->truly_madly_deeply();
		Modules::run('ref_role/permissions');
		$this->load->model('Master_model');
		$this->load->helper('tool');
	}


	public function truly_madly_deeply()
	{

		if (!$this->session->userdata('truly_madly_deeply')) {
			redirect('signon');
		}
	}


	public function index($offset = 0)
	{
		$data['breadcrumbs'] = array(
			array(
				'link' => '#',
				'name' => 'Home'
			),
			array(
				'link' => '#',
				'name' => 'Master Peraturan'
			)
		);

		$data['listData'] = $this->Master_model->listData();
		$data['role'] = get_role($this->session->userdata('sesi_bo_user_group'));
		$data['sub_judul_form'] = "Master Peraturan";
		$this->template->load('template_admin', 'v_index', $data);
	}



	public function add()
	{
		get_role($this->session->userdata('sesi_bo_user_group'), 'insert');
		$data['judul_form'] = "Tambah Data";
		$data['sub_judul_form'] = "File Peraturan";

		$data['breadcrumbs'] =
			array(
				array(
					'link' => '#',
					'name' => 'Settings'
				),
				array(
					'link' => 'master_peraturan',
					'name' => 'Master Peraturan'
				),
				array(
					'link' => '#',
					'name' => 'Tambah Data'
				)
			);

		$this->template->load('template_admin', 'v_add', $data);
	}



	public function submit()
	{
		try {
			$id = $this->input->post('id');
			$data = [
				'nama'	=> $this->input->post("nama"),
			];

			// Upload
			$dir = './uploads/files/';
			if (!file_exists(FCPATH . $dir)) {
				mkdir(FCPATH . $dir, 0777, true);
			}

			if (isset($_FILES['file'])) {
				if ($_FILES['file']['name'] != '') {
					$config['upload_path']          = $dir;
					$config['allowed_types']        = 'pdf';
					$config['overwrite']            = true;
					$config['max_size']             = 5024; // 5MB
					$config['encrypt_name']         = TRUE;
					$this->load->library('upload', $config);

					if ($this->upload->do_upload('file')) {
						$data['file'] = $this->upload->data("file_name");
					} else {
						$this->session->set_flashdata('message_sukses', $this->upload->display_errors());
						return redirect(base_url("master_peraturan/add"));
					}
				}
			}

			if ($id == '' || $id == null) {
				get_role($this->session->userdata('sesi_bo_user_group'), 'insert');

				$this->db->set($data);
				$this->db->insert("master_peraturan");
				$id =  $this->db->insert_id();
				$this->session->set_flashdata('message_sukses', 'Data Berhasil Disimpan');
			} else {
				get_role($this->session->userdata('sesi_bo_user_group'), 'update');

				$this->db->where('id', $id);
				$this->db->set($data);
				$this->db->update('master_peraturan');
				$this->session->set_flashdata('message_sukses', 'Perubahan Data Berhasil Disimpan');
			}
			return redirect(base_url("master_peraturan/edit/" . $id));
		} catch (Exception $err) {
			log_message("error", $err->getMessage());
			return show_error($err->getMessage());
		}
	}


	public function edit($id)
	{
		get_role($this->session->userdata('sesi_bo_user_group'), 'update');
		$data['judul_form'] = "Edit Data";
		$data['sub_judul_form'] = "File Peraturan";
		$data['data'] = $this->Master_model->getData($id);
		$data['breadcrumbs'] =
			array(
				array(
					'link' => '#',
					'name' => 'Settings'
				),
				array(
					'link' => 'master_peraturan',
					'name' => 'Master Peraturan'
				),
				array(
					'link' => '#',
					'name' => 'Edit Data'
				)
			);

		$this->template->load('template_admin', 'v_add', $data);
	}


	public function delete($id)
	{
		get_role($this->session->userdata('sesi_bo_user_group'), 'delete');
		$id = deCrypt($id);
		$this->db->where('id', $id);
		$this->db->delete('master_peraturan');

		return redirect(base_url("master_peraturan"));
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
