<div class="container">
	<div class="page-header">
		<div class="pull-left">
			<h1>REKAPITULASI PEMBAYARAN REKENING AIR</h1>
		</div>
		<div class="pull-right">

			<ul class="stats">

				<li class='lightred'>
					<i class="icon-calendar"></i>
					<div class="details">
						<span class="big">February 22, 2013</span>
						<span>Wednesday, 13:56</span>
					</div>
				</li>
			</ul>
		</div>
	</div>

	<div class="row-fluid">
	  <table width="100%" border="1" cellpadding="0" cellspacing="0">
        <col width="35" />
        <col width="79" />
        <col width="191" />
        <col width="42" span="3" />
        <col width="117" />
        <tr>
          <td width="4%">NO</td>
          <td width="9%">NOPEL</td>
          <td width="35%">NAMA PELANGGAN</td>
          <td width="8%">AWAL</td>
          <td width="10%">AKHIR</td>
          <td width="9%">PAKAI</td>
          <td width="25%">TOTAL BAYAR</td>
        </tr>
        <tr>
          <td>1</td>
          <td>0001</td>
          <td>SUTRISNO</td>
          <td>612</td>
          <td>623</td>
          <td>11</td>
          <td>&nbsp;Rp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    21,500.00 </td>
        </tr>
        <tr>
          <td>2</td>
          <td>0002</td>
          <td>MAFTUHIN</td>
          <td>545</td>
          <td>561</td>
          <td>16</td>
          <td>&nbsp;Rp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    29,000.00 </td>
        </tr>
        <tr>
          <td>3</td>
          <td>0003</td>
          <td>SUWARDI</td>
          <td>585</td>
          <td>595</td>
          <td>10</td>
          <td>&nbsp;Rp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    20,000.00 </td>
        </tr>
        <tr>
          <td>4</td>
          <td>0004</td>
          <td>MARDIYANTO</td>
          <td>237</td>
          <td>242</td>
          <td>5</td>
          <td>&nbsp;Rp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    12,500.00 </td>
        </tr>
        <tr>
          <td>5</td>
          <td>0005</td>
          <td>KHOLID</td>
          <td>379</td>
          <td>389</td>
          <td>10</td>
          <td>&nbsp;Rp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    20,000.00 </td>
        </tr>
        <tr>
          <td>6</td>
          <td>0006</td>
          <td>SLAMET SUROTUN</td>
          <td>693</td>
          <td>707</td>
          <td>14</td>
          <td>&nbsp;Rp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    26,000.00 </td>
        </tr>
        <tr>
          <td>7</td>
          <td>0007</td>
          <td>AGUS SUTONO</td>
          <td>637</td>
          <td>657</td>
          <td>20</td>
          <td>&nbsp;Rp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    35,000.00 </td>
        </tr>
        <tr>
          <td>8</td>
          <td>0008</td>
          <td>MARYONO</td>
          <td>181</td>
          <td>184</td>
          <td>3</td>
          <td>&nbsp;Rp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 9,500.00 </td>
        </tr>
        <tr>
          <td>9</td>
          <td>0009</td>
          <td>RUSMIN</td>
          <td>669</td>
          <td>686</td>
          <td>17</td>
          <td>&nbsp;Rp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    30,500.00 </td>
        </tr>
        <tr>
          <td>10</td>
          <td>0010</td>
          <td>MUNTARI</td>
          <td>194</td>
          <td>203</td>
          <td>9</td>
          <td>&nbsp;Rp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    18,500.00 </td>
        </tr>
        <tr>
          <td>11</td>
          <td>0011</td>
          <td>MUHSINUN</td>
          <td>124</td>
          <td>128</td>
          <td>4</td>
          <td>&nbsp;Rp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    11,000.00 </td>
        </tr>
        <tr>
          <td>12</td>
          <td>0012</td>
          <td>ROSIDI</td>
          <td>104</td>
          <td>112</td>
          <td>8</td>
          <td>&nbsp;Rp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    17,000.00 </td>
        </tr>
        <tr>
          <td>13</td>
          <td>0013</td>
          <td>AHMAD ARIFIN</td>
          <td>368</td>
          <td>375</td>
          <td>7</td>
          <td>&nbsp;Rp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    15,500.00 </td>
        </tr>
        <tr>
          <td>14</td>
          <td>0014</td>
          <td>MAHMUD</td>
          <td>504</td>
          <td>518</td>
          <td>14</td>
          <td>&nbsp;Rp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    26,000.00 </td>
        </tr>
        <tr>
          <td>15</td>
          <td>0015</td>
          <td>ROKHANI</td>
          <td>969</td>
          <td>999</td>
          <td>30</td>
          <td>&nbsp;Rp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    50,000.00 </td>
        </tr>
        <tr>
          <td>16</td>
          <td>0016</td>
          <td>MUNAWIR</td>
          <td>501</td>
          <td>517</td>
          <td>16</td>
          <td>&nbsp;Rp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    29,000.00 </td>
        </tr>
        <tr>
          <td>17</td>
          <td>0017</td>
          <td>RADAB</td>
          <td>702</td>
          <td>723</td>
          <td>21</td>
          <td>&nbsp;Rp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    36,500.00 </td>
        </tr>
        <tr>
          <td>18</td>
          <td>0018</td>
          <td>NUROHMAN</td>
          <td>321</td>
          <td>336</td>
          <td>15</td>
          <td>&nbsp;Rp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    27,500.00 </td>
        </tr>
        <tr>
          <td>19</td>
          <td>0019</td>
          <td>SUROTO</td>
          <td>466</td>
          <td>488</td>
          <td>22</td>
          <td>&nbsp;Rp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    38,000.00 </td>
        </tr>
        <tr>
          <td>20</td>
          <td>0020</td>
          <td>BASORI</td>
          <td>473</td>
          <td>485</td>
          <td>12</td>
          <td>&nbsp;Rp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    23,000.00 </td>
        </tr>
        <tr>
          <td>21</td>
          <td>0021</td>
          <td>JUHRI</td>
          <td>431</td>
          <td>443</td>
          <td>12</td>
          <td>&nbsp;Rp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 8,000.00 </td>
        </tr>
        <tr>
          <td>22</td>
          <td>0022</td>
          <td>DARMANTO</td>
          <td>909</td>
          <td>937</td>
          <td>28</td>
          <td>&nbsp;Rp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    47,000.00 </td>
        </tr>
        <tr>
          <td>23</td>
          <td>0023</td>
          <td>SUWENING TRI WIDODO</td>
          <td>881</td>
          <td>903</td>
          <td>22</td>
          <td>&nbsp;Rp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    23,000.00 </td>
        </tr>
        <tr>
          <td>24</td>
          <td>0024</td>
          <td>MUHAMAD JUPRI</td>
          <td>864</td>
          <td>882</td>
          <td>18</td>
          <td>&nbsp;Rp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    32,000.00 </td>
        </tr>
        <tr>
          <td>25</td>
          <td>0025</td>
          <td>TASRI JONO</td>
          <td>339</td>
          <td>350</td>
          <td>11</td>
          <td>&nbsp;Rp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    21,500.00 </td>
        </tr>
        <tr>
          <td>26</td>
          <td>0026</td>
          <td>SURADI</td>
          <td>381</td>
          <td>408</td>
          <td>27</td>
          <td>&nbsp;Rp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    45,500.00 </td>
        </tr>
        <tr>
          <td>27</td>
          <td>0027</td>
          <td>MUH ARWANI</td>
          <td>161</td>
          <td>168</td>
          <td>7</td>
          <td>&nbsp;Rp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    15,500.00 </td>
        </tr>
        <tr>
          <td>28</td>
          <td>0028</td>
          <td>RIFANUDDIN</td>
          <td>848</td>
          <td>872</td>
          <td>24</td>
          <td>&nbsp;Rp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    26,000.00 </td>
        </tr>
        <tr>
          <td>29</td>
          <td>0029</td>
          <td>ARIPIN</td>
          <td>342</td>
          <td>348</td>
          <td>6</td>
          <td>&nbsp;Rp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 5,000.00 </td>
        </tr>
        <tr>
          <td>30</td>
          <td>0030</td>
          <td>SUTONO</td>
          <td>1217</td>
          <td>1243</td>
          <td>26</td>
          <td>&nbsp;Rp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    44,000.00 </td>
        </tr>
        <tr>
          <td>31</td>
          <td>0031</td>
          <td>SAHRONI</td>
          <td>242</td>
          <td>253</td>
          <td>11</td>
          <td>&nbsp;Rp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    21,500.00 </td>
        </tr>
        <tr>
          <td>32</td>
          <td>0032</td>
          <td>ANSORI</td>
          <td>179</td>
          <td>188</td>
          <td>9</td>
          <td>&nbsp;Rp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    18,500.00 </td>
        </tr>
        <tr>
          <td>33</td>
          <td>0033</td>
          <td>SLAMET FAUZI</td>
          <td>298</td>
          <td>312</td>
          <td>14</td>
          <td>&nbsp;Rp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    26,000.00 </td>
        </tr>
        <tr>
          <td>34</td>
          <td>0034</td>
          <td>MAHFUDZ</td>
          <td>157</td>
          <td>167</td>
          <td>10</td>
          <td>&nbsp;Rp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    20,000.00 </td>
        </tr>
        <tr>
          <td>35</td>
          <td>0035</td>
          <td>LUKMAN HAKIM</td>
          <td>119</td>
          <td>131</td>
          <td>12</td>
          <td>&nbsp;Rp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    23,000.00 </td>
        </tr>
        <tr>
          <td>36</td>
          <td>0036</td>
          <td>IRHAMNI</td>
          <td>18</td>
          <td>21</td>
          <td>3</td>
          <td>&nbsp;Rp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 9,500.00 </td>
        </tr>
        <tr>
          <td>37</td>
          <td>0037</td>
          <td>BASIRUN</td>
          <td>4</td>
          <td>9</td>
          <td>5</td>
          <td>&nbsp;Rp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    12,500.00 </td>
        </tr>
        <tr>
          <td>38</td>
          <td>0038</td>
          <td>ABDUL ALI HAJAD</td>
          <td>4</td>
          <td>22</td>
          <td>18</td>
          <td>&nbsp;Rp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    32,000.00 </td>
        </tr>
        <tr>
          <td>39</td>
          <td>0051</td>
          <td>NGATEMAN</td>
          <td>2</td>
          <td>2</td>
          <td>0</td>
          <td>&nbsp;Rp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 5,000.00 </td>
        </tr>
        <tr>
          <td>40</td>
          <td>0052</td>
          <td>SURATMAN</td>
          <td>1</td>
          <td>4</td>
          <td>3</td>
          <td>&nbsp;Rp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 9,500.00 </td>
        </tr>
        <tr>
          <td>41</td>
          <td>0053</td>
          <td>MUHTAROM</td>
          <td>2</td>
          <td>2</td>
          <td>0</td>
          <td>&nbsp;Rp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 5,000.00 </td>
        </tr>
        <tr>
          <td>42</td>
          <td>0054</td>
          <td>NASRUL AMIN</td>
          <td>1</td>
          <td>1</td>
          <td>0</td>
          <td>&nbsp;Rp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 5,000.00 </td>
        </tr>
        <tr>
          <td>43</td>
          <td>0055</td>
          <td>JUBAIDI</td>
          <td>2</td>
          <td>3</td>
          <td>1</td>
          <td>&nbsp;Rp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 6,500.00 </td>
        </tr>
        <tr>
          <td>44</td>
          <td>0056</td>
          <td>MUH SININ</td>
          <td>0</td>
          <td>5</td>
          <td>5</td>
          <td>&nbsp;Rp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    12,500.00 </td>
        </tr>
        <tr>
          <td>45</td>
          <td>0057</td>
          <td>KOLID</td>
          <td>0</td>
          <td>1</td>
          <td>1</td>
          <td>&nbsp;Rp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 6,500.00 </td>
        </tr>
        <tr>
          <td>46</td>
          <td>0058</td>
          <td>MUHAMAD SOLIKIN</td>
          <td>2</td>
          <td>2</td>
          <td>0</td>
          <td>&nbsp;Rp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 5,000.00 </td>
        </tr>
        <tr>
          <td>47</td>
          <td>0059</td>
          <td>SAEFUL MUJAB</td>
          <td>0</td>
          <td>1</td>
          <td>1</td>
          <td>&nbsp;Rp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 6,500.00 </td>
        </tr>
        <tr>
          <td>48</td>
          <td>0060</td>
          <td>MARDI</td>
          <td>1</td>
          <td>9</td>
          <td>8</td>
          <td>&nbsp;Rp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    17,000.00 </td>
        </tr>
        <tr>
          <td>49</td>
          <td>0061</td>
          <td>SUWARLAN</td>
          <td>0</td>
          <td>9</td>
          <td>9</td>
          <td>&nbsp;Rp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    18,500.00 </td>
        </tr>
        <tr>
          <td>50</td>
          <td>0062</td>
          <td>KASTOLANI KURDI</td>
          <td>1</td>
          <td>3</td>
          <td>2</td>
          <td>&nbsp;Rp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 8,000.00 </td>
        </tr>
        <tr>
          <td>51</td>
          <td>0063</td>
          <td>REMANTO</td>
          <td>3</td>
          <td>6</td>
          <td>3</td>
          <td>&nbsp;Rp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 9,500.00 </td>
        </tr>
        <tr>
          <td>52</td>
          <td>0064</td>
          <td>PURWANTO</td>
          <td>3</td>
          <td>4</td>
          <td>1</td>
          <td>&nbsp;Rp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 6,500.00 </td>
        </tr>
        <tr>
          <td>53</td>
          <td>0065</td>
          <td>JASMAN</td>
          <td>0</td>
          <td>0</td>
          <td>0</td>
          <td>&nbsp;Rp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 5,000.00 </td>
        </tr>
        <tr>
          <td>54</td>
          <td>0101</td>
          <td>MADRASAH DINIYYAH</td>
          <td>137</td>
          <td>138</td>
          <td>1</td>
          <td>&nbsp;Rp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 6,500.00 </td>
        </tr>
        <tr>
          <td>55</td>
          <td>0102</td>
          <td>SUPRIYANTO</td>
          <td>1</td>
          <td>1</td>
          <td>0</td>
          <td>&nbsp;Rp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 5,000.00 </td>
        </tr>
        <tr>
          <td>56</td>
          <td>0103</td>
          <td>ZULLY WIDYA YANTI</td>
          <td>2</td>
          <td>9</td>
          <td>7</td>
          <td>&nbsp;Rp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    15,500.00 </td>
        </tr>
        <tr>
          <td>57</td>
          <td>0104</td>
          <td>SOMYANI</td>
          <td>2</td>
          <td>8</td>
          <td>6</td>
          <td>&nbsp;Rp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    14,000.00 </td>
        </tr>
        <tr>
          <td>58</td>
          <td>0105</td>
          <td>IRMA FATIHATUR ROHMAH</td>
          <td>1</td>
          <td>2</td>
          <td>1</td>
          <td>&nbsp;Rp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 6,500.00 </td>
        </tr>
        <tr>
          <td>59</td>
          <td>0106</td>
          <td>MISBACHUL MUNIR</td>
          <td>3</td>
          <td>8</td>
          <td>5</td>
          <td>&nbsp;Rp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    12,500.00 </td>
        </tr>
        <tr>
          <td>60</td>
          <td>0107</td>
          <td>PADMONO</td>
          <td>3</td>
          <td>10</td>
          <td>7</td>
          <td>&nbsp;Rp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    15,500.00 </td>
        </tr>
        <tr>
          <td>61</td>
          <td>0108</td>
          <td>MUH.FADILAH</td>
          <td>1</td>
          <td>8</td>
          <td>7</td>
          <td>&nbsp;Rp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    15,500.00 </td>
        </tr>
        <tr>
          <td>62</td>
          <td>0109</td>
          <td>KARSIN</td>
          <td>0</td>
          <td>6</td>
          <td>6</td>
          <td>&nbsp;Rp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    14,000.00 </td>
        </tr>
        <tr>
          <td>63</td>
          <td>0110</td>
          <td>ROHMAD</td>
          <td>1</td>
          <td>2</td>
          <td>1</td>
          <td>&nbsp;Rp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 6,500.00 </td>
        </tr>
        <tr>
          <td>64</td>
          <td>0111</td>
          <td>MUHAMMAD SHOLIKHIN</td>
          <td>0</td>
          <td>1</td>
          <td>1</td>
          <td>&nbsp;Rp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 6,500.00 </td>
        </tr>
        <tr>
          <td>65</td>
          <td>0112</td>
          <td>SYAHRI</td>
          <td>2</td>
          <td>19</td>
          <td>17</td>
          <td>&nbsp;Rp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    30,500.00 </td>
        </tr>
        <tr>
          <td>66</td>
          <td>0113</td>
          <td>NURROKHIM</td>
          <td>4</td>
          <td>5</td>
          <td>1</td>
          <td>&nbsp;Rp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 6,500.00 </td>
        </tr>
        <tr>
          <td>67</td>
          <td>0114</td>
          <td>AMIR</td>
          <td>1</td>
          <td>1</td>
          <td>0</td>
          <td>&nbsp;Rp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 5,000.00 </td>
        </tr>
        <tr>
          <td>68</td>
          <td>0115</td>
          <td>MUKMININ</td>
          <td>3</td>
          <td>5</td>
          <td>2</td>
          <td>&nbsp;Rp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 8,000.00 </td>
        </tr>
        <tr>
          <td>69</td>
          <td>0116</td>
          <td>SUTINI</td>
          <td>1</td>
          <td>3</td>
          <td>2</td>
          <td>&nbsp;Rp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    11,000.00 </td>
        </tr>
        <tr>
          <td>70</td>
          <td>0117</td>
          <td>SAEFUDIN</td>
          <td>3</td>
          <td>9</td>
          <td>6</td>
          <td>&nbsp;Rp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    14,000.00 </td>
        </tr>
        <tr>
          <td>71</td>
          <td>0118</td>
          <td>MUNIP</td>
          <td>1</td>
          <td>3</td>
          <td>2</td>
          <td>&nbsp;Rp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 8,000.00 </td>
        </tr>
        <tr>
          <td>72</td>
          <td>0119</td>
          <td>SUYADI</td>
          <td>3</td>
          <td>4</td>
          <td>1</td>
          <td>&nbsp;Rp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 6,500.00 </td>
        </tr>
        <tr>
          <td>73</td>
          <td>0120</td>
          <td>SAMRODIN</td>
          <td>2</td>
          <td>15</td>
          <td>13</td>
          <td>&nbsp;Rp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    24,500.00 </td>
        </tr>
        <tr>
          <td>74</td>
          <td>0151</td>
          <td>SURYADI</td>
          <td>1</td>
          <td>6</td>
          <td>5</td>
          <td>&nbsp;Rp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    12,500.00 </td>
        </tr>
        <tr>
          <td>75</td>
          <td>0152</td>
          <td>SAMSUDIN</td>
          <td>1</td>
          <td>2</td>
          <td>1</td>
          <td>&nbsp;Rp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 6,500.00 </td>
        </tr>
        <tr>
          <td>76</td>
          <td>0153</td>
          <td>AFIF SULISTIYO</td>
          <td>0</td>
          <td>0</td>
          <td>0</td>
          <td>&nbsp;Rp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 5,000.00 </td>
        </tr>
        <tr>
          <td>77</td>
          <td>0154</td>
          <td>SDN TINGKIR TENGAH 02</td>
          <td>0</td>
          <td>0</td>
          <td>0</td>
          <td>&nbsp;Rp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 5,000.00 </td>
        </tr>
        <tr>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td>TOTAL :</td>
          <td>653</td>
          <td>&nbsp;Rp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1,313,500.00 </td>
        </tr>
        <tr>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
        <tr>
          <td></td>
          <td></td>
          <td></td>
          <td colspan="2">BAYAR</td>
          <td colspan="2">&nbsp;Rp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 928,500.00 </td>
        </tr>
        <tr>
          <td></td>
          <td></td>
          <td></td>
          <td colspan="2">BEBAN</td>
          <td colspan="2">&nbsp;Rp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 385,000.00 </td>
        </tr>
        <tr>
          <td></td>
          <td></td>
          <td></td>
          <td colspan="2">TOTAL</td>
          <td colspan="2">&nbsp;Rp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1,313,500.00 </td>
        </tr>
        <tr>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
        <tr>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
      </table>
	  <hr>
		

		
		
	</div>




</div>