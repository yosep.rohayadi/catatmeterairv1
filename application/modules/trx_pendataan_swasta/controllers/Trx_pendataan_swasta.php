<?php if (!defined('BASEPATH')) exit('No direct script access allowed');



class Trx_pendataan_swasta extends MX_Controller
{
	//put your code here
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('utility');
		// 
		$this->truly_madly_deeply();

		Modules::run('ref_role/permissions');
		// $this->load->model('users_model');

		$this->load->helper('login');
		$this->load->helper('tool');
	}

	public function truly_madly_deeply()
	{

		if (!$this->session->userdata('truly_madly_deeply')) {
			redirect('signon');
		}
	}



	public function index($offset = 0)

	{
		
	



				

				
				
				$sesi_bo_user_group = $this->session->userdata('sesi_bo_user_group');
				
				
				
				
				if (isset($_POST['cari_personal'])) {
					$data1 = array('s_cari_personal' => $_POST['cari_personal']);
					$this->session->set_userdata($data1);
				}
				
				
				if (isset($_POST['id_kategori'])) {
					$data1 = array('s_id_kategori' => $_POST['id_kategori']);
					$this->session->set_userdata($data1);
				}
				
				if (isset($_POST['status_keberadaan'])) {
					$data1 = array('s_status_keberadaan' => $_POST['status_keberadaan']);
					$this->session->set_userdata($data1);
				}
				
				
				if (isset($_POST['status_bantuan'])) {
					$data1 = array('s_status_bantuan' => $_POST['status_bantuan']);
					$this->session->set_userdata($data1);
				}
				
				if (isset($_POST['approved'])) {
					$data1 = array('s_approved' => $_POST['approved']);
					$this->session->set_userdata($data1);
				}
				
				if (isset($_POST['tgl_register1'])) {
					$data1 = array('s_tgl_register1' => $_POST['tgl_register1']);
					$this->session->set_userdata($data1);
				}
				
				if (isset($_POST['tgl_register2'])) {
					$data1 = array('s_tgl_register2' => $_POST['tgl_register2']);
					$this->session->set_userdata($data1);
				}
				
				
				
				
				

				// echo $id_prov;

				$per_page = 50;
				$qry = "
						SELECT 
a.*,
b.nama AS kategori
FROM 
trx_pendataan a,
ref_kategori b
WHERE
a.id_kategori=b.id and a.jenis_pendataan='PPKS PANTI SWASTA'
					";

				if ($sesi_bo_user_group == 5) {
					
					$sesi_bo_id_panti = $this->session->userdata('sesi_bo_id_panti');
					
					
					$qry .= " and a.id_panti='$sesi_bo_id_panti' 
					";
				}
				
				
				if ($this->session->userdata('s_id_kategori') != "") {
					
					$qry .= "  and a.id_kategori = '".$this->db->escape_like_str($this->session->userdata('s_id_kategori'))."'";
					
				}
				
				if ($this->session->userdata('s_status_keberadaan') != "") {
					
					$qry .= "  and a.status_keberadaan = '". $this->db->escape_like_str($this->session->userdata('s_status_keberadaan'))."'";
					
				}
				
				
				
				
				
				if ($this->session->userdata('s_approved') != "") {
					
					$qry .= "  and a.approved = '" .$this->db->escape_like_str($this->session->userdata('s_approved'))."'";
					
				}
				
				
				
				
				if ($this->session->userdata('s_tgl_register1') != "" && $this->session->userdata('s_tgl_register2') != "") {
					
					$qry .= "  and a.tgl_register between '" .$this->db->escape_like_str($this->session->userdata('s_tgl_register1'))."'  and '" .$this->db->escape_like_str($this->session->userdata('s_tgl_register2'))."'";
					
				}

				if ($this->session->userdata('s_cari_personal') != "") {
					$qry .= "  and (a.nama like '%" . $this->db->escape_like_str($this->session->userdata('s_cari_personal')) . "%'
							OR a.alamat like '%" . $this->db->escape_like_str($this->session->userdata('s_cari_personal')) . "%' 
							OR a.nik like '%" . $this->db->escape_like_str($this->session->userdata('s_cari_personal')) . "%'
							OR a.nokk like '%" . $this->db->escape_like_str($this->session->userdata('s_cari_personal')) . "%'
							OR a.sex like '%" . $this->db->escape_like_str($this->session->userdata('s_cari_personal')) . "%'
							OR a.kode_register like '%" . $this->db->escape_like_str($this->session->userdata('s_cari_personal')) . "%'
						 )  ";
				} elseif ($this->session->userdata('s_cari_personal') == "") {
					$this->session->unset_userdata('s_cari_personal');
				}


				$qry .= " ORDER BY a.id asc";
				
				//echo $qry;


				$offset = ($this->uri->segment(3) != '' ? $this->uri->segment(3) : 0);
				$config['total_rows'] = $this->db->query($qry)->num_rows();
				$config['per_page'] = $per_page;
				$config['full_tag_open'] = '<div class="table-pagination">';
				$config['full_tag_close'] = '</div>';
				$config['cur_tag_open'] = '<a href="#" class="active"><b>';
				$config['cur_tag_close'] = '</b></a>';
				$config['first_link'] = 'First';
				$config['last_link'] = 'Last';
				$config['next_link'] = 'Next';
				$config['prev_link'] = 'Previous';
				$config['last_tag_open'] = "<span>";
				$config['first_tag_close'] = "</span>";
				$config['uri_segment'] = 3;
				$config['base_url'] = base_url() . '/trx_pendataan_swasta/index';
				$config['suffix'] = '?' . http_build_query($_GET, '', "&");
				$this->pagination->initialize($config);
				$data['paginglinks'] = $this->pagination->create_links();
				$data['per_page'] = $this->uri->segment(3);
				$data['offset'] = $offset;
				if ($data['paginglinks'] != '') {
					$data['pagermessage'] = 'Showing ' . ((($this->pagination->cur_page - 1) * $this->pagination->per_page) + 1) . ' to ' . ($this->pagination->cur_page * $this->pagination->per_page) . ' of ' . $this->db->query($qry)->num_rows();
				}
				$qry .= " limit {$per_page} offset {$offset} ";
				
				$data['ListData'] = $this->db->query($qry)->result_array();
				
				$data['role'] = get_role($this->session->userdata('sesi_bo_user_group'));



				$data['breadcrumbs'] = array(
					array(
						'link' => '',
						'name' => 'Pendataan'
					),
					array(
						'link' => 'trx_pendataan',
						'name' => 'Pendataan PPKS Oleh Panti Swasta '
					)
				);



				$data['sub_judul_form'] = "Pendataan PPKS Oleh Panti Swasta ";
				
				
				
				$sql_kat = "select * from ref_kategori where flag='PPKS' order by nama asc ";
				$query_kat = $this->db->query($sql_kat);
				$data["list_kategori"] = $query_kat->result();
				
	
					
				$this->template->load('template_admin', 'v_index', $data);
				
				
		
		
	}



	public function export()

	{
		
	



				

				
				
				$sesi_bo_user_group = $this->session->userdata('sesi_bo_user_group');
				
				
				
				
				if (isset($_POST['cari_personal'])) {
					$data1 = array('s_cari_personal' => $_POST['cari_personal']);
					$this->session->set_userdata($data1);
				}
				
				
				if (isset($_POST['id_kategori'])) {
					$data1 = array('s_id_kategori' => $_POST['id_kategori']);
					$this->session->set_userdata($data1);
				}
				
				if (isset($_POST['status_keberadaan'])) {
					$data1 = array('s_status_keberadaan' => $_POST['status_keberadaan']);
					$this->session->set_userdata($data1);
				}
				
				
				if (isset($_POST['status_bantuan'])) {
					$data1 = array('s_status_bantuan' => $_POST['status_bantuan']);
					$this->session->set_userdata($data1);
				}
				
				if (isset($_POST['approved'])) {
					$data1 = array('s_approved' => $_POST['approved']);
					$this->session->set_userdata($data1);
				}
				
				if (isset($_POST['tgl_register1'])) {
					$data1 = array('s_tgl_register1' => $_POST['tgl_register1']);
					$this->session->set_userdata($data1);
				}
				
				if (isset($_POST['tgl_register2'])) {
					$data1 = array('s_tgl_register2' => $_POST['tgl_register2']);
					$this->session->set_userdata($data1);
				}
				
				
				if (isset($_POST['status_panti'])) {
					$data1 = array('s_status_panti' => $_POST['status_panti']);
					$this->session->set_userdata($data1);
				}
				
				

				// echo $id_prov;

				
				$qry = "
						SELECT 
a.*,
b.nama AS kategori
FROM 
trx_pendataan a,
ref_kategori b
WHERE
a.id_kategori=b.id and a.jenis_pendataan='PPKS PANTI SWASTA'
					";

				if ($sesi_bo_user_group == 5) {
					
					$sesi_bo_id_panti = $this->session->userdata('sesi_bo_id_panti');
					
					
					$qry .= " and a.id_panti='$sesi_bo_id_panti' 
					";
				}
				
				
				if ($this->session->userdata('s_id_kategori') != "") {
					
					$qry .= "  and a.id_kategori = '".$this->db->escape_like_str($this->session->userdata('s_id_kategori'))."'";
					
				}
				
				if ($this->session->userdata('s_status_keberadaan') != "") {
					
					$qry .= "  and a.status_keberadaan = '". $this->db->escape_like_str($this->session->userdata('s_status_keberadaan'))."'";
					
				}
				
				
				if ($this->session->userdata('s_status_bantuan') != "") {
					
					$qry .= "  and a.status_bantuan = '" .$this->db->escape_like_str($this->session->userdata('s_status_bantuan'))."'";
					
				}
				
				
				if ($this->session->userdata('s_approved') != "") {
					
					$qry .= "  and a.approved = '" .$this->db->escape_like_str($this->session->userdata('s_approved'))."'";
					
				}
				
				if ($this->session->userdata('s_status_panti') != "") {
					
					$qry .= "  and a.status_panti = '" .$this->db->escape_like_str($this->session->userdata('s_status_panti'))."'";
					
				}
				
				
				if ($this->session->userdata('s_tgl_register1') != "" && $this->session->userdata('s_tgl_register2') != "") {
					
					$qry .= "  and a.tgl_register between '" .$this->db->escape_like_str($this->session->userdata('s_tgl_register1'))."'  and '" .$this->db->escape_like_str($this->session->userdata('s_tgl_register2'))."'";
					
				}

				if ($this->session->userdata('s_cari_personal') != "") {
					$qry .= "  and (a.nama like '%" . $this->db->escape_like_str($this->session->userdata('s_cari_personal')) . "%'
							OR a.alamat like '%" . $this->db->escape_like_str($this->session->userdata('s_cari_personal')) . "%' 
							OR a.nik like '%" . $this->db->escape_like_str($this->session->userdata('s_cari_personal')) . "%'
							OR a.nokk like '%" . $this->db->escape_like_str($this->session->userdata('s_cari_personal')) . "%'
							OR a.sex like '%" . $this->db->escape_like_str($this->session->userdata('s_cari_personal')) . "%'
							OR a.kode_register like '%" . $this->db->escape_like_str($this->session->userdata('s_cari_personal')) . "%'
						 )  ";
				} elseif ($this->session->userdata('s_cari_personal') == "") {
					$this->session->unset_userdata('s_cari_personal');
				}


				$qry .= " ORDER BY id asc";
				
				
				
				$data['ListData'] = $this->db->query($qry)->result_array();
				
			
					
				$this->load->view('v_index_export', $data);
				
				
		
		
	}



	public function clear_session()
	{


				$this->session->unset_userdata('s_cari_personal');
				$this->session->unset_userdata('s_status_bantuan');
				$this->session->unset_userdata('s_status_keberadaan');
				$this->session->unset_userdata('s_id_kategori');
				$this->session->unset_userdata('s_approved');
				$this->session->unset_userdata('s_tgl_register1');
				$this->session->unset_userdata('s_tgl_register2');
				$this->session->unset_userdata('s_status_panti');
				


		$this->session->unset_userdata('s_cari_personal');
		redirect('trx_pendataan_swasta');
	}

	public function hupus()
	{
		get_role($this->session->userdata('sesi_bo_user_group'), 'delete');
		// $id=$this->uri->segment(3);

		$id = deCrypt($this->uri->segment(3));
		try {

			$this->db->where('id', $id);
			$this->db->delete('trx_pendataan');
			redirect('trx_pendataan');
		} catch (Exception $err) {
			log_message("error", $err->getMessage());
			return show_error($err->getMessage());
		}
	}

	


	
	
	
	
		public function tambih()
	{
		get_role($this->session->userdata('sesi_bo_user_group'), 'insert');
		$data['breadcrumbs'] = array(
			array(
				'link' => 'trx_pendataan_swasta/tambih/#',
				'name' => 'Pendataan'
			),
			array(
				'link' => 'trx_pendataan',
				'name' => 'Pendataan PPKS Oleh Panti Swasta '
			),
			array(
				'link' => 'trx_pendataan_swasta/tambih/#',
				'name' => 'Tambah Data'
			)
		);

		$data['judul_form'] = "Tambah Data Pendataan PPKS Oleh Panti Swasta ";
		$data['sub_judul_form'] = "";
		
		$sql_kat = "select * from ref_kategori where flag='PPKS' order by nama asc ";
		$query_kat = $this->db->query($sql_kat);
		$data["list_kategori"] = $query_kat->result();


		$this->template->load('template_admin', 'v_add', $data);
	}

	public function robih($idx)
	{
		$id = deCrypt($idx);

		$data['field'] = $this->db->query("select * from trx_pendataan where id='$id'")->row();
		
		
		$data['breadcrumbs'] = array(
			array(
				'link' => 'trx_pendataan_swasta/robih/#',
				'name' => 'Pendataan'
			),
			array(
				'link' => 'trx_pendataan',
				'name' => 'Pendataan PPKS Oleh Panti Swasta '
			),
			array(
				'link' => 'trx_pendataan_swasta/robih/'.$idx,
				'name' => 'Ubah Data'
			)
		);

		$data['judul_form'] = "";
		$data['sub_judul_form'] = "Ubah Data Pendataan PPKS Oleh Panti Swasta ";
		
		$sql_kat = "select * from ref_kategori where flag='PPKS' order by nama asc ";
		$query_kat = $this->db->query($sql_kat);
		$data["list_kategori"] = $query_kat->result();

		$this->template->load('template_admin', 'v_add', $data);
	}

	public function tambih_robih()
	{


		try {
			
			$id  					= $this->input->post('id');
			$data['nama']	= strtoupper($this->input->post('nama'));			
			$data['alamat']	= strtoupper($this->input->post('alamat'));
			$data['sex']		= strtoupper($this->input->post('sex'));
			$data['nik'] = strtoupper($this->input->post('nik'));
			$data['tempat_lahir'] = strtoupper($this->input->post('tempat_lahir'));
			$data['nokk'] 			= strtoupper($this->input->post('nokk'));
			$data['tgl_lahir'] 			= strtoupper($this->input->post('tgl_lahir'));
			$data['id_kategori'] 			= $this->input->post('id_kategori');
			
			$data['status_keberadaan'] 			= $this->input->post('status_keberadaan');
			$data['tgl_register'] 			= $this->input->post('tgl_register');
			$data['tgl_out'] 			= $this->input->post('tgl_out');
			
			
			$data['id_panti'] 			= $this->input->post('id_panti');
			
			$data['jenis_pendataan'] 	='PPKS PANTI SWASTA';
			
			
			
			
			
			$queryx = $this->db->query("select id_provinsi,id_kabkot,id_kecamatan,id_kelurahan from ref_panti where id='".$this->input->post('id_panti')."'");
			$rowx = $queryx->row();
			if (isset($rowx))
			{
					$id_provinsi= $rowx->id_provinsi;
					$id_kabkot= $rowx->id_kabkot;
					$id_kecamatan= $rowx->id_kecamatan;
					$id_kelurahan= $rowx->id_kelurahan;
					
			} else {
				
					$id_provinsi= '';
					$id_kabkot= '';
					$id_kecamatan='';
					$id_kelurahan= '';
				
			}
			
			
			$data['id_provinsi'] 			= $id_provinsi;
			$data['id_kabkot'] 			= $id_kabkot;
			$data['id_kecamatan'] 			= $id_kecamatan;
			$data['id_kelurahan'] 			= $id_kelurahan;
			
			
			
			

			$dir = './uploads/trx_pendataan/';
			if (!file_exists(FCPATH . $dir)) {
				mkdir(FCPATH . $dir, 0777, true);
			}

		
			
			
			if (isset($_FILES['berkas']) and is_uploaded_file($_FILES['berkas']['tmp_name'])) {
			  $folder = "uploads/trx_pendataan_swasta/";
			  $upload_image = $_FILES['berkas']['name'];
			  // tentukan ukuran width yang diharapkan
			  $width_size = 100;

			  // tentukan di mana image akan ditempatkan setelah diupload
			  $filesave = $folder . $upload_image;
			  move_uploaded_file($_FILES['berkas']['tmp_name'], $filesave);

			  // menentukan nama image setelah dibuat
			  $resize_image = $folder . "resize_location_" . uniqid(rand()) . ".jpg";

			  // mendapatkan ukuran width dan height dari image
			  list($width, $height) = getimagesize($filesave);

			  // mendapatkan nilai pembagi supaya ukuran skala image yang dihasilkan sesuai dengan aslinya
			  $k = $width / $width_size;

			  // menentukan width yang baru
			  $newwidth = $width / $k;

			  // menentukan height yang baru
			  $newheight = $height / $k;

			  // fungsi untuk membuat image yang baru
			  $thumb = imagecreatetruecolor($newwidth, $newheight);
			  $source = imagecreatefromjpeg($filesave);

			  // men-resize image yang baru
			  imagecopyresized($thumb, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);

			  // menyimpan image yang baru
			  imagejpeg($thumb, $resize_image);

			  imagedestroy($thumb);
			  imagedestroy($source);

			  unlink($filesave);

			  $data['foto'] = $resize_image;
			}



		


			if ($id == '' || $id == null) {
				get_role($this->session->userdata('sesi_bo_user_group'),'insert');	
				$data['kode_register'] 			= time();
				$data['created_by'] = $this->session->userdata('sesi_bo_nama_lengkap');
				$data['created_date'] = date('Y-m-d H:i:s');
				$xss_data = $this->security->xss_clean($data);
				$this->db->insert('trx_pendataan', $xss_data);
				
				$this->session->set_flashdata('message_sukses', 'Penambahan Data Berhasil Disimpan');

				
			} else {
				
				get_role($this->session->userdata('sesi_bo_user_group'),'update');	
				
				$data['update_by'] = $this->session->userdata('sesi_bo_nama_lengkap');
				$data['updated_date'] = date('Y-m-d H:i:s');
				$xss_data = $this->security->xss_clean($data);

				$this->db->where('id', $id);
				$this->db->update('trx_pendataan', $xss_data);
				
				$this->session->set_flashdata('message_sukses', 'Perubahan Data Berhasil Disimpan');

			}


			redirect('trx_pendataan_swasta');

		} catch (Exception $err) {
			log_message("error", $err->getMessage());
			return show_error($err->getMessage());
		}
	}
	
	
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
