<div class="container-fluid">
    <br>
    <div class="breadcrumbs">
        <ul>
            <?php foreach ($breadcrumbs as $key => $value) { ?>
                <li>
                    <a href="<?php echo site_url($value['link']) ?>">
                        <?php echo $value['name']; ?></a>
                    <?php echo (count($breadcrumbs) - 1) == $key ? "" : "<i class='icon-angle-right'></i>"; ?>
                </li>
            <?php } ?>
        </ul>
        <div class="close-bread">
            <a href="#"><i class="icon-remove"></i></a>
        </div>
    </div>
</div>



<div class="row-fluid">
	
	<div class="span1">&nbsp;</div>

    <div class="span10">
        <?php
        if ($this->session->flashdata('message_gagal')) {
            echo '<hr><div class="alert alert-error"><button class="close" data-dismiss="alert" type="button">&times;</button>' . $this->session->flashdata('message_gagal') . '</div>';
        }
        if ($this->session->flashdata('message_sukses')) {
            echo '<hr><div class="alert alert-success"><button class="close" data-dismiss="alert" type="button">&times;</button>' . $this->session->flashdata('message_sukses') . '</div>';
        } ?>

        <div class="box box-bordered box-color">
            <div class="box-title">
                <h3><i class=" icon-plus-sign"></i><?php echo $judul_form . " " . $sub_judul_form; ?> </h3>
            </div>
            <div class="box-content nopadding">
                <!-- <form action="#" method="POST" class='form-horizontal form-validate' id="bb"> -->
                <?php echo form_open('trx_pendataan/upload', array('name' => 'bb', 'id' => 'bb', 'class' => 'form-horizontal form-bordered form-validate form-wysiwyg', 'enctype' => 'multipart/form-data')); ?>


                <input type="hidden" name="id" id="id" value="<?php echo isset($field->id) ? $field->id : ''; ?>">
                <input type="hidden" name="<?= $this->security->get_csrf_token_name(); ?>" value="<?= $this->security->get_csrf_hash(); ?>" style="display: none">

                <input type="hidden" value="<?php echo $this->input->ip_address(); ?>" name="ip_address">
				
				
				
				<?php 
				
				if ($this->session->userdata('sesi_bo_user_group')==1 or $this->session->userdata('sesi_bo_user_group')==2) {
					require_once(APPPATH . '/modules/filter_wilayah/views/v_filter_wilayah.php');  
				} elseif ($this->session->userdata('sesi_bo_user_group')==4) {
					require_once(APPPATH . '/modules/filter_wilayah/views/v_filter_wilayah_text.php');  
				}elseif ($this->session->userdata('sesi_bo_user_group')==9) {
					require_once(APPPATH . '/modules/filter_wilayah/views/v_filter_wilayah_text_opkec.php');  
				}
				
				?>
				
				
				
				

                <div class="control-group">
                    <label class="control-label" for="textfield">File Excel (.xlsx)</label>
                    <div class="controls">
                        <!-- <input type="hidden" name="nip" id="nip" class="input-large" value=""> -->
                        <input required type="file" name="fileURL" id="berkas">
						
						<br>
						<a href="<?php echo base_url();?>uploads/template_ppks_kelurahan.xlsx" target="_blank">DOWNLOAD TEMPLATE ISIAN DATA</a>
                       

                       
                    </div>
                </div>








                <div class="form-actions">
                    <button id="btn_simpan" class="btn btn-blue" type="submit">Simpan</button>
                    <a class="btn btn-danger" href="<?php echo site_url(); ?>trx_pendataan">Kembali</a>
                </div>

                </form>
            </div>
        </div>
		
		
		<div class="span1">&nbsp;</div>
		
    </div>

