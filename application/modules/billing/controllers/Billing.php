<?php if (!defined('BASEPATH')) exit('No direct script access allowed');



class Billing extends MX_Controller
{
	//put your code here
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('utility');
		// 
		$this->truly_madly_deeply();

		Modules::run('ref_role/permissions');
		// $this->load->model('users_model');

		$this->load->helper('login');
		$this->load->helper('tool');
		$this->load->model('Site', 'site');
	}

	public function truly_madly_deeply()
	{

		if (!$this->session->userdata('truly_madly_deeply')) {
			redirect('signon');
		}
	}



	public function index($offset = 0)

	{
		
	



				

				
				
				$sesi_bo_user_group = $this->session->userdata('sesi_bo_user_group');
				
				
				
				
				if (isset($_POST['cari_personal'])) {
					$data1 = array('s_cari_personal' => $_POST['cari_personal']);
					$this->session->set_userdata($data1);
				}
				
				
				if (isset($_POST['id_kategori'])) {
					$data1 = array('s_id_kategori' => $_POST['id_kategori']);
					$this->session->set_userdata($data1);
				}
				
				if (isset($_POST['status_keberadaan'])) {
					$data1 = array('s_status_keberadaan' => $_POST['status_keberadaan']);
					$this->session->set_userdata($data1);
				}
				
				
				if (isset($_POST['status_bantuan'])) {
					$data1 = array('s_status_bantuan' => $_POST['status_bantuan']);
					$this->session->set_userdata($data1);
				}
				
				if (isset($_POST['approved'])) {
					$data1 = array('s_approved' => $_POST['approved']);
					$this->session->set_userdata($data1);
				}
				
				if (isset($_POST['tgl_register1'])) {
					$data1 = array('s_tgl_register1' => $_POST['tgl_register1']);
					$this->session->set_userdata($data1);
				}
				
				if (isset($_POST['tgl_register2'])) {
					$data1 = array('s_tgl_register2' => $_POST['tgl_register2']);
					$this->session->set_userdata($data1);
				}
				
				
				if (isset($_POST['status_panti'])) {
					$data1 = array('s_status_panti' => $_POST['status_panti']);
					$this->session->set_userdata($data1);
				}
				
				

				// echo $id_prov;

				$per_page = 50;
				$qry = "
						SELECT 
a.*,
b.nama AS kategori
FROM 
trx_pendataan a,
ref_kategori b
WHERE
a.id_kategori=b.id and a.jenis_pendataan='PPKS KELURAHAN'
					";

				if ($sesi_bo_user_group == 4) {
					
					$sesi_bo_id_prov = $this->session->userdata('sesi_bo_id_prov');
					$sesi_bo_id_kabkot = $this->session->userdata('sesi_bo_id_kabkot');
					$sesi_bo_id_kec = $this->session->userdata('sesi_bo_id_kec');
					$sesi_bo_id_kel = $this->session->userdata('sesi_bo_id_kel');
					
					$qry .= " and a.id_provinsi='$sesi_bo_id_prov' 
							  and a.id_kabkot='$sesi_bo_id_kabkot' 
							  and a.id_kecamatan='$sesi_bo_id_kec' 
							  and a.id_kelurahan='$sesi_bo_id_kel' 
					";
				}
				
				
				if ($sesi_bo_user_group == 9) {
					
					$sesi_bo_id_prov = $this->session->userdata('sesi_bo_id_prov');
					$sesi_bo_id_kabkot = $this->session->userdata('sesi_bo_id_kabkot');
					$sesi_bo_id_kec = $this->session->userdata('sesi_bo_id_kec');
					
					
					$qry .= " and a.id_provinsi='$sesi_bo_id_prov' 
							  and a.id_kabkot='$sesi_bo_id_kabkot' 
							  and a.id_kecamatan='$sesi_bo_id_kec' 
							 
					";
				}
				
				
				if ($this->session->userdata('s_id_kategori') != "") {
					
					$qry .= "  and a.id_kategori = '".$this->db->escape_like_str($this->session->userdata('s_id_kategori'))."'";
					
				}
				
				if ($this->session->userdata('s_status_keberadaan') != "") {
					
					$qry .= "  and a.status_keberadaan = '". $this->db->escape_like_str($this->session->userdata('s_status_keberadaan'))."'";
					
				}
				
				
				if ($this->session->userdata('s_status_bantuan') != "") {
					
					$qry .= "  and a.status_bantuan = '" .$this->db->escape_like_str($this->session->userdata('s_status_bantuan'))."'";
					
				}
				
				
				if ($this->session->userdata('s_approved') != "") {
					
					$qry .= "  and a.approved = '" .$this->db->escape_like_str($this->session->userdata('s_approved'))."'";
					
				}
				
				if ($this->session->userdata('s_status_panti') != "") {
					
					$qry .= "  and a.status_panti = '" .$this->db->escape_like_str($this->session->userdata('s_status_panti'))."'";
					
				}
				
				
				if ($this->session->userdata('s_tgl_register1') != "" && $this->session->userdata('s_tgl_register2') != "") {
					
					$qry .= "  and a.tgl_register between '" .$this->db->escape_like_str($this->session->userdata('s_tgl_register1'))."'  and '" .$this->db->escape_like_str($this->session->userdata('s_tgl_register2'))."'";
					
				}

				if ($this->session->userdata('s_cari_personal') != "") {
					$qry .= "  and (a.nama like '%" . $this->db->escape_like_str($this->session->userdata('s_cari_personal')) . "%'
							OR a.alamat like '%" . $this->db->escape_like_str($this->session->userdata('s_cari_personal')) . "%' 
							OR a.nik like '%" . $this->db->escape_like_str($this->session->userdata('s_cari_personal')) . "%'
							OR a.nokk like '%" . $this->db->escape_like_str($this->session->userdata('s_cari_personal')) . "%'
							OR a.sex like '%" . $this->db->escape_like_str($this->session->userdata('s_cari_personal')) . "%'
							OR a.kode_register like '%" . $this->db->escape_like_str($this->session->userdata('s_cari_personal')) . "%'
						 )  ";
				} elseif ($this->session->userdata('s_cari_personal') == "") {
					$this->session->unset_userdata('s_cari_personal');
				}


				$qry .= " ORDER BY a.id desc";
				
				//echo $qry;


				$offset = ($this->uri->segment(3) != '' ? $this->uri->segment(3) : 0);
				$config['total_rows'] = $this->db->query($qry)->num_rows();
				$config['per_page'] = $per_page;
				$config['full_tag_open'] = '<div class="table-pagination">';
				$config['full_tag_close'] = '</div>';
				$config['cur_tag_open'] = '<a href="#" class="active"><b>';
				$config['cur_tag_close'] = '</b></a>';
				$config['first_link'] = 'First';
				$config['last_link'] = 'Last';
				$config['next_link'] = 'Next';
				$config['prev_link'] = 'Previous';
				$config['last_tag_open'] = "<span>";
				$config['first_tag_close'] = "</span>";
				$config['uri_segment'] = 3;
				$config['base_url'] = base_url() . '/trx_pendataan/index';
				$config['suffix'] = '?' . http_build_query($_GET, '', "&");
				$this->pagination->initialize($config);
				$data['paginglinks'] = $this->pagination->create_links();
				$data['per_page'] = $this->uri->segment(3);
				$data['offset'] = $offset;
				if ($data['paginglinks'] != '') {
					$data['pagermessage'] = 'Showing ' . ((($this->pagination->cur_page - 1) * $this->pagination->per_page) + 1) . ' to ' . ($this->pagination->cur_page * $this->pagination->per_page) . ' of ' . $this->db->query($qry)->num_rows();
				}
				$qry .= " limit {$per_page} offset {$offset} ";
				
				$data['ListData'] = $this->db->query($qry)->result_array();
				
				$data['role'] = get_role($this->session->userdata('sesi_bo_user_group'));



				$data['breadcrumbs'] = array(
					array(
						'link' => '',
						'name' => 'Pendataan'
					),
					array(
						'link' => 'trx_pendataan',
						'name' => 'Pendataan PPKS Oleh Kelurahan '
					)
				);



				$data['sub_judul_form'] = "Pendataan PPKS Oleh Kelurahan ";
				
				
				
				$sql_kat = "select * from ref_kategori where flag='PPKS' order by nama asc ";
				$query_kat = $this->db->query($sql_kat);
				$data["list_kategori"] = $query_kat->result();
				
	
					
				$this->template->load('template_admin', 'v_index', $data);
				
				
		
		
	}



	public function export()

	{
		
	



				
$sesi_bo_user_group = $this->session->userdata('sesi_bo_user_group');
				
				
				
				
				if (isset($_POST['cari_personal'])) {
					$data1 = array('s_cari_personal' => $_POST['cari_personal']);
					$this->session->set_userdata($data1);
				}
				
				
				if (isset($_POST['id_kategori'])) {
					$data1 = array('s_id_kategori' => $_POST['id_kategori']);
					$this->session->set_userdata($data1);
				}
				
				if (isset($_POST['status_keberadaan'])) {
					$data1 = array('s_status_keberadaan' => $_POST['status_keberadaan']);
					$this->session->set_userdata($data1);
				}
				
				
				if (isset($_POST['status_bantuan'])) {
					$data1 = array('s_status_bantuan' => $_POST['status_bantuan']);
					$this->session->set_userdata($data1);
				}
				
				if (isset($_POST['approved'])) {
					$data1 = array('s_approved' => $_POST['approved']);
					$this->session->set_userdata($data1);
				}
				
				if (isset($_POST['tgl_register1'])) {
					$data1 = array('s_tgl_register1' => $_POST['tgl_register1']);
					$this->session->set_userdata($data1);
				}
				
				if (isset($_POST['tgl_register2'])) {
					$data1 = array('s_tgl_register2' => $_POST['tgl_register2']);
					$this->session->set_userdata($data1);
				}
				
				
				if (isset($_POST['status_panti'])) {
					$data1 = array('s_status_panti' => $_POST['status_panti']);
					$this->session->set_userdata($data1);
				}
				
				

				// echo $id_prov;

				$per_page = 50;
				$qry = "
						SELECT 
a.*,
b.nama AS kategori
FROM 
trx_pendataan a,
ref_kategori b
WHERE
a.id_kategori=b.id and a.jenis_pendataan='PPKS KELURAHAN'
					";

				if ($sesi_bo_user_group == 4) {
					
					$sesi_bo_id_prov = $this->session->userdata('sesi_bo_id_prov');
					$sesi_bo_id_kabkot = $this->session->userdata('sesi_bo_id_kabkot');
					$sesi_bo_id_kec = $this->session->userdata('sesi_bo_id_kec');
					$sesi_bo_id_kel = $this->session->userdata('sesi_bo_id_kel');
					
					$qry .= " and a.id_provinsi='$sesi_bo_id_prov' 
							  and a.id_kabkot='$sesi_bo_id_kabkot' 
							  and a.id_kecamatan='$sesi_bo_id_kec' 
							  and a.id_kelurahan='$sesi_bo_id_kel' 
					";
				}
				
				
				if ($sesi_bo_user_group == 9) {
					
					$sesi_bo_id_prov = $this->session->userdata('sesi_bo_id_prov');
					$sesi_bo_id_kabkot = $this->session->userdata('sesi_bo_id_kabkot');
					$sesi_bo_id_kec = $this->session->userdata('sesi_bo_id_kec');
					
					
					$qry .= " and a.id_provinsi='$sesi_bo_id_prov' 
							  and a.id_kabkot='$sesi_bo_id_kabkot' 
							  and a.id_kecamatan='$sesi_bo_id_kec' 
							 
					";
				}
				
				
				if ($this->session->userdata('s_id_kategori') != "") {
					
					$qry .= "  and a.id_kategori = '".$this->db->escape_like_str($this->session->userdata('s_id_kategori'))."'";
					
				}
				
				if ($this->session->userdata('s_status_keberadaan') != "") {
					
					$qry .= "  and a.status_keberadaan = '". $this->db->escape_like_str($this->session->userdata('s_status_keberadaan'))."'";
					
				}
				
				
				if ($this->session->userdata('s_status_bantuan') != "") {
					
					$qry .= "  and a.status_bantuan = '" .$this->db->escape_like_str($this->session->userdata('s_status_bantuan'))."'";
					
				}
				
				
				if ($this->session->userdata('s_approved') != "") {
					
					$qry .= "  and a.approved = '" .$this->db->escape_like_str($this->session->userdata('s_approved'))."'";
					
				}
				
				if ($this->session->userdata('s_status_panti') != "") {
					
					$qry .= "  and a.status_panti = '" .$this->db->escape_like_str($this->session->userdata('s_status_panti'))."'";
					
				}
				
				
				if ($this->session->userdata('s_tgl_register1') != "" && $this->session->userdata('s_tgl_register2') != "") {
					
					$qry .= "  and a.tgl_register between '" .$this->db->escape_like_str($this->session->userdata('s_tgl_register1'))."'  and '" .$this->db->escape_like_str($this->session->userdata('s_tgl_register2'))."'";
					
				}

				if ($this->session->userdata('s_cari_personal') != "") {
					$qry .= "  and (a.nama like '%" . $this->db->escape_like_str($this->session->userdata('s_cari_personal')) . "%'
							OR a.alamat like '%" . $this->db->escape_like_str($this->session->userdata('s_cari_personal')) . "%' 
							OR a.nik like '%" . $this->db->escape_like_str($this->session->userdata('s_cari_personal')) . "%'
							OR a.nokk like '%" . $this->db->escape_like_str($this->session->userdata('s_cari_personal')) . "%'
							OR a.sex like '%" . $this->db->escape_like_str($this->session->userdata('s_cari_personal')) . "%'
							OR a.kode_register like '%" . $this->db->escape_like_str($this->session->userdata('s_cari_personal')) . "%'
						 )  ";
				} elseif ($this->session->userdata('s_cari_personal') == "") {
					$this->session->unset_userdata('s_cari_personal');
				}


				$qry .= " ORDER BY a.id desc";
				
				//echo $qry;


				
				$data['ListData'] = $this->db->query($qry)->result_array();
				
			
					
				$this->load->view('v_index_export', $data);
				
				
		
		
	}



	public function clear_session()
	{


				$this->session->unset_userdata('s_cari_personal');
				$this->session->unset_userdata('s_status_bantuan');
				$this->session->unset_userdata('s_status_keberadaan');
				$this->session->unset_userdata('s_id_kategori');
				$this->session->unset_userdata('s_approved');
				$this->session->unset_userdata('s_tgl_register1');
				$this->session->unset_userdata('s_tgl_register2');
				$this->session->unset_userdata('s_status_panti');
				


		$this->session->unset_userdata('s_cari_personal');
		redirect('trx_pendataan');
	}

	public function hupus()
	{
		get_role($this->session->userdata('sesi_bo_user_group'), 'delete');
		// $id=$this->uri->segment(3);

		$id = deCrypt($this->uri->segment(3));
		try {

			$this->db->where('id', $id);
			$this->db->delete('trx_pendataan');
			redirect('trx_pendataan');
		} catch (Exception $err) {
			log_message("error", $err->getMessage());
			return show_error($err->getMessage());
		}
	}

	


	
	
	
	
		public function tambih()
	{
		get_role($this->session->userdata('sesi_bo_user_group'), 'insert');
		$data['breadcrumbs'] = array(
			array(
				'link' => 'trx_pendataan/tambih/#',
				'name' => 'Pendataan'
			),
			array(
				'link' => 'trx_pendataan',
				'name' => 'Pendataan PPKS Oleh Kelurahan '
			),
			array(
				'link' => 'trx_pendataan/tambih/#',
				'name' => 'Tambah Data'
			)
		);

		$data['judul_form'] = "Tambah Data Pendataan PPKS Oleh Kelurahan ";
		$data['sub_judul_form'] = "";
		
		$sql_kat = "select * from ref_pelanggan  order by nama asc ";
		$query_kat = $this->db->query($sql_kat);
		$data["list_kategori"] = $query_kat->result();


		$this->template->load('template_admin', 'v_add', $data);
	}

	public function robih($idx)
	{
		$id = deCrypt($idx);

		$data['field'] = $this->db->query("select * from trx_pendataan where id='$id'")->row();
		
		
		$data['breadcrumbs'] = array(
			array(
				'link' => 'trx_pendataan/robih/#',
				'name' => 'Pendataan'
			),
			array(
				'link' => 'trx_pendataan',
				'name' => 'Pendataan PPKS Oleh Kelurahan '
			),
			array(
				'link' => 'trx_pendataan/robih/'.$idx,
				'name' => 'Ubah Data'
			)
		);

		$data['judul_form'] = "";
		$data['sub_judul_form'] = "Ubah Data Pendataan PPKS Oleh Kelurahan ";
		
		$sql_kat = "select * from ref_kategori where flag='PPKS' order by nama asc ";
		$query_kat = $this->db->query($sql_kat);
		$data["list_kategori"] = $query_kat->result();

		$this->template->load('template_admin', 'v_add', $data);
	}

	public function tambih_robih()
	{


		try {
			
			$id  					= $this->input->post('id');
			$data['nama']	= strtoupper($this->input->post('nama'));			
			$data['alamat']	= strtoupper($this->input->post('alamat'));
			$data['sex']		= strtoupper($this->input->post('sex'));
			$data['nik'] = strtoupper($this->input->post('nik'));
			$data['tempat_lahir'] = strtoupper($this->input->post('tempat_lahir'));
			$data['nokk'] 			= strtoupper($this->input->post('nokk'));
			$data['tgl_lahir'] 			= strtoupper($this->input->post('tgl_lahir'));
			$data['id_kategori'] 			= $this->input->post('id_kategori');
			
			$data['status_keberadaan'] 			= $this->input->post('status_keberadaan');
			$data['tgl_register'] 			= $this->input->post('tgl_register');
			
			$data['jenis_pendataan'] 	='PPKS KELURAHAN';
			
			
			if ($this->input->post('status_keberadaan')=="LKS") {
				
				$data['nama_lks'] 	=	$this->input->post('nama_lks');
			
			} elseif ($this->input->post('status_keberadaan')=="MASYARAKAT") { 
				
				$data['status_bantuan'] =	$this->input->post('status_bantuan');
				
					if ($this->input->post('status_bantuan')=="YA") {
						
						$data['status_panti'] =	$this->input->post('status_panti');
					}
				
				
			}
			
			
			
			$data['id_provinsi'] 			= $this->input->post('id_provinsi');
			$data['id_kabkot'] 			= $this->input->post('id_kabkot');
			$data['id_kecamatan'] 			= $this->input->post('id_kecamatan');
			$data['id_kelurahan'] 			= $this->input->post('id_kelurahan');
			
			
			
			

			$dir = './uploads/trx_pendataan/';
			if (!file_exists(FCPATH . $dir)) {
				mkdir(FCPATH . $dir, 0777, true);
			}

		
			
			
			if (isset($_FILES['berkas']) and is_uploaded_file($_FILES['berkas']['tmp_name'])) {
			  $folder = "uploads/trx_pendataan/";
			  $upload_image = $_FILES['berkas']['name'];
			  // tentukan ukuran width yang diharapkan
			  $width_size = 100;

			  // tentukan di mana image akan ditempatkan setelah diupload
			  $filesave = $folder . $upload_image;
			  move_uploaded_file($_FILES['berkas']['tmp_name'], $filesave);

			  // menentukan nama image setelah dibuat
			  $resize_image = $folder . "resize_location_" . uniqid(rand()) . ".jpg";

			  // mendapatkan ukuran width dan height dari image
			  list($width, $height) = getimagesize($filesave);

			  // mendapatkan nilai pembagi supaya ukuran skala image yang dihasilkan sesuai dengan aslinya
			  $k = $width / $width_size;

			  // menentukan width yang baru
			  $newwidth = $width / $k;

			  // menentukan height yang baru
			  $newheight = $height / $k;

			  // fungsi untuk membuat image yang baru
			  $thumb = imagecreatetruecolor($newwidth, $newheight);
			  $source = imagecreatefromjpeg($filesave);

			  // men-resize image yang baru
			  imagecopyresized($thumb, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);

			  // menyimpan image yang baru
			  imagejpeg($thumb, $resize_image);

			  imagedestroy($thumb);
			  imagedestroy($source);

			  unlink($filesave);

			  $data['foto'] = $resize_image;
			}



		


			if ($id == '' || $id == null) {
				get_role($this->session->userdata('sesi_bo_user_group'),'insert');	
				$data['kode_register'] 			= time();
				$data['created_by'] = $this->session->userdata('sesi_bo_nama_lengkap');
				$data['created_date'] = date('Y-m-d H:i:s');
				$xss_data = $this->security->xss_clean($data);
				$this->db->insert('trx_pendataan', $xss_data);
				
				$this->session->set_flashdata('message_sukses', 'Penambahan Data Berhasil Disimpan');

				
			} else {
				
				get_role($this->session->userdata('sesi_bo_user_group'),'update');	
				
				$data['update_by'] = $this->session->userdata('sesi_bo_nama_lengkap');
				$data['updated_date'] = date('Y-m-d H:i:s');
				$xss_data = $this->security->xss_clean($data);

				$this->db->where('id', $id);
				$this->db->update('trx_pendataan', $xss_data);
				
				$this->session->set_flashdata('message_sukses', 'Perubahan Data Berhasil Disimpan');

			}


			redirect('trx_pendataan');

		} catch (Exception $err) {
			log_message("error", $err->getMessage());
			return show_error($err->getMessage());
		}
	}
	
	
	
	
	
	
	
	
	
	public function tambih_excel()
	{
		get_role($this->session->userdata('sesi_bo_user_group'), 'insert');
		$data['breadcrumbs'] = array(
			array(
				'link' => 'trx_pendataan/tambih/#',
				'name' => 'Pendataan'
			),
			array(
				'link' => 'trx_pendataan',
				'name' => 'Pendataan PPKS Oleh Kelurahan '
			),
			array(
				'link' => 'trx_pendataan/tambih/#',
				'name' => 'Tambah Data Dengan Excel'
			)
		);

		$data['judul_form'] = "Tambah Data Dengan Excel Pendataan PPKS Oleh Kelurahan ";
		$data['sub_judul_form'] = "";
		


		$this->template->load('template_admin', 'v_add_excel', $data);
	}
	
	
	
	
	
		 public function upload() {
        $data = array();
        
      /*   // Load form validation library
         $this->load->library('form_validation');
         $this->form_validation->set_rules('fileURL', 'Upload File', 'callback_checkFileValidation');
         if($this->form_validation->run() == false) {
             echo "xxx"; die;
            $this->template->load('template_admin', 'v_index', $data);
         } else {
			 
			*/
			 
            // If file uploaded
            if(!empty($_FILES['fileURL']['name'])) {

			$id_provinsi			= $this->input->post('id_provinsi');
			$id_kabkot			= $this->input->post('id_kabkot');
			$id_kecamatan			= $this->input->post('id_kecamatan');
			$id_kelurahan			= $this->input->post('id_kelurahan');
			
			$tgl_register			= date("Y-m-d");
			
			
			$jenis_pendataan			=  "PPKS KELURAHAN";
			
			$created_by = $this->session->userdata('sesi_bo_nama_lengkap');
			$created_date = date('Y-m-d H:i:s');
				
                // get file extension
                $extension = pathinfo($_FILES['fileURL']['name'], PATHINFO_EXTENSION);
 
                if($extension == 'csv'){
                    $reader = new \PhpOffice\PhpSpreadsheet\Reader\Csv();
                } elseif($extension == 'xlsx') {
                    $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
                } else {
                    $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
                }
                // file path
                $spreadsheet = $reader->load($_FILES['fileURL']['tmp_name']);
                $allDataInSheet = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
            
                // array Count
                $arrayCount = count($allDataInSheet);
                $flag = 0;
                $createArray = array('id_kategori', 'nama', 'alamat', 'nik', 'nokk', 'sex', 'tempat_lahir', 'tgl_lahir');
                $makeArray = array('id_kategori' => 'id_kategori', 'nama' => 'nama', 'alamat' => 'alamat', 'nik' => 'nik', 'nokk' => 'nokk', 'sex' => 'sex', 'tempat_lahir' => 'tempat_lahir', 'tgl_lahir' => 'tgl_lahir');
                $SheetDataKey = array();
                foreach ($allDataInSheet as $dataInSheet) {
                    foreach ($dataInSheet as $key => $value) {
                        if (in_array(trim($value), $createArray)) {
                            $value = preg_replace('/\s+/', '', $value);
                            $SheetDataKey[trim($value)] = $key;
                        } 
                    }
                }
                $dataDiff = array_diff_key($makeArray, $SheetDataKey);
                if (empty($dataDiff)) {
                    $flag = 1;
                }
                // match excel sheet column
                if ($flag == 1) {
                    for ($i = 2; $i <= $arrayCount; $i++) {
                        $addresses = array();
						$kode_register			=  time().$i;
						
                        $id_kategorix = $SheetDataKey['id_kategori'];
                        $namax = $SheetDataKey['nama'];
                        $alamatx = $SheetDataKey['alamat'];
                        $nikx = $SheetDataKey['nik'];
                        $nokkx = $SheetDataKey['nokk'];
						$sexx = $SheetDataKey['sex'];
						$tempat_lahirx = $SheetDataKey['tempat_lahir'];
						$tgl_lahirx = $SheetDataKey['tgl_lahir'];
						
 
                        $id_kategorix = filter_var(trim($allDataInSheet[$i][$id_kategorix]), FILTER_SANITIZE_STRING);
                        $namax = filter_var(trim($allDataInSheet[$i][$namax]), FILTER_SANITIZE_STRING);
                        $alamatx = filter_var(trim($allDataInSheet[$i][$alamatx]), FILTER_SANITIZE_STRING);
                        $nikx = filter_var(trim($allDataInSheet[$i][$nikx]), FILTER_SANITIZE_STRING);
                        $nokkx = filter_var(trim($allDataInSheet[$i][$nokkx]), FILTER_SANITIZE_STRING);
						$sexx = filter_var(trim($allDataInSheet[$i][$sexx]), FILTER_SANITIZE_STRING);
						$tempat_lahirx = filter_var(trim($allDataInSheet[$i][$tempat_lahirx]), FILTER_SANITIZE_STRING);
						$tgl_lahirx = filter_var(trim($allDataInSheet[$i][$tgl_lahirx]), FILTER_SANITIZE_STRING);
						
                        $fetchData[] = array(
						'id_kategori' => $id_kategorix, 
						'nama' => $namax, 
						'alamat' => $alamatx, 
						'nik' => $nikx, 
						'sex' => $nokkx, 
						'nokk' => $sexx, 
						'tempat_lahir' => $tempat_lahirx, 
						'tgl_lahir' => $tgl_lahirx, 
						'id_provinsi' => $id_provinsi, 
						'id_kabkot' => $id_kabkot,
						'id_kecamatan' => $id_kecamatan,
						'id_kelurahan' => $id_kelurahan,
						'kode_register' => $kode_register,
						'tgl_register' => $tgl_register,
						'jenis_pendataan'=> $jenis_pendataan,
						'created_by'=> $created_by,
						'created_date'=> $created_date
						
						);
                    }   
					
					
					
                    $data['dataInfo'] = $fetchData;
                    $this->site->setBatchImport($fetchData);
                    $this->site->importData();
                } else {
                    echo "Please import correct file, did not match excel sheet column";
                }
                
				redirect('trx_pendataan');
            }              
      //  }
    }
 
    // checkFileValidation
    public function checkFileValidation($string) {
      $file_mimes = array('text/x-comma-separated-values', 
        'text/comma-separated-values', 
        'application/octet-stream', 
        'application/vnd.ms-excel', 
        'application/x-csv', 
        'text/x-csv', 
        'text/csv', 
        'application/csv', 
        'application/excel', 
        'application/vnd.msexcel', 
        'text/plain', 
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
      );
      if(isset($_FILES['fileURL']['name'])) {
            $arr_file = explode('.', $_FILES['fileURL']['name']);
            $extension = end($arr_file);
            if(($extension == 'xlsx' || $extension == 'xls' || $extension == 'csv') && in_array($_FILES['fileURL']['type'], $file_mimes)){
                return true;
            }else{
                $this->form_validation->set_message('checkFileValidation', 'Please choose correct file.');
                return false;
            }
        }else{
            $this->form_validation->set_message('checkFileValidation', 'Please choose a file.');
            return false;
        }
    }

	
	
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
