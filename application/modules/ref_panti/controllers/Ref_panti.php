<?php if (!defined('BASEPATH')) exit('No direct script access allowed');



class Ref_panti extends MX_Controller
{
	//put your code here
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('utility');
		// 
		$this->truly_madly_deeply();

		Modules::run('ref_role/permissions');
		// $this->load->model('users_model');

		$this->load->helper('login');
		$this->load->helper('tool');
	}

	public function truly_madly_deeply()
	{

		if (!$this->session->userdata('truly_madly_deeply')) {
			redirect('signon');
		}
	}



	public function index($offset = 0)

	{
		
	



				if (isset($_POST['cari_global_lembaga'])) {
					$data1 = array('s_cari_global_lembaga' => $_POST['cari_global_lembaga']);
					$this->session->set_userdata($data1);
				}

				$sesi_bo_id_panti = $this->session->userdata('sesi_bo_id_panti');
				

				// echo $id_prov;

				$per_page = 10;
				$qry = "
						SELECT * 
						FROM ref_panti a
						WHERE jenis_panti='PEMERINTAH'
					";

				if ($sesi_bo_id_panti != null) {
					$qry .= " and a.id='$sesi_bo_id_panti' ";
				}

				

				if ($this->session->userdata('s_cari_global_lembaga') != "") {
					$qry .= "  and (a.nama like '%" . $this->db->escape_like_str($this->session->userdata('s_cari_global_lembaga')) . "%'
							OR a.alamat like '%" . $this->db->escape_like_str($this->session->userdata('s_cari_global_lembaga')) . "%'
						 )  ";
				} elseif ($this->session->userdata('s_cari_global_lembaga') == "") {
					$this->session->unset_userdata('s_cari_global_lembaga');
				}


				$qry .= " ORDER BY id asc";
				//echo $qry;


				$offset = ($this->uri->segment(3) != '' ? $this->uri->segment(3) : 0);
				$config['total_rows'] = $this->db->query($qry)->num_rows();
				$config['per_page'] = $per_page;
				$config['full_tag_open'] = '<div class="table-pagination">';
				$config['full_tag_close'] = '</div>';
				$config['cur_tag_open'] = '<a href="#" class="active"><b>';
				$config['cur_tag_close'] = '</b></a>';
				$config['first_link'] = 'First';
				$config['last_link'] = 'Last';
				$config['next_link'] = 'Next';
				$config['prev_link'] = 'Previous';
				$config['last_tag_open'] = "<span>";
				$config['first_tag_close'] = "</span>";
				$config['uri_segment'] = 3;
				$config['base_url'] = base_url() . '/ref_panti/index';
				$config['suffix'] = '?' . http_build_query($_GET, '', "&");
				$this->pagination->initialize($config);
				$data['paginglinks'] = $this->pagination->create_links();
				$data['per_page'] = $this->uri->segment(3);
				$data['offset'] = $offset;
				if ($data['paginglinks'] != '') {
					$data['pagermessage'] = 'Showing ' . ((($this->pagination->cur_page - 1) * $this->pagination->per_page) + 1) . ' to ' . ($this->pagination->cur_page * $this->pagination->per_page) . ' of ' . $this->db->query($qry)->num_rows();
				}
				$qry .= " limit {$per_page} offset {$offset} ";
				$data['ListData'] = $this->db->query($qry)->result_array();
				$data['role'] = get_role($this->session->userdata('sesi_bo_user_group'));



				$data['breadcrumbs'] = array(
					array(
						'link' => '',
						'name' => 'Data Master'
					),
					array(
						'link' => 'ref_panti',
						'name' => 'Panti Pemerintah '
					)
				);



				$data['sub_judul_form'] = "Panti Pemerintah ";
				$this->template->load('template_admin', 'v_index', $data);
		
		
	}

	public function clear_session()
	{



		$this->session->unset_userdata('s_cari_global_lembaga');
		redirect('ref_panti');
	}

	public function hupus()
	{
		get_role($this->session->userdata('sesi_bo_user_group'), 'delete');
		// $id=$this->uri->segment(3);

		$id = deCrypt($this->uri->segment(3));
		try {

			$this->db->where('id', $id);
			$this->db->delete('ref_panti');
			redirect('ref_panti');
		} catch (Exception $err) {
			log_message("error", $err->getMessage());
			return show_error($err->getMessage());
		}
	}

	


	
	
	
	
		public function tambih()
	{
		get_role($this->session->userdata('sesi_bo_user_group'), 'insert');
		$data['breadcrumbs'] = array(
			array(
				'link' => 'ref_panti/tambih/#',
				'name' => 'Data Master'
			),
			array(
				'link' => 'ref_panti',
				'name' => 'Panti Pemerintah '
			),
			array(
				'link' => 'ref_panti/tambih/#',
				'name' => 'Tambah Data'
			)
		);

		$data['judul_form'] = "Tambah Data Panti Pemerintah ";
		$data['sub_judul_form'] = "";


		$this->template->load('template_admin', 'v_add', $data);
	}

	public function robih($idx)
	{
		$id = deCrypt($idx);

		$data['field'] = $this->db->query("select * from ref_panti where id='$id'")->row();
		
		
		$data['breadcrumbs'] = array(
			array(
				'link' => 'ref_panti/robih/#',
				'name' => 'Data Master'
			),
			array(
				'link' => 'ref_panti',
				'name' => 'Panti Pemerintah '
			),
			array(
				'link' => 'ref_panti/robih/#',
				'name' => 'Ubah Data'
			)
		);

		$data['judul_form'] = "";
		$data['sub_judul_form'] = "Ubah Data Panti Pemerintah ";

		$this->template->load('template_admin', 'v_add', $data);
	}

	public function tambih_robih()
	{


		try {
			
			$id  					= $this->input->post('id');
			$data['nama']	= strtoupper($this->input->post('nama'));			
			$data['alamat']	= strtoupper($this->input->post('alamat'));
			$data['ketua_yayasan']		= strtoupper($this->input->post('ketua_yayasan'));
			$data['no_telp'] = strtoupper($this->input->post('no_telp'));
		
			$data['akreditasi'] 			= strtoupper($this->input->post('akreditasi'));
			
			$data['id_provinsi'] 			= $this->input->post('id_provinsi');
			$data['id_kabkot'] 			= $this->input->post('id_kabkot');
			$data['id_kecamatan'] 			= $this->input->post('id_kecamatan');
			$data['id_kelurahan'] 			= $this->input->post('id_kelurahan');
			
			$data['jenis_panti'] 			= 'PEMERINTAH';
			

		


			if ($id == '' || $id == null) {
				get_role($this->session->userdata('sesi_bo_user_group'),'insert');	
				$data['created_by'] = $this->session->userdata('sesi_bo_nama_lengkap');
				$data['created_date'] = date('Y-m-d H:i:s');
				$xss_data = $this->security->xss_clean($data);
				$this->db->insert('ref_panti', $xss_data);
				
				$this->session->set_flashdata('message_sukses', 'Penambahan Data Berhasil Disimpan');

				
			} else {
				
				get_role($this->session->userdata('sesi_bo_user_group'),'update');	
				
				$data['update_by'] = $this->session->userdata('sesi_bo_nama_lengkap');
				$data['updated_date'] = date('Y-m-d H:i:s');
				$xss_data = $this->security->xss_clean($data);

				$this->db->where('id', $id);
				$this->db->update('ref_panti', $xss_data);
				
				$this->session->set_flashdata('message_sukses', 'Perubahan Data Berhasil Disimpan');

			}


			redirect('ref_panti');

		} catch (Exception $err) {
			log_message("error", $err->getMessage());
			return show_error($err->getMessage());
		}
	}
	
	
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
