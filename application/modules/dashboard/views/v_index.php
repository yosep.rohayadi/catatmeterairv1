<div class="container">
	<div class="page-header">
		<div class="pull-left">
			<h1>Dashboard</h1>
		</div>
		<div class="pull-right">

			<ul class="stats">

				<li class='lightred'>
					<i class="icon-calendar"></i>
					<div class="details">
						<span class="big">February 22, 2013</span>
						<span>Wednesday, 13:56</span>
					</div>
				</li>
			</ul>
		</div>
	</div>

	<div class="row-fluid">
	  <table width="100%" border="1" cellpadding="0" cellspacing="0">
        <tr>
          <td colspan="4" height="25">&nbsp;</td>
          <td>JAN</td>
          <td>FEB</td>
          <td>MAR</td>
          <td>APR</td>
          <td>MEI</td>
          <td>JUN</td>
          <td>JUL</td>
          <td>AGU</td>
          <td>SEP</td>
          <td>OKT</td>
          <td>NOV</td>
          <td>DES</td>
        </tr>
        <tr>
          <td colspan="4" height="25">TOTAL PEMAKAIAN&nbsp;</td>
          <td width="6%">7</td>
          <td width="6%">8</td>
          <td width="6%">8</td>
          <td width="7%">9</td>
          <td width="6%">9</td>
          <td width="6%">10</td>
          <td width="6%">11</td>
          <td width="6%">14</td>
          <td width="6%">14</td>
          <td width="6%">0</td>
          <td width="6%">0</td>
          <td width="21%">0</td>
        </tr>
      </table>
      <HR />
      
      
      <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.js"></script>
<body>
<canvas id="myChart" style="width:100%;max-width:100%"></canvas>

<script>
var xValues = ['JAN','FEB','MAR','APR','MEI','JUN','JUL','AGU','SEP','OKT','NOV'];
var yValues = [7,8,8,9,9,9,10,11,14,14,15];

new Chart("myChart", {
  type: "line",
  data: {
    labels: xValues,
    datasets: [{
      fill: false,
      lineTension: 0,
      backgroundColor: "rgba(0,0,255,1.0)",
      borderColor: "rgba(0,0,255,0.1)",
      data: yValues
    }]
  },
  options: {
    legend: {display: false},
    scales: {
      yAxes: [{ticks: {min: 6, max:16}}],
    }
  }
});
</script>
      
      
	</div>




</div>