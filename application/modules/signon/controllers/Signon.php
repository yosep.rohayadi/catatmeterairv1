<?php
class Signon extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('captcha');
		$this->load->model('m_user');
		$this->load->helper('login');
		$this->load->helper('log');
		$this->load->helper('tool');
	}


	public function indexKu()
	{
		echo "a";
	}


	public function index()
	{
		//validating form fields
		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('user_password', 'Password', 'required');
		$this->form_validation->set_rules('userCaptcha', 'Captcha', 'required|callback_check_captcha');
		//$userCaptcha = $this->input->post('userCaptcha');

		$periodeAktif 			= $this->m_user->periodeAktif();
		$data['running_text'] 	= $this->m_user->list_running_text();

		if ($this->form_validation->run() == false) {
			// numeric random number for captcha
			$random_number = substr(number_format(time() * rand(), 0, '', ''), 0, 4);
			// setting up captcha config
			$vals = array(
				'word' => $random_number,
				'img_path' => './captcha/',
				'img_url' => base_url() . 'captcha/',
				'img_width' => 140,
				'img_height' => 40,
				'expiration' => 7200
			);

			$data['captcha'] = create_captcha($vals);
			$this->session->set_userdata('captchaWordAdmin', $data['captcha']['word']);
			$this->template->load('template_login', 'index', $data);
		} else {

			$result = $this->m_user->m_cek_mail();
			if ($result->num_rows() == 1) {

				$db = $result->row();
				if (hash_verified($this->input->post('user_password'), $db->password)) {

					// CEK STATUS AKTIF
					if ($db->status_aktif == 'Y') {

						$data_login = array(

							'sesi_bo_id' => $db->id,
							'sesi_bo_username' => $db->username,
							'sesi_bo_user_group' => $db->id_user_group,
							'sesi_bo_email' => $db->email,
							'sesi_bo_nama_lengkap' => $db->nama_lengkap,
							'sesi_bo_jabatan' => $db->jabatan,
							'sesi_bo_pangkat' => $db->pangkat,
							'sesi_bo_id_kabkot' => $db->id_kabkot,
							'sesi_bo_id_prov' => $db->id_prov,
							'sesi_bo_id_kel' => $db->id_kel,
							'sesi_bo_id_kec' => $db->id_kec,
							'sesi_bo_id_lks' => $db->id_lks,
							'sesi_bo_id_panti' => $db->id_panti,

							'sesi_bo_nama_kabkot' => getKabkot($db->id_prov, $db->id_kabkot),
							'sesi_bo_nama_prov' => getProv($db->id_prov),
							'sesi_bo_nama_kel' => getKel($db->id_prov, $db->id_kabkot, $db->id_kec, $db->id_kel),
							'sesi_bo_nama_kec' => getKec($db->id_prov, $db->id_kabkot, $db->id_kec),


							// STATUS LOGIN
							'truly_madly_deeply' => TRUE

						);

						$this->session->set_userdata($data_login);

						$this->db->query("UPDATE ref_users SET  sys_ip_address='" . $this->input->ip_address() . "'  ,  sys_login_time='" . date("Y-m-d H:i:s") . "' WHERE username='" . $this->session->userdata('sesi_bo_username') . "'");

						logaksi(
							$this->session->userdata('sesi_bo_username') . " - " . $this->session->userdata('nama_lengkap'),
							'Login',
							$this->session->userdata('sesi_bo_id_prov'),
							$this->session->userdata('sesi_bo_id_kabkot'),
							getProv($this->session->userdata('sesi_bo_id_prov')) . " - " . getKabkot($this->session->userdata('sesi_bo_id_kabkot'), $this->session->userdata('sesi_bo_id_prov'))
						);


						if (!is_dir('./uploads/' . $this->session->userdata('sesi_bo_id'))) {
							mkdir('./uploads/' . $this->session->userdata('sesi_bo_id'), 0777, true);
						}
						redirect('tentang');
					} else {
						$data["pesan"] = "Akun Anda Belum Diaktifkan Oleh Administrator.";
					}
				} else {
					$data["pesan"] = "Username / Password Anda Salah !!!";
				}
			} else {
				$data["pesan"] = "Username / Password Anda Tidak Terdaftar !!!";
			}

			$random_number = substr(number_format(time() * rand(), 0, '', ''), 0, 4);
			$vals = array(
				'word' => $random_number,
				'img_path' => './captcha/',
				'img_url' => base_url() . 'captcha/',
				'img_width' => 140,
				'img_height' => 32,
				'expiration' => 7200
			);
			$data['captcha'] = create_captcha($vals);
			$this->session->set_userdata('captchaWordAdmin', $data['captcha']['word']);
			$this->template->load('template_login', 'index', $data);
		}
	}


	public function check_captcha($str)
	{
		$word = $this->session->userdata('captchaWordAdmin');
		if (strcmp(strtoupper($str), strtoupper($word)) == 0) {
			return true;
		} else {
			$this->form_validation->set_message('check_captcha', 'KODE CAPTCHA SALAH !');
			return false;
		}
	}


	public function logout()
	{

		$this->db->query("UPDATE ref_users SET  sys_ip_address='" . $this->input->ip_address() . "'  ,  sys_logout_time='" . date("Y-m-d H:i:s") . "' WHERE username='" . $this->session->userdata('sesi_bo_username') . "'");
		$this->session->sess_destroy();

		redirect('signon');
	}
}
