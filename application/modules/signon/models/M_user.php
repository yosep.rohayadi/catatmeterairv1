<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_user extends CI_Model
{

	public function m_register()
	{
		$data = array(
			'nama' => $this->input->post('nama'),
			'email' => $this->input->post('email'),
			'password' => get_hash($this->input->post('password'))
		);
		return $this->db->insert('ref_users', $data);
	}


	public function m_cek_mail()
	{
		return $this->db->get_where('ref_users', array('username' => $this->input->post('username')));
	}


	public function periodeAktif()
	{
		$this->db->where('status_aktif', 'Y');
		return $this->db->get("trx_periode")->row_array();
	}


	public function list_running_text()
	{
		$query = "SELECT * FROM trx_runing_text WHERE STR_TO_DATE(NOW(),'%Y-%m-%d') BETWEEN tgl_awal_tampil AND tgl_akhir_tampil ORDER BY created_at ASC";
		return $this->db->query($query)->result_array();
	}
}

/* End of file M_user.php */
/* Location: ./application/models/M_user.php */
