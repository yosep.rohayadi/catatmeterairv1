<div class="container-fluid">
    <br>
    <div class="breadcrumbs">
        <ul>
            <?php foreach ($breadcrumbs as $key => $value) { ?>
                <li>
                    <a href="<?php echo site_url($value['link']) ?>">
                        <?php echo $value['name']; ?></a>
                    <?php echo (count($breadcrumbs) - 1) == $key ? "" : "<i class='icon-angle-right'></i>"; ?>
                </li>
            <?php } ?>
        </ul>
        <div class="close-bread">
            <a href="#"><i class="icon-remove"></i></a>
        </div>
    </div>
</div>



<div class="row-fluid">
	
	<div class="span1">&nbsp;</div>

    <div class="span10">
        <?php
        if ($this->session->flashdata('message_gagal')) {
            echo '<hr><div class="alert alert-error"><button class="close" data-dismiss="alert" type="button">&times;</button>' . $this->session->flashdata('message_gagal') . '</div>';
        }
        if ($this->session->flashdata('message_sukses')) {
            echo '<hr><div class="alert alert-success"><button class="close" data-dismiss="alert" type="button">&times;</button>' . $this->session->flashdata('message_sukses') . '</div>';
        } ?>

        <div class="box box-bordered box-color">
            <div class="box-title">
                <h3><i class=" icon-plus-sign"></i><?php echo $judul_form . " " . $sub_judul_form; ?> </h3>
            </div>
            <div class="box-content nopadding">
                <!-- <form action="#" method="POST" class='form-horizontal form-validate' id="bb"> -->
                <?php echo form_open('validasi_ppks/tambih_robih', array('name' => 'bb', 'id' => 'bb', 'class' => 'form-horizontal form-bordered form-validate form-wysiwyg', 'enctype' => 'multipart/form-data')); ?>


                <input type="hidden" name="id" id="id" value="<?php echo isset($field->id) ? $field->id : ''; ?>">
                <input type="hidden" name="<?= $this->security->get_csrf_token_name(); ?>" value="<?= $this->security->get_csrf_hash(); ?>" style="display: none">

                <input type="hidden" value="<?php echo $this->input->ip_address(); ?>" name="ip_address">
				
				
				
				<?php 
				
				if ($this->session->userdata('sesi_bo_user_group')==1 or $this->session->userdata('sesi_bo_user_group')==2) {
					require_once(APPPATH . '/modules/filter_wilayah/views/v_filter_wilayah.php');  
				} else {
					require_once(APPPATH . '/modules/filter_wilayah/views/v_filter_wilayah_text.php');  
				}
				
				?>
				
				
				<div class="control-group">
										<label class="control-label" for="textfield">Tgl.Register</label>                                        
                                        <div class="controls">
                                        <input type="date" placeholder="" id="tgl_register" class="input-large" name="tgl_register" value="<?php echo isset($field->tgl_register) ? $field->tgl_register : $this->input->post("tgl_register"); ?>"  required>
										
										</div>
									</div>
				
				
				<div class="control-group">
                    <label for="textfield" class="control-label">Nama</label>
                    <div class="controls">
                        <input type="text"  id="nama" class="input-xxlarge" name="nama" value="<?php echo isset($field->nama) ? $field->nama : $this->input->post("nama"); ?>"  required>
                        <span class="required-server"><?php echo form_error('nama', '<p style="color:#F83A18">', '</p>'); ?></span>
                    </div>
                </div>
				
				
				<div class="control-group">
										<label class="control-label" for="textfield">NIK</label>                                        
                                        <div class="controls">
                                        <input maxlength="16" data-rule-minlength="16" type="number" placeholder="" id="nik" class="input-large" name="nik" value="<?php echo isset($field->nik) ? $field->nik : $this->input->post("nik"); ?>"  >
										</div>
									</div>
				
				
				
				<div class="control-group">
                    <label for="textfield" class="control-label">NO.KK </label>
                    <div class="controls">
					   <input type="number" placeholder="" id="nokk" class="input-xxlarge" name="nokk" value="<?php echo isset($field->nokk) ? $field->nokk : $this->input->post("nokk"); ?>"  ><span class="required-server"><?php echo form_error('nokk', '<p style="color:#F83A18">', '</p>'); ?></span>
                    </div>
                </div>
				
				
				<div class="control-group">
                    <label for="textfield" class="control-label">Tempat Lahir</label>
                    <div class="controls">
										   <input type="text" placeholder="" id="tempat_lahir" class="input-xxlarge" name="tempat_lahir" value="<?php echo isset($field->tempat_lahir) ? $field->tempat_lahir : $this->input->post("tempat_lahir"); ?>"  >

 <span class="required-server"><?php echo form_error('tempat_lahir', '<p style="color:#F83A18">', '</p>'); ?></span>
                    </div>
                </div>

                
				<div class="control-group">
										<label class="control-label" for="textfield">Tgl.Lahir</label>                                        
                                        <div class="controls">
                                        <input type="date" placeholder="" id="tgl_lahir" class="input-large" name="tgl_lahir" value="<?php echo isset($field->tgl_lahir) ? $field->tgl_lahir : $this->input->post("tgl_lahir"); ?>"  required>
										
										</div>
									</div>
									
									
				<div class="control-group">
			<?php $sex= isset($field->sex)?$field->sex:'';?>
										<label class="control-label" for="textfield">Jenis Kelamin</label>                                        
                                        <div class="controls">
                                        <select name="sex" required>
										<option value="">-Pilih-</option>
                                           <option value="LAKI-LAKI" <?php if ($sex=="LAKI-LAKI") { echo "selected";}?>>LAKI-LAKI</option>
                                          <option value="PEREMPUAN"  <?php if ($sex=="PEREMPUAN") { echo "selected";}?>>PEREMPUAN</option>
                                        </select>
										</div>
									</div>
									
									
									
				<div class="control-group">
                    <label for="textfield" class="control-label">Alamat</label>
                    <div class="controls">
                        <input type="text" placeholder="" id="alamat" class="input-xxlarge" name="alamat" value="<?php echo isset($field->alamat) ? $field->alamat : $this->input->post("alamat"); ?>"  >
                        <span class="required-server"><?php echo form_error('alamat', '<p style="color:#F83A18">', '</p>'); ?></span>
                    </div>
                </div>
				
				
				
				
				
			
			
				
				
				<div class="control-group">
                    <label for="textfield" class="control-label">Kategori PPKS</label>
                    <div class="controls">
                        <?php $id_kategori = isset($field->id_kategori) ? $field->id_kategori : $this->input->post("id_kategori"); ?>

                        

							<select class="input-xxlarge" name="id_kategori" required>
							
							<option value="">-Pilih-</option>
							<?php foreach ($list_kategori as $row) { ?>
							<option value="<?php echo $row->id; ?>" <?php if ($id_kategori == $row->id) { echo "selected";} ?>><?php echo $row->nama; ?></option>
							<?php } ?>
							</select>
                            <hr>

                        



                        <span class="required-server"><?php echo form_error('uraian', '<p style="color:#F83A18">', '</p>'); ?></span>
                    </div>
                </div>
                
                
				
				
				<div class="control-group">
			<?php $status_keberadaan= isset($field->status_keberadaan)?$field->status_keberadaan:'';?>
										<label class="control-label" for="textfield">Keberadaan Saat Ini ?</label>                                        
                                        <div class="controls">
                                        <select name="status_keberadaan" required   onchange="doKeberadaan(this.value);">
										<option value="">-Pilih-</option>
                                           <option value="MASYARAKAT" <?php if ($status_keberadaan=="MASYARAKAT") { echo "selected";}?>>MASYARAKAT</option>
                                          <option value="LKS"  <?php if ($status_keberadaan=="LKS") { echo "selected";}?>>LKS</option>
                                        </select>
										</div>
									</div>
					
				
				
				
				
								
				
				
				


                

				<div id="v_lks">
				
				
					<div class="control-group">
											<label class="control-label" for="textfield">Nama LKS</label>                                        
											<div class="controls">
											<input type="text" placeholder="" id="nama_lks" class="input-xxlarge" name="nama_lks" value="<?php echo isset($field->nama_lks) ? $field->nama_lks : $this->input->post("nama_lks"); ?>"  required>
											</div>
					</div>
				
				
				
				</div>
                                    
				
				
				<div id="v_masyarakat">
				
				
					<div class="control-group">
											<label class="control-label" for="textfield">Sudah Mendapatkan Bantuan ?</label>                                        
											<div class="controls">
											<?php $status_bantuan= isset($field->status_bantuan)?$field->status_bantuan:'';?> 
											<select name="status_bantuan" required   onchange="doBantuan(this.value);">
										<option value="">-Pilih-</option>
                                           <option value="YA" <?php if ($status_bantuan=="YA") { echo "selected";}?>>YA</option>
                                          <option value="TIDAK"  <?php if ($status_bantuan=="TIDAK") { echo "selected";}?>>TIDAK</option>
                                        </select>
											
											</div>
					</div>
					
					
			
				<div class="control-group" id="v_butuh_panti">
											<label class="control-label" for="textfield">Butuh Panti ?</label>                                        
											<div class="controls">
											<?php $status_panti= isset($field->status_panti)?$field->status_panti:'';?> 
											<select name="status_panti" >
										<option value="">-Pilih-</option>
                                           <option value="YA" <?php if ($status_panti=="YA") { echo "selected";}?>>YA</option>
                                          <option value="TIDAK"  <?php if ($status_panti=="TIDAK") { echo "selected";}?>>TIDAK</option>
                                        </select>
											
											</div>
					</div>
				
				
				</div>
				
				
				
				
				
				<script type="text/javascript">
				
				
				
				
				   <?php if ($status_keberadaan=="MASYARAKAT") { ?>
				   
							$("#v_lks").hide();
							$("#v_masyarakat").show();
							 
							 <?php if ($status_bantuan=="YA") { ?>
								$("#v_butuh_panti").show();
							 <?php } else { ?>
								$("#v_butuh_panti").hide();
							 <?php } ?>
							
				   
				   
				   <?php } elseif ($status_keberadaan=="LKS") { ?>
				   
							$("#v_lks").show();
							$("#v_masyarakat").hide();
							$("#v_butuh_panti").hide();
				   
				   <?php } else { ?>
				   
				    $("#v_lks").hide();
					$("#v_masyarakat").hide();
				   
				   <?php } ?>
					
					
					function doKeberadaan(flag) {
						
						//alert(flag);
						if (flag=="MASYARAKAT") {
							
							$("#v_lks").hide();
							$("#v_masyarakat").show();
							$("#v_butuh_panti").hide();
							
						} else if (flag=="LKS") { 
						
							$("#v_lks").show();
							$("#v_masyarakat").hide();
							$("#v_butuh_panti").hide();
						}
						
					}
					
					
					
						function doBantuan(flag) {
						
						//alert(flag);
						if (flag=="YA") {
							
							$("#v_butuh_panti").show();
							
						} else if (flag=="TIDAK") { 
						
						    $("#v_butuh_panti").hide();

						}
						
					}
					
					
			    </script>
				
				
				
				
				

				
				

                <div class="control-group">
                    <label class="control-label" for="textfield">Photo Personal</label>
                    <div class="controls">
                        <!-- <input type="hidden" name="nip" id="nip" class="input-large" value=""> -->
                        <input type="file" name="berkas" id="berkas" accept="image/*">
                        <span class="required-server">
                            <p>*<small>File tipe gambar</small>
							<br/>*<small>Ukuran maksimal 1 MB</small>
							</p>
                        </span>

                        <?php if (isset($field->foto)) { ?>
                            <img src="<?php echo base_url(); ?>/<?php echo isset($field->foto) ? $field->foto : ''; ?>" >
                        <?php } ?>
                    </div>
                </div>








                <div class="form-actions">
                    <button id="btn_simpan" class="btn btn-blue" type="submit">Simpan</button>
                    <a class="btn btn-danger" href="<?php echo site_url(); ?>trx_pendataan">Kembali</a>
                </div>

                </form>
            </div>
        </div>
		
		
		<div class="span1">&nbsp;</div>
		
    </div>

