<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Form extends MX_Controller
{



	public function __construct()
	{
		parent::__construct();
		$this->load->model("Main_model");
	}


	public function index()
	{
		$data["test"] = "";
		$this->template->load('template_admin', 'v_form', $data);
	}


	public function load_kabkota_tugas($str)
	{


		$sesi_bo_id_kabkot = $this->session->userdata('sesi_bo_id_kabkot');

		$sql = "select * from ref_wil_kabkot where id_provinsi='$str'";
		if ($sesi_bo_id_kabkot != "") {
			$sql .= " and kode_kabkot='$sesi_bo_id_kabkot'";
		}

		echo '<select name="id_kabkot" class="form-control" id="id_kabkot_tugas" data-rule-required="false"  >';
		$query1 = $this->db->query($sql);
		echo '<option value="">-Pilih-</option>';
		foreach ($query1->result_array() as $row) {
			$id = $row['kode_kabkot'];
			$nama = $row['nama'];

			echo '<option value=' . $id . '>' . $nama . '</option>';
		}
		echo '</select>';
	}



	public function load_kabkota_global($str)
	{
		$sql = "select * from ref_wil_kabkot where id_provinsi='$str'";

		echo '<select name="id_kabkot" class="form-control" id="id_kabkot_tugas" data-rule-required="false"  >';
		$query1 = $this->db->query($sql);
		echo '<option value="">-Pilih-</option>';
		foreach ($query1->result_array() as $row) {
			$id = $row['kode_kabkot'];
			$nama = $row['nama'];

			echo '<option value=' . $id . '>' . $nama . '</option>';
		}
		echo '</select>';
	}





	public function load_kabkot()
	{

		$id = $this->input->post('id');
		$sql = "SELECT kode_kabkot, nama
				from ref_wil_kabkot 
				WHERE id_provinsi = " . $this->db->escape($id) . "
		";

		$result = $this->db->query($sql)->result();

		echo "<option value=''>PILIH</option>";
		if (count($result) > 0) {
			foreach ($result as $key => $value) {
				$txt = "<option value = '" . $value->kode_kabkot . "' >";
				$txt .= $value->nama;
				$txt .= "</option>";
				echo $txt;
			}
		}
	}


	public function list_kabkot()
	{
		$id_provinsi 	= $this->input->post('id_provinsi');
		$id_kabkot 		= $this->input->post('id_kabkot');

		$data['id_kabkot'] = $id_kabkot;
		$data['list_data'] = $this->Main_model->list_kabkot($id_provinsi);
		echo $this->load->view('ajax/list_kabkot', $data);
	}


	public function list_kecamatan()
	{
		$id_kabkot 		= $this->input->post('id_kabkot');
		$id_kecamatan 	= $this->input->post('id_kecamatan');

		$data['id_kecamatan'] 	= $id_kecamatan;
		$data['list_data'] 		= $this->Main_model->list_kecamatan($id_kabkot);
		echo $this->load->view('ajax/list_kecamatan', $data);
	}


	public function list_kelurahan()
	{
		$id_kecamatan 	= $this->input->post('id_kecamatan');
		$id_kelurahan 		= $this->input->post('id_kelurahan');

		$data['id_kelurahan'] 	= $id_kelurahan;
		$data['list_data'] 		= $this->Main_model->list_kelurahan($id_kecamatan);
		echo $this->load->view('ajax/list_kelurahan', $data);
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
