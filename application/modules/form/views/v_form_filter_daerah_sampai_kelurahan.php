<div class="control-group">
    <label for="textfield" class="control-label">Provinsi</label>
    <?php $list_prov = $this->db->get("ref_wil_provinsi")->result_array(); ?>
    <div class="controls">
        <select name="id_provinsi" id="id_provinsi" class="input-xxlarge select2" onchange="load_kabkot(this.value, '')">
            <option value="">PILIH</option>
            <?php foreach ($list_prov as $key => $value) { ?>
                <?php $s = $value['id_provinsi'] == $id_provinsi ? "selected" : ''; ?>
                <option value="<?php echo $value['id_provinsi'] ?>" <?php echo $s ?>>
                    <?php echo $value['nama'] ?>
                </option>
            <?php } ?>
        </select>
    </div>
</div>


<div class="control-group">
    <label for="textfield" class="control-label">Kabupaten / Kota</label>
    <div class="controls">
        <select name="id_kabkot" id="id_kabkot" class="input-xxlarge select2" onchange="load_kecamatan(this.value, '')">
            <option value="">PILIH</option>
        </select>
    </div>
</div>


<div class="control-group">
    <label for="textfield" class="control-label">Kecamatan</label>
    <div class="controls">
        <select name="id_kecamatan" id="id_kecamatan" onchange="load_kelurahan(this.value, '')" class="input-xxlarge select2">
            <option value="">PILIH</option>
        </select>
    </div>
</div>

<div class="control-group">
    <label for="textfield" class="control-label">Kelurahan</label>
    <div class="controls">
        <select name="id_kelurahan" id="id_kelurahan" class="input-xxlarge select2">
            <option value="">PILIH</option>
        </select>
    </div>
</div>

<script>
    let id_provinsi = '';
    let id_kabkot = '';
    let id_kecamatan = '';
    let id_kelurahan = '';

    if ('<?php echo $id_provinsi ?? '' ?>' != '') {
        load_kabkot('<?php echo $id_provinsi ?? '' ?>', '<?php echo $id_kabkot ?? '' ?>');
    }

    if ('<?php echo $id_kabkot ?? '' ?>' != '') {
        load_kecamatan('<?php echo $id_kabkot ?? '' ?>', '<?php echo $id_kecamatan ?? '' ?>');
    }

    if ('<?php echo $id_kecamatan ?? '' ?>' != '') {
        load_kelurahan('<?php echo $id_kecamatan ?? '' ?>', '<?php echo $id_kelurahan ?? '' ?>');
    }

    function load_kabkot(id_prov, id_kbkt) {
        this.id_provinsi = id_prov;
        $.ajax({
            url: '<?php echo base_url('form/list_kabkot') ?>',
            type: 'post',
            data: {
                'id_provinsi': id_prov,
                'id_kabkot': id_kbkt,
            },
            success: function(response) {
                $('#id_kabkot').html(response);
            },
            error: function(response) {
                alert(response.error);
            }
        });
    }

    function load_kecamatan(id_kbkt, id_kec) {
        this.id_kabkot = id_kbkt;
        $.ajax({
            url: '<?php echo base_url('form/list_kecamatan') ?>',
            type: 'post',
            data: {
                'id_kabkot': id_kbkt,
                'id_kecamatan': id_kec,
            },
            success: function(response) {
                $('#id_kecamatan').html(response);
            },
            error: function(response) {
                alert(response.error);
            }
        });
    }

    function load_kelurahan(id_kec, id_kel) {
        this.id_kecamatan = id_kec;

        $.ajax({
            url: '<?php echo base_url('form/list_kelurahan') ?>',
            type: 'post',
            data: {
                'id_provinsi': this.id_provinsi,
                'id_kabkot': this.id_kabkot,
                'id_kecamatan': id_kec,
                'id_kelurahan': id_kel,
            },
            success: function(response) {
                $('#id_kelurahan').html(response);
            },
            error: function(response) {
                alert(response.error);
            }
        });
    }
</script>