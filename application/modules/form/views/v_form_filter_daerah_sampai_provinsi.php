<div class="control-group">
    <label for="textfield" class="control-label">Provinsi</label>
    <?php $list_prov = $this->db->get("ref_wil_provinsi")->result_array(); ?>
    <div class="controls">
        <select name="id_provinsi" class="input-xlarge select2" onchange="load_kabkot(this.value, '')">
            <option value="">PILIH</option>
            <?php foreach ($list_prov as $key => $value) { ?>
                <?php $s = $value['id_provinsi'] == $id_provinsi ? "selected" : ''; ?>
                <option value="<?php echo $value['id_provinsi'] ?>" <?php echo $s ?>>
                    <?php echo $value['nama'] ?>
                </option>
            <?php } ?>
        </select>
    </div>
</div>