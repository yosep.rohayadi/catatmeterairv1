<option value="">PILIH</option>
<?php if (count($list_data) > 0) { ?>
    <?php foreach ($list_data as $key => $value) { ?>
        <?php $s = $value['id_kelurahan'] == $id_kelurahan ? 'selected' : '' ?>
        <option value="<?php echo $value['id_kelurahan'] ?>" <?= $s ?>>
            <?php echo $value['nama'] ?>
        </option>
    <?php } ?>
<?php } ?>