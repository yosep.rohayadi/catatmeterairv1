<option value="">PILIH</option>
<?php if (count($list_data) > 0) { ?>
    <?php foreach ($list_data as $key => $value) { ?>
        <?php $s = $value['id_kabkot'] == $id_kabkot ? 'selected' : '' ?>
        <option value="<?php echo $value['id_kabkot'] ?>" <?= $s ?>>
            <?php echo $value['nama'] ?>
        </option>
    <?php } ?>
<?php } ?>