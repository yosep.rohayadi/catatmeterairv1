<option value="">PILIH</option>
<?php if (count($list_data) > 0) { ?>
    <?php foreach ($list_data as $key => $value) { ?>
        <?php $s = $value['id_kecamatan'] == $id_kecamatan ? 'selected' : '' ?>
        <option value="<?php echo $value['id_kecamatan'] ?>" <?= $s ?>>
            <?php echo $value['nama'] ?>
        </option>
    <?php } ?>
<?php } ?>