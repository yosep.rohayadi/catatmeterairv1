<div class="alert alert-success">
    <b>
        Periode PPKM yang sekarang diterapkan adalah mulai dari tanggal <?php echo date('d-m-Y', strtotime($this->session->userdata('tgl_awal_aktif'))) ?> sampai dengan <?php echo date('d-m-Y', strtotime($this->session->userdata('tgl_akhir_aktif'))) ?>
    </b>
</div>