<input type="hidden" name="id_provinsi" value="<?php echo $this->session->userdata('sesi_bo_id_prov'); ?>">
<input type="hidden" name="id_kabkot" value="<?php echo $this->session->userdata('sesi_bo_id_kabkot'); ?>">
<input type="hidden" name="id_kec" value="<?php echo $this->session->userdata('sesi_bo_id_kec'); ?>">
<input type="hidden" name="id_kel" value="<?php echo $this->session->userdata('sesi_bo_id_kel'); ?>">

<div class="control-group">
    <label for="textfield" class="control-label">Provinsi</label>
    <div class="controls">
        <?php echo $this->session->userdata('sesi_bo_nama_prov'); ?>
    </div>
</div>

<div class="control-group">
    <label for="textfield" class="control-label">Kabkot</label>
    <div class="controls">
        <?php echo $this->session->userdata('sesi_bo_nama_kabkot'); ?>
    </div>
</div>


<div class="control-group">
    <label for="textfield" class="control-label">Kecamatan</label>
    <div class="controls">
        <?php echo $this->session->userdata('sesi_bo_nama_kec'); ?>
    </div>
</div>

<div class="control-group">
    <label for="textfield" class="control-label">Kelurahan</label>
    <div class="controls">
        <?php echo $this->session->userdata('sesi_bo_nama_kel'); ?>
    </div>
</div>