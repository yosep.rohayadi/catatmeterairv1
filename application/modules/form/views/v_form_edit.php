
				<div class="control-group">
					<label for="textfield" class="control-label">Nama</label>
					<div class="controls">
						
						<input type="text" name="nama" class="input-xxlarge" data-rule-required="true"  value="<?php echo $data_pemohon->nama;?>">
	                	
					</div>
				</div>
				
				
				<div class="control-group">
					<label for="textfield" class="control-label">Alamat</label>
					<div class="controls">
						
						<input type="text" name="alamat" class="input-xxlarge" data-rule-required="true"  value="<?php echo $data_pemohon->alamat;?>">
	                	
					</div>
				</div>
				
				
				<div class="control-group">
					<label for="textfield" class="control-label">NIK (No. KTP-el)</label>
					<div class="controls">
						
						<input type="text" name="ktp" class="input-xxlarge" maxlength="16" data-rule-required="true" data-rule-number="true"  value="<?php echo $data_pemohon->ktp;?>" >
	                	
					</div>
				</div>
				
				<div class="control-group">
					<label for="textfield" class="control-label">File Scan KTP-el</label>
					<div class="controls">
						
						<input type="text" name="file_scan_ktp_elektronik" class="input-xxlarge" data-rule-required="true" readonly value="<?php echo $data_pemohon->file_scan_ktp_elektronik;?>">
						<a href="javascript: void(0);" class="btn btn-primary" onclick='window.open("<?php echo site_url();?>media/index_general/file_scan_ktp_elektronik/", "", "width=600,height=300");'>Upload</a>&nbsp;
	                	
					</div>
				</div>
				
				
				
				<div class="control-group">
					<label for="textfield" class="control-label">Pemda / Lembaga / Umum</label>
					<div class="controls">
					
					

		        <select name="id_instansi_pemohon" name="id_instansi_pemohon" id="id_instansi_pemohon" data-rule-required="true" class="input-xxlarge" onchange="doView(this.value)" >
						<option value="">Pilih</option>
						<?php
		            $sql_instansi_pemohon = "SELECT * FROM ref_instansi_pemohon";
		            $query_instansi_pemohon = $this->db->query($sql_instansi_pemohon);
					 foreach ($query_instansi_pemohon->result() as $row_instansi_pemohon) {
						?>
						
						<option value="<?php echo $row_instansi_pemohon->id; ?>"<?php echo isset($data_pemohon->id_instansi_pemohon)?($row_instansi_pemohon->id==$data_pemohon->id_instansi_pemohon?'selected=""':''):''?>><?php echo $row_instansi_pemohon->nama; ?></option>
					 <?php } ?>
						</select>
						
						
						
						
	                	
					</div>
				</div>
				
				
				
				
				
				<div class="control-group" id="MyProvinsi">
				<label class="control-label" for="textfield">Provinsi </label>

				<div class="controls">
					
					<select name="id_prov_tugas" id="id_prov_tugas"  class="form-control" data-rule-required="true" >
                				<option value="">Pilih</option>
								<?php 
											  $id_prov_tugas = $this->input->post("id_prov_tugas");
											  $q2=$this->db->query("select * from ref_provinsi order by id asc");
											  foreach($q2->result() as $rq2){
											  ?>
								
								<option value="<?php echo $rq2->id; ?>" <?php echo isset($data_pemohon->id_prov)?($rq2->id==$data_pemohon->id_prov?'selected=""':''):''?>><?php echo $rq2->nama; ?></option>
											  <?php } ?>
					</select>
					
				</div>
				</div>
				
				
				
				<div id="Mykabkot">
			 
			 
			 <div class="control-group">
				<label class="control-label" for="textfield">Provinsi </label>

				<div class="controls">


					
					<select name="id_prov_tugasx" id="id_prov_tugasx"  class="form-control" data-rule-required="true" onchange="showUser(this.value);" >
                				<option value="">Pilih</option>
								<?php 
											  $id_prov_tugas = $this->input->post("id_prov_tugas");
											  $q2=$this->db->query("select * from ref_provinsi order by id asc");
											  foreach($q2->result() as $rq2){
											  ?>
								
								<option value="<?php echo $rq2->id; ?>" <?php echo isset($data_pemohon->id_prov)?($rq2->id==$data_pemohon->id_prov?'selected=""':''):''?>><?php echo $rq2->nama; ?></option>
											  <?php } ?>
					</select>
					
				</div>
			 </div>
			 
			 
			 
			 <div class="control-group" >
				<label class="control-label" for="textfield">Kab/Kota </label>

				<div class="controls">
					
					
					<div id ="txtHint" >
					<select name="id_kabkot_tugas" id="id_kabkot_tugas"  class="form-control" data-rule-required="true" >
												<option value="">Pilih</option>			
												
												<?php 		$id_prov_tugas = $data_pemohon->id_prov;
															$id_kabkot_tugas = $this->input->post("id_kabkot_tugas");
															$query1 = $this->db->query("select * from ref_kabupaten where prov_id='".$id_prov_tugas."'");
															foreach ($query1->result_array() as $row)
															{
																$id=$row['id'];
																$nama=$row['nama'];
															?>
																<option value=<?php echo $id; ?>  <?php echo isset($data_pemohon->id_kabkot)?($id==$data_pemohon->id_kabkot?'selected=""':''):''?>><?php echo $nama; ?></option>


															<?php } ?>

												
												</select>
					
					</div>
					
				</div>
			 </div>
			 
			 
			 </div>
			 
			 
			 <div class="control-group"  id="MyInst">
				<label class="control-label" for="textfield">Nama Lembaga / Umum</label>

				<div class="controls">
					
					
					<input type="text" class="input-xlarge" placeholder="" id="nama_instansi_pemohon" name="nama_instansi_pemohon" data-rule-required="true"  value="<?php echo  $this->input->post('nama_instansi_pemohon'); ?>">
					
					
				</div>
			 </div>
				

<script>



$(document).ready(function(){

	

	var id_prov_tugas = $("#id_prov_tugas").val();
	var id_kabkot_tugas = $("#id_kabkot_tugas").val();
	var id_instansi_pemohon = $("#id_instansi_pemohon").val();

	// alert(id_instansi_pemohon);

					
	$("#MyProvinsi").hide();
	$("#Mykabkot").hide();
	$("#MyInst").hide();

	if (id_instansi_pemohon=="3") {
		$("#Mykabkot").show();
	}
	else if (id_kabkot_tugas!="1") {
		$("#MyProvinsi").show();
	}
	else if (id_kabkot_tugas!="2") {
		$("#MyProvinsi").show();
	}
	

});




function doView(id_instansi){
	
	
	if (id_instansi==1 || id_instansi==2) {
		
		$("#MyProvinsi").show();
		$("#Mykabkot").hide();
		$("#MyInst").hide();
		
	} else if (id_instansi==3) {
		
		$("#MyProvinsi").hide();
		$("#Mykabkot").show();
		$("#MyInst").hide();
		
	} else if (id_instansi==4) {
		
		$("#MyProvinsi").hide();
		$("#Mykabkot").hide();
		$("#MyInst").show();
		
	} 
	
	
	
}



function showUser(str) {
    if (str == "") {
        document.getElementById("txtHint").innerHTML = "";
        return;
    } else { 
        if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else {
            // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("txtHint").innerHTML = this.responseText;
             
			} else {
                document.getElementById("txtHint").innerHTML = "Mohon Tunggu....";
            }
        };
        xmlhttp.open("GET","<?php echo site_url(); ?>form/load_kabkota_tugas/"+str,true);
        xmlhttp.send();
    }
}


 
</script>