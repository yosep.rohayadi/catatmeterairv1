<div style="margin-bottom: 10px;">
    <label>Provinsi</label>
    <?php $list_prov = $this->db->get("ref_wil_provinsi")->result_array(); ?>
    <select name="id_provinsi" class="form-control select2" onchange="load_kabkot(this.value, '')">
        <option value="">PILIH</option>
        <?php foreach ($list_prov as $key => $value) { ?>
            <?php $s = $value['id_provinsi'] == $id_provinsi ? "selected" : ''; ?>
            <option value="<?php echo $value['id_provinsi'] ?>" <?php echo $s ?>>
                <?php echo $value['nama'] ?>
            </option>
        <?php } ?>
    </select>
</div>
<div style="margin-bottom: 10px;">
    <label>Kabupaten / Kota</label>
    <select name="id_kabkot" id="id_kabkot" class="form-control select2" onchange="load_kecamatan(this.value, '')">
        <option value="">PILIH</option>
    </select>
</div>
<div style="margin-bottom: 10px;">
    <label>Kecamatan</label>
    <select name=" id_kecamatan" id="id_kecamatan" class="form-control select2">
        <option value="">PILIH</option>
    </select>
</div>


<script>
    if ('<?php echo $id_kabkot ?? '' ?>' != '') {
        load_kecamatan('<?php echo $id_kabkot ?? '' ?>', '<?php echo $id_kecamatan ?? '' ?>');
    }

    if ('<?php echo $id_kecamatan ?? '' ?>' != '') {
        load_kelurahan('<?php echo $id_kecamatan ?? '' ?>', '<?php echo $id_kelurahan ?? '' ?>');
    }

    function load_kabkot(id_provinsi, id_kabkot) {
        $.ajax({
            url: '<?php echo base_url('form/list_kabkot') ?>',
            type: 'post',
            data: {
                'id_provinsi': id_provinsi,
                'id_kabkot': id_kabkot,
            },
            success: function(response) {
                $('#id_kabkot').html(response);
            },
            error: function(response) {
                alert(response.error);
            }
        });
    }

    function load_kecamatan(id_kabkot, id_kecamatan) {
        $.ajax({
            url: '<?php echo base_url('form/list_kecamatan') ?>',
            type: 'post',
            data: {
                'id_kabkot': id_kabkot,
                'id_kecamatan': id_kecamatan,
            },
            success: function(response) {
                $('#id_kecamatan').html(response);
            },
            error: function(response) {
                alert(response.error);
            }
        });
    }
</script>