<div class="control-group">
    <label for="textfield" class="control-label">Provinsi</label>
    <?php $list_prov = $this->db->get("ref_wil_provinsi")->result_array(); ?>
    <div class="controls">
        <select name="id_provinsi" class="input-xlarge select2" onchange="load_kabkot(this.value, '')">
            <option value="">PILIH</option>
            <?php foreach ($list_prov as $key => $value) { ?>
                <?php $s = $value['id_provinsi'] == $id_provinsi ? "selected" : ''; ?>
                <option value="<?php echo $value['id_provinsi'] ?>" <?php echo $s ?>>
                    <?php echo $value['nama'] ?>
                </option>
            <?php } ?>
        </select>
    </div>
</div>


<div class="control-group">
    <label for="textfield" class="control-label">Kabupaten / Kota</label>
    <div class="controls">
        <select name="id_kabkot" id="id_kabkot" class="input-xlarge select2" onchange="load_kecamatan(this.value, '')">
            <option value="">PILIH</option>
        </select>
    </div>
</div>


<div class="control-group">
    <label for="textfield" class="control-label">Kecamatan</label>
    <div class="controls">
        <select name=" id_kecamatan" id="id_kecamatan" class="input-xlarge select2">
            <option value="">PILIH</option>
        </select>
    </div>
</div>


<script>
    if ('<?php echo $id_kabkot ?? '' ?>' != '') {
        load_kecamatan('<?php echo $id_kabkot ?? '' ?>', '<?php echo $id_kecamatan ?? '' ?>');
    }

    if ('<?php echo $id_kecamatan ?? '' ?>' != '') {
        load_kelurahan('<?php echo $id_kecamatan ?? '' ?>', '<?php echo $id_kelurahan ?? '' ?>');
    }

    function load_kabkot(id_provinsi, id_kabkot) {
        $.ajax({
            url: '<?php echo base_url('form/list_kabkot') ?>',
            type: 'post',
            data: {
                'id_provinsi': id_provinsi,
                'id_kabkot': id_kabkot,
            },
            success: function(response) {
                $('#id_kabkot').html(response);
            },
            error: function(response) {
                alert(response.error);
            }
        });
    }

    function load_kecamatan(id_kabkot, id_kecamatan) {
        $.ajax({
            url: '<?php echo base_url('form/list_kecamatan') ?>',
            type: 'post',
            data: {
                'id_kabkot': id_kabkot,
                'id_kecamatan': id_kecamatan,
            },
            success: function(response) {
                $('#id_kecamatan').html(response);
            },
            error: function(response) {
                alert(response.error);
            }
        });
    }
</script>