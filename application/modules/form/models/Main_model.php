<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Main_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}


	public function list_provinsi()
	{
		return $this->db->get("ref_wil_provinsi")->result_array();
	}

	public function list_kabkot($id_provinsi)
	{
		$this->db->where('id_provinsi', $id_provinsi);
		return $this->db->get("ref_wil_kabkot")->result_array();
	}

	public function list_kecamatan($id_kabkot)
	{
		$this->db->where('id_kabkot', $id_kabkot);
		return $this->db->get("ref_wil_kec")->result_array();
	}


	public function list_kelurahan($id_kecamatan)
	{
		$this->db->where('id_kecamatan', $id_kecamatan);
		return $this->db->get("ref_wil_kel")->result_array();
	}
}
