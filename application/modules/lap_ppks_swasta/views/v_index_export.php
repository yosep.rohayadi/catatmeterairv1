<?php 

header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
header("Content-Disposition: attachment;filename=\"export_ppks_panti_swasta.xls\"");
header("Cache-Control: max-age=0");


?>

<table width="100%" border="1" cellpadding="0" cellspacing="0" class="table table-hover table-striped table-bordered">
<thead>
                                
                                <tr> <th width="10%">Nama Panti</th>
                                  <th width="10%">Kategori PPKS</th>
                                  <th width="10%">Kode/Tgl.Register</th>
                                  <th width="10%">NIK/KK</th>
                                  <th width="10%"><center>
                                    Nama
                                  </center></th>
                                  <th width="10%">Jenis Kelamin</th>
                                  <th width="11%">TTL</th>
                                  <th width="11%">Alamat</th>
                                  <th width="11%">Status</th>
                                  <th width="11%">Tgl.Keluar</th>
                                  <th width="9%">Created By</th>
                              <th width="9%">Updated By</th>
                              <th width="9%">Validasi Kab/Kot By</th>
                              <th width="9%">Catatan Validasi</th>
                              </tr>
                            </thead>
                            <tbody>

                                <?php if (count($ListData) > 0) {
                                    foreach ($ListData as $key => $value) {
                                ?>

                                        <tr style="font-size:10px;">
										<td><?php echo $value['nama_panti'] ?></td>
                                          <td><?php echo $value['kategori'] ?></td>
                                            <td><?php echo $value['kode_register'] ?>/<?php echo $value['tgl_register'] ?></td>
                                            <td><?php echo $value['nik'] ?> / <?php echo $value['nokk'] ?></td>
                                            <td><?php echo $value['nama'] ?></td>
                                            <td><?php echo $value['sex'] ?></td>
                                            <td><?php echo $value['tempat_lahir'] ?><br />
                                            <?php echo $value['tgl_lahir'] ?>
                                            <td><?php echo $value['alamat'] ?>                                            
                                            <td><?php echo $value['status_keberadaan'] ?>
                                          <td><?php echo $value['tgl_out'] ?>
                                          <td><?php echo $value['created_by'] ?><br />
                                        <?php echo $value['created_date'] ?></td>
                                      <td><?php echo $value['update_by'] ?><br />
                                        <?php echo $value['updated_date'] ?></td>
                                        
                                        <?php 
										if ($value['approved']=="Approved") { $bgcolor="#00FF00"; $cat="Approved"; } 
										elseif ($value['approved']=="Not Approved") {  $bgcolor="#FF0000"; $cat="Not Approved"; } 
										elseif ($value['approved']=="Belum Di-Validasi") {  $bgcolor="#FFFF00"; $cat="Belum Di-Validasi"; } 
										?>
                                        
                                      <td style="background-color:<?php echo $bgcolor; ?>"><?php echo $cat; ?><br />
                                        <?php echo $value['approve_date'] ?><br />
                                        <?php echo $value['approve_by'] ?></td>
                                      <td><?php echo $value['catatan_validasi'] ?></td>
                                      <!--
											-->
                              </tr>

                                <?php
                                    }
                                    $paging = (!empty($pagermessage) ? $pagermessage : '');


                                    echo "<tr><td colspan='13'><div style='background:000;'>$paging &nbsp;" . $this->pagination->create_links() . "</div></td></tr>";
                                } else {
                                    echo "<tbody><tr><td colspan='13' style='padding:10px; background:#F00; border:none; color:#FFF;'>Data Tidak Tersedia</td></tr></tbody>";
                                }

                                ?>
                            </tbody>
                        </table>
