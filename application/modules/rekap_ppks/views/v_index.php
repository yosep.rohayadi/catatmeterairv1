<div class="container-fluid">
    <br>
    <div class="breadcrumbs">
        <ul>
            <?php foreach ($breadcrumbs as $key => $value) { ?>
                <li>
                    <a href=<?php echo site_url($value['link']) ?>>
                        <?php echo $value['name']; ?></a>
                    <?php echo (count($breadcrumbs) - 1) == $key ? "" : "<i class='icon-angle-right'></i>"; ?>
                </li>
            <?php } ?>
        </ul>
        <div class="close-bread">
            <a href="#"><i class="icon-remove"></i></a>
        </div>
    </div>
</div>

<div class="row-fluid">
    <div class="span12">
        <div class="box">
            <div class="box-title">
                <h3>
                    <i class="icon-reorder"></i>
                    <?php echo $sub_judul_form; ?>
                </h3>
            </div>
            <div class="box-content">
                <?php
                if ($this->session->flashdata('message_gagal')) {
                    echo '<div class="alert alert-error"><button class="close" data-dismiss="alert" type="button">&times;</button>' . $this->session->flashdata('message_gagal') . '</div>';
                }
                if ($this->session->flashdata('message_sukses')) {
                    echo '<hr><div class="alert alert-success"><button class="close" data-dismiss="alert" type="button">&times;</button>' . $this->session->flashdata('message_sukses') . '</div>';
                } ?>
                <form action="<?php echo site_url('rekap_ppks/index'); ?>" method="post" name="form1" class="form-horizontal form-bordered">
                    <input type="hidden" name="<?= $this->security->get_csrf_token_name(); ?>" value="<?= $this->security->get_csrf_hash(); ?>" style="display: none">

                    <?php if ($role['insert'] == "TRUE") { ?>
                        <div align="right">
                            <a class="btn btn-blue" href="<?php echo site_url("lap_ppks/tambih "); ?>"><i class="icon-plus-sign"></i> Tambah Data</a>
                        </div>
                    <?php } ?>
						
						
			
						
						
						<div class="control-group">
										<label class="control-label" for="textfield">Periode Tgl.Register</label>                                        
                                        <div class="controls">
                                        <input type="date" placeholder="" id="tgl_register" class="input-large" name="tgl_register1" value="<?php echo $this->session->userdata('s_tgl_register1'); ?>"  >
										s/d
										<input type="date" placeholder="" id="tgl_register" class="input-large" name="tgl_register2" value="<?php echo $this->session->userdata('s_tgl_register2'); ?>"  >
										</div>
									</div>
						
						
						
						<div class="control-group">
                            <label class="control-label" for="textfield">Keberadaan saat ini?</label>
                            <div class="controls">
							<?php $status_keberadaan= $this->session->userdata('s_status_keberadaan');?>
                                <select name="status_keberadaan"    onchange="doKeberadaan(this.value);">
										<option value="">-Semua-</option>
                                           <option value="MASYARAKAT" <?php if ($status_keberadaan=="MASYARAKAT") { echo "selected";}?>>MASYARAKAT</option>
                                          <option value="LKS"  <?php if ($status_keberadaan=="LKS") { echo "selected";}?>>LKS</option>
                                        </select>
                            </div>
                        </div>
						
						<div class="control-group">
                            <label class="control-label" for="textfield">Sudah Mendapatkan Bantuan?</label>
                            <div class="controls">
                                <?php $status_bantuan= $this->session->userdata('s_status_bantuan');?> 
								<select name="status_bantuan"    onchange="doBantuan(this.value);">
										<option value="">-Semua-</option>
                                           <option value="YA" <?php if ($status_bantuan=="YA") { echo "selected";}?>>YA</option>
                                          <option value="TIDAK"  <?php if ($status_bantuan=="TIDAK") { echo "selected";}?>>TIDAK</option>
                                        </select>
								
                            </div>
                        </div>
						
						
						
						<div class="control-group" id="v_butuh_panti">
											<label class="control-label" for="textfield">Butuh Panti ?</label>                                        
											<div class="controls">
											<?php $status_panti= $this->session->userdata('s_status_panti');?> 
											<select name="status_panti" >
										<option value="">-Semua-</option>
                                           <option value="YA" <?php if ($status_panti=="YA") { echo "selected";}?>>YA</option>
                                          <option value="TIDAK"  <?php if ($status_panti=="TIDAK") { echo "selected";}?>>TIDAK</option>
                                        </select>
											
											</div>
					</div>
						
						
						
						
              
                        <div class="control-group">
                            <label class="control-label" for="textfield">Pencarian kata kunci</label>
                            <div class="controls">
                              
                                <input type="submit" name="submit" value="Cari" class="btn btn-blue">
								 
                                <a class="btn btn-red" href="<?php echo site_url(); ?>rekap_ppks/clear_session"><i class="icon-trash"></i> Hapus Pencarian</a>
                            </div>
                        </div>
                    

					<?php if ($param_kabkot=="" && $param_kec=="") { ?>
                    <?php include("v_kabkot.php");?>
					<?php } elseif ($param_kabkot!="" && $param_kec=="") { ?>
					<?php include("v_kec.php");?>
					<?php } elseif ($param_kabkot!="" && $param_kec!="") { ?>
					<?php include("v_kel.php");?>
					<?php } ?>

                </form>

            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    $('#add-peserta').click(function() {
        window.open('<?php echo site_url('/penerimaan_berkas/list_peserta/' . $kode_usulan) ?>', '1576730953022', 'width=1000,height=650,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=0,top=0');
    })
</script>
