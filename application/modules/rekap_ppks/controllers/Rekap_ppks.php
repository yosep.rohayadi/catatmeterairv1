<?php if (!defined('BASEPATH')) exit('No direct script access allowed');



class Rekap_ppks extends MX_Controller
{
	//put your code here
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('utility');
		// 
		$this->truly_madly_deeply();

		Modules::run('ref_role/permissions');
		// $this->load->model('users_model');

		$this->load->helper('login');
		$this->load->helper('tool');
	}

	public function truly_madly_deeply()
	{

		if (!$this->session->userdata('truly_madly_deeply')) {
			redirect('signon');
		}
	}



	public function index($offset = 0)

	{
		
	



				

				
				
				$sesi_bo_user_group = $this->session->userdata('sesi_bo_user_group');
				
				
				
				
				if (isset($_POST['status_keberadaan'])) {
					$data1 = array('s_status_keberadaan' => $_POST['status_keberadaan']);
					$this->session->set_userdata($data1);
				}
				
				
				if (isset($_POST['status_bantuan'])) {
					$data1 = array('s_status_bantuan' => $_POST['status_bantuan']);
					$this->session->set_userdata($data1);
				}
				
				if (isset($_POST['approved'])) {
					$data1 = array('s_approved' => $_POST['approved']);
					$this->session->set_userdata($data1);
				}
				
				if (isset($_POST['tgl_register1'])) {
					$data1 = array('s_tgl_register1' => $_POST['tgl_register1']);
					$this->session->set_userdata($data1);
				}
				
				if (isset($_POST['tgl_register2'])) {
					$data1 = array('s_tgl_register2' => $_POST['tgl_register2']);
					$this->session->set_userdata($data1);
				}
				
				
				if (isset($_POST['status_panti'])) {
					$data1 = array('s_status_panti' => $_POST['status_panti']);
					$this->session->set_userdata($data1);
				}
				
				
				if (isset($_POST['sumber_data'])) {
					$data1 = array('s_sumber_data' => $_POST['sumber_data']);
					$this->session->set_userdata($data1);
				}
				
				
				
				
				$data['role'] = get_role($this->session->userdata('sesi_bo_user_group'));



				$data['breadcrumbs'] = array(
					array(
						'link' => 'rekap_ppks',
						'name' => 'Rekapitulasi '
					),
					array(
						'link' => 'rekap_ppks',
						'name' => 'Rekapitulasi PPKS Dari Kelurahan & Panti Swasta  '
					)
				);



				$data['sub_judul_form'] = "Rekapitulasi PPKS Dari Kelurahan  & Panti Swasta ";
				
				
				
				$sql_kabkot="select * from ref_wil_kabkot";
				$query_kabkot = $this->db->query($sql_kabkot);
				$data["list_kabkot"]=$query_kabkot->result();
				
				
				$sql_kategori="select * from ref_kategori where flag='PPKS'";
				$query_kategori = $this->db->query($sql_kategori);
				$data["list_kategori"]=$query_kategori->result();
				
				$data["param_kabkot"]='';
				$data["param_kec"]='';
				$data["param_kel"]='';
					
				$this->template->load('template_admin', 'v_index', $data);
				
				
		
		
	}



	public function kec($id_kabkot=null)

	{ 
	$data['role'] = get_role($this->session->userdata('sesi_bo_user_group'));
		
		$data['breadcrumbs'] = array(
					array(
						'link' => 'rekap_ppks',
						'name' => 'Rekapitulasi '
					),
					array(
						'link' => 'rekap_ppks',
						'name' => 'Rekapitulasi PPKS Dari Kelurahan & Panti Swasta  '
					)
				);
				
				
				$sql_kec="select * from ref_wil_kec where id_kabkot='$id_kabkot'";
				$query_kec = $this->db->query($sql_kec);
				$data["list_kecamatan"]=$query_kec->result();
				
				
				$sql_kategori="select * from ref_kategori where flag='PPKS'";
				$query_kategori = $this->db->query($sql_kategori);
				$data["list_kategori"]=$query_kategori->result();



				$data['sub_judul_form'] = "Rekapitulasi PPKS Dari Kelurahan  & Panti Swasta ";
		
		
				$data["param_kabkot"]=$id_kabkot;
				$data["param_kec"]='';
				$data["param_kel"]='';
				
				$data["id_kabkot"]=$id_kabkot;
		
		$this->template->load('template_admin', 'v_index', $data);
	}
	
	
	
		public function kel($id_kabkot=null,$id_kecamatan=null)

	{ 
	$data['role'] = get_role($this->session->userdata('sesi_bo_user_group'));
		
		$data['breadcrumbs'] = array(
					array(
						'link' => 'rekap_ppks',
						'name' => 'Rekapitulasi '
					),
					array(
						'link' => 'rekap_ppks',
						'name' => 'Rekapitulasi PPKS Dari Kelurahan & Panti Swasta  '
					)
				);
				
				
				$sql_kec="select * from ref_wil_kel where id_kabkot='$id_kabkot' and  id_kecamatan='$id_kecamatan'";
				$query_kec = $this->db->query($sql_kec);
				$data["list_kelurahan"]=$query_kec->result();
				
				
				$sql_kategori="select * from ref_kategori where flag='PPKS'";
				$query_kategori = $this->db->query($sql_kategori);
				$data["list_kategori"]=$query_kategori->result();



				$data['sub_judul_form'] = "Rekapitulasi PPKS Dari Kelurahan  & Panti Swasta ";
		
		
				$data["param_kabkot"]=$id_kabkot;
				$data["param_kec"]=$id_kecamatan;
				$data["param_kel"]='';
				$data["id_kabkot"]=$id_kabkot;
				$data["id_kecamatan"]=$id_kecamatan;
		
		$this->template->load('template_admin', 'v_index', $data);
	}



	public function clear_session()
	{


				$this->session->unset_userdata('s_cari_personal');
				$this->session->unset_userdata('s_status_bantuan');
				$this->session->unset_userdata('s_status_keberadaan');
				$this->session->unset_userdata('s_id_kategori');
				$this->session->unset_userdata('s_approved');
				$this->session->unset_userdata('s_tgl_register1');
				$this->session->unset_userdata('s_tgl_register2');
				$this->session->unset_userdata('s_status_panti');
				$this->session->unset_userdata('s_sumber_data');
				   
				$this->session->unset_userdata('s_id_provinsi');
				$this->session->unset_userdata('s_id_kategori');
				$this->session->unset_userdata('s_id_kecamatan');
				$this->session->unset_userdata('s_id_kelurahan');
				


		$this->session->unset_userdata('s_cari_personal');
		redirect('rekap_ppks');
	}

	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
