<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Editpass_admin extends MX_Controller{

	public function Editpass(){
		parent::__construct();
		
		$this->truly_madly_deeply();
		
		 
		
	}
	
	
	public function truly_madly_deeply(){
		
		if(!$this->session->userdata('truly_madly_deeply')){
			redirect('signon');
		}
    }
	
	
	public function index()
	{

		$this->template->load('template_admin','v_editpass');
		
		
	}
	
	
	public function submit ()
	{
	
	 try 
	 {
		$this->load->model('m_user');
		$this->load->helper('login');
			/*	$kunci_masuk_lama = $this->input->post('kunci_masuk_lama');
				$kunci_masuk_baru = sha1(md5($this->input->post('kunci_masuk_baru')));
				$kunci_masuk_lama_enkripsi = sha1(md5($kunci_masuk_lama));
				
				
					$sql = "select  password from ref_users  where nip= '".$this->session->userdata('sesi_nip')."'";
					//echo $sql ; exit;
					$row = $this->db->query($sql)->row_array();
					
					
					
					if ($row['password']==$kunci_masuk_lama_enkripsi) {
					
						$data=array(
						'password' => $kunci_masuk_baru
						);
									
						$this->db->where('nip', $this->session->userdata('sesi_nip'));
						$this->db->update('ref_users', $data);
						
						echo "<script>alert('Password Anda Berhasil Diubah, Silahkan Login Menggunakan Password Baru Anda');window.location='".site_url('editpass_admin/keluar')."';</script>"; 
					
					} else {
					
					
					// password lama salah
					echo "<script>alert('Password Lama Anda Salah');window.location='".site_url('editpass_admin')."';</script>"; 
					//redirect('editpass_admin');
					
					}
				
				
				 */
				 
				 
				
				 if($this->m_user->m_cek_mail()->num_rows()==1) {
          
             $db=$this->m_user->m_cek_mail()->row();
             if(hash_verified($this->input->post('kunci_masuk_lama'),$db->password)) {
			 
			 //bener
			 $data=array(
						'password' => get_hash($this->input->post('kunci_masuk_baru'))
						);
									
						$this->db->where('id', $this->session->userdata('sesi_bo_id'));
						$this->db->update('ref_users', $data);
						
						echo "<script>alert('Password Anda Berhasil Diubah, Silahkan Login Menggunakan Password Baru Anda');window.location='".site_url('signon/logout')."';</script>"; 
					

                        } else {
						
						
						//salah
						echo "<script>alert('Password Lama Anda Salah');window.location='".site_url('editpass_admin')."';</script>"; 

                        }

         } else { // jika email tidak terdaftar!
		 
		 // salah juga
		 echo "<script>alert('Password Lama Anda Salah');window.location='".site_url('editpass_admin')."';</script>"; 

        }
				
						
						
						
						
			
		
	
	 } catch (Exception $e) {
            echo $e->getMessage() . "\r\n" . $e->getTraceAsString();
            $this->showError($e);
			$this->template->load('template_admin','v_editpass_admin',$data);
     }
	
	}
	

	function keluar(){
		$this->session->sess_destroy();
        redirect('admin');
	}
	
	
	
	 
	
	
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
