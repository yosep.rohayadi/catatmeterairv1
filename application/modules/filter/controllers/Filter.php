<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Filter extends CI_Controller {


	
	function __construct(){
		parent::__construct();
		$this->load->helper('tool');
	}


	public function index() {
		$data["test"]="";
		$this->template->load('template_admin','v_form',$data);
   }
   
   
   public function load_kabkota_tugas($str) {

		
		$sesi_bo_id_kabkot = $this->session->userdata('sesi_bo_id_kabkot');
		
		$sql="select * from kabkota where kode_provinsi='$str'";
		if ($sesi_bo_id_kabkot!="") {
			$sql.= " and kode_kabkot='$sesi_bo_id_kabkot'";
		}
		
		echo '<select name="id_kabkot" class="form-control" id="id_kabkot_tugas" data-rule-required="false" onchange="document.form1.submit();"  >';
		$query1 = $this->db->query($sql);
		echo '<option value="">-Pilih-</option>';
		foreach ($query1->result_array() as $row)
		{
			$id=$row['kode_kabkot'];
			$nama=$row['nama'];
		
		echo '<option value='.$id.'>'.$nama.'</option>';
		}
		echo '</select>';
		
		//$this->load->view('v_kabkot_tugas',$data);
		
		
	}
	
	
	
	public function load_kabkota_global($str) {

		
		
		
		$sql="select * from kabkota where kode_provinsi='$str'";
		
		echo '<select name="id_kabkot" class="form-control" id="id_kabkot_tugas" data-rule-required="false"  >';
		$query1 = $this->db->query($sql);
		echo '<option value="">-Pilih-</option>';
		foreach ($query1->result_array() as $row)
		{
			$id=$row['kode_kabkot'];
			$nama=$row['nama'];
		
		echo '<option value='.$id.'>'.$nama.'</option>';
		}
		echo '</select>';
		
		//$this->load->view('v_kabkot_tugas',$data);
		
		
	}





	public function load_kabkot()
	{	

		$id = $this->input->post('id');
		$sql = "SELECT kode_kabkot, nama
				FROM kabkota 
				WHERE kode_provinsi = ".$this->db->escape($id)."
		";

		$result = $this->db->query($sql)->result();

		echo "<option value=''>PILIH</option>";
		if(count($result) > 0){
			foreach ($result as $key => $value) {
				$txt = "<option value = '".$value->kode_kabkot."' >";
				$txt .= $value->nama;
				$txt .= "</option>";
				echo $txt;
			}
		}
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
