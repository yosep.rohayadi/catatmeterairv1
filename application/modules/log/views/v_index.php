<div class="container-fluid">
    <br>
    <div class="breadcrumbs">
        <ul>
            <?php foreach ($breadcrumbs as $key => $value) { ?>
                <li>
                    <a href=<?php echo site_url($value[ 'link'])?> >
				<?php echo $value['name']; ?></a>
                    <?php echo (count($breadcrumbs)-1)==$key?"":"<i class='icon-angle-right'></i>"; ?>
                </li>
                <?php } ?>
        </ul>
        <div class="close-bread">
            <a href="#"><i class="icon-remove"></i></a>
        </div>
    </div>
</div>

<div class="row-fluid">
    <div class="span12">
        <div class="box">
            <div class="box-title">
                <h3>
    				<i class="icon-reorder"></i>
    				<?php echo $sub_judul_form;?>
    			</h3>
            </div>
            <div class="box-content">
                <?php 
                if ($this->session->flashdata('message_gagal')) {
                  echo '<div class="alert alert-error"><button class="close" data-dismiss="alert" type="button">&times;</button>'.$this->session->flashdata('message_gagal').'</div>';
                }         
                if ($this->session->flashdata('message_sukses')) {
                  echo '<hr><div class="alert alert-success"><button class="close" data-dismiss="alert" type="button">&times;</button>'.$this->session->flashdata('message_sukses').'</div>';
                }?>
                <form action="<?php echo site_url('log/index'); ?>" method="post" name="form1" class="form-horizontal form-bordered">
                    <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" style="display: none">
                    

                    <div class="control-group">
                        <label class="control-label" for="textfield">Pencarian</label>
                        <div class="controls">
                            <input type="text" value="<?php echo $this->session->userdata('s_cari_global'); ?>" class="form-control" name="cari_global_log" placeholder="Masukan kata kunci... (Nama)">

                        </div>
                    </div> 
        
                    <div class="control-group">
                        
                        <div class="controls">
                            <button type="submit" class="btn btn-primary">Cari</button>
                            <a href="<?php echo base_url(); ?>log/clear_session" class="btn btn-danger">Hapus Pencarian</a>

                        </div>
                    </div>
                    <div class="table-responsive">
                        <table width="100%" class="table table-hover">
                            <thead>
                                <tr>
                                    <th>NO</th>
                                    <th>Identitas</th>
                                    <th>Tgl Akses</th>
                                    <th colspan="2">OPSI</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no=1; if (count($ListData) > 0) {
										foreach($ListData as $row){	?>
                                    <tr>
                                        <td>
                                            <?php echo $no++; ?>                                        </td>
                                       
                                       
                                        <td>
                                            <?php echo $row['identitas']; ?>                                        <br />
                                            <?php echo $row['instansi']; ?></td>
                                        <td><?php echo $row['tgl']; ?></td>
                                        <td>
                                              
                                            <?php if ($role['delete'] == "TRUE") { ?>
                                            <a class="btn btn-mini btn-danger" href="<?php echo site_url('log/hupus/'.enCrypt($row['id']));?>" onclick="return confirm('Anda Yakin ingin Menghapus?'); "><i class="icon-trash"></i> Hapus</a>
                                        <?php } ?>
                                    <td>                                    </tr>

                                    <?php

				$paging=(!empty($pagermessage) ? $pagermessage : '');

					}
					echo "<tr><td colspan='10'><div style='background:000;'>$paging &nbsp;".$this->pagination->create_links()."</div></td></tr>";
				} else {
					echo "<tbody><tr><td colspan='10' style='padding:10px; background:#F00; border:none; color:#FFF;'>Data Tidak Tersedia</td></tr></tbody>";
				}
				?>
                            </tbody>
                        </table>
                  </div>

                </form>

            </div>
        </div>
    </div>
</div>