<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Log extends MX_Controller{
    //put your code here
    public function __construct() {
        parent::__construct();
		$this->load->helper('utility');
		// 
		$this->truly_madly_deeply();
		
		Modules::run('ref_role/permissions');
		// $this->load->model('users_model');
		
			 $this->load->helper('login');
			  $this->load->helper('tool');
    }   
    
    
	public function truly_madly_deeply(){
		
		if(!$this->session->userdata('truly_madly_deeply')){
			redirect('signon');
		}
    }

	
	
	
	public function index( $offset = 0 ) {
	
		
				if (isset($_POST['cari_global_log'])) {
				$data5 = array('s_cari_global_log' => $_POST['cari_global_log']);
				$this->session->set_userdata($data5);			
			}

			// echo $id_prov;
			
			$per_page = 20;  
			$qry = "SELECT a.*
				FROM 
				log a ";


if ($this->session->userdata('s_cari_global_log')!="") {
			$qry.="  where identitas like '%".$this->db->escape_like_str($this->session->userdata('s_cari_global_log'))."%' or
instansi like '%".$this->db->escape_like_str($this->session->userdata('s_cari_global_log'))."%'
			";
			} 
			elseif ($this->session->userdata('s_cari_global_log')=="") {
			$this->session->unset_userdata('s_cari_global_log');
			} 	
			
		
			
			
			$qry.= " ORDER BY a.id desc"; 
			//echo $qry;
		

			$offset = ($this->uri->segment(3) != '' ? $this->uri->segment(3):0);
			$config['total_rows'] = $this->db->query($qry)->num_rows();
			$config['per_page']= $per_page;
			$config['full_tag_open'] = '<div class="table-pagination">';
                        $config['full_tag_close'] = '</div>';
                        $config['cur_tag_open'] = '<a href="#" class="active"><b>';
                        $config['cur_tag_close'] = '</b></a>';
			$config['first_link'] = 'First';
			$config['last_link'] = 'Last';
                        $config['next_link'] = 'Next';
                        $config['prev_link'] = 'Previous';
                        $config['last_tag_open'] = "<span>";
                        $config['first_tag_close'] = "</span>";
			$config['uri_segment'] = 3;
			$config['base_url']= base_url().'/log/index'; 
			$config['suffix'] = '?'.http_build_query($_GET, '', "&"); 
			$this->pagination->initialize($config);
			$data['paginglinks'] = $this->pagination->create_links();    
			$data['per_page'] = $this->uri->segment(3);      
			$data['offset'] = $offset ;
			if($data['paginglinks']!= '') {
			  $data['pagermessage'] = 'Showing '.((($this->pagination->cur_page-1)*$this->pagination->per_page)+1).' to '.($this->pagination->cur_page*$this->pagination->per_page).' of '.$this->db->query($qry)->num_rows();
			}   
			$qry .= " limit {$per_page} offset {$offset} ";
			$data['ListData'] = $this->db->query($qry)->result_array(); 
			$data['role'] = get_role($this->session->userdata('sesi_bo_user_group')); 

// echo $this->db->query($qry)->num_rows();
// die;


	
		
	
	$data['breadcrumbs'] = array(
		array (
			'link' => 'log/#',
			'name' => 'Log History '
		),
		array (
			'link' => 'log',
			'name' => 'Log History '
		)
	);
	
	$data['sub_judul_form']="Log History ";
	$this->template->load('template_admin','v_index',$data);	

	
   }
	
	
	   public function clear_session(){
   		$this->session->unset_userdata('s_cari_global_log');
   		
			redirect('log');

   }
	
   public function hupus() {
  
		// $id=$this->uri->segment(3);
   	   	get_role($this->session->userdata('sesi_bo_user_group'),'delete'); 

		 $id = deCrypt($this->uri->segment(3));
			try {

			
		
			$this->db->where('id',$id);
			$this->db->delete('log');
			redirect('log');
			
			}
			catch(Exception $err)
			{
			log_message("error",$err->getMessage());
			return show_error($err->getMessage());
			}
   
   }






	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
