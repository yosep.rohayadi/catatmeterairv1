<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Dompdf\Dompdf;

class Lap_profil extends MX_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('utility');
		$this->truly_madly_deeply();

		Modules::run('ref_role/permissions');
		$this->load->helper('login');
		$this->load->helper('tool');
		$this->load->model('Main_model');
	}


	public function truly_madly_deeply()
	{
		if (!$this->session->userdata('truly_madly_deeply')) {
			redirect('signon');
		}
	}


	public function index()
	{
		$id_provinsi = $this->session->userdata("id_provinsi");
		if ($this->input->post("id_provinsi") != null && $this->input->post("submit") != null) {
			$id_provinsi 	= $this->input->post("id_provinsi");
			$this->session->set_userdata("id_provinsi", $id_provinsi);
		} else if ($this->input->post("id_provinsi") == null && $this->input->post("submit") != null) {
			$id_provinsi = null;
			$this->session->unset_userdata("id_provinsi");
		}

		$id_kabkot = $this->session->userdata("id_kabkot");
		if ($this->input->post("id_kabkot") != null && $this->input->post("submit") != null) {
			$id_kabkot		= $this->input->post("id_kabkot");
			$this->session->set_userdata("id_kabkot", $id_kabkot);
		} else if ($this->input->post("id_kabkot") == null && $this->input->post("submit") != null) {
			$id_kabkot = null;
			$this->session->unset_userdata("id_kabkot");
		}

		$id_kecamatan = $this->session->userdata("id_kecamatan");
		if ($this->input->post("id_kecamatan") != null && $this->input->post("submit") != null) {
			$id_kecamatan	= $this->input->post("id_kecamatan");
			$this->session->set_userdata("id_kecamatan", $id_kecamatan);
		} else if ($this->input->post("id_kecamatan") == null && $this->input->post("submit") != null) {
			$id_kecamatan = null;
			$this->session->unset_userdata("id_kecamatan");
		}

		$data['role'] 			= get_role($this->session->userdata('sesi_bo_user_group'));
		$data['breadcrumbs'] 	= array(
			array(
				'link' => '#',
				'name' => 'Laporan'
			),
			array(
				'link' => 'lap_profil',
				'name' => 'Laporan Profile LKS'
			)
		);

		$data['id_provinsi']	= $id_provinsi;
		$data['id_kabkot']		= $id_kabkot;
		$data['id_kecamatan']	= $id_kecamatan;
		$data['title'] 			= 'Laporan Profile LKS';
		$this->template->load('template_admin', 'v_index', $data);
	}


	public function load_list_data()
	{
		$request = $_GET;
		$start 	 = $request['start'];
		$length  = $request['length'];

		// UNTUK PENCARIAN
		$data_cari = $request['search']['value'];
		$cari_spesifik 	= $this->db->escape($data_cari);
		$cari_like 		= $this->db->escape('%' . $data_cari . '%');
		$this->session->set_userdata('cari_data', $data_cari);

		// UNTUK FILTER
		$id_provinsi	= $this->session->userdata("id_provinsi");
		$id_kabkot		= $this->session->userdata("id_kabkot");
		$id_kecamatan	= $this->session->userdata("id_kecamatan");

		$qury = "SELECT 
					A.*,
					B.nama AS PROVINSI,
					C.nama AS KABKOT,
					D.nama AS KECAMATAN,
					A.nama AS KELURAHAN
				FROM ref_lks A 
					LEFT JOIN ref_wil_provinsi B ON A.id_provinsi = B.id_provinsi
					LEFT JOIN ref_wil_kabkot C ON A.id_provinsi = C.id_provinsi AND A.id_kabkot = C.id_kabkot
					LEFT JOIN ref_wil_kec D ON A.id_provinsi = D.id_provinsi AND A.id_kabkot = D.id_kabkot AND D.id_kecamatan = A.id_kec
				WHERE 1=1 ";

		if ($id_provinsi != null) {
			$id_provinsi = $this->db->escape($id_provinsi);
			$qury .= " AND A.id_provinsi = $id_provinsi ";
		}

		if ($id_kabkot != null) {
			$id_kabkot = $this->db->escape($id_kabkot);
			$qury .= "AND  A.id_kabkot = $id_kabkot ";
		}

		if ($id_kecamatan != null) {
			$id_kecamatan = $this->db->escape($id_kecamatan);
			$qury .= "AND A.id_kec = $id_kecamatan ";
		}

		if ($data_cari != null) {
			$qury .= " AND ( A.nama LIKE $cari_like 
							OR A.alamat LIKE $cari_like 
							OR B.nama LIKE $cari_like 
							OR C.nama LIKE $cari_like
							OR D.nama LIKE $cari_like 
							OR A.dasar_pembentukan LIKE $cari_like 
							OR A.no_telp LIKE $cari_like)";
		}


		// TOTAL ROW
		$total_data = $this->db->query($qury)->num_rows();

		// ORDERY BY colum table ('nol' berarti column table yang paling kiri)
		$orderby = $request['order'][0]['dir'];
		if ($request['order'][0]['column'] == 0) {
			$qury .= " ORDER BY B.id_provinsi ASC";
		}

		// 	LIMIT
		$qury .= " LIMIT $start, $length ";


		$result = $this->db->query($qury)->result_array();
		$arr 	= [];
		foreach ($result as $key => $value) {
			$arr = array_merge($arr, [
				[
					$start + ($key + 1),
					$value['KELURAHAN'] ?? '-',
					
					$value['alamat'] ?? '-',
					$value['tgl_pembentukan'] ?? '-',
					$value['visi'] ?? '-',
					$value['misi'] ?? '-',
					$value['dasar_pembentukan'] ?? '-',
					$value['no_telp'] ?? '-',
					$value['luas_wil'] ?? '-',
					number_format(($value['jml_penduduk_laki'] ?? 0)) ?? '-',
					number_format(($value['jml_penduduk_perempuan'] ?? 0)) ?? '-',
					$value['jml_kk'] ?? '-',
					$value['jml_rt'] ?? '-',
					$value['jml_rw'] ?? '-',
					$value['foto_lokasi'] != null ? base_url($value['foto_lokasi']) : '-',
				]
			]);
		}

		$data = [
			'draw' 				=> $request['draw'],
			"recordsTotal"		=> $total_data,
			"recordsFiltered"	=> $total_data,
			"data"				=> $arr,
		];
		echo json_encode($data);
	}


	public function reset_filter()
	{
		$this->session->unset_userdata(['id_provinsi', 'id_kabkot', 'id_kecamatan', 'periode']);
		return redirect(base_url('lap_profil'));
	}


	public function export_excell()
	{
		$spreadsheet 	= new Spreadsheet();
		$sheet 			= $spreadsheet->getActiveSheet();
		$nama_excell 	= 'Laporan Profile Kelurahan';

		// FILTER
		$id_provinsi	= $this->session->userdata("id_provinsi");
		$id_kabkot		= $this->session->userdata("id_kabkot");
		$id_kecamatan	= $this->session->userdata("id_kecamatan");
		$cari_data		= $this->session->userdata("cari_data");
		$data 			= $this->Main_model->geteTotalListData($id_provinsi, $id_kabkot, $id_kecamatan, $cari_data);

		$sheet->setCellValue('A1', 'NAMA KELURAHAN');
		$sheet->setCellValue('B1', 'KECAMATAN');
		$sheet->setCellValue('C1', 'KAB/KOTA');
		$sheet->setCellValue('D1', 'PROVINSI');
		$sheet->setCellValue('E1', 'ALAMAT');
		$sheet->setCellValue('F1', 'TGL PEMBENTUKAN');
		$sheet->setCellValue('G1', 'VISI');
		$sheet->setCellValue('H1', 'MISI');
		$sheet->setCellValue('I1', 'DASAR PEMBENTUKAN');
		$sheet->setCellValue('J1', 'NO TELP');
		$sheet->setCellValue('K1', 'LUAS WILAYAH');
		$sheet->setCellValue('L1', 'JML PENDUDUK LAKI-LAKI');
		$sheet->setCellValue('M1', 'JML PENDUDUK PEREMPUAN');
		$sheet->setCellValue('N1', 'JML KK');
		$sheet->setCellValue('O1', 'JML RT');
		$sheet->setCellValue('P1', 'JML RW');
		$sheet->setCellValue('Q1', 'FOTO LOKASI');
		$sheet->setCellValue('R1', 'FOTO PETA');

		if (count($data) > 0) {
			foreach ($data as $key => $value) {
				$sheet->setCellValue('A' . ($key + 2), $value['KELURAHAN']);
				$sheet->setCellValue('B' . ($key + 2), $value['KECAMATAN']);
				$sheet->setCellValue('C' . ($key + 2), $value['KABKOT']);
				$sheet->setCellValue('D' . ($key + 2), $value['PROVINSI']);
				$sheet->setCellValue('E' . ($key + 2), $value['alamat']);
				$sheet->setCellValue('F' . ($key + 2), $value['tgl_pembentukan']);
				$sheet->setCellValue('G' . ($key + 2), $value['visi']);
				$sheet->setCellValue('H' . ($key + 2), $value['misi']);
				$sheet->setCellValue('I' . ($key + 2), $value['dasar_pembentukan']);
				$sheet->setCellValue('J' . ($key + 2), $value['no_telp']);
				$sheet->setCellValue('K' . ($key + 2), $value['luas_wil']);
				$sheet->setCellValue('L' . ($key + 2), $value['jml_penduduk_laki']);
				$sheet->setCellValue('M' . ($key + 2), $value['jml_penduduk_perempuan']);
				$sheet->setCellValue('N' . ($key + 2), $value['jml_kk']);
				$sheet->setCellValue('O' . ($key + 2), $value['jml_rt']);
				$sheet->setCellValue('P' . ($key + 2), $value['jml_rw']);
				$sheet->setCellValue('Q' . ($key + 2), $value['foto_lokasi']);
				$sheet->setCellValue('R' . ($key + 2), $value['foto_peta']);
			}
		}


		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="' . $nama_excell . ' .xlsx"');
		header('Cache-Control: max-age=0');

		$writer = new Xlsx($spreadsheet);
		$writer->save('php://output');
	}


	public function export_pdf($id_periode)
	{
		$id_periode		= deCrypt($id_periode);
		$nama_dokumen 	= 'Laporan Rekapitulasi Kegiatan PPKM';
		$data 			= $this->Main_model->dataUntukExport($id_periode);

		$dompdf = new Dompdf();
		$html 	= $this->load->view("export/html_for_pdf", ['listData' => $data['data'], 'nama' => $nama_dokumen], true);
		$dompdf->loadHtml($html);
		$dompdf->setPaper('A4', 'landscape');
		$dompdf->render();
		$dompdf->stream($nama_dokumen);
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
