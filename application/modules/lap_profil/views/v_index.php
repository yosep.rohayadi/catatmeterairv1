<style>
    .text-center {
        text-align: center;
    }
</style>

<div class="container-fluid">
    <br>
    <div class="breadcrumbs">
        <ul>
            <?php foreach ($breadcrumbs as $key => $value) { ?>
                <li>
                    <a href=<?php echo site_url($value['link']) ?>>
                        <?php echo $value['name']; ?></a>
                    <?php echo (count($breadcrumbs) - 1) == $key ? "" : "<i class='icon-angle-right'></i>"; ?>
                </li>
            <?php } ?>
        </ul>
        <div class="close-bread">
            <a href="#"><i class="icon-remove"></i></a>
        </div>
    </div>
</div>


<div class="box">
    <div class="box-title">
        <h3>
            <i class="icon-reorder"></i>
            <?php echo $title; ?>
        </h3>
    </div>
    <div class="box-content">
        <form action="<?php echo base_url("lap_profil/index") ?>" method="POST">
            <div class="row">
                <div class="span12">
                    <?php require_once(APPPATH . '/modules/form/views/v_form_filter_rekap.php'); ?>
                </div>
            </div>
            <div>
                <hr>
                <input type="submit" name="submit" value="Filter" class="btn btn-primary">
                <a href="<?php echo base_url('lap_profil/reset_filter') ?>" class="btn btn-danger">Reset Filter</a>
                <span class="pull-right">
                    <a target="_blank" href="<?php echo base_url("lap_profil/export_excell") ?>" class="btn btn-primary">Export Excell</a>
                    <!-- <a href="<?php echo base_url("lap_profil/export_pdf/") ?>" class="btn btn-primary">Export Pdf</a> -->
                </span>
                <hr>
            </div>
        </form>
        <div class="table-responsive">
            <table id="table_serverside" class="display" style="width:100%">
                <thead style="font-size: 12px;">
                    <tr>
                        <th class="text-center">#</th>
                        <th class="text-center">NAMA</th>
                        
                        <th class="text-center">ALAMAT</th>
                        
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>


<script>
    $(document).ready(function() {

        $('.select2').select2();
        if ('<?php echo $id_provinsi ?? '' ?>' != '') {
            load_kabkot('<?php echo $id_provinsi ?? '' ?>', '<?php echo $id_kabkot ?? '' ?>');
        }

        $(document).ready(function() {
            $('#table_serverside').DataTable({
                "processing": true,
                "serverSide": true,
                "ajax": "<?php echo site_url('lap_profil/load_list_data') ?>"
            });
        });
    });
</script>