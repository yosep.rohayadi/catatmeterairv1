<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        .table1 {
            font-family: sans-serif;
            color: #232323;
            border-collapse: collapse;
            font-size: 12px;
        }

        .table1,
        th,
        td {
            border: 1px solid #999;
            padding: 5px;
        }
    </style>
</head>

<body>
    <center>
        <h4><?php echo $nama ?? '-' ?></h4>
    </center>
    <p>Tanggal Cetak : <?php echo date('d-m-Y') ?></p>
    <br>
    <table class="table1">
        <tr>
            <th class="text-center">#</th>
            <th class="text-center">PROVINSI</th>
            <th class="text-center">KAB/KOTA</th>
            <th class="text-center">KECAMATAN</th>
            <th class="text-center">KELURAHAN</th>
            <th class="text-center">JML. POSKO AKTIF</th>
            <th class="text-center">JML. POSKO TIDAK AKTIF</th>
            <th class="text-center">JML TOTAL POSKO</th>
        </tr>
        <?php if (count($listData) > 0) { ?>
            <?php foreach ($listData as $key => $value) { ?>
                <tr>
                    <td><?php echo ($key + 1) ?></td>
                    <td><?php echo $value['nama_provinsi']; ?></td>
                    <td><?php echo $value['nama_kabkot']; ?></td>
                    <td><?php echo $value['nama_kecamatan']; ?></td>
                    <td><?php echo $value['nama']; ?></td>
                    <td><?php echo $value['JML_AKTIF']; ?></td>
                    <td><?php echo $value['NON_AKTIF'];  ?></td>
                    <td><?php echo $value['JML_POSKO'];  ?></td>
                </tr>
            <?php } ?>
            <tr>
                <td colspan="2">TOTAL</td>
                <td colspan="3"></td>
                <td><?php echo number_format($listData[0]['JML_TOTAL_AKTIF'] ?? 0) ?></td>
                <td><?php echo number_format($listData[0]['JML_TOTAL_NONAKTIF'] ?? 0) ?></td>
                <td><?php echo number_format($listData[0]['JML_TOTAL_POSKO'] ?? 0) ?></td>
            </tr>
        <?php } else { ?>
            <tr>
                <td colspan="10">Belum ada data.</td>
            </tr>
        <?php } ?>
    </table>
</body>

</html>