<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        .table1 {
            font-family: sans-serif;
            color: #232323;
            border-collapse: collapse;
            font-size: 12px;
        }

        .table1,
        th,
        td {
            border: 1px solid #999;
            padding: 5px;
        }
    </style>
</head>

<body>
    <center>
        <h4><?php echo $nama ?? '-' ?></h4>
    </center>
    <p>Tanggal Cetak : <?php echo date('d-m-Y') ?></p>
    <br>
    <table class="table1">
        <tr>
            <th class="text-center">#</th>
            <th class="text-center">PROVINSI</th>
            <th class="text-center"><?php echo is_numeric(($listData[0]['KABKOT'] ?? '')) ? 'JML.' : '' ?> KAB/KOTA</th>
            <th class="text-center"><?php echo is_numeric(($listData[0]['KECAMATAN'] ?? '')) ? 'JML.' : '' ?> KECAMATAN</th>
            <th class="text-center"><?php echo is_numeric(($listData[0]['DESA'] ?? '')) ? 'JML.' : '' ?> KELURAHAN</th>
            <th class="text-center">JML. PENCEGAHAN</th>
            <th class="text-center">JML. PENANGANAN</th>
            <th class="text-center">JML. PEMBINAAN</th>
            <th class="text-center">JML. PENDUKUNG</th>
            <th>TOTAL</th>
        </tr>
        <?php if (count($listData) > 0) { ?>
            <?php $total_data = 0; ?>
            <?php foreach ($listData as $key => $value) { ?>
                <tr>
                    <td><?php echo ($key + 1) ?></td>
                    <td><?php echo $value['PROVINSI']; ?></td>
                    <td><?php echo $value['KABKOT']; ?></td>
                    <td><?php echo $value['KECAMATAN']; ?></td>
                    <td><?php echo $value['DESA']; ?></td>
                    <td><?php echo $value['pencegahan']; ?></td>
                    <td><?php echo $value['penanganan'];  ?></td>
                    <td><?php echo $value['pembinaan'];  ?></td>
                    <td><?php echo $value['pendukung'];  ?></td>
                    <td>
                        <?php echo ($value['pencegahan'] + $value['penanganan'] + $value['pembinaan'] + $value['pendukung']) ?>
                        <?php $total_data += ($value['pencegahan'] + $value['penanganan'] + $value['pembinaan'] + $value['pendukung']) ?>
                    </td>
                </tr>
            <?php } ?>
            <tr>
                <td colspan="2">TOTAL</td>
                <td colspan="3"></td>
                <td><?php echo number_format($listData[0]['total_pencegahan'] ?? 0) ?></td>
                <td><?php echo number_format($listData[0]['total_penanganan'] ?? 0) ?></td>
                <td><?php echo number_format($listData[0]['total_pembinaan'] ?? 0) ?></td>
                <td><?php echo number_format($listData[0]['total_pendukung'] ?? 0) ?></td>
                <td><?php echo $total_data ?></td>
            </tr>
        <?php } else { ?>
            <tr>
                <td colspan="10">Belum ada data.</td>
            </tr>
        <?php } ?>
    </table>

</body>

</html>