<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Main_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}

	public function geteTotalListData($id_provinsi, $id_kabkot, $id_kecamatan, $data_cari)
	{
		$sql = "SELECT 
					A.*,
					B.nama AS PROVINSI,
					C.nama AS KABKOT,
					D.nama AS KECAMATAN,
					A.nama AS KELURAHAN
				FROM ref_lks A 
					LEFT JOIN ref_wil_provinsi B ON A.id_provinsi = B.id_provinsi
					LEFT JOIN ref_wil_kabkot C ON A.id_provinsi = C.id_provinsi AND A.id_kabkot = C.id_kabkot
					LEFT JOIN ref_wil_kec D ON A.id_provinsi = D.id_provinsi AND A.id_kabkot = D.id_kabkot AND D.id_kecamatan = A.id_kec
				WHERE 1=1 ";

		if ($id_provinsi != null) {
			$id_provinsi = $this->db->escape($id_provinsi);
			$sql .= " AND A.id_provinsi = $id_provinsi ";
		}

		if ($id_kabkot != null) {
			$id_kabkot = $this->db->escape($id_kabkot);
			$sql .= " AND A.id_kabkot = $id_kabkot ";
		}

		if ($id_kecamatan != null) {
			$id_kecamatan = $this->db->escape($id_kecamatan);
			$sql .= " AND A.id_kec = $id_kecamatan ";
		}

		if ($data_cari != null) {
			$cari_like = $this->db->escape("%" . $data_cari . "%");
			$sql .= " AND ( A.nama LIKE $cari_like 
							OR A.alamat LIKE $cari_like 
							OR B.nama LIKE $cari_like 
							OR C.nama LIKE $cari_like
							OR D.nama LIKE $cari_like 
							OR A.dasar_pembentukan LIKE $cari_like 
							OR A.no_telp LIKE $cari_like)";
		}
		return $this->db->query($sql)->result_array();
	}
}
