<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Ref_users extends MX_Controller
{


	//put your code here
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('utility');
		// 
		$this->truly_madly_deeply();

		Modules::run('ref_role/permissions');
		$this->load->model('users_model');

		$this->load->helper('login');
		$this->load->helper('tool');
	}




	public function gen()
	{

		die();
		$query = $this->db->query("select * from ref_users where id not in (16384,16385)  ");

		//$query = $this->db->query("select * from ref_users where id in (6002,5603)  ");


		foreach ($query->result() as $row) {
			$six_digit_random_number = mt_rand(100000, 999999);
			$id = $row->id;
			$id_kel = $row->id_kel;
			$nama_lengkapx = $row->nama_lengkapx;
			$pwd = get_hash($six_digit_random_number);
			$find = array(" ", "'", "-");
			$replace = array("");
			$arr = strtolower($row->nama_lengkapx);
			$olah = str_replace($find, $replace, $arr);
			$username = $id_kel . "-" . $olah;

			$sqlupd = "update ref_users set username='$username', password='$pwd', passtemp='$six_digit_random_number' where id='$id'";
			$this->db->query($sqlupd);
			//echo $sqlupd."<hr>";



		}
	}



	public function gen2()
	{

		//die();
		$query = $this->db->query("select * from ref_wil_provinsi  ");

		//$query = $this->db->query("select * from ref_users where id in (6002,5603)  ");


		foreach ($query->result() as $row) {
			$six_digit_random_number = mt_rand(100000, 999999);
			$id_prov = $row->id_provinsi;
			//$id_kel=$row->id_kel;
			$nama_lengkapx = $row->nama;
			$pwd = get_hash($six_digit_random_number);
			$find = array(" ", "'", "-");
			$replace = array("");
			$arr = strtolower($row->nama);
			$olah = str_replace($find, $replace, $arr);
			$username = $olah;

			$sqlupd = "insert into ref_users (username,password,nama_lengkap,id_user_group,id_prov,status_aktif,passtemp) 
					VALUES ('$username','$pwd','$nama_lengkapx','10','$id_prov','Y','$six_digit_random_number')
					";
			$this->db->query($sqlupd);
			//echo $sqlupd."<hr>";



		}
	}





	public function gen3()
	{

		//die();
		$query = $this->db->query("select * from ref_wil_kabkot where  id_kabkot <> '' ");

		//$query = $this->db->query("select * from ref_users where id in (6002,5603)  ");


		foreach ($query->result() as $row) {
			$six_digit_random_number = 123456;
			$id_prov = $row->id_provinsi;
			$id_kabkot = $row->id_kabkot;
			//$id_kel=$row->id_kel;
			$nama_lengkapx = $row->nama;
			$pwd = get_hash($six_digit_random_number);
			$find = array(" ", "'", "-");
			$replace = array("");
			$arr = strtolower($row->nama);
			$olah = str_replace($find, $replace, $arr);
			$username = $olah;

			$sqlupd = "insert into ref_users (username,password,nama_lengkap,id_user_group,id_prov,status_aktif,passtemp,id_kabkot) 
					VALUES ('$username','$pwd','$nama_lengkapx','5','$id_prov','Y','$six_digit_random_number','$id_kabkot')
					";
			$this->db->query($sqlupd);
			//echo $sqlupd."<hr>";



		}
	}



	public function truly_madly_deeply()
	{
		if (!$this->session->userdata('truly_madly_deeply')) {
			redirect('signon');
		}
	}




	public function index($offset = 0)
	{
		if (isset($_POST['cari_global'])) {
			$data1 = array('s_cari_global' => $_POST['cari_global']);
			$this->session->set_userdata($data1);
		}

		$per_page = 20;
		$qry = "SELECT u.*,ug.nama_user_group,rw.nama AS nama_wilayah
				FROM 	ref_users u
						left join ref_user_group ug on  u.id_user_group=ug.id_user_group 
						left join ref_wilayah rw on rw.id = u.id_wilayah
				WHERE u.username <> 'raskha.rock83' ";


		if ($this->session->userdata('s_cari_global') != "") {
			$qry .= "  AND nama_lengkap like '%" . $this->db->escape_like_str($this->session->userdata('s_cari_global')) . "%'  ";
		} elseif ($this->session->userdata('s_cari_global') == "") {
			$this->session->unset_userdata('s_cari_global');
		}


		$qry .= " ORDER BY username asc";

		$offset = ($this->uri->segment(3) != '' ? $this->uri->segment(3) : 0);
		$config['total_rows'] = $this->db->query($qry)->num_rows();
		$config['per_page'] = $per_page;
		$config['full_tag_open'] = '<div class="table-pagination">';
		$config['full_tag_close'] = '</div>';
		$config['cur_tag_open'] = '<a href="#" class="active"><b>';
		$config['cur_tag_close'] = '</b></a>';
		$config['first_link'] = 'First';
		$config['last_link'] = 'Last';
		$config['next_link'] = 'Next';
		$config['prev_link'] = 'Previous';
		$config['last_tag_open'] = "<span>";
		$config['first_tag_close'] = "</span>";
		$config['uri_segment'] = 3;
		$config['base_url'] = base_url() . '/ref_users/index';
		$config['suffix'] = '?' . http_build_query($_GET, '', "&");
		$this->pagination->initialize($config);
		$data['paginglinks'] = $this->pagination->create_links();
		$data['per_page'] = $this->uri->segment(3);
		$data['offset'] = $offset;
		if ($data['paginglinks'] != '') {
			$data['pagermessage'] = 'Showing ' . ((($this->pagination->cur_page - 1) * $this->pagination->per_page) + 1) . ' to ' . ($this->pagination->cur_page * $this->pagination->per_page) . ' of ' . $this->db->query($qry)->num_rows();
		}
		$qry .= " limit {$per_page} offset {$offset} ";
		$data['ListData'] = $this->db->query($qry)->result_array();

		$data['breadcrumbs'] = array(
			array(
				'link' => '#',
				'name' => 'Settings'
			),
			array(
				'link' => '#',
				'name' => 'Users'
			)
		);
		$data['role'] = get_role($this->session->userdata('sesi_bo_user_group'));
		$data['sub_judul_form'] = "Data Users ";
		$this->template->load('template_admin', 'v_index', $data);
	}



	public function tambih()
	{

		$data['breadcrumbs'] = array(
			array(
				'link' => '#',
				'name' => 'Settings'
			),
			array(
				'link' => 'ref_users',
				'name' => 'Users'
			),
			array(
				'link' => '#',
				'name' => 'Tambah Data'
			)
		);

		$data['judul_form'] 	= "Tambah Data";
		$data['sub_judul_form'] = "Users ";
		$data['id_provinsi']	= null;
		$data['id_kabkot']		= null;
		$data['id_kecamatan']	= null;
		$data['id_kelurahan']	= null;
		$data['user_group'] 	= $this->users_model->get_group();

		$this->template->load('template_admin', 'v_add', $data);
	}




	public function tambih_robih()
	{
		try {
			$id = $this->input->post('id_users');
			$ip = $this->input->post('ip_address');
			$username = $this->input->post('username_users');
			$nama = $this->input->post('nama_users');
			$email = $this->input->post('email_users');
			$hp = $this->input->post('hp_users');
			$id_wilayah = $this->input->post('id_wilayah');
			$status_aktif = $this->input->post('status_aktif');

			$kode_prov = $this->input->post('kode_prov');
			$kabkot = $this->input->post('kabkot');

			//$pwd = sha1(md5($this->input->post('password_users')));

			$pwd = get_hash($this->input->post('password_users'));

			$group = $this->input->post('group_users');

			if ($group == 2) {
				$tim_penilai = $this->input->post('tim_penilai');
			} else {
				$tim_penilai = "";
			}



			$password_users = $this->input->post('password_users');
			$password_users_text = $this->input->post('password_users_text');

			//echo $password_users."<br>".$password_users_text; exit;




			if ($id == '' || $id == null) {

				//cek username ganda			

				$sql     = "select count(username) as ceknik from ref_users where username = '" . $username . "'";
				$row     = $this->db->query($sql)->row_array();

				if ($row['ceknik'] == 0) {

					$data['ip'] = $ip;
					$data['username'] = $username;
					$data['ip'] = $ip;
					$data['password'] = $pwd;
					//strlen($password)>0?$data['password']=$password:'';
					$data['nama_lengkap'] = $nama;
					$data['email'] = $email;
					$data['status_aktif'] = $status_aktif;
					$data['hp'] = $hp;
					$data['id_wilayah'] = $id_wilayah;
					$data['id_user_group']	= $group;
					$data['id_prov'] 		= $kode_prov;
					$data['id_kabkot'] 		= $kabkot;

					// ALAMAT DAERAH
					$data['id_prov']	= $this->input->post('id_provinsi');
					$data['id_kabkot']	= $this->input->post('id_kabkot');
					$data['id_kec']		= $this->input->post('id_kecamatan');
					$data['id_kel']		= $this->input->post('id_kelurahan');

					$data['update_oleh']   = $this->session->userdata('sesi_bo_nama_lengkap');
					$data['update_tgl']		= date('Y-m-d H:i:s');
					// $data['tim_penilai_ke']=$tim_penilai;


					$data['pangkat'] = $this->input->post('pangkat');
					$data['jabatan'] = $this->input->post('jabatan');
					$data['tgl_daftar'] = date('Y-m-d H:i:s');

					$xss_data = $this->security->xss_clean($data);
					$this->db->insert('ref_users', $xss_data);
					$this->session->set_flashdata('message_sukses', 'Data Berhasil Disimpan');
					$this->tambih();
				} else {

					$this->session->set_flashdata('message_sukses', 'NIP Yang Anda Masukan Sudah Ada !!!');
					$this->tambih();
				}
			} else {

				if ($password_users != $password_users_text) {

					$data['password'] = $pwd;
				} else {

					$data['password'] = $password_users_text;
				}

				$data['ip'] = $ip;

				$data['update_oleh']   = $this->session->userdata('sesi_bo_nama_lengkap');
				$data['update_tgl']		= date('Y-m-d H:i:s');

				$data['username'] = $username;
				$data['status_aktif'] = $status_aktif;
				$data['id_wilayah'] = $id_wilayah;
				$data['nama_lengkap'] = $nama;
				$data['email'] = $email;
				$data['hp'] = $hp;
				$data['pangkat'] = $this->input->post('pangkat');
				$data['jabatan'] = $this->input->post('jabatan');
				$data['id_user_group'] = $group;

				// ALAMAT DAERAH
				$data['id_prov']	= $this->input->post('id_provinsi');
				$data['id_kabkot']	= $this->input->post('id_kabkot');
				$data['id_kec']		= $this->input->post('id_kecamatan');
				$data['id_kel']		= $this->input->post('id_kelurahan');


				$xss_data = $this->security->xss_clean($data);
				$this->db->where('id', $id);
				$this->db->update('ref_users', $xss_data);

				$this->session->set_flashdata('message_sukses', 'Perubahan Data Berhasil Disimpan');
				redirect('ref_users');

				$data['judul_form'] = "Ubah Data";
				$data['sub_judul_form'] = "Users ";
				$data['field'] = $this->users_model->get_all(array('id' => $id));
				$data['breadcrumbs'] = array(
					array(
						'link' => '#',
						'name' => 'Settings'
					),
					array(
						'link' => 'ref_users',
						'name' => 'Users'
					),
					array(
						'link' => '#',
						'name' => 'Ubah Data'
					)
				);

				$this->template->load('template_admin', 'v_add', $data);
			}
		} catch (Exception $err) {
			log_message("error", $err->getMessage());
			return show_error($err->getMessage());
		}
	}




	public function tambih_robih_area()
	{

		try {
			$id = $this->input->post('id_users');
			$ip = $this->input->post('ip_address');
			$flag_instansi = $this->input->post('flag_instansi');
			$kode_prov = $this->input->post('kode_prov');
			$kode_kabkot = $this->input->post('kode_kabkot');




			//echo $password_users."<br>".$password_users_text; exit;




			if ($id == '' || $id == null) {


				$data['flag_instansi'] = $flag_instansi;
				$data['id_prov'] = $kode_prov;
				$data['id_kabkot'] = $kode_kabkot;


				$xss_data = $this->security->xss_clean($data);
				$this->db->insert('ref_users', $xss_data);
				$this->session->set_flashdata('message_sukses', 'Data Berhasil Disimpan');
				$this->tambih();
			} else {


				$data['flag_instansi'] = $flag_instansi;
				$data['id_prov'] = $kode_prov;
				$data['id_kabkot'] = $kode_kabkot;

				$xss_data = $this->security->xss_clean($data);
				$this->db->where('id', $id);
				$this->db->update('ref_users', $xss_data);

				$this->session->set_flashdata('message_sukses', 'Perubahan Data Berhasil Disimpan');
				echo "<script>alert('Setting Area Anda Berhasil di Rubah');window.location='" . site_url('ref_users') . "';</script>";

				$data['judul_form'] = "Ubah Data";
				$data['sub_judul_form'] = "Users ";
				$data['field'] = $this->users_model->get_all(array('id' => $id));
				$data['breadcrumbs'] = array(
					array(
						'link' => '#',
						'name' => 'Settings'
					),
					array(
						'link' => 'ref_users',
						'name' => 'Users'
					),
					array(
						'link' => '#',
						'name' => 'Ubah Data'
					)
				);

				$this->template->load('template_admin', 'v_add', $data);
			}
		} catch (Exception $err) {
			log_message("error", $err->getMessage());
			return show_error($err->getMessage());
		}
	}



	public function tambih_setting_layanan()
	{

		try {
			$id = $this->input->post('id_users');
			$ip = $this->input->post('ip_address');
			$username = $this->input->post('username_users');
			$nama = $this->input->post('nama_users');


			//echo $password_users."<br>".$password_users_text; exit;




			if ($id != '' || $id != null) {



				$this->db->where('id_users', $id);
				$this->db->delete('ref_users_layanan');

				$aDoor = $_POST['id_layanan'];
				if (empty($aDoor)) {
					echo ("You didn't select any buildings.");
					$this->session->set_flashdata('message_error', 'You didnt select any buildings');
					redirect('ref_users/setting_layanan/' . $id);
				} else {
					$N = count($aDoor);

					// echo("You selected $N door(s): ");
					for ($i = 0; $i < $N; $i++) {
						// echo($aDoor[$i] . " ");
						$data['id_layanan'] = $aDoor[$i];
						$data['id_users'] = $id;
						$xss_data = $this->security->xss_clean($data);
						$this->db->insert('ref_users_layanan', $xss_data);
					}

					$this->session->set_flashdata('message_sukses', 'Perubahan Setting Layanan Berhasil Disimpan');
					redirect('ref_users/setting_layanan/' . $id);
				}


				// 	$data['username']=$username;
				// 	$data['ip']=$ip;

				// 	$data['nama_lengkap']=$nama;
				// 	$data['email']=$email;
				// 	$data['status_aktif']="Y";	
				// 	$data['hp']=$hp;
				// 	$data['id_user_group']=$group;
				// 	$data['id_dinas']=$id_dinas;
				// 	$data['pangkat']= $this->input->post('pangkat');
				// 	$data['jabatan']= $this->input->post('jabatan');
				// 	$data['tgl_daftar']=date('Y-m-d H:i:s');
				// 	$data['id_komponen']=$id_komponen;

				// $xss_data = $this->security->xss_clean($data);
				// $this->db->insert('ref_users', $xss_data);
				// $this->session->set_flashdata('message_sukses', 'Data Berhasil Disimpan');
				// $this->tambih();






			}
		} catch (Exception $err) {
			log_message("error", $err->getMessage());
			return show_error($err->getMessage());
		}
	}



	public function submit_sub_flow()
	{

		try {

			$id = $this->input->post('id');
			$id_layanan = $this->input->post('id_layanan');


			// $this->db->where('id_users',$id);
			// $this->db->delete('ref_users_layanan');

			// $this->db->where('id',$id);
			// $this->db->update('ref_users',$xss_data);

			$aDoor = $_POST['id_subflow'];
			if (empty($aDoor)) {
				echo ("You didn't select any buildings.");
				$this->session->set_flashdata('message_gagal', 'You didnt select any buildings');
				redirect('ref_users/setting_layanan/' . $id);
			} else {
				$N = count($aDoor);
				$array_cek = array();

				// echo("You selected $N door(s): ");
				for ($i = 0; $i < $N; $i++) {
					// echo($aDoor[$i] . " ");
					array_push($array_cek, $aDoor[$i]);
				}

				// echo "<pre>";
				// print_r($array_cek);
				// echo "</pre>";

				$data['id_sub_flow'] = implode(",", $array_cek);
				$data['id_users'] = $id;
				$xss_data = $this->security->xss_clean($data);
				$this->db->where('id_layanan', $id_layanan);
				$this->db->update('ref_users_layanan', $xss_data);

				$this->session->set_flashdata('message_sukses', 'Perubahan Setting Sub Flow Berhasil Disimpan');
				redirect('ref_users/setting_layanan/' . $id);
			}
		} catch (Exception $err) {
			log_message("error", $err->getMessage());
			return show_error($err->getMessage());
		}
	}



	public function robih()
	{
		$id = $this->uri->segment(3);
		$data['user_group'] 	= $this->users_model->get_group();
		$data['field'] 			= $this->users_model->get_all(array('id' => $id));
		$data['wilayah_ref'] 	= $this->db->query("SELECT * from ref_wilayah where id > 0")->result_array();

		// print_r($data['wilayah_ref']);exit;

		$data['breadcrumbs'] = array(
			array(
				'link' => '#',
				'name' => 'Settings'
			),
			array(
				'link' => 'ref_users',
				'name' => 'Users'
			),
			array(
				'link' => '#',
				'name' => 'Ubah Data'
			)
		);

		$data['judul_form'] = "Ubah Data";
		$data['sub_judul_form'] = "Users ";

		$sql = $this->db->query("SELECT * FROM ref_wil_provinsi");
		$data['prov'] 			= $sql;
		$data['id_provinsi']	= $data['field']->id_prov;
		$data['id_kabkot']		= $data['field']->id_kabkot;
		$data['id_kecamatan']	= $data['field']->id_kec;
		$data['id_kelurahan']	= $data['field']->id_kel;
		$this->template->load('template_admin', 'v_add', $data);
	}


	public function robih_area()
	{
		$id = $this->uri->segment(3);
		$data['user_group'] = $this->users_model->get_group();
		$data['field'] = $this->users_model->get_all(array('id' => $id));

		$data['breadcrumbs'] = array(
			array(
				'link' => '#',
				'name' => 'Settings'
			),
			array(
				'link' => 'ref_users',
				'name' => 'Users'
			),
			array(
				'link' => '#',
				'name' => 'Ubah Data'
			)
		);

		$data['judul_form'] = "Ubah Data";
		$data['sub_judul_form'] = "Users ";
		$data['dinas'] = Modules::run("master/get_dinas");
		$this->template->load('template_admin', 'v_area', $data);
	}

	public function load_instansi()
	{

		$flag_instansi = $_POST["flag_instansi"];

		$kode_prov = isset($_POST["kode_prov"]) ? $_POST["kode_prov"] : 'null';
		$data["kode_prov"] = $kode_prov;


		if ($flag_instansi == 2) {
			$this->load->view('v_instansi');
		} elseif ($flag_instansi == 3) {
			$this->load->view('v_instansi');
			// $this->load->view('v_kabkot',$data);
		} else {
			echo "";
		}
	}

	public function load_kabkota()
	{

		$kode_prov = $_POST["kode_prov"];

		$data["kode_prov"] = $kode_prov;
		$this->load->view('v_kabkot', $data);
	}

	public function setting_layanan()
	{
		$id = $this->uri->segment(3);
		$data['user_group'] = $this->users_model->get_group();
		$data['field'] = $this->users_model->get_all(array('id' => $id));
		// $data['field_layanan']=$this->db->query("select a.* from ref_layanan a where a.id_dinas = '".$this->uri->segment(4)."' and a.id_komponen = '".$this->uri->segment(5)."' and a.status_aktif='Y'")->result_array();
		$data['field_layanan'] = $this->db->query("select a.* from ref_layanan a where  a.status_aktif='Y' ORDER BY a.nama")->result_array();

		$data['breadcrumbs'] = array(
			array(
				'link' => '#',
				'name' => 'Settings'
			),
			array(
				'link' => 'ref_users',
				'name' => 'Users'
			),
			array(
				'link' => '#',
				'name' => 'Ubah Data'
			)
		);

		$data['judul_form'] = "Form Setting Layanan";
		$data['sub_judul_form'] = " ";
		$data['dinas'] = Modules::run("master/get_dinas");
		$this->template->load('template_admin', 'v_setting_layanan', $data);
	}

	public function setting_sub_flow($id_layanan, $id)
	{


		if ($id != "") {
			// $this->db->where('id_subflow',$id);
			// $query = $this->db->get('ref_subflow');
			$data['id'] = $id;
			$data['id_layanan'] = $id_layanan;
			// $data['row'] = $query->row();
			$data['field_sub_flow'] = $this->db->query("select a.* from ref_subflow a where id_layanan = '" . $id_layanan . "' and a.status_aktif='Y' ORDER BY a.nama_subflow")->result_array();

			$data['pertanyaan_form'] = "Edit Data";
			$data['sub_pertanyaan_form'] = "Basis Data Layanan";

			$this->load->view('v_setting_sub_flow', $data);
		} else {
			redirect('ref_layanan');
		}
	}

	public function lengkep()
	{
	}


	public function hupus()
	{

		$id = $this->uri->segment(3);
		try {

			$this->db->where('id', $id);
			$this->db->delete('ref_users');
			redirect('ref_users');
		} catch (Exception $err) {
			log_message("error", $err->getMessage());
			return show_error($err->getMessage());
		}
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
