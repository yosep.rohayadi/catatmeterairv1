<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Master_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}


	public function listData()
	{
		return $this->db->get("trx_runing_text")->result_array();
	}


	public function getData($id)
	{
		$this->db->where('id', $id);
		return $this->db->get("trx_runing_text")->row_array();
	}
}
