<div class="container-fluid">
	<br>

	<div class="breadcrumbs">
		<ul>
			<?php foreach ($breadcrumbs as $key => $value) { ?>
				<li>
					<a href=<?php echo site_url($value['link']) ?>>
						<?php echo $value['name']; ?></a>
					<?php echo (count($breadcrumbs) - 1) == $key ? "" : "<i class='icon-angle-right'></i>"; ?>
				</li>
			<?php } ?>
		</ul>
		<div class="close-bread">
			<a href="#"><i class="icon-remove"></i></a>
		</div>
	</div>
</div>


<div class="row-fluid">
	<div class="span12">
		<div class="box">
			<div class="box-title">
				<h3><i class=" icon-plus-sign"></i><?php echo $judul_form . " " . $sub_judul_form; ?> </h3>
			</div>
			<div class="box-content">

				<?php echo form_open('master_running_text/submit', array('name' => 'bb', 'id' => 'bb', 'class' => 'form-horizontal form-validate form-wysiwyg', 'enctype' => 'multipart/form-data')); ?>

				<?php
				if ($this->session->flashdata('message_gagal')) {
					echo '<hr><div class="alert alert-error"><button class="close" data-dismiss="alert" type="button">&times;</button>' . $this->session->flashdata('message_gagal') . '</div>';
				}
				if ($this->session->flashdata('message_sukses')) {
					echo '<hr><div class="alert alert-success"><button class="close" data-dismiss="alert" type="button">&times;</button>' . $this->session->flashdata('message_sukses') . '</div>';
				} ?>

				<input type="hidden" name="id" id="id" value="<?php echo $data['id'] ?? null ?>">

				<div class="control-group">
					<label for="textfield" class="control-label">Running Text</label>
					<div class="controls">
						<textarea name="text" class="input-xxlarge" required rows="10"><?php echo $data['text'] ?? null ?></textarea>
					</div>
				</div>

				<div class="control-group">
					<label for="textfield" class="control-label">Tanggal Awal Tampil</label>
					<div class="controls">
						<input type="date" name="tgl_awal_tampil" id="tgl_awal_tampil" class="input-xxlarge" value="<?php echo $data['tgl_awal_tampil'] ?? '' ?>" required>
					</div>
				</div>

				<div class="control-group">
					<label for="textfield" class="control-label">Tanggal Akhir Tampil</label>
					<div class="controls">
						<input type="date" name="tgl_akhir_tampil" id="tgl_akhir_tampil" class="input-xxlarge" value="<?php echo $data['tgl_akhir_tampil'] ?? '' ?>" required>
					</div>
				</div>


				<div class="form-actions">
					<button class="btn btn-primary" type="submit">Simpan</button>
					<a class="btn btn-danger" href="<?php echo site_url(); ?>master_running_text/">Kembali</a>
				</div>

				</form>
			</div>
		</div>
	</div>
</div>