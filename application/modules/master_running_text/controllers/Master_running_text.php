<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


class Master_running_text extends MX_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->truly_madly_deeply();
		Modules::run('ref_role/permissions');
		$this->load->model('Master_model');
		$this->load->helper('tool');
	}


	public function truly_madly_deeply()
	{

		if (!$this->session->userdata('truly_madly_deeply')) {
			redirect('signon');
		}
	}


	public function index($offset = 0)
	{
		$data['breadcrumbs'] = array(
			array(
				'link' => '#',
				'name' => 'Home'
			),
			array(
				'link' => '#',
				'name' => 'Running Text Depan'
			)
		);

		$data['listData'] 		= $this->Master_model->listData();
		$data['role'] 			= get_role($this->session->userdata('sesi_bo_user_group'));
		$data['sub_judul_form'] = "List Data Running Text Depan ";
		$this->template->load('template_admin', 'v_index', $data);
	}


	public function add()
	{
		get_role($this->session->userdata('sesi_bo_user_group'), 'insert');
		$data['judul_form'] = "Tambah Data";
		$data['sub_judul_form'] = "Running Text Depan";

		$data['breadcrumbs'] =
			array(
				array(
					'link' => '#',
					'name' => 'Settings'
				),
				array(
					'link' => 'master_running_text',
					'name' => 'Running Text Depan'
				),
				array(
					'link' => '#',
					'name' => 'Tambah Data'
				)
			);

		$this->template->load('template_admin', 'v_add', $data);
	}



	public function submit()
	{
		try {
			$id = $this->input->post('id');
			$data = [
				'text'					=> $this->input->post('text'),
				'tgl_awal_tampil'		=> $this->input->post("tgl_awal_tampil"),
				'tgl_akhir_tampil'		=> $this->input->post("tgl_akhir_tampil"),
				'created_at'			=> $this->input->post("status_aktif"),
				'updated_at'			=> date('Y-m-d H:i:s'),
			];

			if ($id == '' || $id == null) {
				get_role($this->session->userdata('sesi_bo_user_group'), 'insert');

				$this->db->set($data);
				$this->db->insert("trx_runing_text");
				$id =  $this->db->insert_id();
				$this->session->set_flashdata('message_sukses', 'Data Berhasil Disimpan');
			} else {
				get_role($this->session->userdata('sesi_bo_user_group'), 'update');

				$this->db->where('id', $id);
				$this->db->set($data);
				$this->db->update('trx_runing_text');
				$this->session->set_flashdata('message_sukses', 'Perubahan Data Berhasil Disimpan');
			}
			return redirect(base_url("master_running_text/edit/" . $id));
		} catch (Exception $err) {
			log_message("error", $err->getMessage());
			return show_error($err->getMessage());
		}
	}


	public function edit($id)
	{
		get_role($this->session->userdata('sesi_bo_user_group'), 'update');
		$data['judul_form'] = "Edit Data";
		$data['sub_judul_form'] = "Running Text Depan";
		$data['data'] = $this->Master_model->getData($id);
		$data['breadcrumbs'] =
			array(
				array(
					'link' => '#',
					'name' => 'Settings'
				),
				array(
					'link' => 'master_running_text',
					'name' => 'Running Text Depan'
				),
				array(
					'link' => '#',
					'name' => 'Edit Data'
				)
			);

		$this->template->load('template_admin', 'v_add', $data);
	}


	public function delete($id)
	{
		get_role($this->session->userdata('sesi_bo_user_group'), 'delete');
		$id = deCrypt($id);
		$this->db->where('id', $id);
		$this->db->delete('trx_periode');

		return redirect(base_url("master_running_text"));
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
