<div class="container-fluid">
    <br>
    <div class="breadcrumbs">
        <ul>
            <?php foreach ($breadcrumbs as $key => $value) { ?>
                <li>
                    <a href=<?php echo site_url($value['link']) ?>>
                        <?php echo $value['name']; ?></a>
                    <?php echo (count($breadcrumbs) - 1) == $key ? "" : "<i class='icon-angle-right'></i>"; ?>
                </li>
            <?php } ?>
        </ul>
        <div class="close-bread">
            <a href="#"><i class="icon-remove"></i></a>
        </div>
    </div>
</div>

<div class="row-fluid">
    <div class="span12">
        <div class="box">
            <div class="box-title">
                <h3>
                    <i class="icon-reorder"></i>
                    <?php echo $sub_judul_form; ?>
                </h3>
            </div>
            <div class="box-content">
                <?php
                if ($this->session->flashdata('message_gagal')) {
                    echo '<div class="alert alert-error"><button class="close" data-dismiss="alert" type="button">&times;</button>' . $this->session->flashdata('message_gagal') . '</div>';
                }
                if ($this->session->flashdata('message_sukses')) {
                    echo '<hr><div class="alert alert-success"><button class="close" data-dismiss="alert" type="button">&times;</button>' . $this->session->flashdata('message_sukses') . '</div>';
                } ?>
                <form action="<?php echo site_url('profil/index'); ?>" method="post" name="form1" class="form-horizontal form-bordered">
                    <input type="hidden" name="<?= $this->security->get_csrf_token_name(); ?>" value="<?= $this->security->get_csrf_hash(); ?>" style="display: none">

                    <?php if ($role['insert'] == "TRUE") { ?>
                        <div align="right">
                            <a class="btn btn-blue" href="<?php echo site_url("profil/tambih "); ?>"><i class="icon-plus-sign"></i> Tambah Data</a>
                        </div>
                    <?php } ?>

                    <?php if ($role['insert'] == "TRUE") { ?>
                        <div class="control-group">
                            <label class="control-label" for="textfield">Pencarian</label>
                            <div class="controls">
                                <input type="text" value="<?php echo $this->session->userdata('s_cari_global_lembaga'); ?>" class="form-control" name="cari_global_lembaga" placeholder="Masukan kata kunci pencarian...">
                                <input type="submit" name="submit" value="Cari" class="btn btn-blue">
                                <a class="btn btn-red" href="<?php echo site_url(); ?>profil/clear_session"><i class="icon-trash"></i> Hapus Pencarian</a>
                            </div>
                        </div>
                    <?php } ?>


                    <div class="table-responsive">
                        <table width="100%" class="table table-hover table-striped table-bordered">
<thead>
                                
                                <tr>
                                  <th width="10%"><center>
                                    Profil
                                  </center></th>
                                  <th width="11%">Luas Wilayah</th>
                                  <th width="14%">Jumlah Penduduk</th>
                                  <th width="9%">Jumlah KK</th>
                                  <th width="12%">Jumlah RT/RW</th>
                                  <th width="9%">Created By</th>
                              <th width="9%">Updated By</th>
                              <th colspan="2">&nbsp;</th>
                              </tr>
                            </thead>
                            <tbody>

                                <?php if (count($ListData) > 0) {
                                    foreach ($ListData as $key => $value) {
                                ?>

                                        <tr>
                                            <td>Kelurahan <?php echo $value['nama'] ?><br />
                                              Alamat : <?php echo $value['alamat'] ?><br />
                                              No.Telp : <?php echo $value['no_telp'] ?></td>
                                            <td><?php echo $value['luas_wil'] ?>
                                          <td>Laki - Laki : <?php echo $value['jml_penduduk_laki'] ?>                                            
                                            <br />
                                          Perempuan : <?php echo $value['jml_penduduk_perempuan'] ?>
                                          <td><?php echo $value['jml_kk'] ?>                                          
                                          <td>RT : <?php echo $value['jml_rt'] ?> <br />
RW : <?php echo $value['jml_rw'] ?>
                                          <td><?php echo $value['created_by'] ?><br />
                                        <?php echo $value['created_date'] ?></td>
                                      <td><?php echo $value['update_by'] ?><br />
                                        <?php echo $value['updated_date'] ?></td>
                                      <!--
											-->
                                          <td width="13%">
                          <center>
                                                    <?php if ($role['update'] == "TRUE") { ?>
                                                        <a class="btn btn-green " href="<?php echo site_url(); ?>profil/robih/<?php echo enCrypt($value['id']); ?>"><i class="icon-pencil"></i> Ubah Data</a>

                                                    <?php } ?>
                                                    <hr />
													<!--
                                                    <?php if ($role['update'] == "TRUE") { ?>
                                                        <a class="btn btn-red " href="#"><i class="icon-print"></i> Export Pdf</a>

                                                    <?php } ?> -->
                                                </center>                                            </td>
                                      <td width="13%">
                      <center>
                                                <?php if ($role['delete'] == "TRUE") { ?>
                                                    <a class="btn btn-red" href="<?php echo site_url('profil/hupus/' . enCrypt($value['id'])); ?>" onclick="return confirm('Anda Yakin ingin Menghapus?'); "><i class="icon-trash"></i> Hapus</a>
                                                <?php } ?>
                                                </center>                                            </td>
                              </tr>

                                <?php
                                    }
                                    $paging = (!empty($pagermessage) ? $pagermessage : '');


                                    echo "<tr><td colspan='13'><div style='background:000;'>$paging &nbsp;" . $this->pagination->create_links() . "</div></td></tr>";
                                } else {
                                    echo "<tbody><tr><td colspan='13' style='padding:10px; background:#F00; border:none; color:#FFF;'>Data Tidak Tersedia</td></tr></tbody>";
                                }

                                ?>
                            </tbody>
                        </table>
                  </div>

                </form>

            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    $('#add-peserta').click(function() {
        window.open('<?php echo site_url('/penerimaan_berkas/list_peserta/' . $kode_usulan) ?>', '1576730953022', 'width=1000,height=650,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=0,top=0');
    })
</script>
