<div class="container-fluid">
    <br>
    <div class="breadcrumbs">
        <ul>
            <?php foreach ($breadcrumbs as $key => $value) { ?>
                <li>
                    <a href="<?php echo site_url($value['link']) ?>">
                        <?php echo $value['name']; ?></a>
                    <?php echo (count($breadcrumbs) - 1) == $key ? "" : "<i class='icon-angle-right'></i>"; ?>
                </li>
            <?php } ?>
        </ul>
        <div class="close-bread">
            <a href="#"><i class="icon-remove"></i></a>
        </div>
    </div>
</div>



<div class="row-fluid">
	
	<div class="span1">&nbsp;</div>

    <div class="span10">
        <?php
        if ($this->session->flashdata('message_gagal')) {
            echo '<hr><div class="alert alert-error"><button class="close" data-dismiss="alert" type="button">&times;</button>' . $this->session->flashdata('message_gagal') . '</div>';
        }
        if ($this->session->flashdata('message_sukses')) {
            echo '<hr><div class="alert alert-success"><button class="close" data-dismiss="alert" type="button">&times;</button>' . $this->session->flashdata('message_sukses') . '</div>';
        } ?>

        <div class="box box-bordered box-color">
            <div class="box-title">
                <h3><i class=" icon-plus-sign"></i><?php echo $judul_form . " " . $sub_judul_form; ?> </h3>
            </div>
            <div class="box-content nopadding">
                <!-- <form action="#" method="POST" class='form-horizontal form-validate' id="bb"> -->
                <?php echo form_open('profil/tambih_robih', array('name' => 'bb', 'id' => 'bb', 'class' => 'form-horizontal form-bordered form-validate form-wysiwyg', 'enctype' => 'multipart/form-data')); ?>


                <input type="hidden" name="id" id="id" value="<?php echo isset($field->id) ? $field->id : ''; ?>">
                <input type="hidden" name="<?= $this->security->get_csrf_token_name(); ?>" value="<?= $this->security->get_csrf_hash(); ?>" style="display: none">

                <input type="hidden" value="<?php echo $this->input->ip_address(); ?>" name="ip_address">

                <?php
                $sesi_bo_id_prov = $this->session->userdata('sesi_bo_id_prov');
                $sesi_bo_id_kabkot = $this->session->userdata('sesi_bo_id_kabkot');
                if ($sesi_bo_id_prov == "" && $sesi_bo_id_kabkot == "") {
                    require_once(APPPATH . '/modules/form/views/v_form_global.php');
                } else {
                    require_once(APPPATH . '/modules/form/views/v_form_text.php');
                }


                ?>
                <div class="control-group">
                    <label for="textfield" class="control-label">Nama Kelurahan</label>
                    <div class="controls">
                        <input type="text" readonly id="nama" class="input-large" name="nama" value="<?php echo $this->session->userdata('sesi_bo_nama_kel'); ?>" style="text-transform:uppercase" required>
                        <span class="required-server"><?php echo form_error('nama', '<p style="color:#F83A18">', '</p>'); ?></span>
                    </div>
                </div>
				
				
				<div class="control-group">
                    <label for="textfield" class="control-label">Alamat</label>
                    <div class="controls">
                        <input type="text" placeholder="" id="alamat" class="input-xxlarge" name="alamat" value="<?php echo isset($field->alamat) ? $field->alamat : $this->input->post("alamat"); ?>" style="text-transform:uppercase" >
                        <span class="required-server"><?php echo form_error('alamat', '<p style="color:#F83A18">', '</p>'); ?></span>
                    </div>
                </div>
				
				
				<div class="control-group">
										<label class="control-label" for="textfield">No.Telp</label>                                        
                                        <div class="controls">
                                        <input type="number" placeholder="" id="no_telp" class="input-large" name="no_telp" value="<?php echo isset($field->no_telp) ? $field->no_telp : $this->input->post("no_telp"); ?>" style="text-transform:uppercase" required>
										</div>
									</div>
				
				<div class="control-group">
                    <label for="textfield" class="control-label">Visi</label>
                    <div class="controls">
                        <textarea class="input-xxlarge ckeditor" id="ckeditor" name="visi"><?php echo isset($field->visi)?$field->visi: $this->input->post("visi");?></textarea>
                        <span class="required-server"><?php echo form_error('visi', '<p style="color:#F83A18">', '</p>'); ?></span>
						
                    </div>
                </div>



                
<div class="control-group">
                    <label for="textfield" class="control-label">Misi</label>
                    <div class="controls">
                       <textarea class="input-xxlarge ckeditor" id="ckeditor" name="misi"><?php echo isset($field->misi)?$field->misi: $this->input->post("misi");?></textarea>
 <span class="required-server"><?php echo form_error('misi', '<p style="color:#F83A18">', '</p>'); ?></span>
                    </div>
                </div>


                <div class="control-group">
                    <label for="textfield" class="control-label">Dasar hukum pembentukan Pemerintah Desa / Kelurahan </label>
                    <div class="controls">
                       <!-- <textarea class="input-xxlarge ckeditor" id="ckeditor" name="dasar_pembentukan"><?php echo isset($field->dasar_pembentukan)?$field->dasar_pembentukan: $this->input->post("dasar_pembentukan");?></textarea> -->
					   
					   <input type="text" placeholder="" id="dasar_pembentukan" class="input-xxlarge" name="dasar_pembentukan" value="<?php echo isset($field->dasar_pembentukan) ? $field->dasar_pembentukan : $this->input->post("dasar_pembentukan"); ?>" style="text-transform:uppercase" >
                      

                        <span class="required-server"><?php echo form_error('dasar_pembentukan', '<p style="color:#F83A18">', '</p>'); ?></span>
                    </div>
                </div>
                

                
                
				<div class="control-group">
										<label class="control-label" for="textfield">Luas Wilayah</label>                                        
                                        <div class="controls">
                                        <input type="number" placeholder="" id="luas_wil" class="input-small" name="luas_wil" value="<?php echo isset($field->luas_wil) ? $field->luas_wil : $this->input->post("luas_wil"); ?>"  required>
										Ha.
										</div>
									</div>
				
                                    
				<div class="control-group">
										<label class="control-label" for="textfield">Jumlah Penduduk Laki-Laki</label>                                        
                                        <div class="controls">
                                        <input type="number" placeholder="" id="jml_penduduk_laki" class="input-small" name="jml_penduduk_laki" value="<?php echo isset($field->jml_penduduk_laki) ? $field->jml_penduduk_laki : $this->input->post("jml_penduduk_laki"); ?>"  required>
										</div>
				</div>
				
				
				<div class="control-group">
										<label class="control-label" for="textfield">Jumlah Penduduk Perempuan</label>                                        
                                        <div class="controls">
                                        <input type="number" placeholder="" id="jml_penduduk_perempuan" class="input-small" name="jml_penduduk_perempuan" value="<?php echo isset($field->jml_penduduk_perempuan) ? $field->jml_penduduk_perempuan : $this->input->post("jml_penduduk_perempuan"); ?>"  required>
										</div>
				</div>
				
				
					<div class="control-group">
										<label class="control-label" for="textfield">Jumlah Kepala Keluarga</label>                                        
                                        <div class="controls">
                                        <input type="number" placeholder="" id="jml_kk" class="input-small" name="jml_kk" value="<?php echo isset($field->jml_kk) ? $field->jml_kk : $this->input->post("jml_kk"); ?>"  required>
										</div>
				</div>
				
				
				<div class="control-group">
										<label class="control-label" for="textfield">Jumlah RT/RW</label>                                        
                                        <div class="controls">
                                        <input type="number" placeholder="" id="jml_rt" class="input-small" name="jml_rt" value="<?php echo isset($field->jml_rt) ? $field->jml_rt : $this->input->post("jml_rt"); ?>"  required> /
										<input type="number" placeholder="" id="jml_rw" class="input-small" name="jml_rw" value="<?php echo isset($field->jml_rw) ? $field->jml_rw : $this->input->post("jml_rw"); ?>"  required>
										</div>
				</div>
				
				<div class="control-group">
                    <label class="control-label" for="textfield">Photo Peta Kelurahan</label>
                    <div class="controls">
                        <!-- <input type="hidden" name="nip" id="nip" class="input-large" value=""> -->
                        <input type="file" name="berkas_peta" id="berkas_peta" accept="image/*">
                        <span class="required-server">
                            <p>*<small>File tipe gambar</small>
							<br/>*<small>Ukuran maksimal 1 MB</small>
							</p>
                        </span>

                        <?php if (isset($field->id)) { ?>
                            <img src="<?php echo base_url(); ?>/<?php echo isset($field->foto_peta) ? $field->foto_peta : ''; ?>" width="400">
                        <?php } ?>
                    </div>
                </div>
				
				

                <div class="control-group">
                    <label class="control-label" for="textfield">Photo Kelurahan</label>
                    <div class="controls">
                        <!-- <input type="hidden" name="nip" id="nip" class="input-large" value=""> -->
                        <input type="file" name="berkas" id="berkas" accept="image/*">
                        <span class="required-server">
                            <p>*<small>File tipe gambar</small>
							<br/>*<small>Ukuran maksimal 1 MB</small>
							</p>
                        </span>

                        <?php if (isset($field->id)) { ?>
                            <img src="<?php echo base_url(); ?>/<?php echo isset($field->foto_lokasi) ? $field->foto_lokasi : ''; ?>" width="400">
                        <?php } ?>
                    </div>
                </div>








                <div class="form-actions">
                    <button id="btn_simpan" class="btn btn-blue" type="submit">Simpan</button>
                    <a class="btn btn-danger" href="<?php echo site_url(); ?>profil">Kembali</a>
                </div>

                </form>
            </div>
        </div>
		
		
		<div class="span1">&nbsp;</div>
		
    </div>


