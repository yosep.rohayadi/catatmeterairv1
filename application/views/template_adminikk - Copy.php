<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<!-- Apple devices fullscreen -->
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<!-- Apple devices fullscreen -->
	<meta names="apple-mobile-web-app-status-bar-style" content="black-translucent" />

	<title>E-PELAPORAN IKK</title>

	<!-- Bootstrap -->
	<link rel="stylesheet" href="<?php echo base_url()?>assets_users/css/wizards.css">
	<link rel="stylesheet" href="<?php echo base_url()?>assets_users/css/bootstrap.min.css">
	<!-- Bootstrap responsive -->
	<link rel="stylesheet" href="<?php echo base_url()?>assets_users/css/bootstrap-responsive.min.css">
	<!-- jQuery UI -->
	<link rel="stylesheet" href="<?php echo base_url()?>assets_users/css/plugins/jquery-ui/smoothness/jquery-ui.css">
	<link rel="stylesheet" href="<?php echo base_url()?>assets_users/css/plugins/jquery-ui/smoothness/jquery.ui.theme.css">
	<!-- icheck -->
	<link rel="stylesheet" href="<?php echo base_url()?>assets_users/css/plugins/icheck/all.css">
	<!-- Theme CSS -->
	<link rel="stylesheet" href="<?php echo base_url()?>assets_users/css/style.css">
	<!-- Color CSS -->
	<link rel="stylesheet" href="<?php echo base_url()?>assets_users/css/themes.css">
	<link rel="stylesheet" href="<?php echo base_url()?>assets_users/css/plugins/datepicker/datepicker.css">
	<link rel="stylesheet" href="<?php echo base_url()?>assets_users/font-custom/font-custom.css">
	<link rel="stylesheet" href="<?php echo base_url()?>assets_users/css/btn-sandy.css">
	<!-- <link rel="stylesheet" href="<?php echo base_url()?>assets_users/css/table_bootstrap4.css"> -->

	<!-- jQuery -->
	<script src="<?php echo base_url()?>assets_users/js/jquery.min.js"></script>

	<!-- Nice Scroll -->
	<script src="<?php echo base_url()?>assets_users/js/plugins/nicescroll/jquery.nicescroll.min.js"></script>
	<!-- imagesLoaded -->
	<script src="<?php echo base_url()?>assets_users/js/plugins/imagesLoaded/jquery.imagesloaded.min.js"></script>
	<!-- jQuery UI -->
	<script src="<?php echo base_url()?>assets_users/js/plugins/jquery-ui/jquery.ui.core.min.js"></script>
	<script src="<?php echo base_url()?>assets_users/js/plugins/jquery-ui/jquery.ui.widget.min.js"></script>
	<script src="<?php echo base_url()?>assets_users/js/plugins/jquery-ui/jquery.ui.mouse.min.js"></script>
	<script src="<?php echo base_url()?>assets_users/js/plugins/jquery-ui/jquery.ui.resizable.min.js"></script>
	<script src="<?php echo base_url()?>assets_users/js/plugins/jquery-ui/jquery.ui.sortable.min.js"></script>
	<!-- slimScroll -->
	<script src="<?php echo base_url()?>assets_users/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<!-- Bootstrap -->
	<script src="<?php echo base_url()?>assets_users/js/bootstrap.min.js"></script>
	<!-- Bootbox -->
	<script src="<?php echo base_url()?>assets_users/js/plugins/bootbox/jquery.bootbox.js"></script>
	<!-- Bootbox -->
	<script src="<?php echo base_url()?>assets_users/js/plugins/form/jquery.form.min.js"></script>
	<!-- CKEditor -->
	<script src="<?php echo base_url()?>assets_users/ckeditor/ckeditor.js"></script>

	


	<!-- Validation -->
	<script src="<?php echo base_url()?>assets_users/js/plugins/validation/jquery.validate.min.js"></script>
	<script src="<?php echo base_url()?>assets_users/js/plugins/validation/additional-methods.min.js"></script>
      <script src="<?php echo base_url()?>assets_users/js/plugins/complexify/jquery.complexify-banlist.min.js"></script>
	<script src="<?php echo base_url()?>assets_users/js/plugins/complexify/jquery.complexify.min.js"></script>
	<!-- icheck -->
	<script src="<?php echo base_url()?>assets_users/js/plugins/icheck/jquery.icheck.min.js"></script>

        <!-- Datepicker -->
	<script src="<?php echo base_url(); ?>assets_users/js/plugins/datepicker/bootstrap-datepicker.js"></script>

	<!-- Theme framework -->
	<script src="<?php echo base_url()?>assets_users/js/eakroko.js"></script>
	<!-- Theme scripts -->
	<script src="<?php echo base_url()?>assets_users/js/application.min.js"></script>
	<!-- Just for demonstration -->
	<script src="<?php echo base_url()?>assets_users/js/demonstration.min.js"></script>

	<!-- DATATABLES -->
	<link rel="stylesheet" href="<?php echo base_url()?>assets_users/datatables/datatables.css">
	<script src="<?php echo base_url()?>assets_users/datatables/datatables.js"></script>



	<!--[if lte IE 9]>
		<script src="<?php echo base_url()?>assets_users/js/plugins/placeholder/jquery.placeholder.min.js"></script>
		<script>
			$(document).ready(function() {
				$('input, textarea').placeholder();
			});
		</script>
	<![endif]-->

	<!-- Favicon -->
			<!--
<link rel="shortcut icon" href="<?php echo base_url()?>assets/img/logobandung.png" />
	<link rel="apple-touch-icon-precomposed" href="<?php echo base_url()?>assets/img/logobandung.png" />
-->




<style>

.overlay {
  height: 100%;
  width: 0;
  position: fixed;
  z-index: 1;
  top: 0;
  left: 0;
  background-color: rgb(0,0,0);
  background-color: rgba(0,0,0, 0.9);
  overflow-x: hidden;
  transition: 0.5s;
}

.overlay-content {
  position: relative;
  top: 25%;
  width: 100%;
  text-align: left;
  margin-top: 30px;
}

.overlay a {
  padding: 8px;
 
  font-size: 12px;
  color: #818181;
  display: block;
  transition: 0.1s;
}

.overlay a:hover, .overlay a:focus {
  color: #f1f1f1;
}

.overlay .closebtn {
  position: absolute;
  top: 20px;
  right: 45px;
  font-size: 20px;
}

@media screen and (max-height: 450px) {
  .overlay a {font-size: 20px}
  .overlay .closebtn {
  font-size: 10px;
  top: 15px;
  right: 35px;
  }
}
</style>



</head>



<body class="theme-blue" data-theme="theme-blue">
<table width="100%" border="0">
  <tr>
    <td width="68%" rowspan="2"><a href="<?php echo site_url('signonikk') ?>">
			<img src="<?php echo base_url()?>assets_users/img/header_frontendikk.png" />
		</a></td>
    <td align="right" width="32%">
<?php if($this->session->userdata('truly_madly_deeply')){ ?>

	<button type="button" class="btn btn-primary" onclick="openNav()"><i class="icon-file"></i> Menus</button>    
	
	<div id="myNav" class="overlay">
  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
  <div class="overlay-content">
    
	<?php
				echo menu_nav2();
				?>
	
  </div>
</div>


<span style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776; open</span>

<script>
function openNav() {
  document.getElementById("myNav").style.width = "100%";
}

function closeNav() {
  document.getElementById("myNav").style.width = "0%";
}
</script>
	
	
	
<?php } ?>

	</td>
  </tr>
  <tr align="right">
    <td>
	<?php if($this->session->userdata('truly_madly_deeply')){ ?>
	Selamat datang, <strong><?php echo $this->session->userdata('sesi_bo_nama_lengkap'); ?>&nbsp;</strong>
	<?php } ?>
	</td>
  </tr>
</table>

<div style="background-color:#3399FF; height:10px;"></div>
	
	






<!-- Trigger the modal with a button -->


<!-- Modal -->



	

	

	



<div  class="container-fluid nav-hidden" id="content" >


				<div id="main" style="margin-left:0px; font-family: 'Roboto', Helvetica, Arial, sans-serif;">

				<?php
					ini_set('memory_limit', '512M');
					echo $contents;
				?>

				</div>

		</div>




	</body>
	<script type="text/javascript">
		$('.datepick').datepicker({
    		format: 'yyyy-mm-dd'
 		});

	</script>
	</html>
