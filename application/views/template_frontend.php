<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<!-- Apple devices fullscreen -->
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<!-- Apple devices fullscreen -->
	<meta names="apple-mobile-web-app-status-bar-style" content="black-translucent" />
	
	<title>SIM-SATPOL PP</title>

	<!-- Bootstrap -->
	<link rel="stylesheet" href="<?php echo base_url()?>assets_users/css/bootstrap.min.css">
	<!-- Bootstrap responsive -->
	<link rel="stylesheet" href="<?php echo base_url()?>assets_users/css/bootstrap-responsive.min.css">
	<!-- jQuery UI -->
	<link rel="stylesheet" href="<?php echo base_url()?>assets_users/css/plugins/jquery-ui/smoothness/jquery-ui.css">
	<link rel="stylesheet" href="<?php echo base_url()?>assets_users/css/plugins/jquery-ui/smoothness/jquery.ui.theme.css">
	<!-- Theme CSS -->
	<link rel="stylesheet" href="<?php echo base_url()?>assets_users/css/style.css">
	<!-- Color CSS -->
	<link rel="stylesheet" href="<?php echo base_url()?>assets_users/css/themes.css">
	<link rel="stylesheet" href="<?php echo base_url()?>assets_users/css/plugins/datepicker/datepicker.css">
	<link rel="stylesheet" href="<?php echo base_url()?>assets_users/css/btn-sandy.css">


	<!-- jQuery -->
	<script src="<?php echo base_url()?>assets_users/js/jquery.min.js"></script>
	
	<!-- Nice Scroll -->
	<script src="<?php echo base_url()?>assets_users/js/plugins/nicescroll/jquery.nicescroll.min.js"></script>
	<!-- imagesLoaded -->
	<script src="<?php echo base_url()?>assets_users/js/plugins/imagesLoaded/jquery.imagesloaded.min.js"></script>
	<!-- jQuery UI -->
	<script src="<?php echo base_url()?>assets_users/js/plugins/jquery-ui/jquery.ui.core.min.js"></script>
	<script src="<?php echo base_url()?>assets_users/js/plugins/jquery-ui/jquery.ui.widget.min.js"></script>
	<script src="<?php echo base_url()?>assets_users/js/plugins/jquery-ui/jquery.ui.mouse.min.js"></script>
	<script src="<?php echo base_url()?>assets_users/js/plugins/jquery-ui/jquery.ui.resizable.min.js"></script>
	<script src="<?php echo base_url()?>assets_users/js/plugins/jquery-ui/jquery.ui.sortable.min.js"></script>
	<!-- slimScroll -->
	<script src="<?php echo base_url()?>assets_users/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<!-- Bootstrap -->
	<script src="<?php echo base_url()?>assets_users/js/bootstrap.min.js"></script>
	<!-- Bootbox -->
	<script src="<?php echo base_url()?>assets_users/js/plugins/bootbox/jquery.bootbox.js"></script>
	<!-- Bootbox -->
	<script src="<?php echo base_url()?>assets_users/js/plugins/form/jquery.form.min.js"></script>
	<!-- Validation -->
	<script src="<?php echo base_url()?>assets_users/js/plugins/validation/jquery.validate.min.js"></script>
	<script src="<?php echo base_url()?>assets_users/js/plugins/validation/additional-methods.min.js"></script>
      <script src="<?php echo base_url()?>assets_users/js/plugins/complexify/jquery.complexify-banlist.min.js"></script>
	<script src="<?php echo base_url()?>assets_users/js/plugins/complexify/jquery.complexify.min.js"></script>  

        <!-- Datepicker -->
	<script src="<?php echo base_url(); ?>assets_users/js/plugins/datepicker/bootstrap-datepicker.js"></script>

	<!-- Theme framework -->
	<script src="<?php echo base_url()?>assets_users/js/eakroko.min.js"></script>
	<!-- Theme scripts -->
	<script src="<?php echo base_url()?>assets_users/js/application.min.js"></script>
	<!-- Just for demonstration -->
	<script src="<?php echo base_url()?>assets_users/js/demonstration.min.js"></script>

	<!--[if lte IE 9]>
		<script src="<?php echo base_url()?>assets_users/js/plugins/placeholder/jquery.placeholder.min.js"></script>
		<script>
			$(document).ready(function() {
				$('input, textarea').placeholder();
			});
		</script>
	<![endif]-->
	
	<!-- Favicon -->
	<!-- Apple devices Homescreen icon -->
	<link rel="apple-touch-icon-precomposed" href="img/apple-touch-icon-precomposed.png" />

</head>



<body class="theme-darkblue" data-theme="theme-darkblue">

<div align="left"><img src="<?php echo base_url()?>assets_users/img/header_frontend.png"></div>
	<div style="background-color:#336600; height:2px;">&nbsp;</div>
	


	
	
	
	<div id="navigation">
		<div class="container-fluid">
			
		<!--<a id="brand" href="#">SIPP</a>
		<a href="#" class="toggle-nav" rel="tooltip" data-placement="bottom" title="Toggle navigation"><i class="icon-reorder"></i></a>-->
		
		<?php 
		$query_foto1 = $this->db->query("select * from ref_pegawai where nip = '". $this->session->userdata('sesi_nip')."'")->row();
		?>
		<ul class='main-nav' style="<?php if(!empty($query_foto1->foto)){ echo "margin-top: 0%;"; }else{ echo "margin-top: 0%;";} ?>">
				
		
				
				
				
				<?php if(!$this->session->userdata('atos_tiasa_leubeut')){ ?>
				
				
				<li class=''>
					<a href="<?php echo site_url(); ?>">
						<span>Beranda</span>
					</a>
				</li>
				
				<li class=''>
					<a href="<?php echo site_url(); ?>baca/tata_cara">
						<span>Tata Cara Pendaftaran & Usulan</span>
					</a>
				</li>
				
					
				
				<li class=''>
					<a href="<?php echo site_url(); ?>baca/persyaratan">
						<span>Persyaratan</span>
					</a>
				</li>
				
				<li class=''>
					<a href="<?php echo site_url(); ?>baca/peraturan">
						<span>Peraturan</span>
					</a>
				</li>

				<li class=''>
					<a href="<?php echo site_url(); ?>daftar">
						<span>Pendaftaran Akun</span>
					</a>
				</li>
				
				<li class=''>
					<a href="<?php echo site_url(); ?>loginapp">
						<span>Login</span>
					</a>
				</li>
				
				<?php } else { ?>
				
				
				
				<li class=''>
					<a href="<?php echo site_url(); ?>beranda">
						<span>Beranda</span>
					</a>
				</li>
				
				<li>
							<a href="<?php echo site_url();?>editprofil/">Data Pribadi</a>
				</li>
				
				<li class=''>
					<a href="<?php echo site_url(); ?>p2upd_takkah">
						<span>Dokumen Kepegawaian</span>
					</a>
				</li>
				
				
				<li>
							<a href="<?php echo site_url();?>pak_lama">Inisiasi PAK Awal/Lama</a>
						</li>
				
				
				<li class=''>
					<a href="<?php echo site_url(); ?>pengusulan">
						<span>Pengusulan DUPAK</span>
					</a>
				</li>
				
				
				
				
				
				<!--<li>
					<a href="#" data-toggle="dropdown" class='dropdown-toggle'>
						<span>Profil Saya</span>
						<span class="caret"></span>
					</a>
					<ul class="dropdown-menu">
						
						<li>
							<a href="<?php echo site_url();?>p2upd_takkah">File Pendukung Saya (Takkah)</a>
						</li>
						
						
					</ul>
				</li>-->
				
				
				
				<?php } ?>
				
				
				
				
				
				
				
				
				
				
				
				
				
			
				
				
			</ul>
		
		
			
			<div class="user">
				
				<ul class="icon-nav"> </ul>
				
				<?php if($this->session->userdata('atos_tiasa_leubeut')){ ?>
				<div class="dropdown">
					<a href="#" class='dropdown-toggle' data-toggle="dropdown">Selamat datang, <strong>
					<?php echo $this->session->userdata('sesi_nama_lengkap'); ?>
					(<?php echo $this->session->userdata('sesi_jabatan'); ?> - <?php echo $this->session->userdata('sesi_subjabatan'); ?>)
					
					</strong> 
					<?php 
						$query_foto = $this->db->query("select * from ref_pegawai where nip = '". $this->session->userdata('sesi_nip')."'")->row();
					 ?>
					<img src="<?php if(!empty($query_foto->foto)){ echo base_url().$query_foto->foto.".thumb.jpg"; } else{ echo base_url()?>assets_users/img/default.png <?php } ?>" alt=""></a>
					<ul class="dropdown-menu pull-right">
					   
						<li>
							<a href="<?php echo site_url();?>editpass/">Ubah Password</a>
						</li>
						<li>
							<a href="<?php echo site_url();?>loginapp/logout/">Logout</a>
						</li>
					</ul>
					
				</div>
				<?php } ?>
				
				
			</div>
		</div>
	</div>
	
	
	
	
	
	
	<!--<div class="container-fluid nav-hidden" id="content">
		
		<div id="row-fluid"></div> 
		
		<div id="main" style="margin-left:0px;">
			<div class="container-fluid">
				
				
				
				
				
				
				init
				
				
			</div>
		</div>
		</div>-->
		
<div  class="container-fluid nav-hidden" id="content">
		
				
				<div id="main" style="margin-left:0px;">
				
				<?php
					ini_set('memory_limit', '512M');
					echo $contents;
				?>
				
				</div>
			
		</div>
		
		
		
		
	</body>

	</html>