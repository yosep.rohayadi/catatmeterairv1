<!doctype html>
<html>

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<!-- Apple devices fullscreen -->
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<!-- Apple devices fullscreen -->
	<meta names="apple-mobile-web-app-status-bar-style" content="black-translucent" />

	<title>AKUR-ANOTASI DIGITAL</title>

	<!-- Bootstrap -->
	<link rel="stylesheet" href="<?php echo base_url() ?>assets_users/css/wizards.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>assets_users/css/bootstrap.min.css">
	<!-- Bootstrap responsive -->
	<link rel="stylesheet" href="<?php echo base_url() ?>assets_users/css/bootstrap-responsive.min.css">
	<!-- jQuery UI -->
	<link rel="stylesheet" href="<?php echo base_url() ?>assets_users/css/plugins/jquery-ui/smoothness/jquery-ui.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>assets_users/css/plugins/jquery-ui/smoothness/jquery.ui.theme.css">
	<!-- icheck -->
	<link rel="stylesheet" href="<?php echo base_url() ?>assets_users/css/plugins/icheck/all.css">
	<!-- Theme CSS -->
	<link rel="stylesheet" href="<?php echo base_url() ?>assets_users/css/style.css">
	<!-- Color CSS -->
	<link rel="stylesheet" href="<?php echo base_url() ?>assets_users/css/themes.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>assets_users/css/plugins/datepicker/datepicker.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>assets_users/font-custom/font-custom.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>assets_users/css/btn-sandy.css">
	<!-- <link rel="stylesheet" href="<?php echo base_url() ?>assets_users/css/table_bootstrap4.css"> -->

	<!-- FANCY BOX -->
	<link rel="stylesheet" href="<?php echo base_url("assets_users/node_modules/@fancyapps/fancybox/dist/jquery.fancybox.css") ?>" />

	<!-- SELECT 2 -->
	<link href="<?php echo base_url("assets_users/bower_components/select2/dist/css/select2.min.css") ?>" rel="stylesheet" />

	<!-- jQuery -->
	<script src="<?php echo base_url() ?>assets_users/js/jquery.min.js"></script>

	<!-- Nice Scroll -->
	<script src="<?php echo base_url() ?>assets_users/js/plugins/nicescroll/jquery.nicescroll.min.js"></script>
	<!-- imagesLoaded -->
	<script src="<?php echo base_url() ?>assets_users/js/plugins/imagesLoaded/jquery.imagesloaded.min.js"></script>
	<!-- jQuery UI -->
	<script src="<?php echo base_url() ?>assets_users/js/plugins/jquery-ui/jquery.ui.core.min.js"></script>
	<script src="<?php echo base_url() ?>assets_users/js/plugins/jquery-ui/jquery.ui.widget.min.js"></script>
	<script src="<?php echo base_url() ?>assets_users/js/plugins/jquery-ui/jquery.ui.mouse.min.js"></script>
	<script src="<?php echo base_url() ?>assets_users/js/plugins/jquery-ui/jquery.ui.resizable.min.js"></script>
	<script src="<?php echo base_url() ?>assets_users/js/plugins/jquery-ui/jquery.ui.sortable.min.js"></script>
	<!-- slimScroll -->
	<script src="<?php echo base_url() ?>assets_users/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<!-- Bootstrap -->
	<script src="<?php echo base_url() ?>assets_users/js/bootstrap.min.js"></script>
	<!-- Bootbox -->
	<script src="<?php echo base_url() ?>assets_users/js/plugins/bootbox/jquery.bootbox.js"></script>
	<!-- Bootbox -->
	<script src="<?php echo base_url() ?>assets_users/js/plugins/form/jquery.form.min.js"></script>
	<!-- CKEditor -->
	<script src="<?php echo base_url() ?>assets_users/ckeditor/ckeditor.js"></script>




	<!-- Validation -->
	<script src="<?php echo base_url() ?>assets_users/js/plugins/validation/jquery.validate.min.js"></script>
	<script src="<?php echo base_url() ?>assets_users/js/plugins/validation/additional-methods.min.js"></script>
	<script src="<?php echo base_url() ?>assets_users/js/plugins/complexify/jquery.complexify-banlist.min.js"></script>
	<script src="<?php echo base_url() ?>assets_users/js/plugins/complexify/jquery.complexify.min.js"></script>
	<!-- icheck -->
	<script src="<?php echo base_url() ?>assets_users/js/plugins/icheck/jquery.icheck.min.js"></script>

	<!-- Datepicker -->
	<script src="<?php echo base_url(); ?>assets_users/js/plugins/datepicker/bootstrap-datepicker.js"></script>

	<!-- Theme framework -->
	<script src="<?php echo base_url() ?>assets_users/js/eakroko.js"></script>
	<!-- Theme scripts -->
	<script src="<?php echo base_url() ?>assets_users/js/application.min.js"></script>
	<!-- Just for demonstration -->
	<script src="<?php echo base_url() ?>assets_users/js/demonstration.min.js"></script>

	<!-- DATATABLES -->
	<link rel="stylesheet" href="<?php echo base_url() ?>assets_users/datatables/datatables.css">
	<script src="<?php echo base_url() ?>assets_users/datatables/datatables.js"></script>

	<!-- FANCY BOX -->
	<script src="<?php echo base_url("assets_users/node_modules/@fancyapps/fancybox/dist/jquery.fancybox.js") ?>"></script>

	<!-- SELECT 2 -->
	<script src="<?php echo base_url("assets_users/bower_components/select2/dist/js/select2.min.js") ?>"></script>

	<!--[if lte IE 9]>
		<script src="<?php echo base_url() ?>assets_users/js/plugins/placeholder/jquery.placeholder.min.js"></script>
		<script>
			$(document).ready(function() {
				$('input, textarea').placeholder();
			});
		</script>
	<![endif]-->
</head>



<body class="theme-dark-blue" data-theme="theme-dark-blue">
	<div>
		<a href="<?php echo site_url('tentang') ?>">
			<img src="<?php echo base_url() ?>assets_users/img/bg_header.jpg" />
		</a>
	</div>







	<div id="navigation">
		<div class="container-fluid">




			<ul class='main-nav'>





				

				<?php if (!$this->session->userdata('truly_madly_deeply')) { ?>
					<li class=''>
						<a href="<?php echo site_url(); ?>signon">
							<span>LOGIN</span>
						</a>
					</li>
				<?php } ?>

				<?php if ($this->session->userdata('truly_madly_deeply')) { ?>


					<?php
					echo menu_nav();
					?>

				<?php } ?>








			</ul>


			<?php if ($this->session->userdata('truly_madly_deeply')) { ?>


				<div class="user">
					<div class="dropdown">
						<a href="#" class='dropdown-toggle' data-toggle="dropdown">Selamat datang, <strong><?php echo $this->session->userdata('sesi_bo_nama_lengkap'); ?></strong> <img src="<?php echo base_url() ?>assets_users/img/default.png" alt=""></a>
						<ul class="dropdown-menu pull-right">

							<li>
								<a href="<?php echo site_url(); ?>editpass_admin/">Ubah Password</a>
							</li>
							<li>
								<a href="<?php echo site_url(); ?>signon/logout/">Logout</a>
							</li>
						</ul>

					</div>
				</div>

			<?php } else { ?>


				<div class="user">
					<div class="dropdown">
						<a href="#" class='dropdown-toggle' data-toggle="dropdown">
							<img src="<?php echo base_url() ?>assets_users/img/default.png" alt=""></a>
						<ul class="dropdown-menu pull-right">
							<li>
								<a href="<?php echo site_url(); ?>signon">Login</a>
							</li>
						</ul>

					</div>
				</div>

			<?php } ?>

		</div>
	</div>





	<div class="container-fluid nav-hidden" id="content">


		<div id="main" style="margin-left:0px; font-family: 'Roboto', Helvetica, Arial, sans-serif;">

			<?php
			ini_set('memory_limit', '512M');
			echo $contents;
			?>

		</div>

	</div>




</body>
<script type="text/javascript">
	$('.datepick').datepicker({
		format: 'yyyy-mm-dd'
	});
</script>

</html>