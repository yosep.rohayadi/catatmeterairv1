<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<!-- Apple devices fullscreen -->
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<!-- Apple devices fullscreen -->
	<meta names="apple-mobile-web-app-status-bar-style" content="black-translucent" />
	
	<title>Backoffice Registrasi Poltekkes</title>

	<!-- Bootstrap -->
	<link rel="stylesheet" href="<?php echo base_url()?>assets_users/css/bootstrap.min.css">
	<!-- Bootstrap responsive -->
	<link rel="stylesheet" href="<?php echo base_url()?>assets_users/css/bootstrap-responsive.min.css">
	<!-- jQuery UI -->
	<link rel="stylesheet" href="<?php echo base_url()?>assets_users/css/plugins/jquery-ui/smoothness/jquery-ui.css">
	<link rel="stylesheet" href="<?php echo base_url()?>assets_users/css/plugins/jquery-ui/smoothness/jquery.ui.theme.css">
	<!-- Theme CSS -->
	<link rel="stylesheet" href="<?php echo base_url()?>assets_users/css/style.css">
	<!-- Color CSS -->
	<link rel="stylesheet" href="<?php echo base_url()?>assets_users/css/themes.css">
	<link rel="stylesheet" href="<?php echo base_url()?>assets_users/css/btn-sandy.css">


	<!-- jQuery -->
	<script src="<?php echo base_url()?>assets_users/js/jquery.min.js"></script>
	
	<!-- Nice Scroll -->
	<script src="<?php echo base_url()?>assets_users/js/plugins/nicescroll/jquery.nicescroll.min.js"></script>
	<!-- imagesLoaded -->
	<script src="<?php echo base_url()?>assets_users/js/plugins/imagesLoaded/jquery.imagesloaded.min.js"></script>
	<!-- jQuery UI -->
	<script src="<?php echo base_url()?>assets_users/js/plugins/jquery-ui/jquery.ui.core.min.js"></script>
	<script src="<?php echo base_url()?>assets_users/js/plugins/jquery-ui/jquery.ui.widget.min.js"></script>
	<script src="<?php echo base_url()?>assets_users/js/plugins/jquery-ui/jquery.ui.mouse.min.js"></script>
	<script src="<?php echo base_url()?>assets_users/js/plugins/jquery-ui/jquery.ui.resizable.min.js"></script>
	<script src="<?php echo base_url()?>assets_users/js/plugins/jquery-ui/jquery.ui.sortable.min.js"></script>
	<!-- slimScroll -->
	<script src="<?php echo base_url()?>assets_users/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<!-- Bootstrap -->
	<script src="<?php echo base_url()?>assets_users/js/bootstrap.min.js"></script>
	<!-- Bootbox -->
	<script src="<?php echo base_url()?>assets_users/js/plugins/bootbox/jquery.bootbox.js"></script>
	<!-- Bootbox -->
	<script src="<?php echo base_url()?>assets_users/js/plugins/form/jquery.form.min.js"></script>
	<!-- Validation -->
	<script src="<?php echo base_url()?>assets_users/js/plugins/validation/jquery.validate.min.js"></script>
	<script src="<?php echo base_url()?>assets_users/js/plugins/validation/additional-methods.min.js"></script>
              <!-- CKEditor -->
	<script src="<?php echo base_url()?>assets_users/js/plugins/ckeditor/ckeditor.js"></script>
	<!-- Theme framework -->
	<script src="<?php echo base_url()?>assets_users/js/eakroko.min.js"></script>
	<!-- Theme scripts -->
	<script src="<?php echo base_url()?>assets_users/js/application.min.js"></script>
	<!-- Just for demonstration -->
	<script src="<?php echo base_url()?>assets_users/js/demonstration.min.js"></script>

		
 
	<!--[if lte IE 9]>
		<script src="<?php echo base_url()?>assets_users/js/plugins/placeholder/jquery.placeholder.min.js"></script>
		<script>
			$(document).ready(function() {
				$('input, textarea').placeholder();
			});
		</script>
	<![endif]-->
	
	<!-- Favicon -->
	<!--
	<link rel="shortcut icon" href="img/favicon.ico" />
	-->
	<!-- Apple devices Homescreen icon -->
	<link rel="apple-touch-icon-precomposed" href="img/apple-touch-icon-precomposed.png" />

</head>



<body class="theme-green" data-theme="theme-green">



<div align="left" style="position:relative"><img src="<?php echo base_url()?>assets_users/img/header_frontend.png">
<?php 
$logo = $this->db->where("id",$this->session->userdata("sesi_idpoltekkes_verifikator"))->get("ref_poltekes")->row();
?>
<?php if($logo): ?>
<img src="<?php echo base_url()?>logo/<?php echo $logo->logo;?>" style="width:80px;position:absolute;right:30px;top:5px">
<?php endif; ?>

</div>


	<div style="background-color:#336600; height:2px;">&nbsp;</div>
	


	
	
	
	<div id="navigation">
		<div class="container-fluid">
			
		<!--<a id="brand" href="#">SIPP</a>
		<a href="#" class="toggle-nav" rel="tooltip" data-placement="bottom" title="Toggle navigation"><i class="icon-reorder"></i></a>-->
		
		
		<ul class='main-nav'>
				<?php 
						//logot member
				if($this->session->userdata("atos_tiasa_leubeut_verifikator")): ?>
				

				

				<li class=''>
					<a href="<?php echo site_url(); ?>dashboard">
						<span>Dashboard</span>
					</a>
				</li>
				
				  <?php if($this->session->userdata("level_user")=="verifikator" && $this->session->userdata("sesi_tipe")==1): ?>
				  
				<li class=''>
					<a href="<?php echo site_url(); ?>verifikator_registrasi">
						<span>Data Registrasi</span>
					</a>
				</li>
				
				<li class=''>
					<a href="<?php echo site_url(); ?>verifikator_konfirmasi">
						<span>Data Konfirmasi Pembayaran</span>
					</a>
				</li>
				<li class=''>
					<a href="<?php echo site_url();?>terima_mahasiswa">Data Mahasiswa Diterima</a>
				</li> 
				<?php endif; ?>


			
				
				<li class=''>
					<?php if($this->session->userdata("sesi_tipe")!=1): ?>
					<a href="#" data-toggle="dropdown" class='dropdown-toggle'>
						<span>Setting</span>
						<span class="caret"></span>
					</a>
					<ul class="dropdown-menu">
                             <?php if($this->session->userdata("level_user")=="verifikator" && $this->session->userdata("sesi_tipe")==2): ?>
				 
                                            <li>
							<a href="<?php echo site_url(); ?>ref_bank">Bank</a>
						</li>
						   <li>
							<a href="<?php echo site_url(); ?>user_poltekes">Pengguna Poltekes</a>
						</li>
						
                                                <li>
							<a href="<?php echo site_url(); ?>ref_jurusan">Jurusan</a>
						</li>

						              <li>
							<a href="<?php echo site_url("ref_poltekes/robih/".$this->session->userdata('sesi_idpoltekkes_verifikator')); ?>">Edit Poltekkes</a>
						</li>

						         <li>
							<a href="<?php echo site_url(); ?>ref_pengumuman">Pengumuman Poltekkes</a>
						</li>

						<li>
							<a href="<?php echo site_url(); ?>ref_materi_uji">Materi Uji</a>
						</li>
                                                <li>
							<a href="<?php echo site_url(); ?>ref_lokasi">Lokasi Ujian</a>
						</li>
						<?php endif; ?>
                         <?php if($this->session->userdata("level_user")=="admin"): ?>
                                         <li>
							<a href="<?php echo site_url(); ?>ref_poltekes">Poltekkes</a>
						</li>
                                                <li>
							<a href="<?php echo site_url(); ?>ref_tahun_ajaran">Tahun Ajaran</a>
						</li>
						<li>
							<a href="<?php echo site_url(); ?>ref_pegawai_poltekkes">User Poltekkes</a>
						</li>
						
						
						
						<?php endif; ?>
                                                
					</ul>
				</li>
			<?php endif; ?>
			 <?php if($this->session->userdata("level_user")=="verifikator" && $this->session->userdata("sesi_tipe")==2): ?>
				 
				<li><a href="<?php echo site_url(); ?>pendaftaran">Info Pendaftaran</a></li>
			<?php endif; ?>

				<?php if($this->session->userdata("level_user")=="admin"): ?>
                         
				<li><a href="<?php echo site_url();?>edit_petunjuk">Petunjuk Pendaftaran</a></li>
			     <?php endif; ?>
		

				
			</ul>
		
		
			
			<div class="user">
				
				<ul class="icon-nav"> </ul>
				
				
				<div class="dropdown">
					<a href="#" class='dropdown-toggle' data-toggle="dropdown">Selamat datang, <strong><?php echo $this->session->userdata('sesi_nama_lengkap_verifikator'); ?></strong> <img src="<?php echo base_url()?>assets_users/img/default.png" alt=""></a>
					<ul class="dropdown-menu pull-right">
					
					
						<?php 
						//logot member
						if($this->session->userdata("level_user")=="verifikator"): ?>
							<li>
							<a href="<?php echo site_url();?>verifikator/ubahprofil/">Ubah Profil</a>
						</li>
						<li>
							<a href="<?php echo site_url();?>verifikator/gantipassword/">Ubah Password</a>
						</li>
						<li>
							<a href="<?php echo site_url();?>verifikator/logout/">Logout</a>
						</li>
							<?php endif;?>
				<?php 
						//logot member
						if($this->session->userdata("level_user")=="admin"): ?>
					
						<li>
							<a href="<?php echo site_url();?>admin/ubahprofil/">Ubah Profil</a>
						</li>
					<li>
							<a href="<?php echo site_url();?>admin/gantipassword/">Ubah Password</a>
						</li>
						<li>
							<a href="<?php echo site_url();?>admin/logout/">Logout</a>
						</li>
				<?php endif;?>

					</ul>
					
				</div>
			
				
				
			</div>
				<?php endif; ?>
		</div>
	</div>
	
	
	
	
	
	
	<!--<div class="container-fluid nav-hidden" id="content">
		
		<div id="row-fluid"></div> 
		
		<div id="main" style="margin-left:0px;">
			<div class="container-fluid">
				
				
				
				
				
				
				init
				
				
			</div>
		</div>
		</div>-->
		
<div  class="container-fluid nav-hidden" id="content">
		
				
				<div id="main" style="margin-left:0px;">
				
				<?php
					ini_set('memory_limit', '512M');
					echo $contents;
				?>
				
				</div>
			
		</div>
		
		 
		
	</body>

	</html>