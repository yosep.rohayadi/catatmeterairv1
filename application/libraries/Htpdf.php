<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once APPPATH.'third_party/htmltopdf/src/Pdf.php';
use mikehaertl\wkhtmlto\Pdf;

class Htpdf extends Pdf
{
    function __construct()
    {
        parent::__construct();
    }
}

/* End of file Pdf.php */
/* Location: ./application/libraries/Pdf.php */