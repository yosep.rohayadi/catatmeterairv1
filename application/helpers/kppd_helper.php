<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

	/*
	Helper ini bertujuan menghitung nilai kppd final untuk penentuan status kinerja pemerintah daerah.
	By Nurul Aziza 
	30 Juni 2021
	*/

	function rangkum_kppdf_prov($klasifikasi) {
		$CI    =& get_instance();
		$tahun = date("Y")-1;
		$table = "trx_peringkat_provinsi_".$tahun;

		$sql_met = "SELECT * FROM trx_ikk_metodologi_pemeringkatan WHERE klasifikasi = '".$klasifikasi."'";
		$qry_met = $CI->db->query($sql_met);
		$row_met = $qry_met->row();
		
		$sql    = "SELECT kode_provinsi,kppd FROM $table WHERE klasifikasi = '".$klasifikasi."'";
		$query  = $CI->db->query($sql);
		$result = $query->result();

		foreach($result as $v){
			$kode_provinsi = $v->kode_provinsi;
			$kppd = $v->kppd;
			
			if($row_met->id_metodologi == '1') {
				$kppd_max_prov = $row_met->max_prov;
				$kppdf_prov = ($kppd_max_prov > 0) ? ($kppd/$kppd_max_prov)*5 : 0;

			} else if($row_met->id_metodologi == '7') {
				$v1_1 = $row_met->eva_1_1_prov; // sangat baik
				$v1_2 = $row_met->eva_1_2_prov;
				$v2_1 = $row_met->eva_2_1_prov; // baik
				$v2_2 = $row_met->eva_2_2_prov;
				$v3_1 = $row_met->eva_3_1_prov; // cukup
				$v3_2 = $row_met->eva_3_2_prov;
				$v4_1 = $row_met->eva_4_1_prov; // buruk
				$v4_2 = $row_met->eva_4_2_prov;
				$v5_1 = $row_met->eva_5_1_prov; // sangat buruk
				$v5_2 = $row_met->eva_5_2_prov;

				if ($kppd >= $v5_1 && $kppd <= $v5_2) {
					$kppdf_prov = 1;
				} else if ($kppd > $v4_1 && $kppd <= $v4_2) {
					$kppdf_prov = 2;
				} else if ($kppd > $v3_1 && $kppd <= $v3_2) {
					$kppdf_prov = 3;
				} else if ($kppd > $v2_1 && $kppd <= $v2_2) {
					$kppdf_prov = 4;
				} else if ($kppd > $v1_1 && $kppd <= $v1_2) {
					$kppdf_prov = 5;
				} else {
					$kppdf_prov = 0;
				}
			}

			$sqlup="update $table set kppd_final_metodologi='".$kppdf_prov."' where kode_provinsi='".$kode_provinsi."' ";
			$CI->db->query($sqlup);

		}
	}

	function rangkum_kppdf_kota($klasifikasi) {
		$CI    = & get_instance();
		$tahun = date("Y")-1;
		$tabel = "trx_peringkat_kabkota_" . $tahun;
		$flag  = 'kota'; 

		$sql_met = "SELECT * FROM trx_ikk_metodologi_pemeringkatan WHERE klasifikasi = '".$klasifikasi."'";
		$qry_met = $CI->db->query($sql_met);
		$row_met = $qry_met->row();
		
		$sql    = "SELECT kode_provinsi,kode_kabkot,kppd FROM $tabel WHERE flagkabkot='".$flag."' and  klasifikasi='".$klasifikasi."'";
		$query  = $CI->db->query($sql);
		$result = $query->result();

		foreach($result as $v){
			$kode_provinsi = $v->kode_provinsi;
			$kode_kabkot   = $v->kode_kabkot;
			$kppd          = $v->kppd;

			if($row_met->id_metodologi == '1') {
				$kppd_max = $row_met->max_kota;
				$kppdf    = ($kppd_max > 0) ? ($kppd/$kppd_max)*5 : 0;

			} else if($row_met->id_metodologi == '7') {
				$v1_1 = $row_met->eva_1_1_kota; // sangat baik
				$v1_2 = $row_met->eva_1_2_kota;
				$v2_1 = $row_met->eva_2_1_kota; // baik
				$v2_2 = $row_met->eva_2_2_kota;
				$v3_1 = $row_met->eva_3_1_kota; // cukup
				$v3_2 = $row_met->eva_3_2_kota;
				$v4_1 = $row_met->eva_4_1_kota; // buruk
				$v4_2 = $row_met->eva_4_2_kota;
				$v5_1 = $row_met->eva_5_1_kota; // sangat buruk
				$v5_2 = $row_met->eva_5_2_kota;

				if ($kppd >= $v5_1 && $kppd <= $v5_2) {
					$kppdf = 1;
				} else if ($kppd > $v4_1 && $kppd <= $v4_2) {
					$kppdf = 2;
				} else if ($kppd > $v3_1 && $kppd <= $v3_2) {
					$kppdf = 3;
				} else if ($kppd > $v2_1 && $kppd <= $v2_2) {
					$kppdf = 4;
				} else if ($kppd > $v1_1 && $kppd <= $v1_2) {
					$kppdf = 5;
				} else {
					$kppdf = 0;
				}
			}

			// $kppdf = NULL;
			$sqlup="update $tabel set kppd_final_metodologi='".$kppdf."' where kode_provinsi='".$kode_provinsi."' and kode_kabkot='".$kode_kabkot."'";
			$CI->db->query($sqlup);

		}
	}

	function rangkum_kppdf_kab($klasifikasi) {
		$CI    = & get_instance();
		$tahun = date("Y")-1;
		$tabel = "trx_peringkat_kabkota_" . $tahun;
		$flag  = 'kab'; 

		$sql_met = "SELECT * FROM trx_ikk_metodologi_pemeringkatan WHERE klasifikasi = '".$klasifikasi."'";
		$qry_met = $CI->db->query($sql_met);
		$row_met = $qry_met->row();
		
		$sql    = "SELECT kode_provinsi,kode_kabkot,kppd FROM $tabel WHERE flagkabkot='".$flag."' and  klasifikasi='".$klasifikasi."'";
		$query  = $CI->db->query($sql);
		$result = $query->result();

		foreach($result as $v){
			$kode_provinsi = $v->kode_provinsi;
			$kode_kabkot   = $v->kode_kabkot;
			$kppd          = $v->kppd;

			if($row_met->id_metodologi == '1') {
				$kppd_max = $row_met->max_kab;
				$kppdf    = ($kppd_max > 0) ? ($kppd/$kppd_max)*5 : 0;

			} else if($row_met->id_metodologi == '7') {
				$v1_1 = $row_met->eva_1_1_kab; // sangat baik
				$v1_2 = $row_met->eva_1_2_kab;
				$v2_1 = $row_met->eva_2_1_kab; // baik
				$v2_2 = $row_met->eva_2_2_kab;
				$v3_1 = $row_met->eva_3_1_kab; // cukup
				$v3_2 = $row_met->eva_3_2_kab;
				$v4_1 = $row_met->eva_4_1_kab; // buruk
				$v4_2 = $row_met->eva_4_2_kab;
				$v5_1 = $row_met->eva_5_1_kab; // sangat buruk
				$v5_2 = $row_met->eva_5_2_kab;

				if ($kppd >= $v5_1 && $kppd <= $v5_2) {
					$kppdf = 1;
				} else if ($kppd > $v4_1 && $kppd <= $v4_2) {
					$kppdf = 2;
				} else if ($kppd > $v3_1 && $kppd <= $v3_2) {
					$kppdf = 3;
				} else if ($kppd > $v2_1 && $kppd <= $v2_2) {
					$kppdf = 4;
				} else if ($kppd > $v1_1 && $kppd <= $v1_2) {
					$kppdf = 5;
				} else {
					$kppdf = 0;
				}
			}

			// $kppdf = NULL;
			$sqlup="update $tabel set kppd_final_metodologi='".$kppdf."' where kode_provinsi='".$kode_provinsi."' and kode_kabkot='".$kode_kabkot."'";
			$CI->db->query($sqlup);

		}
	}


