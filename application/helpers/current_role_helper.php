<?php

function isAdminProvinsi()
{
	$CI = &get_instance();
	if ($CI->session->userdata("sesi_bo_user_group") == "10") {
		return true;
	}
	return false;
}


function isAdminKabkot()
{
	$CI = &get_instance();
	if ($CI->session->userdata("sesi_bo_user_group") == "5") {
		return true;
	}
	return false;
}



function isAdminKecamatan()
{
	$CI = &get_instance();
	if ($CI->session->userdata("sesi_bo_user_group") == "6") {
		return true;
	}
	return false;
}
