<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

	function eva_ckup($data){
		$hasil    = 0;
		$cp_bobot =  0;

		if (!empty($data)) {
			$jenis   = $data['jenis'];
			$max     = $data['max'];
			$min     = $data['min'];
			$capaian = $data['capaian'];
			$langkah = $data['langkah'];
			$bobot   = $data['bobot'];
			
			$v1_1 = $data['v1_1']; // sangat baik
			$v1_2 = $data['v1_2'];
			$v2_1 = $data['v2_1']; // baik
			$v2_2 = $data['v2_2'];
			$v3_1 = $data['v3_1']; // cukup
			$v3_2 = $data['v3_2'];
			$v4_1 = $data['v4_1']; // buruk
			$v4_2 = $data['v4_2'];
			$v5_1 = $data['v5_1']; // sangat buruk
			$v5_2 = $data['v5_2'];

			$hasil   = 0;

			if ($jenis == 1) {
				if($max != 0) {
					$nilai = ($capaian / $max) * 5;
					$hasil = ($nilai > 5 || $nilai < 0) ? 0 : $nilai;
				} else {
					$hasil = 0;
				}
			} else if ($jenis == 2) {
				if($capaian != 0){
					$nilai = ($min / $capaian) * 5;
					$hasil = ($nilai > 5 || $nilai < 0) ? 0 : $nilai;
				} else {
					$hasil = 0;
				}
				
			} else if ($jenis == 3 || $jenis == 5) {
				$max_range = 0;
				$min_range = 0;
				$i = 0;
				$hasil   = 0;

				if ($capaian <= $min) {
					$hasil = 1;
				} else if ($capaian >= $max) {
					$hasil = 5;
				} else {
					for ($min_range = $min; $min_range <= $max; $min_range += $langkah) {
						$max_range = $min_range + $langkah;
						$i++;

						if ($capaian > $min_range && $capaian <= $max_range) {
							$hasil = $i;
						}
					}
				}
			} else if ($jenis == 4 || $jenis == 6) {
				$max_range = 0;
				$min_range = 0;
				$i = 6;
				$hasil   = 0;

				if ($capaian <= $min) {
					$hasil = 5;
				} else if ($capaian >= $max) {
					$hasil = 1;
				} else {
					for ($min_range = $min; $min_range <= $max; $min_range += $langkah) {
						$max_range = $min_range + $langkah;
						$i--;

						if ($capaian > $min_range && $capaian <= $max_range) {
							$hasil = $i;
						}
					}
				}
			} else if ($jenis == 7) {
				if ($capaian >= $v5_1 && $capaian <= $v5_2) {
					$hasil = 1;
				} else if ($capaian > $v4_1 && $capaian <= $v4_2) {
					$hasil = 2;
				} else if ($capaian > $v3_1 && $capaian <= $v3_2) {
					$hasil = 3;
				} else if ($capaian > $v2_1 && $capaian <= $v2_2) {
					$hasil = 4;
				} else if ($capaian > $v1_1 && $capaian <= $v1_2) {
					$hasil = 5;
				}
			} else if ($jenis == 8) {
				if ($capaian >= $v5_1 && $capaian <= $v5_2) {
					$hasil = 5;
				} else if ($capaian > $v4_1 && $capaian <= $v4_2) {
					$hasil = 4;
				} else if ($capaian > $v3_1 && $capaian <= $v3_2) {
					$hasil = 3;
				} else if ($capaian > $v2_1 && $capaian <= $v2_2) {
					$hasil = 2;
				} else if ($capaian > $v1_1 && $capaian <= $v1_2) {
					$hasil = 1;
				}
			}

			$cp_bobot = ($hasil * $bobot) / 100;
		} 

		$arr_hasil = array(
			'hasil' => $hasil,
			'bobot' => $cp_bobot,
		);
		return $arr_hasil;
	}

?>
