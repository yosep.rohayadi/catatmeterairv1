<?php if (!defined('BASEPATH')) exit('No direct script access allowed');





	function menu_nav2(){
		$CI =& get_instance();
		$menunav = '';
		$user_id = $CI->session->userdata('sesi_bo_id');
		$user_id_group = info_user($user_id)->id_user_group;
		$sql = "SELECT ref_menu.id_menu, nama_menu, link
				FROM ref_menu
				LEFT JOIN ref_group_menu ON ref_group_menu.id_menu = ref_menu.id_menu
				WHERE id_user_group = '".$user_id_group."' AND parrent = 0 ORDER BY urutan asc";
		$query = $CI->db->query($sql);
		
		//echo $sql;
		
		$i=0;
		foreach($query->result_array() as $row)
		{
			if(toogle($row['id_menu'],$user_id_group) > 0){
				$menunav .= "<li>";
				$menunav .= '<a href=#>
								<span>'.$row['nama_menu'].'</span>
								<b class="caret"></b>
							</a>';
				$menunav .=	formatTree2($row['id_menu'],$user_id_group);
				$menunav .= "</li>";
			}else{
				$menunav .= "<li>";
				$menunav .= '<a href="'.site_url($row['link']).'">
								<span>'.$row['nama_menu'].'</span>
							</a>';
				$menunav .= "</li>";
			}
			$i++;
		}

		echo $menunav;
	}

	function formatTree2($id_parent,$user_id_group){
		$CI =& get_instance();

		$sql = "SELECT ref_menu.id_menu, nama_menu, link
				FROM ref_menu
				LEFT JOIN ref_group_menu ON ref_group_menu.id_menu = ref_menu.id_menu
				WHERE id_user_group = '".$user_id_group."' AND parrent = '".$id_parent."' ORDER BY urutan asc";


		$query = $CI->db->query($sql);
		$menunav = "<ul class='dropdown-menu'>";
        foreach($query->result_array() as $item){
			if(toogle2($item['id_menu'],$user_id_group) > 0){
				$menunav .= "<li class='dropdown-submenu'>";
				$menunav .= '<a href="#">'.$item['nama_menu'].'</a>';
				$menunav.= formatTree2($item['id_menu'],$user_id_group);
				$menunav.= "</li>";

			}else{
				$menunav .= "<li>";
				$menunav .= '<a href="'.site_url($item['link']).'">'.$item['nama_menu'].'</a>';
				$menunav.= "</li>";
			}
        }


      $menunav.= "</ul>";
	  return $menunav;
    }

	function toogle2($id_parent,$user_id_group){
		$CI =& get_instance();
		$sql = "SELECT ref_menu.id_menu, nama_menu, link
				FROM ref_menu
				LEFT JOIN ref_group_menu ON ref_group_menu.id_menu = ref_menu.id_menu
				WHERE id_user_group = '".$user_id_group."' AND parrent = '".$id_parent."' ORDER BY urutan asc";
		$query = $CI->db->query($sql);
		return $query->num_rows();
    }

	


?>
