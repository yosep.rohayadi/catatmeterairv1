<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/* Menghitung rekap per-urusan sekaligus bobot final
 * Yosep Joe
 * 14/01/2021
 * 
 */
	 function rangkumprov($id_prov)
	{  
		$CI =& get_instance();
	
		
		
		$tahun=date("Y")-1;
		
		$sqldel="delete from trx_ikk_rangkuman_$tahun where id_prov='$id_prov' AND (id_kabkot IS NULL OR id_kabkot='')";
		$CI->db->query($sqldel);
		
		$sqlins="INSERT trx_ikk_rangkuman_$tahun (id_kategori_urusan,kategori_urusan,id_urusan,urusan,bobot_urusan,id_prov,flag) 
SELECT id_kategori_urusan,kategori_urusan,id_urusan,urusan,bobot_urusan,$id_prov,'prov' FROM master_ikk_provinsi_bobot_urusan";
		$CI->db->query($sqlins);
		
		
		$sqlsec=" select * from trx_ikk_rangkuman_$tahun where id_prov='$id_prov' AND (id_kabkot IS NULL OR id_kabkot='') order by id_urusan asc";
		$query = $CI->db->query($sqlsec);

		foreach ($query->result() as $row)
		{
				$bobot_urusan=$row->bobot_urusan;
				$id_urusan=$row->id_urusan;
				$id=$row->id;
				
				//eva_hasil_bobot_capaian
				$sqlsum="SELECT SUM(eva_hasil_capaian) AS sum_urusan FROM trx_ikk_$tahun WHERE id_urusan='$id_urusan' and id_prov='$id_prov' AND (id_kabkot IS NULL OR id_kabkot='')";
				$query = $CI->db->query($sqlsum);
				$row = $query->row();

				if (isset($row)) { 	$bobot_capaian_urusan=$row->sum_urusan; } else { 	$bobot_capaian_urusan=0; }
				
				//echo $sqlsum."<br>";
				
				$gt=($bobot_capaian_urusan*$bobot_urusan)/100;
				
				$sqlupd="update trx_ikk_rangkuman_$tahun set bobot_capaian_urusan='$bobot_capaian_urusan',bobot_capaian_urusan_final='$gt' where id_urusan='$id_urusan' and id_prov='$id_prov' AND (id_kabkot IS NULL OR id_kabkot='')";
				$CI->db->query($sqlupd);
				
				//echo $sqlupd."<br>";
				
				
				
	
	
		}
		
		
		$sqlsum1="SELECT SUM(bobot_capaian_urusan) AS ckup FROM trx_ikk_rangkuman_$tahun WHERE id_kategori_urusan <> 4 AND id_prov='$id_prov' ";
				//echo $sqlsum1;
				$query1 = $CI->db->query($sqlsum1);
				$row1 = $query1->row();

				if (isset($row1)) { 	$ckup=$row1->ckup; } else { 	$ckup=0; }
				$sqlpk1="update trx_peringkat_provinsi_$tahun set ckup='$ckup' where kode_provinsi='$id_prov'  ";
				$CI->db->query($sqlpk1);
				
				//echo $sqlpk1."<br>";
				
				$sqlsum2="SELECT SUM(bobot_capaian_urusan) AS ckup_penunjang FROM trx_ikk_rangkuman_$tahun WHERE id_kategori_urusan = 4 AND id_prov='$id_prov' ";
				$query2 = $CI->db->query($sqlsum2);
				$row2 = $query2->row();

				if (isset($row2)) { 	$ckup_penunjang=$row2->ckup_penunjang; } else { 	$ckup_penunjang=0; }				
				$sqlpk2="update trx_peringkat_provinsi_$tahun set ckup_penunjang='$ckup_penunjang' where kode_provinsi='$id_prov'  ";
				$CI->db->query($sqlpk2);
				
				//echo $sqlpk2."<br>";
		
		return true;
	
	}
	
	
	
	
		 function rangkumkabkot($id_prov,$id_kabkot)
	{  
	
		
		// delete insert
		$CI =& get_instance();
		$tahun=date("Y")-1;
		
		$sqldel="delete from trx_ikk_rangkuman_$tahun where id_prov='$id_prov' AND id_kabkot='$id_kabkot'";
		$CI->db->query($sqldel);
		
		$sqlins="INSERT trx_ikk_rangkuman_$tahun (id_kategori_urusan,kategori_urusan,id_urusan,urusan,bobot_urusan,id_prov,id_kabkot,flag) 
SELECT id_kategori_urusan,kategori_urusan,id_urusan,urusan,bobot_urusan,$id_prov,$id_kabkot,'kabkot' FROM master_ikk_kabkot_bobot_urusan";
		$CI->db->query($sqlins);
		
		
		$sqlsec=" select * from trx_ikk_rangkuman_$tahun where id_prov='$id_prov' AND id_kabkot='$id_kabkot' order by id_urusan asc";
		$query = $CI->db->query($sqlsec);

		foreach ($query->result() as $row)
		{
				$bobot_urusan=$row->bobot_urusan;
				$id_urusan=$row->id_urusan;
				$id=$row->id;
				
				//eva_hasil_bobot_capaian
				$sqlsum="SELECT SUM(eva_hasil_capaian) AS sum_urusan FROM trx_ikk_$tahun WHERE id_urusan='$id_urusan' and id_prov='$id_prov' AND id_kabkot='$id_kabkot'";
				$query = $CI->db->query($sqlsum);
				$row = $query->row();

				if (isset($row)) { 	$bobot_capaian_urusan=$row->sum_urusan; } else { 	$bobot_capaian_urusan=0; }
				
				//echo $sqlsum."<br>";
				
				$gt=($bobot_capaian_urusan*$bobot_urusan)/100;
				
				$sqlupd="update trx_ikk_rangkuman_$tahun set bobot_capaian_urusan='$bobot_capaian_urusan',bobot_capaian_urusan_final='$gt' where id_urusan='$id_urusan' and id_prov='$id_prov' AND id_kabkot='$id_kabkot'";
				$CI->db->query($sqlupd);
				//echo $sqlupd."<br>";
				
				
				
				
				
				
		
	
	
		}
		
		
		$sqlsum1="SELECT SUM(bobot_capaian_urusan) AS ckup FROM trx_ikk_rangkuman_$tahun WHERE id_kategori_urusan <> 4 AND id_prov='$id_prov' AND id_kabkot='$id_kabkot'";
				//echo $sqlsum1;
				$query1 = $CI->db->query($sqlsum1);
				$row1 = $query1->row();

				if (isset($row1)) { 	$ckup=$row1->ckup; } else { 	$ckup=0; }
				$sqlpk1="update trx_peringkat_kabkota_$tahun set ckup='$ckup' where kode_provinsi='$id_prov'  AND kode_kabkot='$id_kabkot'";
				$CI->db->query($sqlpk1);
				
				
				$sqlsum2="SELECT SUM(bobot_capaian_urusan) AS ckup_penunjang FROM trx_ikk_rangkuman_$tahun WHERE id_kategori_urusan = 4 AND id_prov='$id_prov' AND id_kabkot='$id_kabkot'";
				$query2 = $CI->db->query($sqlsum2);
				$row2 = $query2->row();

				if (isset($row2)) { 	$ckup_penunjang=$row2->ckup_penunjang; } else { 	$ckup_penunjang=0; }				
				$sqlpk2="update trx_peringkat_kabkota_$tahun set ckup_penunjang='$ckup_penunjang' where kode_provinsi='$id_prov' AND kode_kabkot='$id_kabkot' ";
				$CI->db->query($sqlpk2);
				
				//echo $sqlpk1."<br>";
				//echo $sqlpk2."<br>";
		
		return true;
	
	}
	
	
	
	
	
	
		 function rangkumprov_makro($id_prov)
	{  
		$CI =& get_instance();
	
		
		
		$tahun=date("Y")-1;
		
		
				
				$sqlsum="SELECT SUM(hasil_metodologi_nilai) AS bobot_ckm FROM ref_ikk_makro_$tahun WHERE  id_prov='$id_prov' AND (id_kabkot IS NULL OR id_kabkot='')";
				$query = $CI->db->query($sqlsum);
				$row = $query->row();

				if (isset($row)) { 	$bobot_ckm=$row->bobot_ckm; } else { 	$bobot_ckm=0; }
				
				$sqlupd="update trx_peringkat_provinsi_$tahun set ckm='$bobot_ckm' where  kode_provinsi='$id_prov' ";
				$CI->db->query($sqlupd);
				
				
				$sqlsum="SELECT SUM(hasil_metodologi_laju) AS bobot_laju FROM ref_ikk_makro_$tahun WHERE  id_prov='$id_prov' AND (id_kabkot IS NULL OR id_kabkot='')";
				$query = $CI->db->query($sqlsum);
				$row = $query->row();

				if (isset($row)) { 	$bobot_laju=$row->bobot_laju; } else { 	$bobot_laju=0; }
				
				$sqlupd="update trx_peringkat_provinsi_$tahun set pkm='$bobot_laju' where  kode_provinsi='$id_prov' ";
				$CI->db->query($sqlupd);
	
	
		
		
		return true;
	
	}
	
	
	
		 function rangkumkabkot_makro($id_prov,$id_kabkot)
	{  
		$CI =& get_instance();
	
		
		
		$tahun=date("Y")-1;
		
		
				
				
				$sqlsum="SELECT SUM(hasil_metodologi_nilai) AS bobot_ckm FROM ref_ikk_makro_$tahun WHERE  id_prov='$id_prov' and  id_kabkot='$id_kabkot'";
				$query = $CI->db->query($sqlsum);
				$row = $query->row();

				if (isset($row)) { 	$bobot_ckm=$row->bobot_ckm; } else { 	$bobot_ckm=0; }
				
				$sqlupd="update trx_peringkat_kabkota_$tahun set ckm='$bobot_ckm' where  kode_provinsi='$id_prov' and  kode_kabkot='$id_kabkot'";
				$CI->db->query($sqlupd);
				
				
				
				$sqlsum="SELECT SUM(hasil_metodologi_laju) AS bobot_laju FROM ref_ikk_makro_$tahun WHERE  id_prov='$id_prov' and  id_kabkot='$id_kabkot'";
				$query = $CI->db->query($sqlsum);
				$row = $query->row();

				if (isset($row)) { 	$bobot_laju=$row->bobot_laju; } else { 	$bobot_laju=0; }
				
				$sqlupd="update trx_peringkat_kabkota_$tahun set pkm='$bobot_laju' where  kode_provinsi='$id_prov' and  kode_kabkot='$id_kabkot' ";
				$CI->db->query($sqlupd);
				
				//echo $sqlsum."<br>".$sqlupd;
	
	
		
		
		return true;
	
	}
	
	
	
	
	
	function rangkum_peringkat_prov($klasifikasi)
	{  
		$CI =& get_instance();
		$tahun=date("Y")-1;
		
		$sqlprov="select * from trx_peringkat_provinsi_$tahun where klasifikasi='$klasifikasi' ";
		$query = $CI->db->query($sqlprov);
		$list_prov=$query->result();
		
		foreach ($list_prov as $row)
		{
			
			$kode_provinsi=$row->kode_provinsi; 
			$kppd=($row->ckm*0.05+$row->ckup*0.85+$row->ckup_penunjang*0.1)*0.75+($row->pkm*0.25);
			
			
			
			$sqlupd="update trx_peringkat_provinsi_$tahun set kppd='$kppd' where kode_provinsi='$kode_provinsi'";
			$CI->db->query($sqlupd);
			
			
			
			
			// $sqlsum="SELECT MAX(kppd) AS kppd_max FROM trx_peringkat_provinsi_$tahun where klasifikasi='$klasifikasi'";
			// $query = $CI->db->query($sqlsum);
			// $row = $query->row();

			// if (isset($row)) { 	$kppd_max=$row->kppd_max; } else { 	$kppd_max=0; }
			
			
			// //nilai kppd  di bagi nilai max kppd dikali * 5
			// $kppd_final_metodologi=($kppd/$kppd_max)*5;
			
			// $sqlup="update trx_peringkat_provinsi_$tahun set kppd_final_metodologi='$kppd_final_metodologi' where kode_provinsi='$kode_provinsi' ";
			// $CI->db->query($sqlup);
			
			
			
		}
		
		
		
	}
	
	
	
	
	function rangkum_peringkat_kota($klasifikasi)
	{  
		$CI =& get_instance();
		$tahun=date("Y")-1;
		
		$sql="select * from trx_peringkat_kabkota_$tahun where flagkabkot='kota' and  klasifikasi='$klasifikasi'";
		$query = $CI->db->query($sql);
		$list=$query->result();
		
		foreach ($list as $row)
		{
			
			$kode_provinsi=$row->kode_provinsi; 
			$kode_kabkot=$row->kode_kabkot; 
			
			$kppd=($row->ckm*0.05+$row->ckup*0.85+$row->ckup_penunjang*0.1)*0.75+($row->pkm*0.25);
			
			$sqlupd="update trx_peringkat_kabkota_$tahun set kppd='$kppd' where kode_provinsi='$kode_provinsi' and kode_kabkot='$kode_kabkot'";
	
			$CI->db->query($sqlupd);
			
			
			
			// $sqlsum="SELECT MAX(kppd) AS kppd_max FROM trx_peringkat_kabkota_$tahun where flagkabkot='kota' and  klasifikasi='$klasifikasi'";
			// $query = $CI->db->query($sqlsum);
			// $row = $query->row();

			// if (isset($row)) { 	$kppd_max=$row->kppd_max; } else { 	$kppd_max=0; }
			
			
			// //nilai kppd  di bagi nilai max kppd dikali * 5
			// $kppd_final_metodologi=($kppd/$kppd_max)*5;
			
			// $sqlup="update trx_peringkat_kabkota_$tahun set kppd_final_metodologi='$kppd_final_metodologi' where kode_provinsi='$kode_provinsi' and kode_kabkot='$kode_kabkot'";
			// $CI->db->query($sqlup);
			
			
			
		}
		
		
		
	}
	
	
	
	function rangkum_peringkat_kab($klasifikasi)
	{  
		$CI =& get_instance();
		$tahun=date("Y")-1;
		
		$sql="select * from trx_peringkat_kabkota_$tahun where flagkabkot='kab' and  klasifikasi='$klasifikasi'";
		$query = $CI->db->query($sql);
		$list=$query->result();
		
		foreach ($list as $row)
		{
			
			$kode_provinsi=$row->kode_provinsi; 
			$kode_kabkot=$row->kode_kabkot; 
			
			$kppd=($row->ckm*0.05+$row->ckup*0.85+$row->ckup_penunjang*0.1)*0.75+($row->pkm*0.25);
			
			$sqlupd="update trx_peringkat_kabkota_$tahun set kppd='$kppd' where kode_provinsi='$kode_provinsi' and kode_kabkot='$kode_kabkot'";
	
			$CI->db->query($sqlupd);
			
			
			
			// $sqlsum="SELECT MAX(kppd) AS kppd_max FROM trx_peringkat_kabkota_$tahun where flagkabkot='kab' and  klasifikasi='$klasifikasi'";
			// $query = $CI->db->query($sqlsum);
			// $row = $query->row();

			// if (isset($row)) { 	$kppd_max=$row->kppd_max; } else { 	$kppd_max=0; }
			
			
			// //nilai kppd  di bagi nilai max kppd dikali * 5
			// $kppd_final_metodologi=($kppd/$kppd_max)*5;
			
			// $sqlup="update trx_peringkat_kabkota_$tahun set kppd_final_metodologi='$kppd_final_metodologi' where kode_provinsi='$kode_provinsi' and kode_kabkot='$kode_kabkot'";
			// $CI->db->query($sqlup);
			
			
		}
		
		
		
	}

?>
