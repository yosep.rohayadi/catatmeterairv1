<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/* Manggu Framework
 * Simple PHP Application Development
 * Kusnassriyanto S. Bahri
 * kusnassriyanto@gmail.com
 * 
 */

function getArrayDef($array, $index, $default=null){
    if (isset($array[$index])){
        return $array[$index];
    } else {
        return $default;
    }
}


function getKode($awal,$select,$from)
{
	$CI =& get_instance();
	$sql = "select ".$select." as kode from ".$from;
	$query = $CI->db->query($sql);
	$thn = substr(date('Y'),2,2);
	$bln = date('m');
	$tgl = date('d');
	if((int)$tgl==1){
		$hasil=$awal.$thn.$bln.'00001';
	}else{
		if($query->num_rows() > 0){
			$field = $query->row();
			$inc = (int)substr($field->kode,-5,5);
			$inc = $inc + 1;
			$hasil = $awal.$thn.$bln. sprintf("%05s", $inc);
		}else{
			$hasil=$awal.$thn.$bln.'00001';
		}
	}
	
	return $hasil;
}


function getProv($id_provinsi){
		$CI =& get_instance();
		$str = $CI->db->query("SELECT * FROM ref_wil_provinsi WHERE id_provinsi='$id_provinsi'")->row()->nama;

		return $str; 
	}


	function getKabkot($id_provinsi, $id_kabkot){
		$CI =& get_instance();
		$str = $CI->db->query("SELECT * FROM ref_wil_kabkot WHERE id_kabkot='$id_kabkot' and id_provinsi='$id_provinsi'")->row();

		if(isset($str)){

			return $str->nama; 
		}else{
			return '';
		}
	}

	function getKec($id_provinsi, $id_kabkot, $id_kecamatan){
			$CI =& get_instance();
			$str = $CI->db->query("SELECT * FROM ref_wil_kec WHERE id_provinsi='$id_provinsi' and id_kabkot='$id_kabkot' and id_kecamatan='$id_kecamatan'  ")->row();

			if(isset($str)){

				return $str->nama; 
			}else{
				return '';
			}
		}
		
	function getKel($id_provinsi, $id_kabkot, $id_kecamatan, $id_kelurahan){
		$CI =& get_instance();
		$str = $CI->db->query("SELECT * FROM ref_wil_kel WHERE id_provinsi='$id_provinsi' and 
		id_kabkot='$id_kabkot' and 
		id_kecamatan='$id_kecamatan'  and
		id_kelurahan='$id_kelurahan'  
		")->row();

		if(isset($str)){

			return $str->nama; 
		}else{
			return '';
		}
	}


	function enCrypt($string){
		$CI =& get_instance();
		$enc_idx=$CI->encryption->encrypt($string);
		$enc_idx=str_replace(array('+', '/', '='), array('-', '_', '~'), $enc_idx);    

		return $enc_idx;
	}

	function deCrypt($stringEncrypted){
		$CI =& get_instance();
		$dec_idx=str_replace(array('-', '_', '~'), array('+', '/', '='), $stringEncrypted);
		$dec_idx=$CI->encryption->decrypt($dec_idx);
	
		return $dec_idx;
	}
	   
	
	// helper for aktivasi pelaporan
	/*
	Key : 
	- trx_ikk
	- trx_ikk_apip
	- ikk_makro
	- ikk_makro_apip
	*/
	function aktivasi_pelaporan($key){
		$CI =& get_instance();
		$query = $CI->db->query("SELECT * FROM aktivasi_pelaporan WHERE key_pelaporan='$key'")->row();

		if(isset($query)){
			return $query->is_active; 
		}else{
			return '';
		}
	}







?>
